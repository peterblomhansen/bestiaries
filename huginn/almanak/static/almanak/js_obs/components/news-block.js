var newsBlock = Vue.component('news-block', {
    template: '\
		<div>\
			<div v-if="((newsObject.count > 0) && (newsObject.frame == \'campaign\'))" style="float:right">\
				<button class="btn-news btn-ingame" style="width:100px" type="button" @click="approveAllCampaignRelation();updateCount(-10)">\
					<span v-if="newsObject.type == \'new\'">\
						approve all\
					</span>\
					<span v-if="newsObject.type == \'updated\'">\
						reapprove all\
					</span>\
				</button>\
			</div>\
			<h2 class="rubrik">{{ heading }}</h2>\
			<div v-if="newsObject">\
				<div v-if="newsObject.array">\
					<div v-for="n in newsObject.array" style="width: 100%">\
						<div v-if="n.user_permissions.read">\
							<div style="display: flex; flex-direction: row; margin-top: 5px">\
								<div style="flex: 2" v-if="(newsObject.area == \'element\')">\
									<p v-html="n.links.type_spec_long_br"></p>\
								</div>\
								<div v-if="(newsObject.area == \'description\')" style="flex: 2; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">\
									<p v-html="n.content_stripped.link"></p>\
								</div>\
								<div style="flex: 1">\
									<p v-html="n.author.links.name_user"></p>\
								</div>\
								<div style="flex: 1">\
									<div v-if="(newsObject.type == \'new\')">\
										<p>{{ n.created_obj.dato }}</p>\
									</div>\
									<div v-if="(newsObject.type == \'updated\')">\
										<p>{{ n.updated_obj.dato }}</p>\
									</div>\
								</div>\
								<div style="flex: 2">\
									<div v-if="(newsObject.frame == \'campaign\')">\
										<div v-if="(newsObject.type == \'new\')">\
											<div>\
												<div v-if="n.user_permissions.campaign_manage_session" style="display: flex; justify-content: flex-end; flex-wrap: wrap">\
													<div v-if="n.campaigns_containing_session.approved == 0">\
														<button class="btn-news btn-ingame" type="button" @click="updateCampaignRelation(n,{\'approved\':2});updateCount(-1)">approve</button>\
													</div>\
													<div v-if="n.campaigns_containing_session.approved == 2">\
														<button class="btn-news btn-ingame--active" @click="updateCampaignRelation(n,{\'approved\':0,\'canon\':0}); updateCount(1)" style="font-size:10px; padding-top: 5px">approved</button>\
													</div>\
													<div v-if="n.campaigns_containing_session.canon == 0">\
														<button class="btn-news btn-ingame" type="button" @click="updateCampaignRelation(n,{\'canon\':1,\'approved\':2}); if(n.campaigns_containing_session.approved == 0){ updateCount(-1) }">canonize</button>\
													</div>\
													<div v-if="n.campaigns_containing_session.canon == 1">\
														<button class="btn-news btn-ingame--active" @click="updateCampaignRelation(n,{\'canon\':0})" style="font-size:10px; padding-top: 5px">canonized</button>\
													</div>\
													<button class="btn-news btn-ingame" type="button" @click="removeCampaignRelation(n,\'new\');updateCount(-1)">remove</button>\
												</div>\
											</div>\
										</div>\
										<div v-if="(newsObject.type == \'updated\')">\
											<div v-if="n.user_permissions.campaign_manage_session" style="display: flex; justify-content: flex-end; flex-wrap: wrap">\
												<div v-if="n.campaigns_containing_session.approved == 1">\
													<button class="btn-news btn-ingame" type="button" @click="updateCampaignRelation(n,{\'approved\':2});updateCount(-1)">reapprove</button>\
												</div>\
												<div v-if="n.campaigns_containing_session.approved == 2">\
													<button class="btn-news btn-ingame--active" @click="updateCampaignRelation(n,{\'approved\':1,\'canon\':0}); updateCount(1)" style="font-size:10px; padding-top: 5px">approved</button>\
												</div>\
												<div v-if="n.campaigns_containing_session.canon == 0">\
													<button class="btn-news btn-ingame" type="button" @click="updateCampaignRelation(n,{\'canon\':1,\'approved\':2}); if(n.campaigns_containing_session.approved == 0){ updateCount(-1) }">canonize</button>\
												</div>\
												<div v-if="n.campaigns_containing_session.canon == 1">\
													<button class="btn-news btn-ingame--active" @click="updateCampaignRelation(n,{\'canon\':0})" style="font-size:10px; padding-top: 5px">canonized</button>\
												</div>\
												<div v-if="n.user_permissions.campaign_manage_session">\
													<button class="btn-news btn-ingame" type="button" @click="removeCampaignRelation(n,\'new\');updateCount(-1)">remove</button>\
												</div>\
											</div>\
										</div>\
									</div>\
									<div v-if="(newsObject.frame == \'universe\')">\
										<button class="btn-news btn-ingame" type="button" @click="createCampaignRelation(n)">add</button>\
									</div>\
								</div>\
							</div>\
						</div>\
					</div>\
					<div v-if="newsObject.allLoaded" class="rubrik">\
						<div v-if="(newsObject.area == \'element\')">\
							<div v-if="(newsObject.type == \'new\')">\
								<i>All new pages have been loaded.</i>\
							</div>\
							<div v-if="(newsObject.type == \'updated\')">\
								<i>All updated pages have been loaded.</i>\
							</div>\
						</div>\
						<div v-if="(newsObject.area == \'description\')">\
							<div v-if="(newsObject.type == \'new\')">\
								<i>All new descriptions have been loaded.</i>\
							</div>\
							<div v-if="(newsObject.type == \'updated\')">\
								<i>All updated descriptions have been loaded.</i>\
							</div>\
						</div>\
					</div>\
					<div v-if="newsObject.count == 0" class="rubrik">\
						<div v-if="(newsObject.area == \'element\')">\
							<div v-if="(newsObject.type == \'new\')">\
								<i>No new pages.</i>\
							</div>\
							<div v-if="(newsObject.type == \'updated\')">\
								<i>No updated pages.</i>\
							</div>\
						</div>\
						<div v-if="(newsObject.area == \'description\')">\
							<div v-if="(newsObject.type == \'new\')">\
								<i>No new descriptions.</i>\
							</div>\
							<div v-if="(newsObject.type == \'updated\')">\
								<i>No updated descriptions.</i>\
							</div>\
						</div>\
					</div>\
					<div v-if="(newsObject.count != 0) && !(newsObject.allLoaded)">\
						<button class="btn-offgame--active" v-if="!newsObject.allLoaded" type=button @click="loadMore()">Load more</button>\
					</div>\
					<br><br>\
				</div>\
			</div>\
		</div>\
    ',
    props: {
		newsObject: Object,
		heading: String,
	},
    mounted: function(){
		console.debug(this.$store.state.profile.news);
	},
    data: function() {
        return {
		
		};
    },
    computed: {
		session: function(){
			return this.$store.state.session;
		},
	},
	methods: {
		updateCount: function (add){
			var self = this;
			self.newsObject.count += add;
		},
		removeCampaignRelation: function(object,state){
			var self = this;
			axios.delete("/api/campaign" + self.newsObject.area + "/" + object.campaigns_containing_session.id + "/delete/").then(function(response){
				self.newsObject.array.splice(self.newsObject.array.indexOf(object), 1);
				self.$store.commit("getNewsArray",{ frame: 'universe', type: self.newsObject.name});
			}).catch(function(error) {
				console.log(error);
			});
		},
		updateCampaignRelation: function(object,change){
			var self = this;
			axios.patch("/api/campaign" + self.newsObject.area + "/" + object.campaigns_containing_session.id + "/update/",change).then(function(response){
				for (var k in change){
					if (change.hasOwnProperty(k)) {
						 object.campaigns_containing_session[k] = change[k];
					}
				}
			}).catch(function(error) {
				console.log(error);
			});
		},
		createCampaignRelation: function(object){
			var self = this;
			var create = {
				approved	: 0,
				canon		: 0,
				campaign	: self.$store.state.initial.campaignId
			}
			create[self.newsObject.area]	= object.id;
			axios.post("/api/campaign" + self.newsObject.area + "/create/",create).then(function(response){
				self.newsObject.array.splice(self.newsObject.array.indexOf(object), 1);
				self.$store.commit("getNewsArray",{ frame: 'campaign', type: self.newsObject.name});
			}).catch(function(error) {
				console.log(error);
			});
		},
		approveAllCampaignRelation: function(){
			var self = this;
			_.each(self.newsObject.array,function(n){
				self.updateCampaignRelation(n,{'approved':2});
			});
		},
		loadMore: function(){
			var self = this;
			axios.get(self.newsObject.loadPath(self.newsObject.count)).then(function(response) {
				_.each(response.data,function(e){
					self.newsObject.array.push(e);
				});
				self.newsObject.count += response.data.length;
				if(response.data.length < 10){
					self.newsObject.allLoaded = true;
				}
			}).catch(function(error) {
				console.log(error);
			});
		},
	}
});
