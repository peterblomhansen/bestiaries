FROM python:3.8

RUN useradd -ms /bin/bash gitlab-runner

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

RUN mkdir -p /data/static
RUN mkdir -p /data/media
RUN mkdir /code

COPY ./huginn /code/

RUN chmod -R 777 /code  

WORKDIR /code

RUN python3 -m pip install -r requirements.txt --no-cache-dir