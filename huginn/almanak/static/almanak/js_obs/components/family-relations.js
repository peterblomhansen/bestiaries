var familyRelations = Vue.component('family-relations', {
    template: "\
    <div>\
        <h3 class=\"rubrik\">Family</h3>\
        <h4 class=\"rubrik\">Parents <span v-if=\"element.user_permissions.write == true\" class=\"rubrik\" style=\"cursor:pointer\" @click=\"addParentsChooser = true\">+</span></h4>\
        <div v-if=\"addParentsChooser == true\">\
            <p class=\"rubrik\">Add parent <input placeholder=\"Search\" v-model=\"addParentsQuery\" @keyup=\"getResults('parents',addParentsQuery)\">\
                <button @click=\"addParentsChooser = false;addParentsShowList = false;addParentsQuery = ''\">Cancel</button>\
            </p>\
          <div v-if=\"addParentsShowList == true\" class=\"typeahead__list\">\
            <p v-for=\"r in addParentsResult\" v-if=\"matchTypeIndivid(r)\" >\
              <img style=\"height:20px\" :src=\"st + 'images/icons/type/rubrik/' + r.type_grund + '.svg'\" />\
               <a class=\"ink\" :href=\"'/almanak/side/' + r.element.id\">{{ r.name }}</a>\
                <button @click=\"createFamilyRelation(r.element,element.id,'familie')\">Relate</button>\
             </p>\
          </div>\
        </div>\
        <div v-if=\"parents.length > 0\">\
          <p v-for=\"p in parents\"><a class=\"ink\" :href=\"'/almanak/side/' + p.above.id\">{{ p.above.present_name }}</a>\
            <strong class=\"rubrik\" v-if=\"element.user_permissions.write == true\" style=\"cursor:pointer\" @click=\"p.delete_question = true\"> &times;</strong><br/>\
            <span v-if=\"p.delete_question == true\">Do you want to remove {{ p.above.present_name }} as a parent <button @click=\"deleteRelation(p.id)\" >Yes</button> <button @click=\"p.delete_question = false\" >No</button></span>\
          </p>\
        </div>\
        <div v-else>\
          <p>No parents known</p>\
        </div>\
        \
\
        <div v-if=\"siblings.length > 0\">\
        <h4 class=\"rubrik\" >Siblings</h4>\
            <p v-for=\"s in siblings\"><a class=\"ink\" :href=\"'/almanak/side/' + s.below.id\">{{ s.below.present_name }}</a></p>\
\
        </div>\
	        <h4 class=\"rubrik\">Children <span v-if=\"element.user_permissions.write == true\" class=\"rubrik\" style=\"cursor:pointer\" @click=\"addChildrenChooser = true\">+</span></h4>\
        <div v-if=\"addChildrenChooser == true\">\
            <p class=\"rubrik\">Add child <input placeholder=\"Search\" v-model=\"addChildrenQuery\" @keyup=\"getResults('children',addChildrenQuery)\">\
                <button @click=\"addChildrenChooser = false;addChildrenShowList = false;addChildrenQuery = ''\">Cancel</button>\
            </p>\
            <div v-if=\"addChildrenShowList == true\" class=\"typeahead__list\">\
                <p v-for=\"r in addChildrenResult\" v-if=\"matchTypeIndivid(r)\" >\
                    <img style=\"height:20px\" :src=\"st + 'images/icons/type/rubrik/' + r.type_grund + '.svg'\" />\
                    <a class=\"ink\" :href=\"'/almanak/side/' + r.element.id\">{{ r.name }}</a>\
                    <button @click=\"createFamilyRelation(element.id,r.element,'familie')\">Relate</button>\
                </p>\
          </div>\
        </div>\
        <div v-if=\"children.length > 0\">\
          <p v-for=\"c in children\"><a class=\"ink\" :href=\"'/almanak/side/' + c.below.id\">{{ c.below.present_name }}</a>\
            <strong class=\"rubrik\" v-if=\"element.user_permissions.write == true\" style=\"cursor:pointer\" @click=\"c.delete_question = true\"> &times;</strong><br/>\
            <span v-if=\"c.delete_question == true\">Do you want to remove {{ c.below.present_name }} as a child\
                <button @click=\"deleteRelation(c.id)\" >Yes</button> <button @click=\"c.delete_question = false\" >No</button>\
            </span>\
\
          </p>\
        </div>\
        <div v-else>\
          <p  >No children</p>\
        </div>\
        \
        <h4 class=\"rubrik\">Partners <span v-if=\"element.user_permissions.write == true\" class=\"rubrik\" style=\"cursor:pointer\" @click=\"addPartnerChooser = true\">+</span></h4>\
        <div v-if=\"addPartnerChooser == true\">\
            <p class=\"rubrik\">Add partner <input placeholder=\"Search\" v-model=\"addPartnerQuery\" @keyup=\"getResults('partners',addPartnerQuery)\">\
                <button @click=\"addPartnerChooser = false;addPartnerShowList = false;addPartnerQuery = ''\">Cancel</button>\
            </p>\
          <div v-if=\"addPartnerShowList == true\" class=\"typeahead__list\">\
            <p v-for=\"r in addPartnerResult\" v-if=\"matchTypeIndivid(r)\" >\
                <img style=\"height:20px\" :src=\"st + 'images/icons/type/rubrik/' + r.type_grund + '.svg'\" />\
                <a class=\"ink\" :href=\"'/almanak/side/' + r.element.id\">{{ r.name }}</a>\
                <button @click=\"createFamilyRelation(element.id,r.element,'individ')\">Relate</button>\
            </p>\
          </div>\
        </div>\
        <div v-if=\"partners.length > 0\">\
          <p v-for=\"p in partners\" v-if=\"p.below == element.id\"><a class=\"ink\" :href=\"'/almanak/side/' + p.above.id\">{{ p.above.present_name }}</a>\
            <strong class=\"rubrik\" v-if=\"element.user_permissions.write == true\" style=\"cursor:pointer\" @click=\"p.delete_question = true\"> &times;</strong><br/>\
            <span class=\"rubrik\" v-if=\"p.delete_question == true\">Do you want to remove {{ p.above.present_name }} as a partner to {{ element.present_name }}\
                <button @click=\"deleteRelation(p.id)\" >Yes</button> <button @click=\"p.delete_question = false\" >No</button>\
            </span>\
          </p>\
          <p v-for=\"p in partners\" v-if=\"p.above == element.id\">\
            <a class=\"ink\" :href=\"'/almanak/side/' + p.below.id\">{{ p.below.present_name }}</a>\
                <strong class=\"rubrik\" v-if=\"element.user_permissions.write == true\" style=\"cursor:pointer\" @click=\"p.delete_question = true\"> &times;</strong><br/>\
                <span class=\"rubrik\" v-if=\"p.delete_question == true\">Do you want to remove {{ p.below.present_name }} as a partner to {{ element.present_name }}\
                <button @click=\"deleteRelation(p.id)\" >Yes</button> <button @click=\"p.delete_question = false\" >No</button></span>\
            </a>\
          </p>\
        </div>\
        <div v-else>\
          <p >No partners</p>\
        </div>\
\
    </div>",
    props: {
        //element: Object,
        //st: String,
    },
    data: function() {
        return {
            addParentsChooser: "",
            addParentsShowList: false,
            addParentsQuery: "",
            addParentsResult: [],
            addPartnerChooser: "",
            addPartnerShowList: false,
            addPartnerQuery: "",
            addPartnerResult: [],
            addChildrenChooser: "",
            addChildrenShowList: false,
            addChildrenQuery: "",
            addChildrenResult: [],
        }
    },
    mounted: function () {
        //this.getSiblings(elementId);
        this.$store.dispatch("getElementRelations");

    },
    methods: {
        // getSiblings: function(id) {
        //
        //     var self = this;
        //     axios.get('/api/siblings/' + id + '/').then(function(response) {
        //         console.log(response)
        //         self.$store.state.element.relations.append(response.data);
        //         //self.$store.commit("getElementRelations");
        //         //eventHub.$emit('reloadData');
        //     }).catch(function(error) {
        //         console.log(error);
        //     });
        // },
        deleteRelation: function(id) {
            var self = this;
            axios.patch('/api/elementrelation/' + id + '/update/', {
                deleted: true
            }).then(function(response) {
                console.log(response)
                self.$store.commit("getElementRelations");
                //eventHub.$emit('reloadData');
            }).catch(function(error) {
                console.log(error);
            });
        },
        getResults: function(typeResult, query) {
            var self = this;
            var results = [];
            if (this.addParentsQuery.length > 2) {
                search(query, typeResult)
            } else if (this.addParentsQuery.length < 3) {
                this.addParentsShowList = false;
            }
            if (this.addChildrenQuery.length > 2) {
                search(query, typeResult)
            } else if (this.addChildrenQuery.length < 3) {
                this.addChildrenShowList = false;
            }
            if (this.addPartnerQuery.length > 2) {
                search(query, typeResult)
            } else if (this.addPartnerQuery.length < 3) {
                this.addPartnerShowList = false;
            }

            function search(query, typeResult) {
                axios.get('/api/element_by_name/?search=' + query).then(function(response) {
                    console.log(response.data)
                    if (typeResult == "partners") {
                        self.addPartnerResult = response.data;
                        self.addPartnerShowList = true;
                    } else if (typeResult == "children") {
                        self.addChildrenResult = response.data;
                        self.addChildrenShowList = true;
                    } else if (typeResult == "parents") {
                        self.addParentsResult = response.data;
                        self.addParentsShowList = true;
                    }
                }).catch(function(error) {
                    console.log(error);
                });
            }
        },
        matchTypeIndivid(el) {
            var match = false;
            if (el.type_spec == "individ" || el.type_spec == "player_character") {
                match = true;
            }
            return match;
        },
        createFamilyRelation: function(elAbove, elBelow, relType) {
            var self = this;
            axios.post("/api/elementrelation/create/", {
                above: elAbove,
                below: elBelow,
                relation_type: relType
            }).then(function(response) {
                console.log(response)
                self.$store.commit("getElementRelations");
                //eventHub.$emit('reloadData');
                self.addParentsChooser = false;
                self.addChildrenChooser = false;
                self.addPartnerChooser = false;
                self.addParentsShowList = false;
                self.addChildrenShowList = false;
                self.addPartnerShowList = false;
                self.addParentsQuery = "";
                self.addChildrenQuery = "";
                self.addPartnerQuery = "";
            }).catch(function(error) {
                console.log(error);
            });
        }
    },
    computed: {
		// siblings: function (){
		//
		// 	// 	this.$nextTick(function () {
	    //     //     	this.$store.dispatch("getSiblings");
		// 	// 	}
	    //     // }
		// 	if(this.parents.length > 0) {
		// 		if(this.$store.state.element.relations.siblings) {
		// 			return
		// 		}
		//
		//
		//
		//
		//
		// },
        element: function(){
            return this.$store.state.element.masterdata;
        },
        st: function (){
            return this.$store.state.initial.staticUrl;
        },
        parents: function() {
            var parents = [];
            if(this.$store.state.element.relations.above) {
                this.$store.state.element.relations.above.forEach(function(a) {
                    if (a.relation_type == "familie" && a.deleted == false) {
                        parents.push(a);
                    }
                })
            }
            return parents;
        },
        parentIds: function() {
            var parents = [];
            if(this.$store.state.element.relations.above) {
                this.$store.state.element.relations.above.forEach(function(a) {
                    if (a.relation_type == "familie" && a.deleted == false) {
                        parents.push(a.above.id);
                    }
                })
            }
            return parents;
        },
        children: function() {
            var children = [];
            if(this.$store.state.element.relations.below) {
                this.$store.state.element.relations.below.forEach( function(b) {
                    if (b.relation_type == "familie" && b.deleted == false) {
                        children.push(b);
                    }
                })
            }
            return children;
        },
        siblings: function() {
            if(this.$store.state.element.relations.siblings) {
                return this.$store.state.element.relations.siblings;
            }
            else {
				this.$store.dispatch("getSiblings");
				return [];
			}
        },
        partners: function() {
            var partners = [];
            if(this.$store.state.element.relations.below) {
                this.$store.state.element.relations.below.forEach( function(b) {
                    if (b.relation_type == "individ" && b.deleted == false) {
                        partners.push(b);
                    }
                })
            }
            if(this.$store.state.element.relations.below) {
                this.$store.state.element.relations.above.forEach(  function(a) {
                    if (a.relation_type == "individ" && a.deleted == false) {
                        partners.push(a);
                    }
                })
            }
            return partners;
        }
    }
});
