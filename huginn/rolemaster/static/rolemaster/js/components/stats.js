var stats = Vue.component('stats', {
	template: "\
	<div>STATS\
		\
		{{ levelsWithoutStats }}\
		{{levelsWithoutStats.indexOf(level.viewed)}}\
		<div v-if=\"statsAll.length == 0\">Character has no stats\
			<table>\
				<thead>\
				<tr>\
					<th>Stat</th>\
					<th>Temp</th>\
					<th>Pot</th>\
				</tr>\
				</thead>\
				<tbody>\
					<tr v-for=\"s in statNames\"  >\
						<td>{{ s }}</td>\
						<td><input type=\"number\" v-model=\"iniStats[s].temp\" /></td>\
						<td><input type=\"number\" v-model=\"iniStats[s].pot\" /></td>\
						<td v-html=\"iniStats[s].msg\"></td>\
					</tr>\
				</tbody>\
			</table>\
			<button @click=\"submitInitialStats()\" :disabled=\"!initialStatsValidated\">Submit Initial Stats</button>\
		</div>\
		<div v-if=\"statsAll.length > 0 && levelsWithoutStats.indexOf(parseInt(level.viewed)) != -1\">\
		Character has no stats for this level\
		<div v-if=\"statsViewed(levelPrev).length == 2\" >\
		<h3>Roll Stat Gain</h3>\
		<table  >\
			<thead>\
			<tr>\
				<th class=\"faded\" >Stat</th>\
				<th >Temp Gain</th>\
				<th >Pot Gain</th>\
				<th >Status</th>\
				<th >Diff</th>\
				<th class=\"faded\" >Temp</th>\
				<th class=\"faded\" >Pot</th>\
				<th class=\"faded\" >Normal</th>\
				<th class=\"faded\" >Race</th>\
				<th class=\"faded\" >Special</th>\
				<th class=\"faded\" >Total</th>\
				<th class=\"faded\" >Dev</th>\
			</tr>\
			</thead>\
			<tbody >\
				<tr v-for=\"s in statNames\">\
					<td>{{ s.toUpperCase().replace('_',' ') }}</td>\
					<td ><input type=\"number\" v-model=\"statGain[s].temp\" /></td>\
					<td ><input v-if=\"tempPotDiff(s,levelPrev) == 0\" type=\"number\" v-model=\"statGain[s].pot\" /></td>\
					<td v-html=\"statGain[s].msg\"></td>\
					<td >{{ tempPotDiff(s,levelPrev) }}</td>\
					<td class=\"faded\" >{{getTemp(s,levelPrev)}}</td>\
					<td class=\"faded\" >{{getPot(s,levelPrev)}}</td>\
					<td class=\"faded\" >{{ getNormalBonus(s,levelPrev) }}</td>\
					<td class=\"faded\" >{{ getRaceBonus(s,levelPrev) }}</td>\
					<td class=\"faded\" >{{ getSpecialBonus(s,levelPrev) }}</td>\
					<td class=\"faded\" >{{ getTotalBonus(s,levelPrev) }}</td>\
					<td class=\"faded\" v-if=\"isDevStat(s)\">{{ getDevPoints(s,levelPrev) }}</td>\
					<td class=\"faded\" v-else ></td>\
				</tr>\
			</tbody>\
		</table>\
		<button @click=\"submitStatGain()\" :disabled=\"!statGainValidated\">Submit Stat Gain</button>\
		</div>\
		<div v-if=\"statsViewed(levelPrev).length != 2\" >You are missing a Stat Gain on level {{ levelsWithoutStats[0] }} <button @click=\"level.viewed = levelsWithoutStats[0]\">Go to Stat Gain</button></div>\
		</div>\
		<div v-if=\"statsAll.length > 0 && levelsWithoutStats.indexOf(parseInt(level.viewed)) == -1\">\
		Character has stats for this level\
			<table>\
				<thead>\
				<tr>\
					<th>Stat</th>\
					<th>Temp</th>\
					<th>Pot</th>\
					<th>Normal</th>\
					<th>Race</th>\
					<th>Special</th>\
					<th>Total</th>\
					<th>Dev</th>\
				</tr>\
				</thead>\
				<tbody >\
					<tr v-for=\"s in statNames\">\
						<td>{{ s.toUpperCase().replace('_',' ') }}</td>\
						<td>{{getTemp(s)}}</td>\
						<td>{{getPot(s)}}</td>\
						<td>{{ getNormalBonus(s) }}</td>\
						<td>{{ getRaceBonus(s) }}</td>\
						<td>{{ getSpecialBonus(s,levelPrev) }}</td>\
						<td>{{ getTotalBonus(s) }}</td>\
						<td v-if=\"isDevStat(s)\">{{ getDevPoints(s) }}</td>\
					</tr>\
				</tbody>\
			</table>\
		</div>\
	</div>",
    data: function() {
        return {
			showInitialStatForm: false,
			statNames:  [
				"luck",
			    "agility",
			    "constitution",
			    "memory",
			    "reasoning",
			    "self_discipline",
			    "strength",
			    "quickness",
			    "presence",
			    "empathy",
			    "intuition",
			],
			iniStats: {
				"luck": { temp: null, pot: null, msg: "" },
			    "agility": { temp: null, pot: null, msg: "" },
			    "constitution": { temp: null, pot: null, msg: "" },
			    "memory": { temp: null, pot: null, msg: "" },
			    "reasoning": { temp: null, pot: null, msg: "" },
			    "self_discipline": { temp: null, pot: null, msg: "" },
			    "strength": { temp: null, pot: null, msg: "" },
			    "quickness": { temp: null, pot: null, msg: "" },
			    "presence": { temp: null, pot: null, msg: "" },
			    "empathy": { temp: null, pot: null, msg: "" },
			    "intuition": { temp: null, pot: null, msg: "" },
			},
			statGain: {
				"luck": { temp: 0, pot: 0, msg: "" },
			    "agility": { temp: 0, pot: 0, msg: "" },
			    "constitution": { temp: 0, pot: 0, msg: "" },
			    "memory": { temp: 0, pot: 0, msg: "" },
			    "reasoning": { temp: 0, pot: 0, msg: "" },
			    "self_discipline": { temp: 0, pot: 0, msg: "" },
			    "strength": { temp: 0, pot: 0, msg: "" },
			    "quickness": { temp: 0, pot: 0, msg: "" },
			    "presence": { temp: 0, pot: 0, msg: "" },
			    "empathy": { temp: 0, pot: 0, msg: "" },
			    "intuition": { temp: 0, pot: 0, msg: "" },
			},
        }
    },
    methods: {
		submitStatGain: function (){

			var prevLevel = this.level.viewed - 1;
			var newTemp = {
				character: this.$store.state.initial.characterId,
				stat_type: "temp",
				level: this.level.viewed,
				luck: parseInt(this.statGain.luck.temp) + parseInt(this.getTemp("luck",prevLevel)),
			    agility: parseInt(this.statGain.agility.temp) + parseInt(this.getTemp("agility",prevLevel)),
			    constitution: parseInt(this.statGain.constitution.temp) + parseInt(this.getTemp("constitution",prevLevel)),
			    memory: parseInt(this.statGain.memory.temp) + parseInt(this.getTemp("memory",prevLevel)),
			    reasoning: parseInt(this.statGain.reasoning.temp) + parseInt(this.getTemp("reasoning",prevLevel)),
			    self_discipline: parseInt(this.statGain.self_discipline.temp) + parseInt(this.getTemp("self_discipline",prevLevel)),
			    strength: parseInt(this.statGain.strength.temp) + parseInt(this.getTemp("strength",prevLevel)),
			    quickness: parseInt(this.statGain.quickness.temp) + parseInt(this.getTemp("quickness",prevLevel)),
			    presence: parseInt(this.statGain.presence.temp) + parseInt(this.getTemp("presence",prevLevel)),
			    empathy: parseInt(this.statGain.empathy.temp) + parseInt(this.getTemp("empathy",prevLevel)),
			    intuition: parseInt(this.statGain.intuition.temp) + parseInt(this.getTemp("intuition",prevLevel)),
			}
			var newPot = {
				character: this.$store.state.initial.characterId,
				stat_type: "pot",
				level: this.level.viewed,
				luck: parseInt(this.statGain.luck.pot) + parseInt(this.getPot("luck",prevLevel)),
			    agility: parseInt(this.statGain.agility.pot) + parseInt(this.getPot("agility",prevLevel)),
			    constitution: parseInt(this.statGain.constitution.pot) + parseInt(this.getPot("constitution",prevLevel)),
			    memory: parseInt(this.statGain.memory.pot) + parseInt(this.getPot("memory",prevLevel)),
			    reasoning: parseInt(this.statGain.reasoning.pot) + parseInt(this.getPot("reasoning",prevLevel)),
			    self_discipline: parseInt(this.statGain.self_discipline.pot) + parseInt(this.getPot("self_discipline",prevLevel)),
			    strength: parseInt(this.statGain.strength.pot) + parseInt(this.getPot("strength",prevLevel)),
			    quickness: parseInt(this.statGain.quickness.pot) + parseInt(this.getPot("quickness",prevLevel)),
			    presence: parseInt(this.statGain.presence.pot) + parseInt(this.getPot("presence",prevLevel)),
			    empathy: parseInt(this.statGain.empathy.pot) + parseInt(this.getPot("empathy",prevLevel)),
			    intuition: parseInt(this.statGain.intuition.pot) + parseInt(this.getPot("intuition",prevLevel)),
			}

			axios.post("/rm/api/stats/create/",newTemp).then(function(response){

			});
			axios.post("/rm/api/stats/create/",newPot).then(function(response){

			});
			window.location.reload();

		},
		isDevStat: function(stat) {
			var devStats = ["luck","agility","constitution","memory","reasoning","self_discipline"];
			return devStats.indexOf(stat) != -1;
		},
		getTemp: function(statName,lvl = this.level.viewed) {

			if(this.temp(lvl).length > 0) {
				return this.temp(lvl)[0][statName];
			}
		},
		getPot: function(statName,lvl = this.level.viewed) {
			if(this.temp(lvl).length > 0) {
				return this.pot(lvl)[0][statName];
			}
		},
		tempPotDiff: function(statName,lvl = this.level.viewed) {
			if(this.temp(lvl).length > 0) {
				return parseInt(this.pot(lvl)[0][statName]) - parseInt(this.temp(lvl)[0][statName]);
			}
		},
		getNormalBonus: function (statName,lvl = this.level.viewed){
			if(this.temp(lvl).length > 0) {

				var temp = this.temp(lvl)[0][statName];
				var stat = this.statBonusDistribution.find(function(s){ return s.stat == temp });
				if(stat) {
					return stat.bonus;
				}
				else { return 0; }
			}
		},
		getDevPoints: function (statName,lvl = this.level.viewed){
			if(this.temp(lvl).length > 0) {

				var temp = this.temp(lvl)[0][statName];
				var stat = this.statBonusDistribution.find(function(s){ return s.stat == temp });
				if(stat) {
					return stat.dev_points;
				}
				else {
					return 0;
				}
			}
		},
		getRaceBonus: function (statName) {
			if(this.race) {
				return this.race[statName];
			}
			else {
				return 0;
			}
		},
		getSpecialBonus: function(statName){
			var spBonus = 0;
			console.log(statName);

			var statNameTranslater = {
				"luck": "LU",
				"agility":"AG",
				"constitution": "CO",
				"reasoning": "RE",
				"memory": "ME",
				"self_discipline": "SD",
				"strength": "ST",
				"quickness": "QU",
				"presence": "PR",
				"empathy": "EM",
				"intuition": "IN",
			};
			if (this.$store.state.character.stat_special_bonuses.length > 0) {
				spBonusFilter = this.$store.state.character.stat_special_bonuses.filter(function(e){
					return e.stat == statNameTranslater[statName]});
				if(spBonusFilter.length > 0) {
					return spBonusFilter[0].bonus;
				}
					else { return 0;}
			}
			else { return 0;}
		},
		getTotalBonus: function(statName,lvl = this.level.viewed) {
			return this.getNormalBonus(statName,lvl) + this.getRaceBonus(statName) + this.getSpecialBonus(statName);
		},
		submitInitialStats: function () {
			var iniStats = this.iniStats;
			var iniTemp = {
				character: this.$store.state.initial.characterId,
				stat_type: "temp",
				level: 0,
				luck: iniStats.luck.temp,
			    agility:iniStats.agility.temp,
			    constitution: iniStats.constitution.temp,
			    memory: iniStats.memory.temp,
			    reasoning: iniStats.reasoning.temp,
			    self_discipline: iniStats.self_discipline.temp,
			    strength: iniStats.strength.temp,
			    quickness: iniStats.quickness.temp,
			    presence: iniStats.presence.temp,
			    empathy: iniStats.empathy.temp,
			    intuition: iniStats.intuition.temp,
			}
			var iniPot = {
				character: this.$store.state.initial.characterId,
				stat_type: "pot",
				level: 0,
				luck: iniStats.luck.pot,
			    agility:iniStats.agility.pot,
			    constitution: iniStats.constitution.pot,
			    memory: iniStats.memory.pot,
			    reasoning: iniStats.reasoning.pot,
			    self_discipline: iniStats.self_discipline.pot,
			    strength: iniStats.strength.pot,
			    quickness: iniStats.quickness.pot,
			    presence: iniStats.presence.pot,
			    empathy: iniStats.empathy.pot,
			    intuition: iniStats.intuition.pot,
			}

			axios.post("/rm/api/stats/create/",iniTemp).then(function(response){

			});
			axios.post("/rm/api/stats/create/",iniPot).then(function(response){
				window.location.reload();
			});

		},
		temp: function (lvl = this.level.viewed){
			if(this.statsViewed.length > 0) {
				return this.statsViewed(lvl).filter(function(s) { return s.stat_type == "temp" });
			}
			else {
				return [];
			}
		},
		pot: function(lvl = this.level.viewed){
			if(this.statsViewed.length > 0) {
				return this.statsViewed(lvl).filter(function(s) { return s.stat_type == "pot" });
			}
			else {
				return [];
			}
		},
		statsViewed: function (lvl){
			var self = this;

			if(this.statsAll.length > 0){
				var statsViewed = this.statsAll.filter(function(s){

					return parseInt(s.level) == parseInt(lvl);
				});
				return statsViewed;
			}else {

				return [];
			}




		},
	},
	computed: {

		initialStatsValidated: function(){
			var validated = true;

			_.each(this.iniStats,function(d,i) {
				var t = parseInt(d.temp);
				var p = parseInt(d.pot);


				if(d.temp == null || d.pot == null) {
					validated = false;
					d.msg = "Please enter Temporary and Potential values";
				}else if(t < 0 || t > 100 || p < 0 || p > 100){
					d.msg = "Initial stats must be at least 1 and at most 100";
				}
				else if(t > p) {
					validated = false;
					d.msg = " Temporary stat cannot be higher than Potential";
				}else {

					d.msg = " OK";

				}
			});


			return validated;
		},
		statGainValidated: function(){
			var self = this;
			var validated = true;

			_.each(this.statGain,function(d,i) {

				var t = parseInt(d.temp);
				var p = parseInt(d.pot);
				var prevT = self.getTemp(i,self.levelPrev);
				var prevP = self.getPot(i,self.levelPrev);
				if(d.temp == null) {
					d.msg = "No gain";

				}

				else if(parseInt(prevT) + parseInt(d.temp) > parseInt(prevP) + parseInt(d.pot)) {
					validated = false;
					d.msg = "Temp stat cannot gain to be higher than Pot";
				}
				else {

					d.msg = "Gained";
				}


			});


			return validated;
		},
		statBonusDistribution: function (){
			return this.$store.state.rolemasterTables.statBonusDistribution;
		},
		statsAll: function(){
			//console.log(this.$store.state.stats);
			return this.$store.state.character.stats;
		},
		race: function (){
			return this.$store.state.character.race;
		},
		level: function(){
			return this.$store.state.initial.level;
		},
		levelPrev: function(){
			return this.$store.state.initial.level.viewed - 1;
		},
		levelsWithoutStats: function (){
			var self = this;
			var levelArray = [],
			n = 0

			function statsPresent(l,type){
				var found = self.statsAll.filter(function(s) {
					return s.level == l && s.stat_type == type;
				})
				console.log(found.length);
				return found.length;

			}


			while (n <= this.level.current) {
			  if(statsPresent(n,"pot") == 0 || statsPresent(n,"temp") == 0) {
			     levelArray.push(parseInt(n))
		  	  }
			  n++;
			}

			return levelArray;

		},
		missingStatGains: function (){



		},
	}
});
