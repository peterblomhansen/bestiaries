var autocomplete = Vue.component('autocomplete', {
	template: "<div class=\"autocomplete__wrapper\" >\
		<input v-focus :disabled=\"disabled\" @keyup=\"inputOrClear\" type=\"text\" v-model=\"q\" :placeholder=\"placeholderTextComp\" />\
			<div v-if=\"show\" class=\"autocomplete__list paper__sheet border--padding\">\
				<div class=\"border--content\">\
					<p class=\"rubrik\" style=\"text-align:right;\">{{ result.count }} pages found</p><br/>\
					<p v-for=\"n in result.results\" >\
						<img style=\"height:20px\" :src=\"st + 'images/icons/type/rubrik/' + n.type_grund + '.svg' \" />\
						<a class=\"ink\" :href=\"'/almanak/side/' + n.element_id\">{{ n.name }}</a> - <span class=\"rubrik\" >{{ typeSpecText(n.type_spec) }}</span>\
						<button v-if=\"buttonText\" @click=\"resolveButtonClick(n)\" class=\"button--small\" >{{ buttonText }}</button>\
					</p><br/>\
					<p v-if=\"showGetMore\" @click=\"getAdditionalResult()\" class=\"rubrik cursor--pointer\">See more</p>\
				</div>\
			</div>\
	</div>",
	props: {
		onButtonClick: Function,
		placeholderText: String,
		buttonText: String,
		buttonAction: Object,
		filter: String,
		type: String,
		disabled: Boolean,
		initialQuery: String,
		resetOnButtonClick: Boolean,
	},
	mounted: function () {
		if(this.initialQuery && this.initialQuery.length > 0) {
			this.q = this.initialQuery;
			this.timerInactivity();
		}

	},
	data: function() {
		return {
			pageNumberCurrent: 1,
			q: "",
			result: {},
		}
	},
	methods: {
		resolveButtonClick: function (name){
			this.buttonAction ? this.buttonClick(name) : this.onButtonClick(name);
			this.resetOnButtonClick ? this.q = "" : false;
			this.q = "";

		},
		inputOrClear: function (event){
			if(event.key === "Escape") {
				this.q = "";
			} else {
				this.timerInactivity();
			}
		},
		timerInactivity: function(){
			var self = this;
			window.clearTimeout(this.timer);
			this.timer = window.setTimeout(function(){

				self.getResult();

			},150);
		},
		buttonClick: function(nameResult){
			switch (this.buttonAction.type) {
				case "nameShareWith":
					var ajaxElement = new ElementModel(nameResult.element,{id: nameResult.id, name: nameResult.name},nameResult.element_type);
					ajaxElement.delete_question = false;
					this.buttonAction.data.shared_with_full.push(ajaxElement);
				break;
				case "childAdd":
					var childRelation = new ElementRelationModel();
					childRelation.relation_type = "familie";
					childRelation.below = nameResult.element;
					childRelation.above = this.$store.state.element.masterdata.id;
					this.$store.dispatch("create",{ object: childRelation, url: "/api/new/element-relation/", updateAction: this.buttonAction.updateAction });
				break;					
				case "partnerAdd":
					var partnerRelation = new ElementRelationModel();
					partnerRelation.relation_type = "individ";
					partnerRelation.below = nameResult.element;
					partnerRelation.above = this.$store.state.element.masterdata.id;
					this.$store.dispatch("create",{ object: partnerRelation, url: "/api/new/element-relation/", updateAction: this.buttonAction.updateAction });
				break;
				case "membershipAdd":
					var membership = new ElementRelationModel();
					membership.relation_type = "normal";
					membership.below = this.$store.state.element.masterdata.id;
					membership.above = nameResult.element;
					this.$store.dispatch("create",{ object: membership, url: "/api/new/element-relation/", updateAction: this.buttonAction.updateAction });
				break;
				case "ExperienceAdd":
					var experience = new EntityEventRelationModel();
					experience.entity = this.$store.state.element.masterdata.id;
					experience.event = nameResult.element;
					this.$store.dispatch("create",{ object: experience, url: "/api/new/entity-event-relation/", updateAction: this.buttonAction.updateAction });
				break;
				case "propertyAdd":
					var updatedRelation = new ElementItemRelationModel();
					updatedRelation.owner			= this.$store.state.element.masterdata.id;
					updatedRelation.owned			= nameResult.element;
					updatedRelation.number_of_items	= this.buttonAction.data.RelationNumberOfItems;
					updatedRelation.ingame_age		= this.ingameNow.age;
					updatedRelation.ingame_year 	= this.ingameNow.year;
					updatedRelation.ingame_month 	= this.ingameNow.month;
					updatedRelation.ingame_date 	= this.ingameNow.date;
					updatedRelation.ingame_hour 	= this.ingameNow.hour;
					updatedRelation.ingame_minute 	= this.ingameNow.minute;
					updatedRelation.ingame_second 	= this.ingameNow.second;
		
					this.$store.dispatch("create",{ object: updatedRelation, url: "/api/new/element-item-relation/", updateAction: this.buttonAction.updateAction });					
				break;
				case "propertyUniqueAdd":
					var elementUniqueItemRelation = new ElementUniqueItemRelationModel();
					elementUniqueItemRelation.owner = this.$store.state.element.masterdata.id;
					elementUniqueItemRelation.owned = nameResult.element;

					this.$store.dispatch("create",{ object: elementUniqueItemRelation, url: "/api/new/element-unique-item-relation/", updateAction: this.buttonAction.updateAction });
				break;
			}
			this.q = "";
		},
		getResult: function (){
			var self = this;
			this.pageNumberCurrent = 1;
			if(this.q.length > 1) {
				axios.get("/api/new/name/?format=json&search=" + this.q + "&" + this.scopeFilter + this.filterApplied)
					.then(function (response) {
						self.result = response.data;
					})
					.catch(function (error) {
					  console.log(error);
					});
			}
			else {
				self.result = {};
			}
		},
		getAdditionalResult: function (){
			var self = this;
			this.pageNumberCurrent++;
			console.log(this.result.next);
				axios.get("/api/new/name/?format=json&page=" + this.pageNumberCurrent + "&search=" + this.q + "&" + this.scopeFilter + this.filterApplied)
					.then(function (response) {
						_.each(response.data.results,function(r){
							self.result.results.push(r);
						})
					})
					.catch(function (error) {
					  console.log(error);
					});
		},
		typeSpecText: function (typeSpec){
			return typeSpec && typeSpec.text ? typeSpec.text.charAt(0).toUpperCase() + typeSpec.text.slice(1) : "Unknown";
		},

	},
	computed: {
		scopeFilter: function (){
			if(this.scope == "campaign") {
				return "&element__campaigns__id__in=" + this.$store.state.generic.session.base.campaignId;
			}else {
				return "";
			}
		},		
		scope: function (){
			return this.$store.state.generic.session.full.element_search_scope; //search.element.scope;
		},
		filterApplied: function (){
			if (this.filter !== undefined) {
				return this.filter;
			}
			else {
				return "";
			}
		},
		placeholderTextComp: function (){
			if (this.placeholderText) {
				return this.placeholderText;
			}else {
				return "Search";
			}
		},
		st: function(){
			return this.$store.state.generic.staticUrl;
		},
		show: function () {

			if(this.result.count > 0 && this.q.length > 0) {
				return true;
			}else {
				return false;
			}

		},
		showGetMore: function (){
			if(this.pageNumberCurrent * 10 >= this.result.count) 
			{
				return false;
			}else {
				return true;
			}

		},
		ingameNow: function(){
			var cmpgn = this.$store.state.generic.session.full.campaign_full;

			var ingameNow = {
				age: cmpgn.ingame_now_age.id,
				year: cmpgn.ingame_now_year,
				month: cmpgn.ingame_now_month,
				date: cmpgn.ingame_now_date,
				hour: cmpgn.ingame_now_hour,
				minute: cmpgn.ingame_now_minute,
				second: cmpgn.ingame_now_second
			};

			return ingameNow;

		},			
	}

 });
