{/* <div class=\"grid grid--auto-c-c-c rubrik\" ><h4><input type=\"checkbox\" v-model=\"isSecret\" />Secret</h4><h4 style=\"text-align:right\" >Role</h4><h4 style=\"text-align:right\" >Read</h4><h4 style=\"text-align:right;padding-right:20px\" >Write</h4>\
</div>\
<fieldset-wrapper legend=\"GENERAL\">\
  <div class=\"grid grid--auto-c-c-c \" >\
    <p class=\"ink\" >Everyone</p><p class=\"ink\" style=\"text-align:right;font-weight:bold\" >Reader</p><p :class=\"everyoneCanRead.class\" style=\"text-align:right;font-weight:bold\">{{ everyoneCanRead.text }}</p><p class=\"rubrik\" style=\"text-align:right;font-weight:bold\">NO</p>\
  </div>\
  <div class=\"grid grid--auto-c-c-c \" >\
    <p>\
      <span v-if=\"changeAuthorShow == true\" style=\"display:flex;\">\
        <select v-model=\"$store.state.element.masterdata.author\" @change=\"changeAuthor\">\
          <option v-for=\"usr in users\" :value=\"usr.id\">{{ usr.first_name }} {{ usr.last_name }}</option>\
        </select> \
        <button class=\"button--small\" style=\"margin-left:1em;\" @click=\"changeAuthorShow = !changeAuthorShow\" >cancel</button>\
      </span>\
      <span class=\"ink\" v-else >\
        {{ author.first_name }} {{ author.last_name }}\
        <button class=\"button--small\" @click=\"changeAuthorShow = !changeAuthorShow\" >change</button>\
      </span>\
      \
    </p>\
    <p class=\"ink\" style=\"text-align:right;font-weight:bold\" >Author</p><p class=\"leaf\" style=\"text-align:right;font-weight:bold\" >YES</p><p class=\"leaf\" style=\"text-align:right;font-weight:bold\">YES</p>\
  </div>\
  <div class=\"grid grid--auto-c-c-c \" v-if=\"masterdata.type_spec == 'player_character'\" >\
    <p class=\"ink\" >{{ roleplayer.first_name }} {{ roleplayer.last_name }} <button v-if=\"\" class=\"button--small\">change</button></p><p class=\"ink\" style=\"text-align:right;font-weight:bold\" >Role Player</p><p class=\"leaf\" style=\"text-align:right;font-weight:bold\" >YES</p><p class=\"leaf\" style=\"text-align:right;font-weight:bold\">YES</p>\
  </div>\
</fieldset-wrapper>\
<fieldset-wrapper legend=\"CAMPAIGNS\">\
  <div v-for=\"cmpgnRel in masterdata.campaigns_full\"  >\
    <h3 class=\"ink\" >\
      {{ cmpgnRel.campaign_full.present_name.name }} \
      <button class=\"button--small\" @click=\"cmpgnRel.campaign_full.delete_question = true\" >remove</button>\
      <p v-if=\"cmpgnRel.campaign_full.delete_question === true\" >Do you want to remove {{ $store.state.element.masterdata.present_name.name }} from the campaign {{ cmpgnRel.campaign_full.present_name.name }}?<br/>\
        <button @click=\"removeCampaignRelation(cmpgnRel.id)\">Yes</button><button @click=\"cmpgnRel.campaign_full.delete_question = false\">No</button>\
      </p>\
    </h3>\
    <div class=\"grid grid--auto-c-c-c \" v-for=\"gm in cmpgnRel.campaign_full.gamemasters\" >\
      <p class=\"ink\">{{ userFromId(gm).first_name }} {{ userFromId(gm).last_name }}</p>\
      <p class=\"ink\" style=\"text-align:right;font-weight:bold\" >Gamemaster</p><p class=\"leaf\" style=\"text-align:right;font-weight:bold\" >YES</p><p class=\"leaf\" style=\"text-align:right;font-weight:bold\">YES</p>\
    </div>\
  </div>\
  <button v-if=\"campaignAddShow == false\" @click=\"addCampaignShowGet\" class=\"button--small\" >Add Campaign</button>\
  <span v-if=\"campaignAddShow === true\" style=\"display:flex\" >\
    <select v-model=\"campaignToBeAdded\" @change=\"addCampaign\" >\
      <option value=\"0\">Campaign to add</option>\
      <option :value=\"campaign.id\" v-for=\"campaign in campaignsNotRelated\" >{{ campaign.present_name.name }}</option>\
    </select> <button @click=\"campaignAddShow = false\" class=\"button--small\" style=\"margin-left:1em;\" >cancel</button>\
  </span>\
</fieldset-wrapper>\
<fieldset-wrapper legend=\"EDITORS\">\
  <div v-for=\"editor in editors\" class=\"grid grid--auto-c-c-c\" >\
    <p>\
      {{ editor.first_name }} {{ editor.last_name }} <button @click=\"editor.delete_question = true\" class=\"button--small\" style=\"margin-left:1em;\" >remove</button>\
    </p>\
    <p class=\"ink\" style=\"text-align:right;font-weight:bold\" >Editor</p><p :class=\"everyoneCanRead.class\" style=\"text-align:right;font-weight:bold\">{{ everyoneCanRead.text }}</p><p class=\"leaf\" style=\"text-align:right;font-weight:bold\">YES</p>\
    <p v-if=\"editor.delete_question == true\" class=\"rubrik\">\
      Do you want to remove <span class=\"ink\" >{{ editor.first_name }} {{ editor.last_name }}</span> as editor of <span class=\"ink\" >{{ $store.state.element.masterdata.present_name.name }}</span>?\
      <button class=\"button--small\" @click=\"removeEditor(editor)\" >Yes</button>\
      <button @click=\"editor.delete_question = false\" class=\"button--small\" >No</button>\
    </p>\
  </div>\
  <button @click=\"addEditorAddShow = true\" class=\"button--small\" v-if=\"addEditorAddShow === false\" >Add Editor</button>\
  <span style=\"display:flex\" v-if=\"addEditorAddShow === true\">\
    <select v-model=\"newEditorId\" >\
      <option value=\"0\">Choose User to add as editor</option>\
      <option @click=\"addEditor()\" v-for=\"usr in usersNotEditors\" :value=\"usr.id\">{{ usr.first_name }} {{ usr.last_name }}</option>\
    </select>\
    <button @click=\"addEditorAddShow = false\" class=\"button--small\" style=\"margin-left:1em;\" >cancel</button>\
  </span>\
</fieldset-wrapper>\
</div>\
</div>\
<div class=\"column\">\
<div>\
<figure v-if=\"!isSecret\"><img style=\"width:100%;opacity:0.25\" :src=\"static + 'images/icons/key.svg'\" /></figure>\
<div v-if=\"isSecret\" class=\"grid grid--auto-c-c-c rubrik\" ><h4></h4><h4 style=\"text-align:right\" >Role</h4><h4 style=\"text-align:right\" >Read</h4><h4 style=\"text-align:right;padding-right:20px\" >Write</h4></div>\
    <fieldset-wrapper v-if=\"isSecret\" legend=\"Shared With\" >\
      <div v-for=\"user in sharedWith\" class=\"grid grid--auto-c-c-c\" >\
        <p>\
          {{ user.first_name }} {{ user.last_name }} <button @click=\"user.delete_question = true\" class=\"button--small\" style=\"margin-left:1em;\" >remove</button>\
        </p>\
        <p class=\"ink\" style=\"text-align:right;font-weight:bold\" >Shared With</p><p :class=\"everyoneCanRead.class\" style=\"text-align:right;font-weight:bold\">{{ everyoneCanRead.text }}</p><p class=\"leaf\" style=\"text-align:right;font-weight:bold\">YES</p>\
        <p v-if=\"user.delete_question == true\" class=\"rubrik\">\
          Do you want to remove <span class=\"ink\" >{{ user.first_name }} {{ user.last_name }}</span> as editor of <span class=\"ink\" >{{ $store.state.element.masterdata.present_name.name }}</span>?\
          <button class=\"button--small\" @click=\"removeSharedWith(user)\" >Yes</button>\
          <button @click=\"user.delete_question = false\" class=\"button--small\" >No</button>\
        </p>\
      </div>\
      <button @click=\"addSharedWithAddShow = true\" class=\"button--small\" v-if=\"addSharedWithAddShow === false\" >Share with another user</button>\
      <span style=\"display:flex\" v-if=\"addSharedWithAddShow === true\">\
        <select v-model=\"newUserToShareId\" >\
          <option value=\"0\">C hoose User to share </option>\
          <option @click=\"addSharedWith()\" v-for=\"usr in usersNotSharedWith\" :value=\"usr.id\">{{ usr.first_name }} {{ usr.last_name }}</option>\
        </select>\
        <button @click=\"addSharedWithAddShow = false\" class=\"button--small\" style=\"margin-left:1em;\" >cancel</button>\
      </span>\
    </fieldset-wrapper>\
  </div>\ */}



var elementPermission = Vue.component('element-permission', {
  template: "\
    <overlay grid-class=\"tome-grid\">\
      <h1 class=\"ink\" style=\"grid-column:1 / 3;\"><span class=\"rubrik\">Permissions for</span> {{ $store.state.element.masterdata.present_name.name }}</h1>\
      <div style=\"grid-column:4 / 5;text-align:right\">\
        <button @click=\"$store.commit('toggleEditElementPermissions')\" class=\"button--close-modal\" >&times;</button>\
      </div>\
      <h3 class=\"rubrik\" style=\"grid-column:1 / 5;\">{{ secret_public }} {{ masterdata.type_spec_obj.text }}</h3>\
      <h4><input type=\"checkbox\" v-model=\"isSecret\" />Secret</h4>\
      <div class=\"element-permission column\" style=\"	grid-column: 1 / 5;\" v-if=\"users.length > 0\" >\
        <div class=\"\">\
        <br/>\
          <fieldset-wrapper legend=\"GENERAL\" >\
            <div class=\"grid grid--auto-auto-c-c \">\
              <strong class=\"ink\" >Everyone</strong>\
              <p class=\"rubrik\" >(Reader)</p>\
              <strong :class=\"everyoneCanRead.class\" >Read</strong>\
              <strong class=\"rubrik align--right strike-through\" >Write</strong>\
            </div>\
            <div class=\"grid grid--auto-auto-c-c\">\
              <p v-if=\"changeAuthorShow == true\">\
                  <select v-model=\"$store.state.element.masterdata.author\" @change=\"changeAuthor\">\
                    <option v-for=\"usr in users\" :value=\"usr.id\">{{ usr.first_name }} {{ usr.last_name }}</option>\
                  </select> \
                  <button class=\"button--small\" @click=\"changeAuthorShow = !changeAuthorShow\" >\
                    cancel\
                  </button>\
              </p>\
              <strong v-else class=\"ink\" >\
                {{ author.first_name }} {{ author.last_name }}\
                <button class=\"button--small\" @click=\"changeAuthorShow = !changeAuthorShow\" >change</button>\
              </strong>\
              <p class=\"rubrik\" >(Author)</p>\
              <strong class=\"leaf align--right\" >Read</strong>\
              <strong class=\"leaf align--right\" >Write</strong>\
            </div>\
            <div class=\"grid grid--auto-auto-c-c \" v-if=\"masterdata.type_spec == 'player_character'\" >\
              <p v-if=\"changeRolePlayerShow\">\
                <select v-model=\"$store.state.element.masterdata.roleplayer\" @change=\"changeRolePlayer\">\
                  <option v-for=\"usr in users\" :value=\"usr.id\">{{ usr.first_name }} {{ usr.last_name }}</option>\
                </select> \
                <button class=\"button--small\">cancel</button>\
              </p>\
              <strong v-else class=\"ink\" >{{ roleplayer.first_name }} {{ roleplayer.last_name }} \
                <button @click=\"changeRolePlayerShow = true\" class=\"button--small\" >change</button>\
              </strong>\
              <p class=\"rubrik\" >(Roleplayer)</p>\
              <strong class=\"leaf align--right\" >Read</strong>\
              <strong class=\"leaf align--right\" >Write</strong>\
            </div>\
          </fieldset-wrapper>\
          <br/>\
          <fieldset-wrapper legend=\"CAMPAIGN\">\
            <div v-for=\"cmpgnRel in masterdata.campaigns_full\"  >\
              <h3 class=\"ink\" >\
                {{ cmpgnRel.campaign_full.present_name.name }} \
                <button class=\"button--small\" @click=\"cmpgnRel.campaign_full.delete_question = true\" >remove</button>\
                <p class=\"rubrik\" v-if=\"cmpgnRel.campaign_full.delete_question === true\" >Do you want to remove {{ $store.state.element.masterdata.present_name.name }} from the campaign {{ cmpgnRel.campaign_full.present_name.name }}?<br/>\
                  <button @click=\"removeCampaignRelation(cmpgnRel.id)\">Yes</button><button @click=\"cmpgnRel.campaign_full.delete_question = false\">No</button>\
                </p>\
              </h3>\
              <div class=\"grid grid--auto-auto-c-c \" v-for=\"gm in cmpgnRel.campaign_full.gamemasters\" >\
                <strong class=\"ink\">{{ userFromId(gm).first_name }} {{ userFromId(gm).last_name }}</strong>\
                <p class=\"rubrik\" >(Gamemaster)</p>\
                <strong class=\"leaf align--right\" >Read</strong>\
                <strong class=\"leaf align--right\" >Write</strong>\
              </div>\
              <br/>\
            </div>\
            <button v-if=\"campaignAddShow == false\" @click=\"addCampaignShowGet\" class=\"button--small\" >Add Campaign</button>\
            <span v-if=\"campaignAddShow === true\" style=\"display:flex\" >\
              <select v-model=\"campaignToBeAdded\" @change=\"addCampaign\" >\
                <option value=\"0\">Campaign to add</option>\
                <option :value=\"campaign.id\" v-for=\"campaign in campaignsNotRelated\" >{{ campaign.present_name.name }}</option>\
              </select> <button @click=\"campaignAddShow = false\" class=\"button--small\" style=\"margin-left:1em;\" >cancel</button>\
            </span>\
          </fieldset-wrapper>\
        </div>\
        <div class=\"column\">\
          <figure v-if=\"!isSecret\"><img class=\"image--full-width image--watermark\" :src=\"static + 'images/icons/key.svg'\" /></figure>\
          <fieldset-wrapper v-if=\"isSecret\" legend=\"SHARED WITH\" >\
            {{ sharedWithIds }}\
            <div v-for=\"user in sharedWith\" class=\"grid grid--auto-c-c-c\" >\
              <p>\
                {{ user.first_name }} {{ user.last_name }} <button @click=\"user.delete_question = true\" class=\"button--small\" style=\"margin-left:1em;\" >remove</button>\
              </p>\
              <p class=\"ink\" style=\"text-align:right;font-weight:bold\" >Shared With</p><p :class=\"everyoneCanRead.class\" style=\"text-align:right;font-weight:bold\">{{ everyoneCanRead.text }}</p><p class=\"leaf\" style=\"text-align:right;font-weight:bold\">YES</p>\
              <p v-if=\"user.delete_question == true\" class=\"rubrik\">\
                Do you want to remove <span class=\"ink\" >{{ user.first_name }} {{ user.last_name }}</span> as editor of <span class=\"ink\" >{{ $store.state.element.masterdata.present_name.name }}</span>?\
                <button class=\"button--small\" @click=\"removeSharedWith(user)\" >Yes</button>\
                <button @click=\"user.delete_question = false\" class=\"button--small\" >No</button>\
              </p>\
            </div>\
            <button @click=\"addSharedWithAddShow = true\" class=\"button--small\" v-if=\"addSharedWithAddShow === false\" >Share with another user</button>\
            <span style=\"display:flex\" v-if=\"addSharedWithAddShow === true\">\
              <select v-model=\"newUserToShareId\" @change=\"addSharedWith\" >\
                <option value=\"0\">Choose User to share </option>\
                <option v-for=\"usr in usersNotSharedWith\" :value=\"usr.id\">{{ usr.first_name }} {{ usr.last_name }}</option>\
              </select>\
              <button @click=\"addSharedWithAddShow = false\" class=\"button--small\" style=\"margin-left:1em;\" >cancel</button>\
            </span>\
          </fieldset-wrapper>\
        </div>\
      </div>\
    </overlay>",
  data: function (){
    return {   
      changeRolePlayerShow: false,
      changeAuthorShow: false,
      campaignAddShow: false,
      campaignToBeAdded: "0",
      newEditorId: 0,
      addEditorAddShow: false,
      addSharedWithAddShow: false,
      newUserToShareId: 0,
    }
  },
  mounted: function (){

  },
  methods: {
    changeAuthor: function (){

      this.$store.dispatch("update",{
                                object: { author: this.$store.state.element.masterdata.author }, 
                                url: "/api/new/element/" + this.$store.state.element.id + "/",
                                updateAction: "elementGetMasterdata",
                              });
      this.changeAuthorShow = false;

    },
    changeRolePlayer: function (){

      this.$store.dispatch("update",{
        object: { roleplayer: this.$store.state.element.masterdata.roleplayer }, 
        url: "/api/new/player-character/" + this.$store.state.element.id + "/",
        updateAction: "elementGetMasterdata",
      });
      this.changeRolePlayerShow = false;

    },
    addCampaignShowGet: function() {
      var self = this;
       axios.get("/api/new/campaign/?d=present_name")
          .then(function (response) {
            self.$store.state.generic.session.full.universe.campaigns = response.data.results;
            self.campaignAddShow = true;
          })
          .catch(function (error) {

          });     
    },
    removeCampaignRelation(id) {
      this.$store.dispatch("delete",{
        object: {}, 
        url: "/api/new/campaign-element-relation/" + id + "/",
        updateAction: "elementGetMasterdata",
      });     
    },
    addCampaign: function () {
      console.log("CAMPAIGN ID",this.campaignToBeAdded);
      
      campaignConnection = new campaignElementRelationModel({ 
          campaign: this.campaignToBeAdded,   
          element: this.$store.state.element.id,
          approved: 0,
          canon: 0,
        })

      this.$store.dispatch("create",{
        object: campaignConnection, 
        url: "/api/new/campaign-element-relation/",
        updateAction: "elementGetMasterdata",
      });

    },
    removeEditor: function (editor){
      var updatedEditors = new Array();
      updatedEditors = this.editorsIds;
      updatedEditors.splice(this.editorsIds.indexOf(editor.id),1);
      var typeUrl = this.$store.getters.elementTypeTranslated(this.masterdata.type_grund).replace(" ","-").toLowerCase();        
      this.$store.dispatch("update",{ object: { editors: updatedEditors }, url: "/api/new/" + typeUrl + "/" + this.masterdata.id + "/", updateAction: "elementGetMasterdata" });

    },
    addEditor: function () {
      var updatedEditors = new Array();
      updatedEditors = this.editorsIds;
      updatedEditors.push(this.newEditorId);
      var typeUrl = this.$store.getters.elementTypeTranslated(this.masterdata.type_grund).replace(" ","-").toLowerCase();        
      this.$store.dispatch("update",{ object: { editors: updatedEditors }, url: "/api/new/" + typeUrl + "/" + this.masterdata.id + "/", updateAction: "elementGetMasterdata" });
    },
    removeSharedWith: function (user){
      var updatedSharedWith = new Array();
      updatedSharedWith = this.sharedWithIds;
      updatedSharedWith.splice(this.sharedWithIds.indexOf(user.id),1);
      var typeUrl = this.$store.getters.elementTypeTranslated(this.masterdata.type_grund).replace(" ","-").toLowerCase();        
      this.$store.dispatch("update",{ object: { shared_with: updatedSharedWith }, url: "/api/new/" + typeUrl + "/" + this.masterdata.id + "/", updateAction: "elementGetMasterdata" });

    },
    addSharedWith: function () {
      var updatedSharedWith = new Array();
      updatedSharedWith = this.sharedWithIds;
      console.log(updatedSharedWith);

      //console.log(this.newUserToShareId);
      //updatedSharedWith.push(this.newUserToShareId);

      //var typeUrl = this.$store.getters.elementTypeTranslated(this.masterdata.type_grund).replace(" ","-").toLowerCase();        
      //this.$store.dispatch("update",{ object: { shared_with: this.sharedWithIds }, url: "/api/new/element/" + this.masterdata.id + "/", updateAction: "elementGetMasterdata" });
    },    
    // getCampaigns: function (universe_id){
    //   axios.get("/api/new/campaign/?d=cs1_gamemasters__es1_present_name")
    //       .then(function (response) {
    //         console.log(response.data);
    //       })
    //       .catch(function (error) {

    //       });
    // },
    // getUsers: function(universe_id) {
    //   var self = this;
    //   axios.get("/api/new/user/?format=json&universes__universe=" + universe_id)
    //       .then(function (response) {
    //         self.users = response.data.results;
    //       })
    //       .catch(function (error) {
    //         console.log(error);
    //       });      
    // },
    userFromId: function (id){
      return this.users.find(function(u){ return u.id === id });
    },
  },
  computed: {
    campaignsNotRelated: function (){
      var self = this;
      if(this.$store.state.generic.session.full.universe.campaigns) {
        var campaigns = this.$store.state.generic.session.full.universe.campaigns
                .filter(function(cmpgnInUni) {
                      return !_.find(self.$store.state.element.masterdata.campaigns, 
                                    function(c) { return c === cmpgnInUni.id; });
                });
        return _.orderBy(campaigns, ['present_name.name'], ['asc']);
      }
      else {
        return [];
      }
    },
    usersNotEditors: function (){
      var self = this;
      if(this.users) {
        var users = this.users
                .filter(function(user) {
                      return !_.find(self.$store.state.element.masterdata.editors, 
                                    function(uid) { return uid === user.id; });
                });
        return _.orderBy(users, ['first_name','last_name'], ['asc','asc']);
      }
      else {
        return [];
      }     
    },
    usersNotSharedWith: function (){
      var self = this;
      if(this.users) {
        var users = this.users
                .filter(function(user) {
                      return !_.find(self.$store.state.element.masterdata.shared_with, 
                                    function(uid) { return uid === user.id; });
                });
        return _.orderBy(users, ['first_name','last_name'], ['asc','asc']);
      }
      else {
        return [];
      }     
    },    
    usersSelect: function (){
      return this.users.map(function(u){ return { text: u.first_name + " " + u.last_name, value: u.id } });
    },
    permissions: function (){
      return this.$store.state.element.permissions;			
    },
    masterdata: function () {
      return this.$store.state.element.masterdata;
    },
    isSecret: { 
      get: function () {
        return this.masterdata.secret;
      },
      set: function (v) {
        var typeUrl = this.$store.getters.elementTypeTranslated(this.masterdata.type_grund).replace(" ","-").toLowerCase();        
        this.$store.dispatch("update",{ object: { secret: v}, url: "/api/new/" + typeUrl + "/" + this.masterdata.id + "/", updateAction: "elementGetMasterdata" });
      },

    },
    secret_public: function () {
      if(this.masterdata.secret === true) {
        return "Secret";
      } else {
        return "Public";
      }
    },
    everyoneCanRead: function () {
      if(this.masterdata.secret === true) {
        return { text: "NO", class: "rubrik align--right strike-through" };  //"<p class=\"leaf\" style=\"text-align:right;font-weight:bold\">YES</p>";
      } else {
        return { text: "YES", class: "leaf align--right" };"<p class=\"rubrik\" style=\"text-align:right;font-weight:bold\">NO</p>";
      }
    },    
		isUniverseOwner: function() {
			if (this.$store.state.generic.session.full.user_is_universe_owner === true){
				return "(Owner)";
			}else {
				return "";
			}
    },
    gamemasters: function (){
      var self = this;
      var gms = [];
      if(this.users.length > 0 && this.masterdata.campaigns.length > 0) {
        _.each(this.masterdata.campaigns,function (c){
          _.each(c.campaign.gamemasters,function (g){
            gms.push(self.userFromId(g));
          })        
        })
      }
      return gms;
    },
    campaignsList: function (){
      return this.masterdata.campaigns.map(function(c){ return c.campaign.present_name.name + " " }).toString();
    }, 
    author: function (){
      return this.$store.state.element.masterdata.author_full;
    },
    // author: {
    //   get: function() {
    //     return this.$store.state.element.masterdata.author;
    //   },
    //   set: function (value) {
    //     this.$store.state.element.masterdata.author = this.users.find(function(u){ return u.id == value });
    //   }
    // },
    roleplayer: function (){
      return this.masterdata.roleplayer_full;
    },
    editors: function (){
      return this.masterdata.editors_full;
    },
    editorsIds: function (){
      return this.masterdata.editors;
    },  
    sharedWith: function (){
      return this.masterdata.shared_with_full;
    },
    sharedWithIds: function (){
      return this.$store.state.element.masterdata.shared_with;
    },       
    // editorsIds: {
    //   get: function () {
    //     return this.masterdata.editors;
    //   },
    //   set: function (id) {
    //     var typeUrl = this.$store.getters.elementTypeTranslated(this.masterdata.type_grund).replace(" ","-").toLowerCase();        
    //     this.$store.dispatch("update",{ object: { editors: this.masterdata.editors }, url: "/api/new/" + typeUrl + "/" + this.masterdata.id + "/", updateAction: "elementGetMasterdata" });

    //   },
    // },
    users: function (){
      return this.$store.state.generic.session.full.universe.users;
    },
    campaigns: function (){
      // if(this.masterdata.type_spec == "kampagne")
      //   return [this.masterdata];
      // else {
        return this.masterdata.campaigns;
      //}
    },
    static: function (){
      return this.$store.state.generic.staticUrl;
    },

  },
});