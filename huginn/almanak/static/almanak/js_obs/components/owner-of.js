var ownerOf = Vue.component('owner-of', {
	template: "\
	<div v-if=\"element.type_grund != 'artefakt' \">\
		<h3 class=\"rubrik\">Property <span class=\"cursor--pointer\" @click=\"showAddNewProperty = !showAddNewProperty\" v-if=\"element.user_permissions.write == true\" >+</span></h3>\
		<div v-if=\"showAddNewProperty == true\">\
			<p class=\"rubrik\">Add unique item to {{ element.present_name }}s property:</p>\
			<autocomplete :button-method=\"'createElementUniqueItemRelation'\" :button-text=\"'Add Property'\"></autocomplete>\
			<br/>\
			<p class=\"rubrik\">Add items to {{ element.present_name }}s property:</p>\
			<div style=\"display:flex;\">\
				<input type=\"number\" style=\"width:50px;margin-right:0.5em\" v-model=\"newNumItems\"> <autocomplete  v-if=\"newNumItems > 0\" :num-items=\"newNumItems\" filter=\"artefakt\" :button-method=\"'createElementItemRelation'\" :button-text=\"'Add Property'\"></autocomplete><br/><br/>\
			</div>\
		</div>\
		<div v-for=\"u in ownerOfUniques\">\
		<p  class=\"rubrik\">\
			<a class=\"ink\" :href=\"'/almanak/side/' + u.owned.id\" >\
				{{ u.owned.present_name }}\
			</a>\
			{{ property_type(u.owned) }}\
			<span v-if=\"element.user_permissions.write == true\" style=\"cursor:pointer;height:1em;\" @click=\"u.delete_question = !u.delete_question\" >&times;</span>\
		</p>\
		<p v-if=\"u.delete_question == true\">Do you want to remove {{ u.owned.present_name }} as a property of {{ element.present_name }}? <button @click=\"removeUniqueProperty(u)\">Yes</button> <button @click=\"u.delete_question = !u.delete_question\">No</button> </p>\
		</div>\
		<div v-for=\"u in ownerOfItems\">\
			<p  class=\"rubrik\">\
				<span class=\"rubrik\">{{ u.number_of_items }} pcs. </span> \
				<a class=\"ink\" :href=\"'/almanak/side/' + u.owned.id\" >\
					{{ u.owned.present_name }}\
				</a>\
				<span v-if=\"element.user_permissions.write == true\" style=\"cursor:pointer;height:1em;\" @click=\"u.delete_question = !u.delete_question\" >&times;</span>\
				<a href=\"#\" v-if=\"element.user_permissions.write == true\" @click=\"u.update_question = !u.update_question\" >update</a>\
				<div v-if=\"u.update_question == true\" >\
					<input type=\"number\" v-model=\"u.number_of_items\" /> {{ u.owned.present_name }} <button @click=\"createItemElementRelations(u)\" >Update</button>\
				</div>\
			</p>\
		<p v-if=\"u.delete_question == true\">Do you want to remove {{ u.owned.present_name }} as a property of {{ element.present_name }}? <button @click=\"removeProperty(u)\">Yes</button> <button @click=\"u.delete_question = !u.delete_question\">No</button> </p>\
		</div>\
	</div>\
	",
	props: {
		player: Object
	},
	data: function() {
		return {
			showAddNewProperty: false,
			newNumItems: 0,
		}
	},
	methods: {
		createItemElementRelations: function(rel) {
			var self = this;

			var updatedRelation = {
				owner: this.element.id,
				owned: rel.owned.id,
				number_of_items:rel.number_of_items,
				ingame_age: this.ingameNow.age,
				ingame_year:this.ingameNow.year,
				ingame_month:this.ingameNow.month,
				ingame_date:this.ingameNow.date,
				ingame_hours:this.ingameNow.hours,
				ingame_minutes:this.ingameNow.minutes,
				ingame_seconds: this.ingameNow.seconds,

			};

			axios.post("/api/elementitemrelation/create/",updatedRelation).then(function(){
				self.$store.dispatch("getElementRelations");
			}).catch(function(error) {
	            console.log(error);
	        });


		},
		property_type: function(el) {
			return el.type_spec.replace("_"," ");
		},
		removeUniqueProperty: function(u) {
			var self = this;
			axios.patch("/api/elementuniqueitemrelation/" + u.id + "/update/", {deleted: true}).then(function(response){
				self.$store.dispatch("getElementRelations");
			}).catch(function(error) {
	            console.log(error);
	        });
		},
		removeProperty: function(u) {
			var self = this;
			axios.patch("/api/elementitemrelation/" + u.id + "/update/", {deleted: true}).then(function(response){
				self.$store.dispatch("getElementRelations");
			}).catch(function(error) {
	            console.log(error);
	        });
		}
	},
	computed: {
		ingameNow: function(){
			var cmpgn = this.$store.state.session;

			var ingameNow = {
				age: cmpgn.ingame_now_age.id,
				year: cmpgn.ingame_now_year,
				month: cmpgn.ingame_now_month,
				date: cmpgn.ingame_now_date,
				hours: cmpgn.ingame_now_hours,
				minutes: cmpgn.ingame_now_minutes,
				seconds: cmpgn.ingame_now_seconds
			};

			return ingameNow;

		},
		static: function (){

            return this.$store.state.initial.staticUrl;

        },
		ownerOfUniques: function(){
			var id = this.$store.state.element.masterdata.id;
			return this.$store.state.element.relations.uniques.filter(function(e){ return e.owner == id });
		},
		ownerOfItems: function(){
			var id = this.$store.state.element.masterdata.id;
			return this.$store.state.element.relations.items.filter(function(e){ return e.owner == id });
		},
		element: function(){
			return this.$store.state.element.masterdata;
		}

	}
});
