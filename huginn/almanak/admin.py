from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import (
    BestiariesUser, 
    Element, 
    Age, 
    Universe, 
    Event, 
    RpgElement, 
    RpgSession, 
    Campaign, 
    Group, 
    Individual, 
    PlayerCharacter, 
    UniverseMembership, 
    Name, 
    Description, 
    CampaignElement, 
    Kin, 
    CampaignElement, 
    Image,
    ElementItemRelation,
    CampaignDescription,
    CampaignElement,    
)


class NameInline(admin.StackedInline):
    model = Name
    fields = ['name',"name_type","secret","author","shared_with","deleted"]
    extra = 0

class CampaignElementInline(admin.StackedInline):
    model = CampaignElement
    fields = ['campaign']
    extra = 0

class NameAdmin(admin.ModelAdmin):
	search_fields = ['name']

class UniverseAdmin(admin.ModelAdmin):
    pass

class UniverseMembershipAdmin(admin.ModelAdmin):
    pass

class UniverseMembershipInline(admin.StackedInline):
    model = UniverseMembership
    fields = ['universe',]
    extra = 0

class AgeAdmin(admin.ModelAdmin):
    pass





class EventAdmin(admin.ModelAdmin):
    pass

class RpgSessionAdmin(admin.ModelAdmin):
	inlines = [NameInline,]


class CampaignInline(admin.StackedInline):
	model = Campaign.gamemasters.through
	fields = ["rpgelement"]
	extra = 0
	verbose_name = "Gamemaster for"
	exclude = ('rpgsession',)
	#fk_name = "gamemasters"

class ElementAdmin(admin.ModelAdmin):
	inlines = [NameInline,]
	search_fields = ['names__name','id']

class CampaignAdmin(ElementAdmin):
    pass



class GroupAdmin(admin.ModelAdmin):
    pass

class IndividualAdmin(admin.ModelAdmin):
    pass

class PlayerCharacterAdmin(admin.ModelAdmin):
    pass

class PlayerCharacterInline(admin.StackedInline):
	model = PlayerCharacter
	fields = ['individual_ptr',]
	extra = 0
	fk_name = "roleplayer"

class CampaignDescriptionInline(admin.StackedInline):
    model = CampaignDescription
    
#    fields = ['campaigns',]
#    extra = 0
    #fk_name = "campaigns"

class DescriptionAdmin(admin.ModelAdmin):
    model = Description
    search_fields = ['content','title','id']
    inlines = [CampaignDescriptionInline,]

class ImageAdmin(admin.ModelAdmin):
	pass



class BestiariesUserAdmin(UserAdmin):
	inlines = [UniverseMembershipInline,PlayerCharacterInline,CampaignInline]
# class PersonAdmin(admin.ModelAdmin):
#      inlines = [CertificateInline,]
# admin.site.register(Person,PersonAdmin)


class KinAdmin(admin.ModelAdmin):
	pass

class ElementItemRelationAdmin(admin.ModelAdmin):
	pass

class CampaignElementAdmin(admin.ModelAdmin):
	search_fields = ['element__names__name','element__id','campaign__names__name','campaign__id']



admin.site.register(Name, NameAdmin)
admin.site.register(Universe, UniverseAdmin)
admin.site.register(Age, AgeAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(Image, ImageAdmin)
admin.site.register(Element, ElementAdmin)
admin.site.register(BestiariesUser, BestiariesUserAdmin)
admin.site.register(RpgSession,RpgSessionAdmin)
admin.site.register(Campaign, CampaignAdmin)
admin.site.register(Group, GroupAdmin)
admin.site.register(Individual, IndividualAdmin)
admin.site.register(PlayerCharacter, PlayerCharacterAdmin)
admin.site.register(UniverseMembership, UniverseMembershipAdmin)
admin.site.register(Description, DescriptionAdmin)
admin.site.register(Kin, KinAdmin)
admin.site.register(ElementItemRelation, ElementItemRelationAdmin)
admin.site.register(CampaignElement, CampaignElementAdmin)
