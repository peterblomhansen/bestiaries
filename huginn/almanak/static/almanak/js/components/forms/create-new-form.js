var createNewForm = Vue.component('create-new-form', {
	template: "\
	<div  >\
		<div v-for='input in getInputsFromType(type)' >\
			<component :is='input' ></component>\
		</div>\
	</div>",
	props: {
		type: String, 
	},
	data: function (){
		return {
			individual: ["new-name-input","new-secret-input","new-gender-input","above-group-autocomplete"],
			group: ["new-name-input","new-secret-input","above-group-autocomplete"],
			place: ["new-name-input","new-secret-input","above-place-autocomplete"],
			event: ["new-name-input","new-secret-input","above-event-autocomplete"],
			rpg_session: ["new-name-input","new-secret-input","new-rpg-session-number-input"],
			item: ["new-name-input","new-secret-input","new-is-unique-item-input"],
			image: [],
			description: [],

		}
	},
	mounted: function (){
		this.$store.commit("createDataMutate",{ key: "campaign", value: this.$store.state.generic.session.base.campaignId});
		this.$store.commit("createDataMutate",{ key: "universe", value: this.$store.state.generic.session.base.universeId});		
		this.$store.commit("createDataMutate",{ key: "author", value: this.$store.state.generic.session.base.userId});
		
		this.$store.dispatch("createGetCampaignDataForRpgSession",this.$store.state.generic.session.base.campaignId);

	},
	methods: {
		getInputsFromType: function (type) {
			return this[type];
		},
		
	},
	computed: {
	
	}
});
 
