from django.core.management.base import BaseCommand, CommandError
from django.db import connections
from collections import namedtuple
from django.contrib.auth.models import AbstractUser
from almanak.models import Description, Element


class Command(BaseCommand):
	help = 'Closes the specified poll for voting'

	def handle(self, *args, **options):
		self.stdout.write(self.style.WARNING("start writing universes to descriptions"))

		descriptions = Description.objects.all()
		for d in descriptions:
			if(d.elements_in_description.get_queryset()):
				universe = d.elements_in_description.get_queryset()[0].element.universe
				d.universe = universe
				d.save()
			
			

		self.stdout.write(self.style.SUCCESS("universes written"))
