var newsStore = {
    state: {
        elementNewestCreated: {},
        elementNewestUpdated: {},        
    },
    mutations: {
        setLatestElementsOfCampaign: function (state,payload){
            Vue.set(state,"elementNewestCreated",payload)
        },
    },
    actions: {
        getLatestElementsOfCampaign: function (context) {
            var campaignId = context.rootState.generic.session.base.campaignId;
            axios.get("/api/new/element/?ordering=-created&campaigns__id__in=" + campaignId + "&d=present_name&format=json").then(function(response){
                context.commit("setLatestElementsOfCampaign",response.data);
            })
            .catch(function (error) {
              console.log(error);
            });  
        },
    },
}