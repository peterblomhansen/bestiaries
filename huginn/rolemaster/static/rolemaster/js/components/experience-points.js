var experiencePoints = Vue.component('experience-points', {
	template: "\
	<div style=\"text-align:right;\">\
		<button @click=\"showDetails = !showDetails\" >See XP details</button> Experience Points: {{ xpTotal }}\
		<div v-if=\"showDetails == true\" class=\"shader--full-screen\">\
			<div class=\"alert--center\" style=\"width:90%;height:80%;\">\
				<div style=\"overflow:auto;height:100%\">\
					<div style=\"display:flex;flex-direction:row\">\
						<div @click=\"showDetails = !showDetails\" class=\"cursor--pointer rubrik\" style=\"font-size:2em;width:50%;text-align:left\" >&times;</div>\
						<div style=\"text-align:right;width:50%;align-self:flex-end\"><p >{{ character.names[0].name }}</p></div>\
					</div>\
					<div style=\"text-align:left;\">\
						<h2>Experience Points</h2>\
						<h4>Add experience points</h4>\
						<p>\
							<input type=\"number\" v-model=\"newXP.amount\" /> <input type=\"text\" v-model=\"newXP.comment\" /> <button @click=\"addExperience()\" >Add</button>\
						</p>\
						<h4>Character Experience</h4>\
						<p>Total Experience: <strong>{{ xpTotal }}</strong></p>\
						<p v-for=\"xp in xpArray\">\
							<input type=\"number\" v-model=\"xp.amount\" @blur=\"updateExperience(xp)\" /> <input type=\"text\" v-model=\"xp.comment\" @blur=\"updateExperience(xp)\" /> <span class=\"cursor--pointer\" @click=\"deleteExperience(xp)\" >&times;</span>\
						</p>\
					</div>\
				</div>\
			</div>\
		</div>\
	</div>\
	",
    data: function() {
		return {
			showDetails: false,
			newXP: {
				amount: 0,
				//character: this.$store.state.character.id,
				comment: "",
				approved: 0,
			}
		}
    },
	mounted: function (){
	},
    methods: {
		addExperience: function (){
			var self = this;
			self.newXP.character = this.$store.state.character.id;
			axios.post("/rm/api/experiencepoint/create/",this.newXP).then(function(response){
				self.$store.state.character.character_experience.push(response.data);
				self.newXP = {
					amount: 0,
					character: self.$store.state.character.id,
					comment: "",
					approved: 0,
				};

			}).catch(function(error) {
					console.log(error);
			});
		},
		updateExperience: function (xp) {
			var self = this;
			axios.patch("/rm/api/experiencepoint/" + xp.id + "/update/",xp).then(function(response){
				// var i = self.character.character_experience.findIndex(function(x){ return x.id == xp.id });
				// self.character.character_experience.splice(i, 1);
			}).catch(function(error) {
					console.log(error);
			});
		},
		deleteExperience: function (xp) {
			var self = this;
			axios.patch("/rm/api/experiencepoint/" + xp.id + "/update/",{ deleted: true }).then(function(response){
				var i = self.character.character_experience.findIndex(function(x){ return x.id == xp.id });
				self.character.character_experience.splice(i, 1);
			}).catch(function(error) {
					console.log(error);
			});
		},

	},
	computed: {
		xpArray: function (){
			return this.$store.state.character.character_experience.filter(function (x){ return x.deleted == false });
		},
		xpTotal: function (){
			//var array = this.$store.state.character.character_experience.filter(function (x){ return x.deleted == true });;
			var total = 0;
			_.each(this.xpArray,function(xp){
				total = total + parseInt(xp.amount);
			});
			return total;
		},
		character: function (){
			return this.$store.state.character;
		},
	}
});
