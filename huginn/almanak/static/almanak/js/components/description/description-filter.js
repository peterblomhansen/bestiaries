var descriptionFilter = Vue.component('description-filter', {
    template: "\
    <div >\
        <select>\
        <option v-for='option in currentOptions' :value='changeDescriptionContext(option.context)' >{{ option.text }}</option>\
        </select>\
    </div>",
	props:{

	},
	computed: {
        currentOptions: function() {
            var optionsArray = [
                { text: "Page", context: "" },                          
                { text: "Bestiary", context: "" }                
            ];


            return optionsArray;
        }


    },
    methods: {
        changeDescriptionContext: function (x){
            return true;
        },
    },

});
