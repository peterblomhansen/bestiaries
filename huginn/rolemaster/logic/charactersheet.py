from rolemaster.models import RolemasterCharacter, StatBonusDistribution, Skill, SkillArea, DevelopedSkill, SkillRow, ProfessionSkillCost, SkillRank, Profession, ComputedSkillSkillSimilarity, ComputedAreaSimilarity
from operator import attrgetter
import math


class Resistance:

	def __init__(self,stats,level):

		self.level = level
		self.stats = stats

	def body_base(self):
		return round(self.stats.data_table(self.level)[5][1] / 10)

	def will_base(self):
		return round(self.stats.data_table(self.level)[8][1] / 10)


	def spirit_base(self):
		presence = self.stats.data_table(self.level)[11][1]
		empathy = self.stats.data_table(self.level)[12][1]
		intuition = self.stats.data_table(self.level)[13][1]

		spirit_all = [presence,empathy,intuition]

		return round(max(spirit_all) / 10)

class Sanity:

	def __init__(self,stats,level):

		self.stats = stats
		self.level = level

	def get_temp_sanity_base(self):
		presence = self.stats.data_table(self.level)[11][1]
		empathy = self.stats.data_table(self.level)[12][1]
		intuition = self.stats.data_table(self.level)[13][1]

		spirit_all = [presence,empathy,intuition]

		#return "temp"
		return round(sum(spirit_all)/len(spirit_all))


	def get_pot_sanity_base(self):
		presence = self.stats.data_table(self.level)[11][2]
		empathy = self.stats.data_table(self.level)[12][2]
		intuition = self.stats.data_table(self.level)[13][2]

		spirit_all = [presence,empathy,intuition]

		#return "temp"
		return round(sum(spirit_all)/len(spirit_all))

	def get_sanity_total_loss(self):
		pass



class ConcussionHits:


	def __init__(self,hits_rolled,stats,level,level_bonuses):

		self.hits_rolled = hits_rolled
		self.stats = stats
		self.base_hits = 0
		self.total = 0
		self.level = level
		self.level_bonuses = level_bonuses

	def calculate_base_hits(self):
		hits = 0
		lvl_bonus_hits = 0
		if len(self.level_bonuses) == 1:
			lvl_bonus = self.level_bonuses[0].bonus
			print(lvl_bonus)
			lvl_bonus_hits = int(lvl_bonus) * int(self.level)

		print("level_bonus_hits",lvl_bonus_hits)
		for h in self.hits_rolled.get_queryset():
			#print(h.hits)
			hits = hits + h.hits

		return hits + lvl_bonus_hits


	def calculate_total_hits(self):
		total_hits = 0
		con_bonus = self.stats.data_table(self.level)[5][6]
		print(self.stats.data_table(self.level)[5][6])
		total_hits = ((100 + int(con_bonus)) * self.calculate_base_hits()) / 100
		# next((r for r in self.stats if r.name == "Body Development"),False))

		return math.ceil(total_hits)


class SkillBonus:

	def __init__(self,skillrow,level,skillrows_all = None,statstable = None,level_bonuses = None, misc_bonuses = None, skill_comments = None):

		self.skillrow 		= skillrow
		self.level 			= level
		self.skillrows_all 	= skillrows_all
		self.source_name 	= ""
		self.source_ratio 	= ""
		self.statstable 	= statstable
		self.level_bonuses 	= level_bonuses
		self.misc_bonuses	= misc_bonuses
		self.skill_comments = skill_comments

	def find_skillrow(self,skill,area):

		def match_skill_area(r):
			return r.id == skill and r.area == area

		match = list(filter(match_skill_area,self.skillrows_all))
		if len(match) == 1:
			return match[0]



	def calculate_basic_ranks(self,row):

		num_ranks = 0
		if row.dev_ranks != None:
			for dr in row.dev_ranks:
				if dr.level <= int(self.level):
					num_ranks = num_ranks + dr.num_ranks

		if row.gm_ranks:
			num_ranks = num_ranks + row.gm_ranks.num_ranks

		if row.hobby_ranks:
			num_ranks = num_ranks + row.hobby_ranks.num_ranks

		return num_ranks

	def choose_ranks(self,basic,sim):

		choice = basic
		for s in sim:
			if s['num_ranks'] > choice:
				self.source_name = s['src_name']
				self.source_ratio = s['src_ratio']
				choice = s['num_ranks']

		return choice


	def calculate_total_ranks(self):

		num_ranks = 0

		basic = self.calculate_basic_ranks(self.skillrow)

		num_ranks = basic

		if self.skillrow.similarity	or self.skillrow.area_similarity:
			pot_sim = []
			if self.skillrow.similarity:
				for sim in self.skillrow.similarity:
					factorSplit = sim.ratio.split("/")
					factor = int(factorSplit[0]) / int(factorSplit[1])
					sim_skillrow = self.find_skillrow(sim.origin.id,sim.area)
					if sim_skillrow != None:
						#print("sim",self.calculate_basic_ranks(sim_skillrow),sim.ratio)
						pot_sim.append({"src_name": sim_skillrow.name,"src_ratio": sim.ratio, "num_ranks": math.ceil(self.calculate_basic_ranks(sim_skillrow) * factor)})

			if self.skillrow.area_similarity:
				for asim in self.skillrow.area_similarity:

					factorSplit = asim.ratio.split("/")
					factor = int(factorSplit[0]) / int(factorSplit[1])
					asim_skillrow = self.find_skillrow(asim.skill.id,asim.origin.id)
					if asim_skillrow != None:
						pot_sim.append({"src_name": asim_skillrow.area_name,"src_ratio": asim.ratio, "num_ranks": math.ceil(self.calculate_basic_ranks(asim_skillrow) * factor)})

			num_ranks = self.choose_ranks(basic,pot_sim)

		return num_ranks


	def calculate_rank_bonus(self):
		r = self.calculate_total_ranks()
		b = -25
		if r > 0:
			if r < 11:
				b = r * 5
			elif r < 21:
				b = 50 + (r-10) * 3
			elif r < 31:
				b = 70 + (r-20) * 2
			elif r < 41:
				b = 90 + (r-30)


		return b


	def calculate_stat_bonus(self):
		stat_bonus = 0
		stat_index_map = {
			"LU": 3,
			"AG": 4,
			"CO": 5,
			"RE": 6,
			"ME": 7,
			"SD": 8,
			"ST": 9,
			"QU": 10,
			"PR": 11,
			"EM": 12,
			"IN": 13,
		}
		def calc_bonus(stat):
			i = stat_index_map[stat]
			s = self.statstable[i]
			return s[6]

		stat_array = []

		if self.skillrow.stat_1 != None and self.skillrow.stat_1 != '0':
			stat_array.append(calc_bonus(self.skillrow.stat_1.replace(" ","")))
			if self.skillrow.stat_2 != None and self.skillrow.stat_2 != "":
				stat_array.append(calc_bonus(self.skillrow.stat_2.replace(" ","")))
				if self.skillrow.stat_3 != None and self.skillrow.stat_3 != "":
					stat_array.append(calc_bonus(self.skillrow.stat_3.replace(" ","")))


		if len(stat_array) > 0:
			stat_bonus = sum(stat_array) / len(stat_array)

		return math.ceil(stat_bonus)

	def calculate_level_bonus(self):
		level_bonus = 0

		for cat in self.level_bonuses.get_queryset():
			if cat.category == self.skillrow.category:
				level_bonus = int(cat.bonus) * int(self.level)
		#print(self.skillrow.category,self.level_bonuses)
		return level_bonus


	def calculate_misc_bonuses(self):
		misc_bonus = 0
		#print(self.misc_bonuses.get_queryset())
		for b in self.misc_bonuses.get_queryset():
			ms = b.skill.id
			ma = None

			rs = self.skillrow.id
			ra = None

			if self.skillrow.area != None:
				ra = self.skillrow.area

			if b.area != None:
				ma = b.area.id

			if ms == rs and ma == ra:
				misc_bonus = misc_bonus + b.bonus

		return misc_bonus


	def get_skill_comments(self):
		comments = []
		#if len(self.skill_comments):
		for sc in self.skill_comments.get_queryset():
			cs = sc.skill.id
			ca = None

			rs = self.skillrow.id
			ra = None

			if self.skillrow.area != None:
				ra = self.skillrow.area

			if sc.area != None:
				ca = sc.area.id

			if cs == rs and ca == ra:
				comments.append(sc)

		return comments

	def write_stats(self):
		stats_string = ""

		if self.skillrow.stat_1 != None or self.skillrow.stat_1 != 0:
			#stats_string = self.skillrow.stat_1
			if self.skillrow.stat_2 != None:

				if self.skillrow.stat_3 != None:
					stats_string = self.skillrow.stat_1 + "/" + self.skillrow.stat_2 + "/" +self.skillrow.stat_3
				else:
					stats_string = self.skillrow.stat_1 + "/" + self.skillrow.stat_2
			else:
				stats_string = self.skillrow.stat_1

		else:
			stats_string = "None"




		return stats_string


	def total_bonus(self):
		return int(self.calculate_rank_bonus()) + int(self.calculate_stat_bonus()) + int(self.calculate_level_bonus()) + int(self.calculate_misc_bonuses())


class StatsTable:

	def __init__(self, race = None, stats = None,special_bonus = None):


		self.race = race
		self.stats = stats
		self.stat_dist = StatBonusDistribution.objects.all()
		self.special_bonus = special_bonus

	def data_table(self,lvl):


		# print(lvl)
		# print(self.stats)

		def is_this_level(stat):
			return stat.level == int(lvl)

		current_stats = list(filter(is_this_level,self.stats))

		#	print(self.race.__dict__)


		def find_race_bonus(name):
			if self.race != None:
				return getattr(self.race,name)
			else:
				return 0

		def find_normal_bonus(temp):
			for dist in self.stat_dist:
				if dist.stat == temp:
					return dist.bonus


		def find_stat(stype,name):
			if len(current_stats) > 0:
				for s in current_stats:
					if s.stat_type == stype:
						return getattr(s,name)
			else:
				return 0

		def find_special_bonus(stat):
			#print("STAT SPECIAL",self.special_bonus)
			if self.special_bonus != None:

			#bonus = 0
				for s in self.special_bonus:
					#print(s.stat.lower(),stat[:2].lower())
					if s.stat.lower() == stat[:2].lower():
						#print(s.bonus)
						return s.bonus
			return 0



		def write_stat_array(stat):
			comp_name = stat.lower().replace(" ","_")
			#print(comp_name)
			temp = find_stat("temp",comp_name)
			pot = find_stat("pot",comp_name)
			n = 0
			r = 0
			s = 0
			normal_bonus = find_normal_bonus(temp)
			race_bonus = find_race_bonus(comp_name)
			special_bonus = find_special_bonus(comp_name)
			if normal_bonus:
				n = normal_bonus
			if race_bonus:
				r = race_bonus
			if special_bonus:
				s = special_bonus

			total_bonus = n + r + s

			stat_data = [stat,temp,pot,normal_bonus,race_bonus,special_bonus,total_bonus]

			return stat_data

		data = [
			["Stats","","","","","",""],
			["Stat","","","Bonuses","","",""],
			["Name","Temp","Pot","Normal","Race","Special","Total"],
			write_stat_array("LUCK"),
			write_stat_array("AGILITY"),
			write_stat_array("CONSTITUTION"),
			write_stat_array("REASONING"),
			write_stat_array("MEMORY"),
			write_stat_array("SELF DISCIPLINE"),
			write_stat_array("STRENGTH"),
			write_stat_array("QUICKNESS"),
			write_stat_array("PRESENCE"),
			write_stat_array("EMPATHY"),
			write_stat_array("INTUITION"),
		]

		return data


class SkillRows:


	def __init__(self,data,character):

		self.data = data
		self.character = character


	def areas_of_type(self,area_type,skill_id):

		def has_type(area):
			if area.area_type == "skill":
				return area.area_type == area_type and area.skill.id == skill_id
			else:
				return area.area_type == area_type

		return list(filter(has_type,self.data['areas']))



	def get_skill_cost(self,skill):

		empty = {
			"id" : None,
			"cost_1" : 0,
			"cost_2" : 0,
			"cost_3" : 0,
		}


		def has_id(cost):
			return cost.skill == skill
		if self.data['costs']:
			cost = list(filter(has_id,self.data['costs'].get_queryset()))

			if cost:
				return cost[0]
		else:
			return empty


	def find_dev_ranks(self,skill,area,cost):

		ranks = []
		lvls = self.character.level

		for l in range(lvls):
			lvl = 1 + l
			#print(l)
			if area == None:
				dev = next((r for r in self.data['ranks'] if r.skill.id == skill and r.area == None and r.level == lvl),False)
			else:
				dev = next((r for r in self.data['ranks'] if r.skill.id == skill and r.area != None and r.area.id == area and r.level == lvl),False)

			if dev != False:
				a = None
				if dev.area != None:
					a = dev.area.id
				ranks.append(SkillRank(dev.id,dev.skill.id,a,dev.character.id,dev.level,dev.num_ranks,dev.development_type,self.calculate_dev_price(dev.num_ranks,cost)))
			else:
				ranks.append(SkillRank(None,skill,area,self.character.id,lvl,0,"dev",0))

		return ranks


	def find_non_dev_ranks(self,skill,area,rank_type):

		if area == None:
			dev = next((r for r in self.data['ranks'] if r.skill.id == skill and r.area == None and r.level == 0 and r.development_type == rank_type),False)
		else:
			dev = next((r for r in self.data['ranks'] if r.skill.id == skill and r.area != None and r.area.id == area and r.level == 0 and r.development_type == rank_type),False)

		if dev != False:
			a = None
			if dev.area != None:
				a = dev.area.id
			return SkillRank(dev.id,dev.skill.id,a,dev.character.id,dev.level,dev.num_ranks,dev.development_type)
		else:
			return SkillRank(None,skill,area,self.character.id,0,0,rank_type,0)

	def calculate_dev_price(self,num_ranks,cost):

		price = 0

		if num_ranks == 1:
			price = cost.cost_1

		elif num_ranks == 2:
			if cost.cost_2 == -1:
				price = cost.cost_1 * num_ranks
			else:
				price = cost.cost_1 + cost.cost_2

		elif num_ranks == 3:
			if cost.cost_2 == -1:
				price = cost.cost_1 * num_ranks
			elif cost.cost_3 == -1:
				price = cost.cost_1 + cost.cost_2 + cost.cost_3


		elif num_ranks > 3:
			if cost.cost_2 == -1:
				price = cost.cost_1 * num_ranks
			elif cost.cost_3 == -1:
				price = cost.cost_1 + cost.cost_2 * (num_ranks - 1)

		return price


	def find_similarity(self,skill,area):

		similarities = []
		a = None
		if area != None:
			a = area.id
		#print(area,skill.skill_target_for.get_queryset())
		for sim in skill.skill_target_for.get_queryset():

			simSkillSkill = ComputedSkillSkillSimilarity(sim.origin, sim.target, sim.ratio, a)

			similarities.append(simSkillSkill)

		return similarities


	def find_area_similarity(self,skill,area):

		areas_similaritites = []

		for asim in area.area_target_for.get_queryset():
			area_sim = ComputedAreaSimilarity(asim.origin,asim.target,asim.ratio,skill)

			areas_similaritites.append(area_sim)

		return areas_similaritites


	def get_weapons_of_chosen_type(self,skill,weapon_priorities):

		def weapon_category_to_skill(priority):
			return skill.id == priority.skill.id

		current_weapon_cat = list(filter(weapon_category_to_skill,weapon_priorities))
		if len(current_weapon_cat) > 0:
			cat = current_weapon_cat[0].category

			def has_type(area):
				return area.area_type == "weapon" and area.weapon_category == cat


			return list(filter(has_type,self.data['areas']))

		else:
			return []

	def is_selected(self,skill,area):

		check = next(x for x in self.character.selected_skills.get_queryset().filter(selected=True) if x.id == skill and x.area == area)
		print(check)
		is_selected = False

		return is_selected



	def generate_skillrows(self):
		rows = []
		for s in self.data['skills']:
			cost = self.get_skill_cost(s)
			if s.area_support == True:
				dev_ranks = self.find_dev_ranks(s.id,None,cost)
				#print(dev_ranks)
				sr = SkillRow(s.id,None,s.category,s.name,None,s.area_support,s.stat_1,s.stat_2,s.stat_3,cost,None,None,None)
				#print(sr.__dict__)
				rows.append(sr)
				for t in s.supported_area_types.get_queryset():

					if s.id > 80 and s.id < 87:
						weapon_priorities = self.character.weapon_category_priority.get_queryset()
						areas = self.get_weapons_of_chosen_type(s,weapon_priorities)

					else:
						areas = self.areas_of_type(t.area_type,s.id)#list(filter(self.areas_of_type,self.data['areas'])
					for a in areas:
						gm_ranks = self.find_non_dev_ranks(s.id,a.id,"gm")
						hobby_ranks = self.find_non_dev_ranks(s.id,a.id,"hobby")
						dev_ranks = self.find_dev_ranks(s.id,a.id,cost)
						s1 = s.stat_1
						s2 = s.stat_2
						s3 = s.stat_3
						if a.area_type == "weapon":
							s1 = a.stat_1
							s2 = a.stat_2
							s3 = a.stat_3
						sr = SkillRow(s.id,a.id,s.category,s.name, a.name, True,s1,s2,s3,cost,gm_ranks,hobby_ranks,dev_ranks,self.find_similarity(s,a),self.find_area_similarity(s,a))
						rows.append(sr)

			else:
				dev_ranks = self.find_dev_ranks(s.id,None,cost)
				gm_ranks = self.find_non_dev_ranks(s.id,None,"gm")
				hobby_ranks = self.find_non_dev_ranks(s.id,None,"hobby")
				sr = SkillRow(s.id,None,s.category,s.name,None,s.area_support,s.stat_1,s.stat_2,s.stat_3,cost,gm_ranks,hobby_ranks,dev_ranks,self.find_similarity(s,None))
				rows.append(sr)





		return rows
