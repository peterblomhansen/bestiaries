var descriptionShow = Vue.component('description-show', {
    template: "\
	<div class=\"description description--text\" style=\"cursor:default;\">\
		<div style=\"float:right;padding:0.5em;text-align:center\" >\
			<p class=\"rubrik\" >{{ publicSecretString }}</p>\
			<description-relevance :relation=\"data\" />\
			<a :href=\"'/almanak/description/' + data.description.id\" ><button class=\"button--small\" >read</button></a><br />\
			<button\
				@click=\"openEditor(data.description.id)\"\
				class=\"button--small\" v-if=\"data.description.user_can_edit === true\"\
				:disabled=\"data.description.locked_by_user !== null && lockedByOtherUser(data.description.locked_by_user)\"\
			>\
				<span v-if=\"data.description.locked_by_user !== null && lockedByOtherUser(data.description.locked_by_user)\">locked</span>\
				<span v-else >edit</span>\
			</button>\
			<br v-if=\"data.description.user_can_edit === true\" />\
			<button class=\"button--small\" v-if=\"viewFull == true\" @click=\"toggleShort\" >&uarr;</button>\
			<button class=\"button--small\" v-if=\"viewFull == false\" @click=\"toggleFull\" >&darr;</button>\
		</div>\
			<article @click=\"toggleFull\" v-if=\"viewFull == false\" :class=\"descriptionTypeClass\" v-html=\"contentShort\" >\
			</article>\
		<transition name=\"fade\" >\
			<article v-if=\"viewFull == true\" :class=\"descriptionTypeClass\" v-html=\"contentFull\" >\
			</article>\
		</transition>\
		<div class=\"rubrik\" style=\"display:flex;width:100%;\">\
			<p v-if=\"this.data.description.description_type == 'off_game'\" style=\"flex:1;padding-left:0.6em;padding-right:0.6em\" >by <a  :href=\"'/accounts/user/' + data.description.author.id \">{{ author }}</a></p>\
			<p v-else style=\"padding-left:0.6em;padding-right:0.6em;\" >by <element-link :element=\"this.data.description.ingame_author_full\" style=\"flex: 1;\" /></p>\
			<p style=\"flex:1;text-align:right;padding-left:0.6em;padding-right:0.6em\">{{ time }}</p>\
		</div>\
		<br/>\
	</div>",
	props:{
		data: Object,

	},
	data: function (){
		return {
			viewFull: false,			
		}
	},
	computed: {
		publicSecretString: function (){
			return this.data.description.secret === true ? "secret" : "public";
		},
		time: function (){
			if(this.data.description.description_type == "in_game") { 
				return this.data.description.ingame_time_string;
			}
			else {
				return this.data.description.updated.split("T")[0];
			}
			return "time";
		},
		author: function() {
			if(this.data.description.description_type == "in_game") {
				return this.data.description.ingame_author.present_name.name;
			} else {
				return this.data.description.author_full.first_name + " " + this.data.description.author_full.last_name;
			}
		},
		contentViewed: function (){
			if(this.viewFull === true) {
				return this.contentFull;
			}
			else {
				return this.contentShort;
			}
		},
		contentShort: function (){
			return this.data.description.content_stripped.plain_text.substring(0,440) + "&#8230;";
		},
		contentFull: function (){
			return this.contentFixedLinks(this.data.description.content);
		},
		descriptionTypeClass: function(){
			var color = "";
			var blot = "";
			if (this.data.description.description_type == "in_game") {
				color =  "ink";
			}
			else if (this.data.description.description_type == "off_game") {
				color = "rubrik";
			}
			if (this.viewFull === false) {
				blot = " blot description--short";
				if(this.data.relevance < 0) {
					blot += " description--irrelevant";
				}
				if(this.data.description.secret === true ) {
					blot += " description--secret";
				}				
			} 
			
			return color + blot;
		}
	},
	methods: {
		openEditor: function (id){
			var self = this;
			this.$store.dispatch("lockDescription",{id:id, userId: this.$store.state.generic.session.base.userId})
				.then(function(response){
					self.$store.dispatch("loadDescriptionData",{id:id})
					.then(function (response){
						self.$store.dispatch("editorGetDescriptionElementRelations",{id:id})
						.then(function(){
						});
					});
				})
				.catch(function (error) {

				});

		},
		toggleFull: function (event) {
			if(this.viewFull == false) this.viewFull = true ;			
		},
		toggleShort: function (event) {
			if(this.viewFull == true) this.viewFull = false ;
		},		
        contentFixedLinks: function(content) {
            var find = 'href="';
            var re = new RegExp(find, 'g');
            var display = content.replace(re, 'target="_blank" href="/almanak/side/');
            return display;
		},
		lockedByOtherUser: function (userId){
			return userId == this.$store.state.generic.session.base.userId ? false : true;
		},		
	}

});
