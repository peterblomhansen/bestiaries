var imageShow = Vue.component('image-show', {
    template: "\
		<div class=\"description description--image\">\
			<img :src=\"data.description.url\" />\
			<div>\
				<div >\
					<p class=\"rubrik\" >{{ publicSecretString }}</p>\
					<description-relevance :relation=\"data\" />\
					<a :href=\"'/almanak/image/' + data.description.id\" ><button class=\"button--small\" >full</button></a>\
					<button\
							class=\"button--small\" v-if=\"data.description.user_can_edit === true\"\
							:disabled=\"data.description.locked_by_user !== null && lockedByOtherUser(data.description.locked_by_user)\"\
					>\
						<span v-if=\"data.description.locked_by_user !== null && lockedByOtherUser(data.description.locked_by_user)\">locked</span>\
						<span v-else >edit</span>\
					</button>\
				</div>\
			</div>\
    </div>",
	props:{
		data: Object,
	},
	methods: {
		lockedByOtherUser: function (userId){
			return userId == this.$store.state.generic.session.base.userId ? false : true;
		},		
	},
	computed: {
		publicSecretString: function (){
			return this.data.description.secret === true ? "secret" : "public";
		},		
		descriptions: function (){
			return this.$store.state.element.relations.descriptions;
		},
	},

});
