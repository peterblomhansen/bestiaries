var elementDelete = Vue.component('element-delete', {
	template: "<div>\
		<p><a @click=\"delete_question = !delete_question\" class=\"rubrik cursor--pointer\" v-if=\"element.user_permissions.write == true \">Delete Page</a></p>\
		<div v-if=\"delete_question == true \" class=\"shader--full-screen\">\
          <div class=\"alert--center\" style=\"text-align:center\" >\
		  	<h3 class=\"rubrik\" >Are you sure you want to delete</h3>\
				<h4 class=\"ink\">Page {{ element.id }} - {{ element.present_name }}</h4><br/>\
				<button @click=\"deleteElement(element.id)\" >Yes</button> <button @click=\"delete_question = !delete_question\" >No</button>\
		  </div>\
		</div>\
	</div>",
    data: function() {
        return {
			delete_question: false,
        }
    },
	computed: {
		static: function (){

            return this.$store.state.initial.staticUrl;

        },
        element: function(){

            return this.$store.state.element.masterdata;
        },
	},
    methods: {
		deleteElement: function (id){
//			console.log(id);
			var self = this;
			axios.patch('/api/element/' + id + '/update/', {
                deleted: true
            }).then(function(response) {
				var prev = parseInt(id) - 1;
				window.location.href = "/almanak/side/" + prev;
                //console.log(response)
                //eventHub.$emit('reloadData');
            }).catch(function(error) {
                console.log(error);
            });

		}

	}
});
