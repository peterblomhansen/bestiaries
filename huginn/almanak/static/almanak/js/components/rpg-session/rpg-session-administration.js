var rpgSessionAdministration = Vue.component('rpgSessionAdministration', {
    template: "\
    <transition name='fade' v-if='sessions !== null' >\
        <div >\
            <h3 v-if='plannedSessions.length > 0' class='rubrik'>Planned Sessions</h3>\
            <p v-if='plannedSessions.length > 0' v-for='session in plannedSessions'  class='toc__row'>\
                <span class='rubrik'>{{ session.session_number }}.&nbsp;</span>\
                <a :href=\"'/almanak/element/' + session.id + '/'\" class='ink'>{{ session.present_name.name }}</a>\
                <span class='toc__fill faded'></span>\
                <span class='rubrik' style='text-align:right;'>{{ dur(session.offgame_start) }}</span>\
            </p>\
            <h3 class='rubrik'>Played Sessions</h3>\
            <p v-for='session in playedSessions'  class='toc__row'>\
                <span class='rubrik'>{{ session.session_number }}.&nbsp;</span>\
                <a :href=\"'/almanak/element/' + session.id + '/'\" class='ink'>{{ session.present_name.name }}</a>\
                <span class='toc__fill faded'></span>\
                <span class='rubrik' style='text-align:right;'>{{ dur(session.offgame_start) }}</span>\
            </p>\
        </div>\
    </transition>",
	props: {

	},
    data: function() {
        return { }
    },
    mounted: function (){
        this.$store.dispatch("tocGetRpgSessionsOfCurrentCampaign");
    },
    methods: {
        dur: function(yt) {
            var now = new moment();
            var elementEventTime = new moment(yt);
            var duration = moment.duration(elementEventTime.diff(now));
            return duration.humanize(true);
        }
    },
    computed: {
        campaign: function (){
            return this.$store.state.generic.session.full.campaign_full;
        },
        news: function (){
            return this.$store.state.news.elementNewestCreated;
        },
        bestiaryCounts: function (){
        	return this.$store.state.toc.bestiary.counts; 
		},
		sessions: function (){
			return this.$store.state.toc.campaign.sessions;
        },
        playedSessions: function (){
            return this.sessions.results.filter(function (session){
                return new moment(session.offgame_start) < new moment();
            })
        },
        plannedSessions: function (){
            return this.sessions.results.filter(function (session){
                return new moment(session.offgame_start) > new moment();
            })
        },


    }


});
