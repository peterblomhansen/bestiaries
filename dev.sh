#!/bin/bash
# Start containers and django developement server
echo ""
echo "--<< Starting application containers >>--"
docker-compose up -d
echo ""
echo "--<< Starting Django Development Server >>--"
docker-compose exec web python manage.py runserver 0.0.0.0:8080