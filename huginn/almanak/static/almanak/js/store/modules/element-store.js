var elementStore = {
    state: {
        id: document.getElementById("element-id").value,
        masterdata: { },
        names: {},
        relations: {},
        permissions: {
            read: false,
            write: false,
            showEditPermissions: false,
        },
        errors: {
            
        }   
    },
    getters: {
        elementTypeTranslated: function(state) {
            return function(typeOrig) {
                var typeTrans = "";
                if (typeOrig == "sted") {
                    typeTrans = "Place";
                } else if (typeOrig == "individ") {
                    typeTrans = "Individual";
                } else if (typeOrig == "gruppe") {
                    typeTrans = "Group";
                } else if (typeOrig == "kampagne") {
                    typeTrans = "Campaign";
                } else if (typeOrig == "rpg_session") {
                    typeTrans = "RPG Session";
                } else if (typeOrig == "begivenhed") {
                    typeTrans = "Event";
                } else if (typeOrig == "player_character") {
                    typeTrans = "Player Character";
                    } else if (typeOrig == "artefakt") {
                    typeTrans = "Item";
                } else if (typeOrig == "unika") {
                    typeTrans = "Unique Item";
                } else {
                    typeTrans = typeOrig;
                }
                return typeTrans;
            }
        },
        elementTypeIcon: function(state, getters, rootState, rootGetters) {
            return function (type,type_grund,type_spec) {
    
                var filename = type;
                if(type == "begivenhed") {
                if(type_spec == "kampagne" || type_spec == "rpg_session"){
                    filename = type_spec;
                }
                }

            if (type == "entitet") {
                filename = type_grund;
                if(type_grund == "player_character") {
                filename = "individ"
                }
            }
            return rootState.generic.staticUrl + "images/icons/type/white/" + filename + ".svg";
            }
        },        
        monthsOfYear: function(state) {
            return [{
                text: "jan",
                value: 1
            }, {
                text: "feb",
                value: 2
            }, {
                text: "mar",
                value: 3
            }, {
                text: "apr",
                value: 4
            }, {
                text: "may",
                value: 5
            }, {
                text: "jun",
                value: 6
            }, {
                text: "jul",
                value: 7
            }, {
                text: "aug",
                value: 8
            }, {
                text: "sep",
                value: 9
            }, {
                text: "oct",
                value: 10
            }, {
                text: "nov",
                value: 11
            }, {
                text: "dec",
                value: 12
            }, ];
        }
    },
    mutations: {
        toggleEditElementPermissions: function (state){
            state.permissions.showEditPermissions = !state.permissions.showEditPermissions;
        },
    },
    actions: {
        elementGetMasterdata: function (context,id){
            var typeGrund = document.getElementById("type-grund").value;
            var typeUrl = context.getters.elementTypeTranslated(typeGrund).replace(" ","_").toLowerCase();
            axios.get("/api/new/" + typeUrl + "/" + context.state.id + "/?d=es1_can_user_edit__shared_with__es1_author__present_name__campaigns__es1c_campaign__es1c_ces1c_present_name__universe_name__es1_editors__roleplayer__es1c_ces1c_gamemasters&format=json")
                .then(function (response) {
                    Vue.set(context.state,"masterdata",response.data);     
                    context.dispatch("loader",{ map: context.state.masterdata.type_grund, section: "element"});
                    Vue.set(context.state.permissions,"read",true);
                    Vue.set(context.state.permissions,"write",context.state.masterdata.user_can_edit);                        
                })
                .catch(function (error) {
                    console.log("elementGetMasterdata",error);
                    if(error.response.status == 403) {
                        context.state.permissions.read = false;
                    }
                });
        },
        // NAMES
        elementGetNames: function(context){
            var self = this;
            axios.get("/api/new/name/?d=ns1_user_permissions__ns1_shared_with__es1_present_name&format=json&ordering=name&page_size=50&element__id=" + context.state.id)
                .then(function (response) {
                    Vue.set(context.state,"names",response.data);
                    //self.$store.state.generic.pageTitle = "sgsgsg";
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        elementGetRelationsAbove: function (context,load) {
            var pageParam = "";
            if(load && load.page) {
                pageParam = "&page=" + load.page
            };             
            axios.get('/api/new/element-relation/?format=json&below__id=' + context.state.id + '&d=ers1a_present_name__above')	
			.then(function (response) {
                Vue.set(context.state.relations,"above",response.data.results);

			})
			.catch(function (error) {
			  console.log(error);
            });
        },	
        elementGetRelationsBelow: function (context,load) {       
            var pageParam = "";
            if(load && load.page) {
                pageParam = "&page=" + load.page
            };                     		
			axios.get('/api/new/element-relation/?format=json&above__id=' + context.state.id + '&d=ers1b_present_name__below')	
			.then(function (response) {
                Vue.set(context.state.relations,"below",response.data.results);

			})
			.catch(function (error) {
			  console.log(error);
			});	
        },        
        elementGetOwned: function (context, load){
            var self = this;
            var pageParam = "";
            if(load && load.page) {
                pageParam = "&page=" + load.page
            };  
            axios.get("/api/new/element-item-relation/?format=json&d=owned__eirs1od_present_name&distinct=latest&owner__id=" + context.state.id + pageParam)
                .then(function (response) {
                    if(load && load.page) {
                        _.each(response.data.results,function(r){
                            context.state.relations.owned.results.push(r);
                        })
                    }else {
                        Vue.set(context.state.relations,"owned",response.data);

                    }                        
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        elementGetUniqueOwned: function (context, load){
            var self = this;
            var pageParam = "";
            if(load && load.page) {
                pageParam = "&page=" + load.page
            }                  
            axios.get("/api/new/element-unique-item-relation/?format=json&d=owned__euirs1od_present_name&owner__id=" + context.state.id + pageParam)
                .then(function (response) {
                    if(load && load.page) {
                        _.each(response.data.results,function(r){
                            context.state.relations.ownedUnique.results.push(r);
                        })
                    }else {
                        Vue.set(context.state.relations,"ownedUnique",response.data);

                    }                          
                })
                .catch(function (error) {
                    console.log(error);
                });
        },            
        getFamilyRelations: function (context) {
            var self = this;
            axios.get('/api/new/element-relation/?format=json&relation_type=familie&below__id=' + context.state.id + '&d=above__ers1a_present_name')
                .then(function (response) {
                    Vue.set(context.state.relations,"parents",response.data.results);
                    var siblingsAll = [];
                    _.each(context.state.relations.parents, function (p){
                        axios.get('/api/new/element-relation/?format=json&relation_type=familie&above__id=' + p.above_full.id + '&d=below__ers1b_present_name')
                        .then(function (response) {
                            siblingsAll.push(response.data.results);
                        });
                    })
                    Vue.set(context.state.relations,"siblings",siblingsAll);

                })
                .catch(function (error) {
                    console.log(error);
                });
            axios.get('/api/new/element-relation/?format=json&relation_type=familie&above__id=' + context.state.id + '&d=below__ers1b_present_name')
            .then(function (response) {
                Vue.set(context.state.relations,"children",response.data.results);
            })
            .catch(function (error) {
                console.log(error);
            });
            var partners = [];
            axios.get('/api/new/element-relation/?format=json&relation_type=individ&above__id=' + context.state.id + '&d=below__ers1b_present_name')
            .then(function (response) {
                console.log("above",response.data);
                partners.push(response.data.results);
                axios.get('/api/new/element-relation/?format=json&relation_type=individ&below__id=' + context.state.id  + '&d=above__ers1a_present_name')
                .then(function (response) {
                    console.log("below",response.data);
                    partners.push(response.data.results);
                    Vue.set(context.state.relations,"partners",partners[0].concat(partners[1]));

                });
            })

            .catch(function (error) {
                console.log(error);
            });
        },
        getMemberships: function (context,load){
            var pageParam = "";
            var searchQuery = "";
            if(load && load.page) {
                pageParam = "&page=" + load.page
            }
            axios.get('/api/new/element-relation/?below__id=' + context.state.id + '&relation_type=normal&format=json&d=above__ers1a_present_name__ers1_above_title&ordering=above_title' + pageParam)
            .then(function (response) {
                if(load && load.page) {
                    _.each(response.data.results,function(r){
                        context.state.relations.memberships.results.push(r);
                    })				
                } else {
                    Vue.set(context.state.relations,"memberships",response.data);				
                                    
                }
            })
            .catch(function (error) {
                console.log(error);
            });
        },
        getDescriptionsOfElement: function (context, load){
            var pageParam = "";
            var searchQuery = "";
            if(load && load.page) {
                pageParam = "&page=" + load.page
            }
            if(load && load.searchQuery) {
                searchQuery = "&search=" + load.searchQuery
            }                

            axios.get('/api/new/description-element-relation/?format=json&d=description__ders1d_can_user_edit__ders1d_content_stripped__ders1d_author__ders1d_ingame_author__ders1d_ds1ia_present_name&ordering=-relevance&element__id=' + context.state.id + searchQuery + pageParam)//element__id=' + state.element.id + '&&d=ders1_description' + pageParam + "&ordering=-relevance")
            .then(function (response) {
                if(load && load.page) {
                    _.each(response.data.results,function(r){
                        context.state.relations.descriptions.results.push(r);
                    })
                }else {
                    Vue.set(context.state.relations,"descriptions",response.data);
                }

            })
            .catch(function (error) {
                console.log(error);
            });
        },
        getExperiences: function(context,load) {
            var pageParam = "";
            if(load && load.page) {
                pageParam = "&page=" + load.page
            }                
            axios.get("/api/new/entity-event-relation/?format=json&d=event__eers1ev_present_name&entity__id=" + context.state.id + pageParam)
            .then(function (response) {
                if(load && load.page) {
                    _.each(response.data.results,function(r){
                        context.state.relations.experiences.results.push(r);
                    })
                }else {
                    Vue.set(context.state.relations,"experiences",response.data);

                }

            })
            .catch(function (error) {
                console.log(error);
            });
        },
        elementGetOwners:function (context,load) {
            var self = this;
            var pageParam = "";
            if(load && load.page) {
                pageParam = "&page=" + load.page
            }                  
            axios.get("/api/new/element-unique-item-relation/?format=json&d=es1_present_name__euirs1_owner&owned__id=" + context.state.id + pageParam)
                .then(function (response) {
                    if(load && load.page) {
                        _.each(response.data.results,function(r){
                            context.state.relations.ownedBy.results.push(r);
                        })
                    }else {
                        Vue.set(context.state.relations,"ownedBy",response.data);

                    }                          
                })
                .catch(function (error) {
                    console.log(error);
                });                
        }       
    },        
    
};