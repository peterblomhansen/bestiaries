
//** INIT.JS **//
// This is the main .js file, that loads main configuration and the global Vue instance

// CSRF Config for ajax calls
axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
axios.defaults.xsrfCookieName = "csrftoken";
Vue.use(VueQuillEditor)

// Custom directive to enable focus on an HTML element when loaded
Vue.directive('focus', {
	// When the bound element is inserted into the DOM...
	inserted: function (el) {
	  // Focus the element
	  el.focus()
	}
});

// Vue/Django template {{}} conflict fix
// Vue.options.delimiters = ['[[', ']]'];

// Top Vue Element
var tome = new Vue({
	el: "#tome",
	store: store,
	delimiters:['[[', ']]'],	
	data: {},
	
	mounted: function(){

		this.$store.dispatch("getSession");
		this.initialize(this.uriSegments);

	},
	methods: {
		closePopup: function(event) {
			if(!_.includes(event.target.className, "popup-element")) {
				this.$store.state.generic.ui.popupGlobal = false;
			}
		},
		initialize: function(uris){
			var uri = uris[2];
			switch (uri) {
				case "element":
					if(document.getElementById("element-deleted") === null) {
						var id = uris[3];
						this.$store.dispatch("elementGetNames");
						this.$store.dispatch("elementGetMasterdata",id);
					}
				break;
				case "description":
					Vue.set(this.$store.state.description,"id",parseInt(uris[3]));
					this.$store.dispatch("getDescription");
					this.$store.dispatch("getDescriptionElementRelations");
				break;
				case "image":
					Vue.set(this.$store.state.description,"id",parseInt(uris[3]));
					this.$store.dispatch("getDescription");
					this.$store.dispatch("getDescriptionElementRelations");
				break;
				case "profile":

				break;
			}
		},
	},
	computed: {
		uriSegments: function(){
			return window.location.pathname.split("/");

		},
		userIsIngame: function () {
            var userIsIngame = false;
            if(this.$store.state.generic.session.base.userId > 0 && this.$store.state.generic.session.base.universeId > 0 && this.$store.state.generic.session.base.campaignId > 0 && this.$store.state.generic.session.base.roleId > -1) {
                userIsIngame = true;
            }
            return userIsIngame;
		},
		canReadElement: function(){
			return this.$store.state.element.permissions.read;
		},
		title: function () {
			document.title = this.$store.state.generic.pageTitle;
			return this.$store.state.generic.pageTitle;
		}

	}


});
