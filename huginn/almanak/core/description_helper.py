import almanak.models as almanak
from bs4 import BeautifulSoup, Tag, NavigableString
import re



class DescriptionHelper:

    def __init__(self,description):

        self.description = description
        self.relations_content = self.get_links()
        self.new_links = []
        self.obsolete_links = []

    def get_links(self):

        soup = BeautifulSoup(self.description.content,'html.parser')

        relations = []

        for link in soup.find_all('a'):
            if "http://" not in link.get('href') and "https://" not in link.get('href') and link.get('href') not in relations:
                relations.append(int(link.get('href')))

        return relations


    def add_new_relations(self):
        print("Add new",self.relations_content)
        for rel in self.relations_content:
            if rel not in [des_rel.element.id for des_rel in self.description.element_relations.get_queryset()]:
                print("creating new relation")
                print("rel",rel)
                if almanak.Element.objects.filter(pk=rel).exists():
                    new_relation = almanak.DescriptionElementRelation(
                        description = self.description,
                        element = almanak.Element.objects.get(pk=rel)
                    )
                    new_relation.save()

    def remove_obsolete_relations(self):
        print(self.relations_content)

        for rel in self.description.element_relations.get_queryset():
            if rel.element.id not in self.relations_content:
                print("removing obsolete relation " + str(rel.element.id))
                rel.delete()

    def sync_relations(self):
        
        self.add_new_relations()
        self.remove_obsolete_relations()

        #print([des_rel.element.id for des_rel in self.description.element_relations.get_queryset()])





