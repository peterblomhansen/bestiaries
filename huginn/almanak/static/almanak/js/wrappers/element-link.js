var elementLink = Vue.component('element-link', {
    template: "\
        <div style=\"display:inline-block\" v-if=\"!element\" >\
            No element found\
        </div>\
        <div style=\"display:inline-block\" v-else >\
            <span class=\"popup-element ink blot\" @click=\"popup = !popup;popupGlobal = !popupGlobal\" style=\"cursor:default\" >{{ element.present_name.name }}</span>\
            <transition name=\"fade\" >\
                <div v-if=\"isVisible\" class=\"paper__sheet border--padding\" >\
                    <div class=\"border--content\" >\
                        <h3 class=\"ink\" ><a :href=\"'/almanak/element/' + element.id\" target=\"_blank\" >{{ element.present_name.name }}</a></h3>\
                        <p class=\"rubrik\" ><strong>{{ secretPublic }} {{ elementType }}</strong></p>\
                        <p v-if=\"element.campaigns_full\" class=\"rubrik\" ><strong>in <span v-html=\"campaignsString\" ></span></strong></p>\
                    </div>\
                </div>\
            </transition>\
        </div>\
    ",
	props: {
        element: Object,
        linkText: String,
    },
    data: function() {
        return {
            popup: false,
        }
    },
    computed: {
        text: function(){
            return this.linkText ? this.linkText : element.present_name.name;
        },
        popupGlobal: {
            get: function (){
                return this.$store.state.generic.ui.popupGlobal;
            },
            set: function (value) {
                this.$store.state.generic.ui.popupGlobal = value;
            }, 
        },
        isVisible: function (){
            if(this.popup && this.popupGlobal) {
                return true;
            }else {
                this.popup = false;
                return false;
            }
        },
        elementType: function (){
            console.log(this.element.type_grund);
            return this.$store.getters.elementTypeTranslated(this.element.type_spec);
        },
        secretPublic: function(){
            return this.element.secret === false ? "Public" : "Secret";
        },
        campaignsString: function (){
            var campaignsString = ""
            var self = this;
            _.each(this.element.campaigns_full,function(cmp,i){
                campaignsString += "<a class=\"ink\" href=\"/almanak/element/" + cmp.campaign + "\">" + cmp.campaign_full.present_name.name + "</a> ";
                self.element.campaigns_full.length > i + 1 ? campaignsString += "<br/>and ": ""; 
            });
            return campaignsString;
        },


    }
});
