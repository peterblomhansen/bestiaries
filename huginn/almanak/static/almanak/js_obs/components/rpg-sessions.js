//             {% include "almanak/vue/create-new-element.html" %}

var rpgSessions = Vue.component('rpg-sessions', {
	template:"\
	<div>\
       <h4 class=\"rubrik\">\
         Roleplaying Sessions\
         <strong class=\"rubrik\" style=\"cursor:pointer;\" @click=\"editRpgSessions = !editRpgSessions\" >+</strong>\
       </h4>\
       <div v-if=\"editRpgSessions == true\">\
\
         <button @click=\"createDialog = !createDialog\">Create new Roleplaying Session </button>\
\
         <create-new-element v-if=\"dataLoaded == true && createDialog == true\" :show=\"createDialog\" :element=\"element\" type-spec=\"rpg_session\" change-type=\"false\" >\
         </create-new-element>\
\
       </div>\
	   <p v-for=\"s in sessions\" ><span v-if=\"s.offgame_start != null\" class=\"rubrik\" > {{ s.offgame_start.split('T')[0] }}</span><span class=\"rubrik\" v-else >No offgame time</span> - <strong>{{ s.session_number }}.</strong> <a class=\"ink\" :href=\"'/almanak/side/' + s.id\">{{ s.present_name }}</a> </p>\
       <div v-for=\"s in element.elements_below\" v-if=\"s.deleted == false && s.below.type_spec == 'rpg_session'\">\
\
         <div class=\"rubrik\">\
           [[s.below.session_number]]\
           <a class=\"ink\" :href=\"'/almanak/side/' + s.id\">[[s.below.present_name]]</a>\
           <strong v-if=\"isEditor\" style=\"cursor:pointer;\" class=\"rubrik\" @click=\"s.delete_question = !s.delete_question\" >&times;</strong>\
           <p v-if=\"s.delete_question == true\">\
               Do you want to remove\
               <strong>[[ s.present_name ]]</strong> as a Roleplaying Session\
               <button @click=\"deleteSessionCampaignRelation(s)\">Yes</button>\
               <button @click=\"s.delete_question = !s.delete_question\" >No</button>\
           </p>\
         </div>\
       </div>\
       <!-- <pre>[[ sessionsRev ]]</pre> -->\
     </div>\
	\
	",
    props: {
        // sessions: Array,
        // element: Object,
        // isEditor: Boolean,
        // st: String,
        // dataLoaded: Boolean,
    },
    data: function() {
        return {
            editRpgSessions: false,
            createDialog: false,
        }
    },
    beforeDestroy: function() {
        eventHub.$off('hideCreateElementDialog', this.closeCreateElementDialog);
    },
    mounted: function() {
        eventHub.$on('hideCreateElementDialog', this.closeCreateElementDialog);
    },
    methods: {
        closeCreateElementDialog: function() {
            this.createDialog = false
        },
        deleteSessionCampaignRelation: function(el) {
            if (el.id > 0) {
                console.log("chosen", el.id)
                var self = this;
                var i = _.findIndex(self.element.sessions, function(o) {
                    return o.id == el.id;
                });
                console.log("index", i);
                this.element.sessions.splice(i, 1);
                this.updateCampaignRelationAjax();
            }
        },
        updateCampaignRelationAjax: function() {
            var self = this;
            axios.patch('/api/campaign/' + self.element.id + '/update/', {
                sessions: this.sessionIds
            }).then(function(response) {
                console.log(response)
                eventHub.$emit('reloadData');
            }).catch(function(error) {
                console.log(error);
            });
        },
        timestampToValues: function(ts) {
            var datevalues = false;
            if (ts > 0) {
                date = new Date(ts * 1000), datevalues = {
                    year: date.getFullYear(),
                    month: date.getMonth() + 1,
                    date: date.getDate(),
                    hour: date.getHours(),
                    minute: date.getMinutes(),
                    second: date.getSeconds(),
                };
            }
            return datevalues;
        },
        timeToHuman: function(ts) {
            var dateString = "Not set";
			console.log(ts)
            var to = this.timestampToValues(ts);
			console.log(to)
            if (to != false) {
                var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                var mIndex = parseInt(to.month) - 1;
                var ds = to.date + "th";
                if (to.date == 1) {
                    ds = to.date + "st";
                }
                if (to.date == 2) {
                    ds = to.date + "nd";
                }
                if (to.date == 3) {
                    ds = to.date + "rd";
                }
                dateString = monthNames[mIndex] + " " + ds + " " + " " + to.year;
            }
            return dateString;
        }
    },
    computed: {
		sessions: function (){
			if(this.$store.state.element.masterdata.sessions) {
				return _.sortBy(this.$store.state.element.masterdata.sessions, [function(o) { return o.session_number; }]).reverse();
			}
		},
		element: function (){
			return this.$store.state.element.masterdata;
		},
        sessionsIds: function() {
            var ids = []
            _.each(this.element.sessions, function(c) {
                if (c) {
                    ids.push(c.id);
                }
            });
            return ids;
        }
    }
});
