class ParamsManager():
	
	def find_sub_params(params,identifier):
		identifier += "_"
		sub_params = []

		for param in params:	
			if param.find(identifier) == 0:
				sub_param = param[len(identifier):]
				sub_params.append(sub_param)

		return sub_params
		
	def params_split(params):
		params_array = params.split("__")
		return params_array
