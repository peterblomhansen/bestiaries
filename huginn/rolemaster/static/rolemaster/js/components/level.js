var level = Vue.component('level', {
	template: "\
	<div style=\"text-align:right;flex:1;\">\
		<p>Level: {{ level.current }} <button v-if=\"userIsGM\" @click=\"showEditLevel = !showEditLevel\"> edit level</button></p>\
		<div v-if=\"showEditLevel == true\" >Change Level <input type=\"number\" v-model=\"levelNew\"  /><button @click=\"updateLevel()\" >Submit New Level</button></div>\
		<p>\
			View Level: \
			<select v-model=\"level.viewed\">\
				<option v-for=\"lvl in levelsAll\">{{ lvl  }}</option>\
			</select>\
		</p>\
	</div>\
	",
    data: function() {
		return {
			showEditLevel: false,
			levelNew: null,
		}
    },
	mounted: function (){
		this.levelNew = this.level.current;
	},
    methods: {
		updateLevel: function() {

			console.log(this.levelNew);
			axios.patch("/rm/api/rm_character/" + this.character.id + "/update/",{ level: this.levelNew }).then(function(response){
				window.location.reload();
			});

		},

	},
	computed: {
		userIsGM: function() {
			if(this.$store.state.character.campaigns) {
				console.log(campaignId);
				return this.$store.state.character.campaigns.indexOf(parseInt(campaignId)) != -1 && roleId == 0;
			}
		},
		character: function () {
			return this.$store.state.character
		},
		level: function(){
			return this.$store.state.initial.level;
		},
		levelsAll: function (){
			var levelArray = [],
			n = 0

			while (n <= this.level.current) {
			  levelArray.push(n)
			  n++;
			}

			return levelArray.reverse();

		},

	}
});
