var elementNames = Vue.component('element-names', {
    template: "\
    <div >\
      <h1 class='ink' >{{ $store.state.element.masterdata.present_name }} <img v-if='$store.state.element.masterdata.user_permissions.write' :src='static + \"images/icons/pencil.svg\"' style='cursor:pointer' @click='edit = !edit' /></h1>\
      <p class='rubrik' v-if='names && namesNotDeleted.length > 1'>Also known as:\
        <span v-for='(n,i) in namesNotDeleted' v-if='n.id != nameChosenId && n.deleted == false'>\
          <a class='ink' :href=\"'/almanak/side/' + n.element\">{{ n.name }}</a><span v-if='i < names.length - 1'>,&nbsp;</span>\
        </span>\
      </p>\
\
      <div v-if='edit == true'>\
        <h3 class='rubrik'>Change names</h3>\
        <div>\
          <p >\
            <img :src='static + \"images/icons/gears.svg\"'' style='opacity:0.2;height:1em;max-width:100%;max-height:100%;' />\
            <input class='ink' v-model='newName' placeholder='Add new name' />\
            <button @click='createNewName()'>Add</button></p>\
        </div>\
        <div v-for='(n,i) in names' v-if='n.deleted == false'>\
          <p>\
            <img :src='static + \"images/icons/gears.svg\"''style='cursor:pointer;height:14px;max-width:100%;max-height:100%;' @click='n.show_options = !n.show_options' /> <input @keyup='updateName(n,i)' class='ink' v-model='n.name' />\
            <span @click='n.delete_question = !n.delete_question' style='cursor:pointer;' class='rubrik'>&times;</span>\
            <span  class='ajax__update-text'>{{ n.ajax_status }}</span>\
          </p>\
          <p v-if='n.delete_question == true'>Do you want to delete the name: <strong>{{ n.name }}</strong> <button @click='deleteName(n,i)'>Yes</button>  <button @click='n.delete_question = !n.delete_question' >No</button></p>\
          <div v-if='n.show_options == true' style='border:1px solid lightgray;padding:0.3em;' @change='updateName(n,i)'>\
            <p class='rubrik'><strong>Options</strong></p>\
            <p class='rubrik'><input type='checkbox' v-model='n.is_primary' /> Important name</p>\
            <p>Start:\
              date: <input class='input--narrow' v-model.number='n.start_date' type='number' />\
              month:\
              <select select='n.start_month'>\
                <option value='0'>-</option>\
                <option v-for='m in months.options' :value='m.value' >{{ m.text }}</option>\
              </select>\
              year: <input class='input--narrow' v-model.number='n.start_year' type='number' />\
              age:\
              <select v-model='n.start_age'>\
                <option value='null'>-</option>\
                <option v-for='o in ages' :value='o.value'>{{ o.name }}</option>\
              </select>\
            </p>\
\
            <p>End:\
              date: <input class='input--narrow' v-model.number='n.end_date' type='number' />\
              month:\
              <select v-model='n.end_month'>\
                <option value='0'>-</option>\
                <option v-for='m in months.options' :value='m.value'>{{ m.text }}</option>\
              </select>\
              year: <input class='input--narrow' v-model.number='n.end_year' type='number' />\
              age:\
              <select v-model='n.end_age'>\
                <option value='null'>-</option>\
                <option v-for='o in ages' :value='o.value'>{{ o.name }}</option>\
              </select>\
            </p>\
                <span style='color:red;'>AJAX GÅR IKKE IGENNEM: AGE bliver ikke sendt rigigt</span>\
\
          </div>\
\
        </div>\
\
      </div>\
\
    </div>\
    ",
    props: {
        //elementId: Number,
        //names: Array,
        nameChosenId: Number,
        st: String,
        userId: Number,
        //userIsEditor: Boolean,
        //ages: Object,
    },
    data: function() {
        return {
            namesOrigin: [],
            newName: "",
            edit: false,
            months: {
                select: "",
                options: [{
                    text: "jan",
                    value: 1
                }, {
                    text: "feb",
                    value: 2
                }, {
                    text: "mar",
                    value: 3
                }, {
                    text: "apr",
                    value: 4
                }, {
                    text: "may",
                    value: 5
                }, {
                    text: "jun",
                    value: 6
                }, {
                    text: "jul",
                    value: 7
                }, {
                    text: "aug",
                    value: 8
                }, {
                    text: "sep",
                    value: 9
                }, {
                    text: "oct",
                    value: 10
                }, {
                    text: "nov",
                    value: 11
                }, {
                    text: "dec",
                    value: 12
                }, ]
            },
        }
    },
    methods: {
        createNewName: function() {
            var self = this;
            axios.post('/api/name/create/', {
                name: self.newName,
                name_type: "prim",
                deleted: false,
                secret: false,
                element: self.elementId,
                author: self.userId,
            }).then(function(response) {
                console.log(response.data)
                self.newName = "";
                self.$store.commit("getElement");

            }).catch(function(error) {
                console.log(error);
                if (error.response.status == 403) {}
            });
        },
        deleteName: function(n, i) {
            var self = this;
            self.names[i].ajax_status = "deleting";
            axios.patch('/api/name/' + n.id + '/update', {
                deleted: true
            }).then(function(response) {
                console.log(response.data)
                self.$store.commit("getElement");
            }).catch(function(error) {
                console.log(error);
                if (error.response.status == 403) {
                    self.names[i].ajax_status = "permission denied: no changes was written to the Bestiary";
                }
            });
        },
        updateName: function(n, i) {
			var self = this;
            n.ajax_status = 'updating';
			window.clearTimeout(this.timer);

			this.timer = window.setTimeout(function(){
	            self.updateNameAjax(n, i);
			},1000);
        },
        updateNameAjax: function(n, i) {
            var self = this;
            var nameTypeComp = "prim"
            if (n.is_primary == false) {
                nameTypeComp = "sec"
            }
            var start_age = null
            var end_age = null
            if (n.start_age == parseInt(n.start_age, 10)) {
                start_age = n.start_age;
            }
            if (n.end_age == parseInt(n.end_age, 10)) {
                end_age = n.end_age;
            }

            axios.patch('/api/name/' + n.id + '/update', {
                name: n.name,
                name_type: nameTypeComp,
                start_age: start_age,
                start_year: n.start_year,
                start_month: n.start_month,
                start_date: n.start_date,
                start_hours: n.start_hours,
                start_minutes: n.start_minutes,
                start_seconds: n.start_seconds,
                end_age: end_age,
                end_year: n.end_year,
                end_month: n.end_month,
                end_date: n.end_date,
                end_hours: n.end_hours,
                end_minutes: n.end_minutes,
                end_seconds: n.end_seconds,
            }).then(function(response) {
                console.log(response.data)
				self.names[i].ajax_status = "updated";
                //self.names[i].ajax_status = "updated";
                //self.$store.commit("getElement");

            }).catch(function(error) {
                console.log(error);
                if (error.response.status == 403) {
                    self.names[i].ajax_status = "permission denied: no changes was written to the Bestiary";
                }
            });
        },
    },
    computed: {
        static: function() {
            return this.$store.state.initial.staticUrl;
        },
        elementId: function(){
            return elementId;
        },
        names: function() {
            return this.$store.state.element.masterdata.names;
        },
        userIsEditor: function() {
            return this.$store.state.element.masterdata.user_permissions.write;

        },
        ages: function (){
            return this.$store.state.session.universe.ages;

        },
		masterdata: function (){
			return this.$store.state.element.masterdata;
		},
        // chosenName: function() {
        //     var self = this;
        //     var cn = {};
        //     _.each(this.names, function(d) {
        //         console.log(d.id);
        //         console.log(self.nameChosenId);
        //         if (parseInt(d.id) == parseInt(self.nameChosenId))
        //             cn = d;
        //     });
        //     return cn;
        // },
        namesNotDeleted: function() {
            var names = this.names.filter(function(el) {
                return el.deleted == false;
            });
            return names;
        }
    }
});
