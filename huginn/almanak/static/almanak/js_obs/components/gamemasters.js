var gamemasters = Vue.component('gamemasters', {
	template: "\
		<div>\
	      <h4 class=\"rubrik\">Gamemasters:\
	        <img v-if=\"element.user_permissions.write == true\" :src=\"st + 'images/icons/pencil.svg'\" style=\"cursor:pointer;height:1em;\" @click=\"loadUsersInUniverse();editGamemasters = !editGamemasters\" />\
	      </h4>\
	      <div v-if=\"editGamemasters == true\">\
	        <select @change=\"addGamemaster(userToBeGamemaster)\" v-model=\"userToBeGamemaster\">\
	          <option value=\"-1\">Select user</option>\
	          <option v-if=\"alreadyGm(u.id) == -1\" v-for=\"(u,i) in users\" :value=\"i\">\
	            {{ u.first_name }} {{ u.last_name }}\
	          </option>\
			  \
	        </select>\
			\
	      </div>\
		  <p class=\"ink\" v-for=\"(gm,i) in gamemasters\">{{ gm.first_name }} {{ gm.last_name }} <strong style=\"cursor:pointer;\" class=\"rubrik\" @click=\"removeGamemaster(gm)\">&times;</strong></p>\
	    </div>\
	\
	",
    props: {
        // element: Object,
        // gamemasters: Array,
        // users: Array,
        // isEditor: Boolean,
        // st: String,
    },
    data: function() {
        return {
            editGamemasters: false,
            userToBeGamemaster: -1,
        }
    },
    methods: {
		alreadyGm: function (userId) {

			return this.gamemasters.findIndex(function(gm){ return gm.id == userId } );
		},
		loadUsersInUniverse: function() {
			var self = this;
			if(campaignId){
				axios.get("/api/session/" + campaignId + "/?d=us1_ages__cs1_universe__us1_name__us1_users&format=json").then(function(response) {
					self.$store.state.session = response.data;
					self.$store.state.element.loaded.sessiondata = true;
					self.$store.state.initial.sessionLoaded = true
				}).catch(function(error) {
					console.log(error);
				});
			}
		},
        addGamemaster: function(i) {
            var self = this;
            if (i > -1) {
                self.element.gamemasters.push(this.users[i]);
                this.updateGamemastersAjax();
            }
        },
        removeGamemaster: function(c) {
            console.log("chosen", c)
            var self = this;
            var i = _.findIndex(self.element.gamemasters, function(o) {
                return o.id == c.id;
            });
            console.log("index", i);
            this.element.gamemasters.splice(i, 1);
            this.updateGamemastersAjax();
        },
        updateGamemastersAjax: function() {
            var self = this;
			var gmIds = this.element.gamemasters.map(function(gm) {	console.log(gm) ;return gm.id });
			console.log(gmIds);
            axios.patch('/api/rpg_element/' + this.element.id + '/update/', {
                gamemasters: gmIds
            }).then(function(response) {
                console.log(response)
                eventHub.$emit('reloadData');
            }).catch(function(error) {
                console.log(error);
            });

        },
    },
    computed: {
		element: function(){
			return this.$store.state.element.masterdata;
		},
		gamemasters: function () {
			return this.element.gamemasters;
		},
		st: function (){
			return this.$store.state.initial.staticUrl;
		},
		users: function (){
			return this.$store.state.session.universe.users;
		},
		// element: Object,
        // gamemasters: Array,
        // users: Array,
        // isEditor: Boolean,
        // st: String,


        gamemastersIds: function() {
			console.log(this.element.gamemasters.length);
			return this.element.gamemasters.map(function(gm) { return gm.id });
        },//     // var ids = []
        //     // _.each(this.element.gamemasters, function(gm) {
        //     //     if (gm) {
        //     //         ids.push(gm.id);
        //     //     }
        //     // });
        //     // return ids;
        // },
    }
});
