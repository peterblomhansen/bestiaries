from rest_framework.views import APIView
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK,HTTP_400_BAD_REQUEST, HTTP_403_FORBIDDEN, HTTP_202_ACCEPTED
from django.contrib.auth import authenticate, login
from almanak.authentication import BestiariesAuthentication
from almanak.core.time import DateHelper, TimePoint, Interval
from almanak.core.objectifier import SessionHelper
from .filters import (
	UserFilter, 
	ElementRelationFilter, 
	NameFilter, 
	DescriptionFilter, 
	DescriptionElementRelationFilter, 
	EntityEventRelationFilter,
	EventPlaceRelationFilter, 
	ElementItemRelationFilter,
	ElementUniqueItemRelationFilter,
	IndividualFilter,
	ElementFilter,
	RolePlayingSessionFilter
)
from almanak.models import Age, Universe, Description
from django_filters.rest_framework import DjangoFilterBackend
from django_filters import rest_framework as rest_filters
from rest_framework import filters
from django.http import HttpRequest, Http404
from django.shortcuts import get_object_or_404
from django.db.models import Q
import re
from itertools import chain
import operator
import ast
from .pagination import TenPerPagePagination, TwentyPerPagePagination, AlmanakCursorPagination
from . import permissions, write_serializers
from .params import ParamsManager
import sys


# Viewset test
from rest_framework import viewsets
from rest_framework.permissions import (
    IsAuthenticated,
)

from almanak import models as almanak
import almanak.api.serializers as serializers
from django.core import serializers as core_serializers
import almanak.services as services
'''
###############################
##### FOUNDATIONAL VIEWS ######
### Creates a valid session ###
###############################
'''

class GoIngameView(APIView):
	'''
	GoIngameView: posts and validates session to create a functioning session for the authenticated user
	A functioning session includes a Universe ID, A campaign ID and a Role ID (if Id is 0 the user's role is Gamemaster)
	Without a functioning session, foundational data retrieval is not possible.
	'''

	permission_classes = [IsAuthenticated]

	def post(self,request, *args,**kwargs):
		ses = request.session
		user_id = request.user.id
		universe_id = request.data['universe']
		campaign_id = request.data['campaign']
		role_id = request.data['role']

		# Validates the user_id against membership of the universe_id and whether the user has a corresponding
		# role in a campaign identified with campaign_id. 
		ingame_validated = BestiariesAuthentication.go_ingame(self,request,user_id,universe_id,campaign_id,role_id)

		if ingame_validated == True:
			# Fetches and agregates data for Ingame Session
			u = almanak.Universe.objects.get(pk=universe_id)
			c = almanak.Campaign.objects.get(pk=campaign_id)
			u_mem = almanak.UniverseMembership.objects.get(universe__id=universe_id,participant__id=request.user.id)
			gm_for = []
			for el in request.user.gamemaster_for.get_queryset():
				if el.universe.id == universe_id:
					gm_for.append(el.id)

			# Serializes and populates the properties and writes it to the Django session
			ses['user_current'] = user_id
			ses['universe_current'] = universe_id
			ses['campaign_current'] = campaign_id
			ses['role_current'] = role_id
			ses['universe_name'] = u.name
			ses['user_is_universe_owner'] = u_mem.universe_owner
			ses['gamemaster_for'] = gm_for

			if role_id > 0:
				serializer = serializers.IndividualSerializer(almanak.Individual.objects.get(pk=role_id), context={'request': request}, params="present_name")
				ses['player_character'] = serializer.data

			serializer = serializers.BestiariesUserSerializer(request.user,context={'request': request}, params="es1_present_name")
			ses['user_details'] = serializer.data
			serializer = serializers.UniverseSerializer(u,context={'request': request},params=["ages","owners","users","name"])
			ses['universe'] = serializer.data
			serializer = serializers.CampaignSerializer(c,context={'request': request},params=["universe_full","present_name","author"])
			ses["campaign_full"] = serializer.data
			ses["element_search_scope"] = "universe"
			ses.save()

			return Response("is_ingame", status=HTTP_200_OK)

		return Response("User did not go ingame", status=HTTP_403_FORBIDDEN)



### Session View  ###

class SessionView(APIView):

	permission_classes = (IsAuthenticated,)

	def get(self, request, format="json"):
		session = self.request.session

		return Response(session)

	def post(self, request, format="json"):
		ses = request.session
		if request.data.get("element_search_scope"):
			ses["element_search_scope"] = request.data.get("element_search_scope")
		ses.save()
		return Response(request.data)


### 

class SyncDescriptionLinksView(APIView):

	def post(self, request, pk, format="json"):

		ids_content_db = services.get_element_ids_from_links(Description.objects.get(pk=pk).content)
		ids_content_request = services.get_element_ids_from_links(request.data.get("content"))
		is_content_equal = ids_content_db == ids_content_request
		print(ids_content_db)
		print(ids_content_request)

		if is_content_equal == False:
			services.sync_description_element_relations(pk,ids_content_request,ids_content_db)

			return Response("links synced",status=HTTP_202_ACCEPTED)


		return Response("links already in sync", status=HTTP_200_OK)


#### VIEWSETS ####

class ParamsViewSet(viewsets.ModelViewSet):
	'''

	'''
	queryset = almanak.Element.objects.filter(deleted=False)

	def str_to_class(self,classname):
		return getattr(sys.modules[__name__], classname)

	def get_serializer(self, instance=None, data=None, many=False, partial=False):
		params = ParamsManager.params_split(self.request.GET.get("d",""))
		return super(ParamsViewSet,self).get_serializer(instance, many=many, partial=partial,params=params)

	def create(self,request):
		serializer = self.write_serializer_class(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		return Response(serializer.errors)

	def update(self,request, pk=None):
		serializer = self.write_serializer_class(data=request.data)
		if serializer.is_valid():
			serializer.update()
			return Response(serializer.data)
		return Response(serializer.errors)

	def partial_update(self, request, *args, **kwargs):
		instance = self.queryset.get(pk=kwargs.get('pk'))
		self.check_object_permissions(request, instance)		
		serializer = self.write_serializer_class(instance, data=request.data, partial=True)
		serializer.is_valid(raise_exception=True)
		serializer.save()
		return Response(serializer.data)


class NameViewSet(ParamsViewSet):
	queryset = almanak.Name.objects.select_related("element","element__universe","author","start_age","end_age").filter(deleted=False)
	serializer_class = serializers.NameSerializer
	write_serializer_class = write_serializers.NameWriteSerializer
	pagination_class = TenPerPagePagination
	permission_classes = (IsAuthenticated, permissions.NameChangePermission)
	filterset_class = NameFilter
	filter_backends = (DjangoFilterBackend,filters.SearchFilter)
	search_fields = ('name','element__type_spec')

	def get_queryset(self):
		user = self.request.user
		type = self.request.GET.get("type")
		c_universe = self.request.session.get("universe_current")
		name_query = almanak.Name.objects.prefetch_related(
								"element__campaigns",
								"shared_with",
								"element__campaigns__gamemasters"
								).select_related(
								"element","element__universe",
								"author",
								"start_age",
								"end_age"	
								).filter(Q(element__universe__id=c_universe, deleted=False, element__deleted=False),
									Q(secret=False)|
									Q(secret=True,author=user)|
									Q(secret=True,shared_with__in=[user.id])|
									Q(secret=True,element__campaigns__gamemasters__in=[user.id])|
									Q(secret=True,element__id__in=user.player_characters.get_queryset())
									).distinct()
		if type:
			if type == "individual__player_character":
				return name_query.filter(Q(element__type_grund="individ")|Q(element__type_grund="player_character"))
			else:
				return name_query.filter(element__type_grund=type)
		else:
			return name_query




class ElementViewSet(ParamsViewSet):
	queryset = almanak.Element.objects.filter(deleted=False)	
	permission_classes = (IsAuthenticated,permissions.ElementPermission)
	serializer_class = serializers.ElementSerializer
	pagination_class = TenPerPagePagination
	filter_backends = (DjangoFilterBackend,filters.OrderingFilter,)
	filterset_class = ElementFilter
	filter_fields = ("author__id",)
	ordering_fields = "__all__"
	model = almanak.Element

	def get_queryset(self):
		return self.model.objects.select_related(
											"universe",
											"author"
										).prefetch_related(
											"campaigns",
											"campaigns_containing",
											"editors",
											"shared_with",
											"names"
										).filter(Q(universe__id=self.request.session.get("universe_current"), deleted=False),
											Q(secret=False)|
											Q(secret=True,author=self.request.user)|
											Q(secret=True,shared_with__in=[self.request.user.id])|
											Q(secret=True,campaigns__gamemasters__in=[self.request.user.id])|
											Q(secret=True,id__in=self.request.user.player_characters.get_queryset())
										).distinct()


class IndividualViewSet(ElementViewSet):
	model = almanak.Individual
	serializer_class = serializers.IndividualSerializer	
	write_serializer_class = write_serializers.IndividualWriteSerializer		
	pagination_class = TenPerPagePagination
	filter_backends = (DjangoFilterBackend,)
	filterset_class = IndividualFilter
	
	def get_queryset(self):
		return self.model.objects.prefetch_related(
				"names",
				"element_relations_above",
				"element_relations_below",
				"element_relations_below__above",
				"element_relations_above__below",
				"element_relations_above__below__element_relations_above",
				"campaigns_containing",
				"campaigns_containing__campaign__gamemasters",
			).select_related(
				"universe",
				"author"			
			).filter(
				deleted=False,
				universe__id=self.request.session.get("universe_current"),
			)

class PlayerCharacterViewSet(IndividualViewSet):
	queryset = almanak.PlayerCharacter.objects.filter(deleted=False)	
	permission_classes = (IsAuthenticated,permissions.ElementPermission)
	model = almanak.PlayerCharacter
	serializer_class = serializers.PlayerCharacterSerializer
	filter_backends = (DjangoFilterBackend,)
	filterset_class = IndividualFilter

	def get_queryset(self):
		return self.model.objects.prefetch_related(
				"names",
				"element_relations_above",
				"element_relations_below",
				"element_relations_below__above",
				"element_relations_above__below",
				"element_relations_above__below__element_relations_above",
				"campaigns_containing",
				"campaigns_containing__campaign__gamemasters",
			).select_related(
				"universe",
				"author",
				"roleplayer"			
			).filter(
				deleted=False,
				universe__id=self.request.session.get("universe_current"),
			)

class GroupViewSet(ElementViewSet):
	model = almanak.Group
	serializer_class = serializers.GroupSerializer
	write_serializer_class = write_serializers.GroupWriteSerializer

	

class RaceViewSet(GroupViewSet):
	model = almanak.Group
	serializer_class = serializers.GroupSerializer


class EventViewSet(ElementViewSet):
	model = almanak.Event
	serializer_class = serializers.EventSerializer	
	write_serializer_class = write_serializers.EventWriteSerializer


class RpgSessionViewSet(EventViewSet):
	queryset = almanak.RpgSession.objects.filter(deleted=False)	
	model = almanak.RpgSession
	serializer_class = serializers.RpgSessionSerializer
	write_serializer_class = write_serializers.RpgSessionWriteSerializer
	filterset_class = RolePlayingSessionFilter
	filter_backends = (DjangoFilterBackend,filters.OrderingFilter,)	
	ordering_fields = "__all__"

	def get_queryset(self):
		return self.model.objects.prefetch_related(
				"names",
				"participants",
				"participants__entity",
				"participants__entity__names"
			).select_related(
				"universe",
				"author",
				"in_campaign"			
			).filter(
				deleted=False,
				universe__id=self.request.session.get("universe_current"),
			)
		

class CampaignViewSet(EventViewSet):
	queryset = almanak.Campaign.objects.filter(deleted=False)
	model = almanak.Campaign
	permission_classes = (IsAuthenticated,permissions.ElementPermission)
	serializer_class = serializers.CampaignSerializer
	pagination_class = TwentyPerPagePagination
	filter_backends = (DjangoFilterBackend,filters.OrderingFilter,)
	ordering_fields = "__all__"

	def get_queryset(self):
		return almanak.Campaign.objects.select_related(
											"universe",
											"author",
											"ingame_now_age"
										).prefetch_related(
											"editors",
											"gamemasters"
										).filter(
											deleted=False,
											universe__id=self.request.session.get("universe_current")
										)

class PlaceViewSet(ElementViewSet):
	model = almanak.Place
	serializer_class = serializers.PlaceSerializer
	write_serializer_class = write_serializers.PlaceWriteSerializer


class ItemViewSet(ElementViewSet):
	model = almanak.Item
	serializer_class = serializers.ItemSerializer
	write_serializer_class = write_serializers.ItemWriteSerializer

class UserViewSet(ParamsViewSet):
	queryset = almanak.BestiariesUser.objects.prefetch_related("authored_elements","authored_descriptions","authored_images","universes","universes__universe","gamemaster_for","player_characters","gamemaster_for__names","player_characters__names").all()
	serializer_class = serializers.BestiariesUserSerializer
	#pagination_class = TenPerPagePagination
	filter_backends = (DjangoFilterBackend,)
	filterset_class = UserFilter


class DescriptionViewSet(ParamsViewSet):
	queryset = almanak.Description.objects.select_related(
										"universe",
										"author",
										"ingame_author",										
									).prefetch_related(
										"ingame_author__names",
										"element_relations",
										"element_relations__element",
										"element_relations__element__names",
										"shared_with",
										"campaigns",
										"campaigns_containing",
									).filter(deleted=False)
	permission_classes = (IsAuthenticated,permissions.DescriptionPermission)
	serializer_class = serializers.DescriptionSerializer
	filterset_class = DescriptionFilter
	pagination_class = TenPerPagePagination
	write_serializer_class = write_serializers.DescriptionWriteSerializer


	def get_queryset(self):
		user = self.request.user
		q = almanak.Description.objects.select_related(
										"universe",
										"author",
										"ingame_author",										
									).prefetch_related(
										"element_relations",
										"element_relations__element",
										"element_relations__element__names",
										"shared_with",
										"campaigns",
										"campaigns_containing",
									).filter(
										Q(universe__id=self.request.session.get("universe_current"),
										deleted=False)
									).distinct()
		print("universe id")							
		print("from session",self.request.session.get("universe_current"))

		if self.request.session.get("user_is_universe_owner") == False:
			q = q.filter(
					Q(secret=False)|
					Q(secret=True,author=user)|
					Q(secret=True,shared_with__in=[user.id])|
					Q(secret=True,campaigns__gamemasters__in=[user.id])
			)

		return q

class DescriptionElementRelationViewSet(ParamsViewSet):
	queryset = almanak.DescriptionElementRelation.objects.all()
	serializer_class = serializers.DescriptionElementRelationSerializer
	pagination_class = TenPerPagePagination
	filterset_class = DescriptionElementRelationFilter
	filter_backends = (DjangoFilterBackend,filters.OrderingFilter,filters.SearchFilter,)
	ordering_fields = ("relevance","created","above_title")
	search_fields = ('$description__content','$description__title')

	def get_queryset(self):
		user = self.request.user
		q = almanak.DescriptionElementRelation.objects.filter(
										Q(description__universe__id=self.request.session.get("universe_current"),
										element__universe__id=self.request.session.get("universe_current"),
										description__deleted=False)
									).distinct(
									).select_related(
										"description",
										"description__universe",
										"description__author",
										"description__ingame_author",
										"element"
									).prefetch_related(
										"description__shared_with",
										"element__names",
										"element__campaigns",	
										"element__campaigns_containing",
										"description__campaigns",
										"description__campaigns_containing"
									)
		if self.request.session.get("user_is_universe_owner") == False:
			q = q.filter(Q(element__names__secret=False)|
					Q(element__names__secret=True,element__names__author=user)|
					Q(element__names__secret=True,element__names__shared_with__in=[user.id])|
					Q(element__names__secret=True,element__names__element__campaigns__gamemasters__in=[user.id])|
					Q(element__names__secret=True,element__names__element__id__in=user.player_characters.get_queryset())
			)
			q = q.filter(
					Q(description__secret=False)|
					Q(description__secret=True,description__author=user)|
					Q(description__secret=True,description__shared_with__in=[user.id])|
					Q(description__secret=True,description__campaigns__gamemasters__in=[user.id])
				
			)

		return q


class ElementRelationViewSet(ParamsViewSet):
	permission_classes = (IsAuthenticated,permissions.ElementRelationPermission)
	queryset = almanak.ElementRelation.objects.all()
	serializer_class = serializers.ElementRelationSerializer
	pagination_class = TenPerPagePagination
	filter_backends = (DjangoFilterBackend,)
	filterset_class = ElementRelationFilter
	write_serializer_class = write_serializers.ElementRelationWriteSerializer

	def get_queryset(self):
		return almanak.ElementRelation.objects.prefetch_related(
											"above__names",
											"below__names"
										).select_related(
											"above",
											"below",
											"above__universe",
											"below__universe",
											"start_age",
											"end_age",
											"above_title",
											"below_title"
										).filter(
											deleted=False,
											above__deleted=False,
											below__deleted=False,
											above__universe__id=self.request.session.get("universe_current"),
											below__universe__id=self.request.session.get("universe_current"),
										)

class EntityEventRelationViewSet(ParamsViewSet):
	permission_classes = (IsAuthenticated,permissions.EntityEventRelationPermission)
	queryset = almanak.EntityEventRelation.objects.all()
	serializer_class = serializers.EntityEventRelationSerializer
	pagination_class = TenPerPagePagination
	filter_backends = (DjangoFilterBackend,)
	filterset_class = EntityEventRelationFilter
	ordering_fields = ( "event__birth_age",
						"event__birth_year",
						"event__birth_month",
						"event__birth_day",
						"event__birth_hour",
						"event__birth_minute",
						"event__birth_second",)


	def get_queryset(self):
		user = self.request.user
		params = self.request.GET.get("d","")

		q = almanak.EntityEventRelation.objects.prefetch_related(
											"event__names",
											"entity__names",
										).select_related(
											"event",
											"entity",
											"event__universe",
											"entity__universe",
											"start_age",
											"end_age",
										).filter(
											Q(deleted=False,
											event__deleted=False,
											entity__deleted=False,
											event__universe__id=self.request.session.get("universe_current"),
											entity__universe__id=self.request.session.get("universe_current")),										
										).order_by("-event__birth_age__age_number","-event__birth_year", "-event__birth_month","-event__birth_date","-event__birth_hour","-event__birth_minute","-event__birth_second",)		

		# Name permission filter for events 
		if "event" in params:
			q = q.filter(
					Q(event__names__deleted=False),
					Q(event__names__secret=False)|
					Q(event__names__secret=True,event__names__author=self.request.user)|
					Q(event__names__secret=True,event__names__shared_with__in=[self.request.session.get("role_current")])|
					Q(event__names__secret=True,event__campaigns__gamemasters__in=[self.request.user.id])
					).distinct()

		# Name permission filter for entities 
		if "entity" in params:
			q = q.filter(
					Q(entity__names__deleted=False),
					Q(entity__names__secret=False)|
					Q(entity__names__secret=True,entity__names__author=self.request.user)|
					Q(entity__names__secret=True,entity__names__shared_with__in=[self.request.session.get("role_current")])|
					Q(entity__names__secret=True,entity__campaigns__gamemasters__in=[self.request.user.id])
					).distinct()				

		return q		

class EventPlaceRelationViewSet(ParamsViewSet):
	permission_classes = (IsAuthenticated,permissions.EventPlaceRelationPermission)
	queryset = almanak.EventPlaceRelation.objects.all()
	serializer_class = serializers.EventPlaceRelationSerializer
	pagination_class = TenPerPagePagination
	filter_backends = (DjangoFilterBackend,)
	filterset_class = EventPlaceRelationFilter
	ordering_fields = ( "event__birth_age",
						"event__birth_year",
						"event__birth_month",
						"event__birth_day",
						"event__birth_hour",
						"event__birth_minute",
						"event__birth_second",)


	def get_queryset(self):
		user = self.request.user
		params = self.request.GET.get("d","")

		q = almanak.EventPlaceRelation.objects.select_related(
											"event",
											"place",
											"event__universe",
											"place__universe",
											"start_age",
											"end_age",
										).filter(
											Q(deleted=False,
											event__deleted=False,
											place__deleted=False,
											event__universe__id=self.request.session.get("universe_current"),
											place__universe__id=self.request.session.get("universe_current")),										
										).order_by("-event__birth_age__age_number","-event__birth_year", "-event__birth_month","-event__birth_date","-event__birth_hour","-event__birth_minute","-event__birth_second",)		

		# Name permission filter for events 
		if "event" in params:
			q = q.filter(
					Q(event__names__deleted=False),
					Q(event__names__secret=False)|
					Q(event__names__secret=True,event__names__author=self.request.user)|
					Q(event__names__secret=True,event__names__shared_with__in=[self.request.session.get("role_current")])|
					Q(event__names__secret=True,event__campaigns__gamemasters__in=[self.request.user.id])
					).distinct()

		# Name permission filter for entities 
		if "place" in params:
			q = q.filter(
					Q(place__names__deleted=False),
					Q(place__names__secret=False)|
					Q(place__names__secret=True,place__names__author=self.request.user)|
					Q(place__names__secret=True,place__names__shared_with__in=[self.request.session.get("role_current")])|
					Q(place__names__secret=True,place__campaigns__gamemasters__in=[self.request.user.id])
					).distinct()				

		return q	

class ElementItemRelationViewSet(ParamsViewSet):
	permission_classes = (IsAuthenticated,permissions.ElementItemRelationPermission,)
	queryset = almanak.ElementItemRelation.objects.all()
	serializer_class = serializers.ElementItemRelationSerializer
	pagination_class = TenPerPagePagination
	filter_backends = (DjangoFilterBackend,)
	filterset_class = ElementItemRelationFilter

	def get_queryset(self):
		params = self.request.GET.get("d","")	
		current_boolean = self.request.GET.get("current","")				
		q = almanak.ElementItemRelation.objects.prefetch_related(
									"owned__names",
									"owner__names",
								).select_related(
									"owner",
									"owned",
								).filter(
									Q(deleted=False,
									owner__deleted=False,
									owned__deleted=False,
									owner__universe__id=self.request.session.get("universe_current"),
									owned__universe__id=self.request.session.get("universe_current"))
								)
		# Current Filter for displaying the lastest record not higher than ingame now
		if current_boolean == "true":
			q = q.filter()


		# Name permission filter for owner 
		if "eirs1_owner" in params:
			q = q.filter(
					Q(owner__names__deleted=False),
					Q(owner__names__secret=False)|
					Q(owner__names__secret=True,owner__names__author=self.request.user)|
					Q(owner__names__secret=True,owner__names__shared_with__in=[self.request.session.get("role_current")])|
					Q(owner__names__secret=True,owner__campaigns__gamemasters__in=[self.request.user.id])
					).distinct()

		# Name permission filter for owner 
		if "eirs1_owned" in params:
			q = q.filter(
					Q(owned__names__deleted=False),
					Q(owned__names__secret=False)|
					Q(owned__names__secret=True,owned__names__author=self.request.user)|
					Q(owned__names__secret=True,owned__names__shared_with__in=[self.request.session.get("role_current")])|
					Q(owned__names__secret=True,owned__campaigns__gamemasters__in=[self.request.user.id])
					).distinct()


		return q

class ElementUniqueItemRelationViewSet(ParamsViewSet):
	permission_classes = (IsAuthenticated, permissions.ElementItemRelationPermission)
	queryset = almanak.ElementUniqueItemRelation.objects.all()
	serializer_class = serializers.ElementUniqueItemRelationSerializer
	pagination_class = TenPerPagePagination
	filter_backends = (DjangoFilterBackend,)
	filterset_class = ElementUniqueItemRelationFilter

	def get_queryset(self):
		params = self.request.GET.get("d","")		
		q = almanak.ElementUniqueItemRelation.objects.prefetch_related(
									"owned__names",
									"owner__names",
								).select_related(
									"owner",
									"owned",
								).filter(
									Q(deleted=False,
									owner__deleted=False,
									owned__deleted=False,
									owner__universe__id=self.request.session.get("universe_current"),
									owned__universe__id=self.request.session.get("universe_current"))
								)

		# Name permission filter for owner 
		if "euirs1_owner" in params:
			q = q.filter(
					Q(owner__names__deleted=False),
					Q(owner__names__secret=False)|
					Q(owner__names__secret=True,owner__names__author=self.request.user)|
					Q(owner__names__secret=True,owner__names__shared_with__in=[self.request.session.get("role_current")])|
					Q(owner__names__secret=True,owner__campaigns__gamemasters__in=[self.request.user.id])
					).distinct()

		# Name permission filter for owner 
		if "euirs1_owned" in params:
			q = q.filter(
					Q(owned__names__deleted=False),
					Q(owned__names__secret=False)|
					Q(owned__names__secret=True,owned__names__author=self.request.user)|
					Q(owned__names__secret=True,owned__names__shared_with__in=[self.request.session.get("role_current")])|
					Q(owned__names__secret=True,owned__campaigns__gamemasters__in=[self.request.user.id])
					).distinct()


		return q


class CampaignElementViewSet(ParamsViewSet):
	queryset = almanak.CampaignElement.objects.all()
	serializer_class = serializers.CampaignElementSerializer	



#########################
### TYPE CHANGE VIEWS ###
#########################


class ElementTypeChangeView(APIView):

	def post(self,request,*args,**kwargs):

		pk = request.data["id"]

		individual = Individual.objects.get(id=pk)

		rm_character = RolemasterCharacter(individual_ptr=individual)
		rm_character.__dict__.update(individual.__dict__)
		rm_character.save()

		return Response("Character Created")

