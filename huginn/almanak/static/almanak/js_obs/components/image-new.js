var imageNew = Vue.component('image-new', {
	template:"\
	<img v-tooltip=\"'Upload New Image'\" class=\"cursor--pointer icon icon--header\" :src=\"static + '/images/icons/frame.svg'\" @click=\"initializeImage()\" />\
	",
    data: function() {
        return {
			imageInitialData: {
				id: null,
				author: {
					id: this.$store.state.user.id,
					username: this.$store.state.user.username,
					first_name: this.$store.state.user.first_name,
					last_name: this.$store.state.user.last_name,
				},
				campaigns: [{
					 id: this.$store.state.session.id,
					 present_name: this.$store.state.session.present_name,
				}],
				created: "0",
				updated: "0",
				content: "",
				image_type: "off_game",
				secret: true,
				elements_in_image: [],
				deleted: false,
			}

        }
    },
    methods: {
		initializeImage: function(){
			this.$store.state.element.editors.image.data = {};
			this.$store.state.element.editors.image.data = this.imageInitialData;
			this.$store.state.element.editors.image.open = true;
			this.$store.state.element.editors.image.dataStatus = "Create New Image";
		}
	},
	computed: {
		currentUser:function(){
			return this.$store.state.user;
		},
		static: function (){
			return this.$store.state.initial.staticUrl;
		},
	}

});
