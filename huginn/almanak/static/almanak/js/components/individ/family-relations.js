var familyRelations = Vue.component('family-relations', {
	template: "\
	<div>\
		<transition name=\"fade\">\
			<div v-if=\"$store.state.element.relations.partners && $store.state.element.relations.siblings && $store.state.element.relations.parents\">\
				<div v-if=\"parents.length > 0 || user_can_edit == true\" >\
					<p class=\"rubrik\"><strong>Parents</strong></p>\
					<ul class=\"ink\" >\
						<li v-if=\"user_can_edit == true\" ><autocomplete filter=\"&element__element_type=entitet&element__type_grund__not=gruppe\" placeholder-text=\"Add a parent\" button-text=\"Add as parent\" :onButtonClick=\"addParent\" ></autocomplete></li>\
						<li v-for=\"p in parents\">\
							<a :href=\"'/almanak/side/' + p.above_full.id\">{{ p.above_full.present_name.name }}</a> <span @click=\"p.delete_question = !p.delete_question\" class=\"rubrik cursor--pointer\" v-if=\"user_can_edit == true\">&times;</span>\
							<p v-if=\"p.delete_question == true\" >Do you want to remove {{ p.above_full.present_name.name }} as a parent?<br/><button @click=\"deleteRelation(p.id)\" >Yes</button><button @click=\"p.delete_question = !p.delete_question\">No</button ></p>\
						</li>\
					</ul>\
				</div>\
				<div v-if=\"siblings.length > 0 || user_can_edit === true\" >\
					<p class=\"rubrik\"><strong>Siblings</strong> <em v-if=\"siblings.length < 1\" class=\"ink\" >No siblings</em></p>\
					<ul class=\"ink\" >\
						<li v-for=\"s in siblings\"><a :href=\"'/almanak/side/' + s.below_full.id\">{{ s.below_full.present_name.name }}</a></li>\
					</ul>\
				</div>\
				<div v-if=\"children.length > 0 || user_can_edit === true\" >\
					<p class=\"rubrik\"><strong>Children</strong></p>\
					<ul class=\"ink\" >\
						<li v-if=\"user_can_edit == true\" ><autocomplete filter=\"&type=individual__player_character\" placeholder-text=\"Add a child\" button-text=\"Add as child\" :button-action=\"{ type: 'ChildAdd', data: $store.state.element.masterdata, updateMutation: 'getFamilyRelations' }\" ></autocomplete></li>\
						<li v-for=\"c in children\">\
							<a :href=\"'/almanak/side/' + c.below_full.id\">{{ c.below_full.present_name.name }}</a> <span @click=\"c.delete_question = !c.delete_question\" class=\"rubrik cursor--pointer\" v-if=\"user_can_edit == true\">&times;</span>\
							<p v-if=\"c.delete_question == true\" >Do you want to remove {{ c.below_full.present_name.name }} as a child?<br/><button @click=\"deleteRelation(c.id)\" >Yes</button><button @click=\"c.delete_question = !c.delete_question\">No</button ></p>\
							</li>\
					</ul>\
				</div>\
				<div v-if=\"partners.length > 0 || user_can_edit === true\" >\
					<p class=\"rubrik\"><strong>Partners</strong></p>\
					<ul class=\"ink\">\
						<li v-if=\"user_can_edit == true\" ><autocomplete filter=\"&type=individual__player_character\" placeholder-text=\"Add a partner\" button-text=\"Add as partner\" :button-action=\"{ type: 'partnerAdd', data: $store.state.element.masterdata, updateMutation: 'getFamilyRelations' }\" ></autocomplete></li>\
						<li v-for=\"p in partners\" v-if=\"p.below_full.id\" >\
							<a :href=\"'/almanak/side/' + p.below_full.id\">{{ p.below_full.present_name.name }}</a>\
							<span @click=\"p.delete_question = !p.delete_question\" class=\"rubrik cursor--pointer\" v-if=\"user_can_edit == true\">&times;</span>\
							<p v-if=\"p.delete_question == true\" >Do you want to remove {{ p.below_full.present_name.name }} as a partner?<br/><button @click=\"deleteRelation(p.id)\" >Yes</button><button @click=\"p.delete_question = !p.delete_question\">No</button ></p>\
							</li>\
						<li v-for=\"p in partners\" v-if=\"p.above_full.id\" >\
							<a :href=\"'/almanak/side/' + p.above_full.id\">{{ p.above_full.present_name.name }}</a>\
							<span @click=\"p.delete_question = !p.delete_question\" class=\"rubrik cursor--pointer\" v-if=\"user_can_edit == true\">&times;</span>\
							<p v-if=\"p.delete_question == true\" >Do you want to remove {{ p.above_full.present_name.name }} as a partner?<br/><button @click=\"deleteRelation(p.id)\" >Yes</button><button @click=\"p.delete_question = !p.delete_question\">No</button ></p>\
							</li>\
					</ul>\
				</div>\
			</div>\
		</transition>\
		<family-tree></family-tree>\
	</div>\
	",
	mounted: function (){

	},
	methods: {
		deleteRelation: function(id) {
			this.$store.dispatch("update",{ url: "/api/new/element-relation/" + id + "/", object: { deleted: true }, updateAction: "getFamilyRelations" });
		},
		addParent: function (result) {
			var parentRelation = new ElementRelationModel();
			parentRelation.relation_type = "familie";
			parentRelation.above = result.element_id;
			parentRelation.below = this.$store.state.element.masterdata.id;
			this.$store.dispatch("create",{ object: parentRelation, url: "/api/new/element-relation/", updateAction: "getFamilyRelations" });
		}

	},
	computed: {
		user_can_edit: function (){
			return this.$store.state.element.masterdata.user_can_edit;
		},
		parents: function (){
			if(this.$store.state.element.relations.parents) {
				return this.$store.state.element.relations.parents;
			}
			else {
				return [];
			}
		},
		children: function (){
			if(this.$store.state.element.relations.children) {
				return this.$store.state.element.relations.children;
			}
			else {
				return [];
			}
		},
		siblings: function (){
			var self = this;
			if(this.$store.state.element.relations.siblings && this.$store.state.element.relations.siblings.length > 0) {
				var siblingsQueries = this.$store.state.element.relations.siblings;
				return siblingsFlattened = _.unionBy(siblingsQueries[0], siblingsQueries[1], "below_full.id").filter(function (s){ return s.below_full.id != self.$store.state.element.id });
			} else {
				return [];
			}
			// return _.uniqBy(siblingsFlattened, function (e) {
			// 	return e.id;
			// });
		},
        partners: function() {
			if(this.$store.state.element.relations.partners) {
				return this.$store.state.element.relations.partners;
			}
			else {
				return [];
			}
            // var partners = [];
            // if(this.$store.state.element.relations.below) {
            //     this.$store.state.element.relations.below.forEach( function(b) {
            //         if (b.relation_type == "individ" || b.relation_type == "player_character") {
            //             partners.push(b);
            //         }
            //     })
            // }
            // if(this.$store.state.element.relations.above) {
            //     this.$store.state.element.relations.above.forEach(  function(a) {
            //         if (a.relation_type == "individ" || a.relation_type == "player_character") {
            //             partners.push(a);
            //         }
            //     })
            // }
            // return partners;
        }
	},


});
