var autocomplete = Vue.component('autocomplete', {
    template: "<span>" +
      "<input @keyup=\"timerInactivity()\" v-model=\"query\" type=\"text\" :placeholder=\"placeholder\" />" +
      "<div v-if=\"showList == true\" class=\"typeahead__list\">" +
        "<p v-if=\"showList == true && r.user_permissions.read == true\"  v-for=\"r in resultsFiltered\" >\
			<img style=\"height:20px\" :src=\"st + 'images/icons/type/rubrik/' + r.type_grund + '.svg' \" />\
        	<a class=\"ink\" :target=\"linkTarget\" :href=\"'/almanak/side/' + r.element\">{{ r.name }}</a>\
			<span class=\"rubrik\">{{ $store.getters.elementTypeTranslated(r.type_spec) }}</span> <button class=\"btn-offgame\" v-if=\"buttonMethod\" @click=\"buttonFunction(r)\"  >{{ buttonText }}</button>" +
		"</p>" +
      "</div>" +
    "</span>"
    ,
    props: {
        admin: String,
        filter: String,
        parentComponent: String,
        buttonText: String,
        buttonMethod: String,
        actionUrl: String,
		linkTarget: String,
		placeholderText: String,
		numItems: String,

    },
    data: function() {
        return {
            query: "",
            result: [],
            showList: false,
        }
    },
    methods: {
		timerInactivity: function(){
			var self = this;
			console.log("Updating in 3 seconds");

			window.clearTimeout(this.timer);
			this.timer = window.setTimeout(function(){

				self.getResults();

			},300);


		},
        getResults: function() {
            var self = this;
            var filter = "";
            var admin = "";
            if(this.filter) {
              filter = "&type=" + this.filter;
            }
			if(this.admin) {
              admin = "&admin=true";
            }
            var results = [];
            if (this.query.length > 2) {
                axios.get('/api/element_by_name/?search=' + this.query + filter + admin + "&format=json").then(function(response) {
                    console.log(response.data)
                    self.result = response.data;
                    self.showList = true;
                }).catch(function(error) {
                    console.log(error);
                });
            } else {
                self.showList = false;
            }
            console.log(results);
        },
        buttonFunction: function(el) {
          var self = this;
          if(this.buttonMethod) {
		  	if(this.buttonMethod == "addAbove") {

				var current_element_id = self.$store.state.element.masterdata.id;

				axios.post("/api/elementrelation/create/",{ above: el.element, below: current_element_id }).then(function(response){
					console.log(response);
					self.$store.dispatch("getElementRelations");
				});

			}
			if(this.buttonMethod == "addBelow") {

				var current_element_id = self.$store.state.element.masterdata.id;

				axios.post("/api/elementrelation/create/",{ below: el.element, above: current_element_id }).then(function(response){
					console.log(response);
					self.$store.dispatch("getElementRelations");
				});

			}
			if(this.buttonMethod == "createElementItemRelation") {
				console.log("add property",this.numItems,el.name)
				var self = this;

				var ElementItemRelation = {
					owner: this.$store.state.element.masterdata.id,
					owned: el.element,
					number_of_items:this.numItems,
					ingame_age: this.ingameNow.age,
					ingame_year:this.ingameNow.year,
					ingame_month:this.ingameNow.month,
					ingame_date:this.ingameNow.date,
					ingame_hours:this.ingameNow.hours,
					ingame_minutes:this.ingameNow.minutes,
					ingame_seconds: this.ingameNow.seconds,

				};

				axios.post("/api/elementitemrelation/create/",ElementItemRelation).then(function(){
					self.$store.dispatch("getElementRelations");
				}).catch(function(error) {
		            console.log(error);
		        });

			}
            else if(this.buttonMethod == "createElementUniqueItemRelation") {

				var load = {
					owned: el.element,
					owner: self.$store.state.element.masterdata.id,

				};

				this.$store.dispatch(
  				  "consumerRequest",
  				  {
  					method:"post",
  				    url :"/api/elementuniqueitemrelation/create/",
  					data: load,
  					mutation: { name: "getElementRelations", payload: null },
  				  }
  			  );
			}
            else if(this.buttonMethod == "shareDescriptionWith") {

			  var descriptionId = self.$store.state.element.editors.description.data.id;
              console.log(el.element);
			  var sharedWith = self.$store.state.element.editors.description.data.shared_with;
			  var sharedWithIds = [];
			  _.each(sharedWith,function(d,i){
				  sharedWithIds.push(d.id);
			  });
			  sharedWithIds.push(el.element);

			  console.log(sharedWithIds)

              this.$store.dispatch(
				  "consumerRequest",
				  {
					method:"patch",
				    url :"/api/description/" + descriptionId + "/update/",
					data: { shared_with: sharedWithIds },
					mutation: { name: "openDescriptionEditor", payload: descriptionId },
				  }
			  );


            }
			else if(this.buttonMethod == "shareImageWith") {

			  var imageId = self.$store.state.element.editors.image.data.id;
              console.log(el.element);
			  var sharedWith = self.$store.state.element.editors.image.data.shared_with;
			  var sharedWithIds = [];
			  _.each(sharedWith,function(d,i){
				  sharedWithIds.push(d.id);
			  });
			  sharedWithIds.push(el.element);

			  console.log(sharedWithIds)

              this.$store.dispatch(
				  "consumerRequest",
				  {
					method:"patch",
				    url :"/api/image/" + imageId + "/update/",
					data: { shared_with: sharedWithIds },
					mutation: { name: "openImageEditor", payload: imageId },
				  }
			  );


            }
            else if(this.buttonMethod == "setAbove") {

              var above  = [];

              this.$store.state.element.editors.createNewElement.data.above = el;
              this.showList = false;
            }
			else if(this.buttonMethod == "descriptionChangeIngameAuthor") {

				var descriptionId = self.$store.state.element.editors.description.data.id;

				this.$store.dispatch(
  				  "consumerRequest",
  				  {
  					method:"patch",
  				    url :"/api/description/" + descriptionId + "/update/",
  					data: { ingame_author: el.element },
  					mutation: { name: "openDescriptionEditor", payload: descriptionId },
  				  }
  			  );
            }
			else if(this.buttonMethod == "imageChangeIngameAuthor") {

				var imageId = self.$store.state.element.editors.image.data.id;

				this.$store.dispatch(
  				  "consumerRequest",
  				  {
  					method:"patch",
  				    url :"/api/image/" + imageId + "/update/",
  					data: { ingame_author: el.element },
  					mutation: { name: "openImageEditor", payload: imageId },
  				  }
  			  );
		  	}
			else if(this.buttonMethod == "imageNewAddLink") {

				var links = this.$store.state.element.editors.image.newImage.links;

				links.push(el);

		  	}
			else if(this.buttonMethod == "imageAddLink") {

				console.log(el);
				var imageId = this.$store.state.element.editors.image.data.id;

				this.$store.dispatch(
  				  "consumerRequest",
  				  {
  					method:"post",
  				    url :"/api/elementimagerelation/create/",
  					data: {
						"image": imageId,
						"element": el.element,
						"relevance": 0,
					},
  					mutation: { name: "openImageEditor", payload: imageId },
				});

		  	}
            else if(this.buttonMethod == "createRelation") {
                var relType = this.parentComponent;
                if(relType == "history") {

                    var data = {
                        view: "eventplacerelation",
                        createObject: {
                            "place": this.$store.state.element.id,
                            "event": el.element
                        }
                    }
                    this.$store.dispatch("createRelation",data);
                }
                if(relType == "experiences") {

                    var data = {
                        view: "entityeventrelation",
                        createObject: {
                            "entity": this.$store.state.element.id,
                            "event": el.element
                        }
                    }
                    this.$store.dispatch("createRelation",data);
                }
                if(relType == "occured") {

                    var data = {
                        view: "eventplacerelation",
                        createObject: {
                            "event": this.$store.state.element.id,
                            "place": el.element
                        }
                    }
                    this.$store.dispatch("createRelation",data);
                }
                if(relType == "participants") {

                    var data = {
                        view: "entityeventrelation",
                        createObject: {
                            "event": this.$store.state.element.id,
                            "entity": el.element
                        }
                    }
                    this.$store.dispatch("createRelation",data);
                }
            }

          }
          this.showList = false;
          this.query = "";


        },

        setSecrecy:function(isSecret) {



        },

    },
    computed: {
		ingameNow: function(){
			var cmpgn = this.$store.state.session;

			var ingameNow = {
				age: cmpgn.ingame_now_age.id,
				year: cmpgn.ingame_now_year,
				month: cmpgn.ingame_now_month,
				date: cmpgn.ingame_now_date,
				hours: cmpgn.ingame_now_hours,
				minutes: cmpgn.ingame_now_minutes,
				seconds: cmpgn.ingame_now_seconds
			};

			return ingameNow;

		},
        resultsFiltered: function() {
            var self = this;
			var result = [];
			console.log(this.filter);
			if(this.filter) {
				if(this.filter == 'individ') {
					var tf = function(e) { return e.type_grund == "individ" || e.type_grund == "player_character" }
				}else if (this.filter == 'begivenhed'){
					var tf = function(e) {
						return e.type_grund == "begivenhed" || e.type_grund == "kampagne"  }
				}else if (this.filter == 'sted'){
					var tf = function(e) {return e.type_grund == "sted"}
				}else if (this.filter == 'gruppe'){
					var tf = function(e) {return e.type_grund == "gruppe"}
				}else if (this.filter == 'artefakt'){
					var tf = function(e) {return e.type_grund == "artefakt"}
				}else if (this.filter == 'entitet'){
					var tf = function(e) {return e.element_type == "entitet"}
					
				}else {
					return e.type_grund === this.filter;
				}

	            result = self.result.filter(tf);
			}
			else {

				result = self.result;
			}
			if(this.admin) {
				var result2 = result.filter(function(e) { return e.user_permissions.write === true });
			}else {
				var result2 = result;
			}


			return result2;
        //    return _.orderBy(self.result, ['name'], ['asc']);
        },
        st: function(){
            return this.$store.state.initial.staticUrl;
        },
		placeholder: function(){
			if(this.placeholderText) {
				return this.placeholderText;
			}
			else {
				return "Search by name";
			}
		},
    }
});
