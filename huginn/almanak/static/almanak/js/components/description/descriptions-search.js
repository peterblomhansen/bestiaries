var descriptionsSearch = Vue.component('descriptions-search', {
    template: "\
    <div >\
        <input type=\"text\" placeholder=\"Search for descriptions\" @input=\"searchOnInput\" v-model=\"searchQuery\" />\
    </div>",
    data: function() {
        return {
            searchQuery: "",
        }
    },
	props:{
//		data: Object,
    },
    methods: {
        searchOnInput: function (event){
            var self = this;          
            window.clearTimeout(this.timer);
            this.timer = window.setTimeout(function(){

                if(self.searchQuery.length > 0) {
                    self.$store.dispatch("getDescriptionsOfElement",{ searchQuery: self.searchQuery });
                }
                else {
                    self.$store.dispatch("getDescriptionsOfElement");
                }

              },500);
        }
    },
	computed: {
        // searchQuery: {
        //     get: function (){

        //     },
        //     set: function (val){

        //     },
        // },
        // searchState: {
        //     get: function () {
        //         return this.$store.state.search.description.search;
        //     },
        //     set: function(val) {
        //         if(val.length > 0) {
        //             this.$store.state.search.description.search = val;
        //         }else {
        //             this.$store.state.search.description.search = "";
        //         }
        //     }
        //}
        
	},

});