# Generated by Django 2.1 on 2019-01-13 09:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('almanak', '0023_merge_20181116_2336'),
    ]

    operations = [
        migrations.AlterField(
            model_name='descriptionelementrelation',
            name='description',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='element_relations', to='almanak.Description'),
        ),
        migrations.AlterField(
            model_name='descriptionelementrelation',
            name='element',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='description_relations', to='almanak.Element'),
        ),
    ]
