//** INIT.JS **//
// This is the main .js file, that loads main configuration and the global Vue instance

// CSRF Config for ajax calls
axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
axios.defaults.xsrfCookieName = "csrftoken";

// Vue/Django template {{}} conflict fix
//Vue.options.delimiters = ['[[', ']]'];
//

// Her sættes de variable, som afgør, hvilke ting, der skal hentes ind i store via init.
// De er her ikke endnu: De ligger i base.html, men skal flyttes hertil!


var eventHub = new Vue({});

// Top Vue Element
var tome = new Vue({
  el: "#tome",
  store: store,
  delimiters:['[[', ']]'],
  data: {  },

  // CODE INSIDE MOUNTED IS EXCUTED ON EVERY PAGE LOAD
  mounted: function () {

    // Userdata is always written to store
    this.$store.dispatch("getUser");

    // if User is ingame Campaign, Role and universe data i written to store.
    if(this.userIsIngame === true){
        this.$store.dispatch("getSession");

        // If the page is an Element in the bestiary, the Element data is written to store
        if(pageType == "element" && elementId.length > 0){
            this.$store.dispatch("getElement");
        }

		// If the page is a Description
        if(pageType == "description"){
			console.log("description");
            this.$store.dispatch("getDescription");
        }


		// If the page is an Image
        if(pageType == "image"){
			console.log("image");
            this.$store.dispatch("getImage");
        }

		// If the page is user profile page
        if(pageType == "profile"){
            this.$store.dispatch("getNews");
        }


    }


  },
  methods: {
    ingame_date_str: function(day,month,year,age = 0, min = 0, sec = 0){
        dateString = "Not set"
        if(day != 0 && month != 0 && year != 0 && age != 0) {
          var monthNames = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec'];
          var mIndex = parseInt(month) - 1;
          dateString = day + " " + monthNames[mIndex] + " " + year + " " + age.name;
        }


        return dateString;
    },
    ingame_date_interval: function(start = {day: 0,month: 0,year: 0,age: 0, min: 0, sec: 0}, end = {day: 0,month: 0,year: 0,age: 0, min: 0, sec: 0}) {

      var startValid = false;
      var endValid = false;

      var interval = {day: 0,month: 0,year: 0,age: 0, min: 0, sec: 0};

      if(start.day && start.month && start.year && start.age && start.min && start.sec) {
        startValid = true;
      }else {
        console.log("ingame_data_interval","start date not valid");
      }

      if(end.day && end.month && end.year && end.age && end.min && end.sec) {
        endValid = true;
      }else {
        console.log("ingame_date_interval","end date not valid");
      }


      if(startValid === true && endValid === true) {

        if(start.age == end.age) {
        //  interval.age = 0;

          if(start.year == end.year) {
        //    interval.year = 0;

            if(start.month == end.month) {
        //      interval.month = 0;

            }else {
              interval.month = end.month - start.month;
              interval.day = 0;

            }


          }else {
            interval.year = end.year - start.year;
            interval.month = 0;
            interval.day = 0;

          }

        }else {
          interval.age = end.age.age_number - start.age.age_number;
          interval.year = 0;
          interval.month = 0;
          interval.day = 0;

        }

      }

      return interval;
    }

  },
  computed: {
      // pageReady: function () {
      //     var dataLoaded = false;
      //     //console.log(pageType);
      //     if(pageType == "profile"){
      //        // if(this.$store.state.element.loaded.masterdata == true) {
      //             dataLoaded = true;
      //        // }
      //     }
      //     //console.log("master data state",this.$store.state.element.loaded.masterdata);
      //     if(pageType == "element") {
      //         if(this.$store.state.element.loaded.masterdata == true &&
      //            this.$store.state.element.loaded.sessiondata == true &&
      //            this.$store.state.element.loaded.userdata == true) {
      //             dataLoaded = true;
      //         }
      //     }
		  // if(pageType == "description") {
			//   dataLoaded = true;
		  // }
		  // if(pageType == "image") {
			//   dataLoaded = true;
		  // }

      //     //console.log(dataLoaded);
      //     return dataLoaded;
      // },
      userIsIngame: function () {

          var userIsIngame = false;
          if(this.$store.state.initial.universeId.length > 0 && this.$store.state.initial.campaignId.length > 0 && this.$store.state.initial.roleId.length > 0) {
              userIsIngame = true;
          }

          return userIsIngame;
      },
//     currentAges: function () {
// 
//         var opts = [];
//         _.each(this.$store.state.session.universe.ages,function(a){
//           opts.push({text: a.initials.toUpperCase(), value: a.id});
//         });
// 
//         var data = {
//           selected: this.$store.state.session.ingame_now_age.id,
//           options: opts
//         }
// 
//         return data;
// 
//     },
    role: function (){
       if(this.$store.state.session.names) {
          if (this.$store.state.initial.roleId == 0) {
            return "Gamemaster";
          }else {
             var self = this;
             var i = _.findKey(this.$store.state.user.player_characters, function(o) { return o.id == self.ingame.roleId; });
             return this.$store.state.user.player_characters[i].present_name; //console.log(pc);
//             return "Player Character";
          }
        }

    },
    roleLink: function() {
      if(this.session.names) {
         if (this.ingame.roleId == 0) {
           return "/accounts/profile";
         }else {
            var self = this;
            var i = _.findKey(this.user.player_characters, function(o) { return o.id == self.ingame.roleId; });
            return "/almanak/side/" + this.user.player_characters[i].id; //console.log(pc);
//             return "Player Character";
         }
       }
    },
    relationFamily: function () {

      var relFam = [];
      if(this.element) {
        _.each(this.element.elements_above,function(d){
          if(d.relation_type == 'familie') {
            relFam.push(d);
          }

        });
        _.each(this.element.elements_beneath,function(d){
          if(d.relation_type == 'familie') {
            relFam.push(d);
          }      });
      }
      return relFam;

    },
    relationNormal: function (){
      var relNor = [];
      if(this.element) {
        _.each(this.element.elements_above,function(d){
          if(d.relation_type == 'normal') {
            relNor.push(d);
          }

        });
        _.each(this.element.elements_beneath,function(d){
          if(d.relation_type == 'normal') {
            relNor.push(d);
          }
        });
      }
      return relNor;

    },
    relationKin: function (){
      var kin = [];
      if(this.element) {
        _.each(this.element.elements_above,function(d){
          if(d.relation_type == 'race') {
            kin.push(d);
          }

        });
      }

      return kin;

    },


  }


});
