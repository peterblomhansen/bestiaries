var bestiary = Vue.component('bestiary', {
    template: "\
    <transition name=\"fade\" v-if=\"!_.isEmpty(universe)\" >\
        <div style='text-align:center'>\
            <h3 class='rubrik' >Bestiary</h3>\
            <h1 class='ink' style='font-size:5rem;margin-bottom:1rem;' >{{ universe.name }}</h1>\
            <p class='rubrik'>kept by</p>\
            <p class='ink'>{{ universe.owners[0].first_name }} {{ universe.owners[0].last_name }}</p>\
            <br/><br/>\
            <div style='width:50%; margin-left: auto; margin-right: auto' >\
                <p class='toc__row'><span class='ink'>Total number of pages</span><span class='toc__fill faded'></span><span class='rubrik'>{{ bestiaryCounts.elements }}</span></p>\
                <p class='toc__row'><span class='ink'>Total number of descriptions</span><span class='toc__fill faded'></span><span class='rubrik'>{{ bestiaryCounts.descriptions }}</span></p>\
                <br/><br/>\
                <p class='toc__row'><span class='ink'>Player Characters</span><span class='toc__fill faded'></span><span class='rubrik'>{{ bestiaryCounts.playerCharacters }} pages</span></p>\
                <p class='toc__row'><span class='ink'>Campaigns</span><span class='toc__fill faded'></span><span class='rubrik'>{{ bestiaryCounts.roleplayingCampaigns }} pages</span></p>\
                <p class='toc__row'><span class='ink'>Roleplaying Sessions</span><span class='toc__fill faded'></span><span class='rubrik'>{{ bestiaryCounts.roleplayingSessions }} pages</span></p>\
                <br/><br/>\
                <p class='toc__row'><span class='ink'>Individuals</span><span class='toc__fill faded'></span><span class='rubrik'>{{ bestiaryCounts.individuals }} pages</span></p>\
                <p class='toc__row'><span class='ink'>Groups</span><span class='toc__fill faded'></span><span class='rubrik'>{{ bestiaryCounts.groups }} pages</span></p>\
                <p class='toc__row'><span class='ink'>Places</span><span class='toc__fill faded'></span><span class='rubrik'>{{ bestiaryCounts.places }} pages</span></p>\
                <p class='toc__row'><span class='ink'>Events</span><span class='toc__fill faded'></span><span class='rubrik'>{{ bestiaryCounts.events }} pages</span></p>\
            </div>\
        </div>\
    </transition>\
    ",
	props: {
		//text: String
	},
    data: function() {
        return { }
    },
    mounted: function (){

        this.$store.dispatch("tocGetElementCounts");
        // var self = this;
        // var campaignId = this.$store.state.generic.session.base.campaignId;
        // axios.get("/api/new/element/?ordering=-created&campaigns__id__in=" + campaignId + "&d=present_name__campaigns__es1c_campaign__es1c_ces1c_present_name&format=json").then(function(response){
        //     self.$store.state.news.elementNewestCreated = response.data;
        // })
        // .catch(function (error) {
        //   console.log(error);
        // });      
    },
    methods: {
        dur: function(yt) {
            var x = new moment();
            var y = new moment(yt);
            var duration = moment.duration(y.diff(x));
            return duration.humanize(true);
        }
    },
    computed: {
        universe: function (){
            return this.$store.state.generic.session.full.universe;
        },
        news: function (){
            return this.$store.state.news.elementNewestCreated;
        },
        bestiaryCounts: function (){
           return this.$store.state.toc.bestiary.counts; 
        },


    }


});
