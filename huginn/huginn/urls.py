"""huginn URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path

from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from almanak.views import StyleGuideView, OtherUserDetailView
from django.views.generic import RedirectView
from almanak import views as almanak_views
#from almanak.views import StyleGuideView
#urlpatterns = [
#    path('admin/', admin.site.urls),
#]

urlpatterns = [
	path('admin/', admin.site.urls),
	path('api/', include("almanak.api.urls")),
	path('almanak/', include("almanak.urls")),
	path('login/', auth_views.LoginView.as_view(template_name='registration/login.html',)),
	path('logout/', auth_views.LogoutView.as_view(template_name='registration/login.html')),
	path('rm/', include("rolemaster.urls")),
	path('rm/api/', include("rolemaster.api.urls")),
    path('', almanak_views.FrontPage.as_view(), {'file': 'welcome'}, name="root_view"),	
	path('styleguide/', StyleGuideView.as_view()),
#	url('accounts/profile/', ProfileView.as_view(),name="profile"),	
	re_path(r'^accounts/user/(?P<pk>[0-9]+)/$', OtherUserDetailView.as_view(),name="other_user")	
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
