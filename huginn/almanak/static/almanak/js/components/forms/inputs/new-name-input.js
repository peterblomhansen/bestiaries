var newNameInput = Vue.component('new-name-input', {
	template: "\
	<div class='grid grid--create-input'>\
		<label for='new-name'>Name</label>\
		<input name= 'new-name' type='text' v-model='newName' placeholder='The name for the new page' />\
	</div>",
	computed: {
		newName: {
			get: function (){
				return this.$store.state.create.name;
			},
			set: function (value){
				this.$store.commit("createDataMutate", { key: "name", value: value })
			}
		}
	}
});
 
