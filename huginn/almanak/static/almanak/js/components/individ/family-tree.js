var familyTree = Vue.component('family-tree', {
    template: "\
	<!--div>\
		<button type='button' @click='showFamilyTree = true'>Family Tree</button>\
		<transition name=\"fade-slide\" >\
			<div class=\"overlay\" v-if=\"showFamilyTree\">\
				<div class=\"base-layer--paper-sheet\" style=\"z-index:25\">\
					<div class=\"border--padding \">\
						<div class=\"border--content\">\
							{{startingIndividual.present_name.name}}\
							<div v-if=\"startingIndividual.children.length > 0\">\
								<family-tree-descendants :descendants='startingIndividual.children'></family-tree-descendants>\
							</div>\
						</div>\
					</div>\
				</div>\
			</div>\
		</transition>\
	</div-->\
    ",
    props: {
        nameObject: Object,
    },
    data: function(){
        return {
			showFamilyTree: false,
			
        };
        
    },
    methods: { 
		
		getStartingIndividual: function(){
			
			var startingIndividual = {}; 
			
			var self = this;
			
			var path = '/api/new/individual/' + this.$store.state.element.id + '/?format=json&d=es1_present_name__is1_children_relations__is1cr_parent_relations';
			//onsole.debug(path);
			
			axios.get(path)
			.then(function(response){
				
				//onsole.debug(response);
				
				var ind = new individual(response.data);
				
				
				self.$store.state.element.familyTree.startingIndividual = ind;
				//~ console.debug(self.$store.state.element.familyTree.startingIndividual);
			})
			.catch(function (error) {
				console.log(error);
			});
		},
    },
    mounted: function(){
		
		
		
		Vue.set(this.$store.state.element,"familyTree",{});
		Vue.set(this.$store.state.element.familyTree,"startingIndividual",{});
		//~ Vue.set(this.$store.state.element.relations.familyTree,"descendants",[]);
		//~ Vue.set(this.$store.state.element.relations.familyTree,"ancestors",[]);
		//~ Vue.set(this.$store.state.element.relations.familyTree,"partners",[]);
		//~ 
		this.getStartingIndividual();
		
		
	},
    computed: {
		
		startingIndividual: function(){
			return this.$store.state.element.familyTree.startingIndividual;
		}
        //~ isPrimary: {
			//~ get: function(){
				//~ return true ? this.nameObject.name_type == "prim" : false; 		
			//~ },
			//~ set: function (val) {
                //~ console.log(val);
                //~ if(val == true){
                    //~ this.nameObject.name_type = "prim";
                //~ }
                //~ else {
                    //~ this.nameObject.name_type = "sec";
                //~ }
			//~ },            
        //~ },
        //~ updateOrCreate: function() {
            //~ if(this.nameObject.id == null) {
                //~ return "Create";
            //~ }
            //~ else { return "Update"; }
        //~ },
        //~ authorName: function (){
            //~ if(this.nameObject.author === null) {
                //~ return "No Author";
            //~ }else {
                //~ var self = this;
                //~ var user = _.find(this.$store.state.generic.session.full.universe.users, function(u){ return u.id == self.nameObject.author });
                //~ return user.first_name + " " + user.last_name;
            //~ }
        //~ },
        //~ validated: function(){
            //~ var validated = false
            //~ if (this.nameObject.name.length > 0) {
                //~ validated = true; 
            //~ } 
            //~ return validated;
        //~ }

    }


});
