var descriptionEditorRelation = Vue.component('description-editor-relation', {
    template: "\
        <p class=\"description-editor__element-relation\" :class=\"relationBackground\" >\
            <element-link :element=\"relation.element\" /> \
            <img class=\"description-editor__element-relation-icon\" :src=\"icon\" /> \
            <input v-model=\"relevance\" />\
        </p>\
    ",
	props:{
        relationBackground: String,
		relation: Object,
	},
	computed: {
        icon: function (){
            return this.$store.getters.elementTypeIcon(this.relation.element.element_type,this.relation.element.type_grund,this.relation.element.type_spec);
        },
        editorStatusKey: {
            get: function (){
                return this.$store.state.editors.descriptionEditor.contentStatus;
            },
            set: function (value){
                this.$store.state.editors.descriptionEditor.contentStatus = value;
            },
        },        
        relevance: {
            get: function (){
                console.log("get");
                return this.relation.relevance;
            },
            set: function(value) {
                console.log("set");                
                var self = this;    
                self.editorStatusKey = "request";
                window.clearTimeout(this.timer);
                this.timer = window.setTimeout(function(){
                    self.$store.dispatch("editorUpdateDescriptionElementRelation", { id: self.relation.id, data: { relevance: value } })
                    .then(function (){
                        self.editorStatusKey = "response";                        
                    }); 
                },500);

                    //                this.relation.relevance = value;

            }
		},
	},

});
