var skills = Vue.component('skills', {
	template: "\
	<div >SKILLS - <span v-if=\"$store.state.loaded.skills == false\">{{ $store.state.messages.skills }}</span> \
		<table :class=\"fixedClass\" class=\"table__skillcategory\" >\
			<thead>\
				<tr>\
					<th colspan=\"4\" style=\"text-align:right\">Total Ranks</th>\
					<th class=\"skill__num-rank\">GM</th>\
					<th class=\"skill__num-rank\">Hobby</th>\
					<th class=\"skill__num-rank\" v-for=\"l in levels\">{{ l }}</th>\
				</tr>\
				<tr>\
					<th colspan=\"4\" style=\"text-align:right\">Total Available</th>\
					<th class=\"skill__num-rank\">#</th>\
					<th class=\"skill__num-rank\">#</th>\
					<th class=\"skill__num-rank\" v-for=\"l in levels\">{{ developmentPointsOfLevel(l) }}</th>\
				</tr>\
				<tr>\
					<th colspan=\"4\" style=\"text-align:right\">Total Used</th>\
					<th class=\"skill__num-rank\">{{ totalGmRanks }}</th>\
					<th class=\"skill__num-rank\">{{ totalHobbyRanks }}</th>\
					<th class=\"skill__num-rank\" v-for=\"l in levels\">{{ totalPriceOfLevel(l) }}</th>\
				</tr>\
				<tr>\
					<th colspan=\"4\" style=\"text-align:right\">Balance</th>\
					<th class=\"skill__num-rank\">#</th>\
					<th class=\"skill__num-rank\">#</th>\
					<th class=\"skill__num-rank\" v-for=\"l in levels\" :class=\"devBalancecCss(developmentPointsBalance(l))\" >{{ developmentPointsBalance(l) }}</th>\
				</tr>\
			</thead>\
		</table>\
		<table class=\"table__skillcategory\" v-for=\"c in skillCategories\">\
			<thead>\
				<tr >\
					<th class=\"skill__category\" :colspan=\"6 + $store.state.initial.level.viewed\" >{{ c }}</th>\
				</tr>\
				<tr>\
					<th class=\"skill__num-rank\" >!</th>\
					<th class=\"skill_name\" >Skill Name</th>\
					<th>Stats</th>\
					<th>Cost</th>\
					<th># Ranks</th>\
					<th class=\"skill__num-rank\" >GM</th>\
					<th class=\"skill__num-rank\" >Hobby</th>\
					<th class=\"skill__num-rank\" v-for=\"l in levels\">{{ l }}</th>\
				</tr>\
			</thead>\
			<tbody>\
				<skillrow :index=\"i\" v-for=\"(s,i) in $store.state.skillrows\" v-if=\"s.category == c\" :key=\"i\"  >\
				</skillrow>\
			</tbody>\
		</table>\
	</div>\
	",
	data: function (){
		return {
			offset: 0,
		}
	},
	mounted: function (){
		var self = this;
		// what should we do when scrolling occurs
		var runOnScroll = function(evt) {
		  // not the most exciting thing, but a thing nonetheless
		  self.offset = window.scrollY;

		};

		// grab elements as array, rather than as NodeList
		var elements = document.querySelectorAll("body"); // document.getElementById("fixed-accounting");//
		elements = Array.prototype.slice.call(elements);

		// and then make each element do something on scroll
		elements.forEach(function(element) {
		  window.addEventListener("scroll", runOnScroll);
		});

	},
	methods: {
		devBalancecCss: function(balance){
			//console.log(balance);
			cssClass = "";
			if (balance == 0) {
				cssClass = "alert alert--success";
			}
			else if (balance > 0){
				cssClass = "alert alert--warning";
			}
			else if (balance < 0){
				cssClass = "alert alert--danger";
			}


			return cssClass;


		},
		statsViewed: function (lvl){
			var self = this;
			if(this.statsAll.length > 0){
				var statsViewed = this.statsAll.filter(function(s){
	//				console.log(lvl,parseInt(s.level));

					return parseInt(s.level) == parseInt(lvl);
				});
				return statsViewed;
			}else {

				return [];
			}
		},
		temp: function (lvl = this.level.viewed){
			if(this.statsViewed.length > 0) {
				return this.statsViewed(lvl).filter(function(s) { return s.stat_type == "temp" });
			}
			else {
				return [];
			}
		},
		getDevPoints: function (statName,lvl = this.level.viewed){
			if(this.temp(lvl).length > 0) {

				var temp = this.temp(lvl)[0][statName];
				var stat = this.statBonusDistribution.find(function(s){ return s.stat == temp });
				if(stat) {
					return stat.dev_points;
				}
				else {
					return 0;
				}
			}
		},
		developmentPointsBalance: function(level) {
			var b =  this.developmentPointsOfLevel(level) - this.totalPriceOfLevel(level);
			return b.toFixed(0);
		},
		developmentPointsOfLevel(level) {
			var self = this;
			var devStats = [
				"luck",
			    "agility",
			    "constitution",
			    "memory",
			    "reasoning",
			    "self_discipline",
			];
			var totalDevPoints = 0
			_.each(devStats,function(s){
				totalDevPoints += parseFloat(self.getDevPoints(s,level));
			});
			return totalDevPoints.toFixed(0);
		},
		totalPriceOfLevel: function (level){
			var li = level - 1;
			var price = 0;
			_.each(this.$store.state.skillrows,function(r){
				//console.log(price);
				if(r.dev_ranks != null) {
					price = price + parseInt(r.dev_ranks[li].dev_points_used);
				}
			});

			return price;

		},
		skillsOfCategory: function(cat){
			return this.skillsAndAreasAll.filter(function(s){
				return s.category == cat;
			});
		},
		areasOfType: function(type,skillId = null) {
			return this.skillAreasAll.filter(function(s){
				if(type == "skill") {
					return s.area_type == type && s.skill == skillId;
				}
				else {
					return s.area_type == type;
				}
			});
		},
	},
	computed: {
		statBonusDistribution: function (){
			return this.$store.state.rolemasterTables.statBonusDistribution;
		},
		statsAll: function(){
			//console.log(this.$store.state.stats);
			return this.$store.state.character.stats;
		},
		fixedClass: function () {

			if(this.offset > 500) {
				return "dev__accounting";
			}else {
				return "";
			}
		},
		totalGmRanks: function (){
			var num_ranks = 0;
			_.each(this.$store.state.skillrows,function(r){
				if(r.gm_ranks != null) {
					num_ranks = num_ranks + parseInt(r.gm_ranks.num_ranks);
				}
			});

			return num_ranks;
		},
		totalHobbyRanks: function (){
			var num_ranks = 0;
			_.each(this.$store.state.skillrows,function(r){
				if(r.hobby_ranks != null) {
					num_ranks = num_ranks + parseInt(r.hobby_ranks.num_ranks);
				}
			});

			return num_ranks;
		},

		skillsAll: function (){
			return this.$store.state.skills;
		},
		skillAreasAll: function (){
			return this.$store.state.skillareas;
		},
		skillsAndAreasAll: function (){

			var self = this;
			var everything = [];
			_.each(this.skillsAll,function(s,i) {
				if(s.area_support == true) {

					everything.push(s);
					_.each(s.supported_area_types,function(t,j){
						var areas = self.areasOfType(t.area_type,s.id);
						_.each(areas,function(a,l){

							//console.log(a)
							everything.push({
								id: s.id,
								area: a.id, name: "-- " + a.name,
								category: s.category,
								stat_1: s.stat_1,
								stat_2: s.stat_2,
								stat_3: s.stat_3,

							})
						})

					});

				}
				else {
					s.area = null;
					everything.push(s);
				}
			});

			return everything;
		},
		skillCategories: function (){

			var cats = this.skillsAndAreasAll.map(function (s){ return s.category });

			function onlyUnique(value, index, self) {
			    return self.indexOf(value) === index;
			}

			return cats.filter(onlyUnique);


		},
		levels: function (){
			var levels = [];
			n = 1;
			while (n <= this.$store.state.initial.level.viewed) {
				levels.push(n);
				n++;
			}
			return levels;
		},
		character: function (){
			return this.$store.state.character;
		}
	},
});
