var imageFull = Vue.component('image-full', {
    template: "\
	<div v-if=\"$store.state.description.user_can_read === true && id && $store.state.description.descriptionFull && $store.state.description.elementRelations\" class=\"description description--text description--full element-permission\" style=\"grid-column-gap: 2em;\" >\
		<div>\
			<aside style=\"display:flex\">\
				<p class=\"rubrik\" style=\"font-size:1.2rem;flex:1\">\
					{{ time }}<br/>\
					<button class=\"button--small\" v-if=\"description.user_can_edit === true\" >Edit</button>\
				</p>\
				<p class=\"rubrik\" style=\"font-size:1.2rem;text-align:right;flex:1\" >\
					{{ publicSecretString }} image by {{ author }} \
					<span v-if=\"description.campaigns.length > 0\" >for<br/>\
						<span class=\"ink\" v-for=\"cmp in description.campaigns_full\"><a :href=\"'/almanak/element/' + cmp.id\" >{{ cmp.present_name.name }}</a></span>\
					</span>\
					<em v-else class=\"rubrik\" ><br/>Not in any campaigns</em>\
				</p>\
			</aside>\
			<img style=\"width:100%\" :src=\"description.url\" />\
			<div style=\"text-align:center\">\
				<br/>\
				<img class=\"icon--breaker\" :src=\"static + 'images/icons/breaker.svg'\" />\
			</div>\
		</div>\
		<div>\
			<h3 class=\"ink\" >{{ title }}</h3>\
			<p class=\"rubrik\" ><strong>Pages mentioned in this description ({{ elementRelations.count }})</strong></p>\
			<p :class=\"'ink' + relevanceClass(rel.relevance)\" v-for=\"rel in elementRelations.results\" >\
				<a target=\"_blank\" :href=\"'/almanak/element/' + rel.element.id\">\
				 - {{ rel.element.present_name.name }}\
				</a>\
			</p>\
			<button v-if=\"showGetMoreElementRelations\" @click=\"getMoreElementRelations()\" >See more mentions</button>\
		</div>\
	</div>\
	<div v-else-if=\"$store.state.description.user_can_read === false\" class=\"description description--text description--full element-permission\" style=\"grid-column-gap: 2em;\" >\
		<p class=\"ink\">You cannot read this description because of lack of permission</p>\
		<p><a class=\"rubrik\" href=\"/accounts/profile\">Go to Table of Content</a></p>\
	</div>\
	<div v-else >\
		<loading-spinner></loading-spinner>\
	</div>",		
	props:{
		id: Number,
	},
	data: function (){
		return {
			elementRelationsPage: 1,
		}
	},	
	computed: {
		description: function (){
			return this.$store.state.description.descriptionFull;
		},
		elementRelations: function (){
			return this.$store.state.description.elementRelations;
		},	
		static: function (){
			return this.$store.state.generic.staticUrl;
		},
		time: function (){
			if(this.description.description_type == "in_game") { 
				return this.description.ingame_time_string;
			}
			else {
				return this.description.updated && this.description.updated.split("T")[0];
			}	
		},
		author: function() {
			if(this.description.description_type == "in_game") {
				return this.description.ingame_author_full.present_name.name;
			} else {	
				return this.description.author_full.first_name + " " + this.description.author_full.last_name;
			}0
		},		
		publicSecretString: function (){
			return this.description.secret === true ? "Secret" : "Public";
		},
		title: function (){
			return this.description.title ? this.description.title : "Without title";
		},		
		showGetMoreElementRelations: function (){
			if(this.elementRelationsPage * 10 >= this.elementRelations.count) 
			{
				return false;
			}else {
				return true;
			}

		}	
	},
	methods: {
		getMoreElementRelations: function (){
			this.elementRelationsPage++;
			this.$store.dispatch("getDescriptionElementRelations", { page: this.elementRelationsPage });
		},
		relevanceClass: function(relevance) {
			return relevance < 0 ? " element--irrelevant" : "";
		} 		
	}
});
