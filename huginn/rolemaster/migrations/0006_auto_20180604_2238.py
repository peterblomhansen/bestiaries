# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-06-04 22:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rolemaster', '0005_auto_20180511_2151'),
    ]

    operations = [
        migrations.AddField(
            model_name='skillcomment',
            name='deleted',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='skillmiscbonus',
            name='deleted',
            field=models.BooleanField(default=False),
        ),
    ]
