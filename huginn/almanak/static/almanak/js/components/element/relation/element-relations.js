var elementRelations = Vue.component('element-relations', {
    template: "\
	<div>\
		<h3>Below</h3>\
		<ul>\
			<li v-for='b in below'>{{ b.below_full.present_name.name }}</li>\
		</ul>\
    </div>",
	mounted: function (){
		var self = this;
		this.$store.dispatch("elementGetRelationsBelow",{ page:1 });
		this.$store.dispatch("elementGetRelationsAbove",{ page:1 });

		// axios.get('/api/new/element-relation/?format=json&below__id=' + this.$store.state.element.id + '&d=ers1a_present_name__above')	
		// 	.then(function (response) {
        //         Vue.set(self.$store.state.element.relations,"above",response.data.results);
        //         //self.$store.state.session = response.data;

		// 	})
		// 	.catch(function (error) {
		// 	  console.log(error);
		// 	});			
		// 	axios.get('/api/new/element-relation/?format=json&above__id=' + this.$store.state.element.id + '&d=ers1b_present_name__below')	
		// 	.then(function (response) {
        //         Vue.set(self.$store.state.element.relations,"below",response.data.results);
        //         //self.$store.state.session = response.data;

		// 	})
		// 	.catch(function (error) {
		// 	  console.log(error);
		// 	});	

	},
	data: function (){
		return {
			page: 1,
		}
	},
	computed: {
		above: function (){
			return this.$store.state.element.relations.above;
		},	
		below: function (){
			return this.$store.state.element.relations.below;
		},	
	},
	
	
});
