var pageLoading = Vue.component('page-loading', {
    template: "\
        <div class=\"shader--full-screen\">\
          <div class=\"alert--center\">\
            <h3 v-html=\"text\" ></h3>\
            <div style=\"margin-top:2em;display:flex;flex-direction:column;align-items:center;height:100%;width:100%;\">\
                <div class=\"loading\">\
                    <div class=\"spinner\">\
                          <div class=\"mask\">\
                            <div class=\"maskedCircle\"></div>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
            </div>\
          </div>\
        </div>\
    ",
	props: {
		text: String
	},
    data: function() {
        return {
            hello: "Hello World",
            please: "sfdsgdsgsgsd"
        }
    },
    methods: {}
});
