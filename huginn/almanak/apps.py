from django.apps import AppConfig


class AlmanakConfig(AppConfig):
    name = 'almanak'
