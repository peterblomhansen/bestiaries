var profession = Vue.component('profession', {
	template: "<div>\
		<p v-if=\"profession\">Profession: {{ profession.name }}</p>\
		<p>\
			<select v-model=\"professionComp\">\
				<option value=\"0\" >Choose Profession</option>\
				<option v-for=\"p in professions\" :value=\"p.id\" >{{ p.name }}</option>\
			</select>\
			<input placeholder=\"Create Profession\" type=\"text\" v-model=\"newProfessionName\" > <button @click=\"createProfession\" >Create Profession</button></p>\
		<p>{{ professionNameValidateMessage }}</p>\
	</div>",
	data: function (){
		return {
			professionChosen: 0,
			professionNameValidateMessage: "",
			newProfessionName: "",
		}
	},
	methods: {
		createProfession: function(){
			var self = this;
			axios.post("/rm/api/profession/create/", { name: this.newProfessionName }).then(function(response){
				self.$store.state.professions.push(response.data);
				self.professionChosen = response.data.id;
				self.newProfessionName = "";
			 }).catch(function(error) {
                 console.log(error);
             });
		},
		updateCharacter: function(data){
			var self = this;
			axios.patch("/rm/api/rm_character/" + characterId + "/update/", data).then(function(response){
				window.location.reload();
				
				//self.$store.dispatch("getStats");
				//self.$store.dispatch("getskills");
			}).catch(function(error) {
				console.log(error);
			});
		},
	},
	computed: {
		characterId: function(){
			return this.$store.state.initial.characterId;
		},
		profession: function (){
			return this.$store.state.character.profession;
		},
		professions: function (){
			return this.$store.state.professions;
		},
		professionComp: {
			get: function(){
				if(this.profession) {
					return this.profession.id;
				}
				else {
					return 0;
				}
			},
			set: function(value){
				this.$store.state.loaded.skills = false;
				this.$store.state.messages.skills = "Updating skill costs...";
				this.updateCharacter({ profession: value });
				//window.location.reload;

			}
		},
	}
});
