from django.core.management.base import BaseCommand, CommandError
from django.db import connections
from collections import namedtuple
from django.contrib.auth.models import AbstractUser
from almanak.models import Description, Element, Image, CampaignImage
import urllib
import os
from django.conf import settings

class Command(BaseCommand):
	help = 'Closes the specified poll for voting'

	def handle(self, *args, **options):


		crsr = connections['default'].cursor()


		def namedtuplefetchall(cursor):
			#"Return all rows from a cursor as a namedtuple"
			desc = cursor.description
			nt_result = namedtuple('Result', [col[0] for col in desc])
			return [nt_result(*row) for row in cursor.fetchall()]


		self.stdout.write(self.style.WARNING("cleaning up images"))

		crsr.execute("SELECT * FROM almanak_image")
		images = namedtuplefetchall(crsr)

		print(settings.MEDIA_ROOT)

		for i in images:
			path = settings.MEDIA_ROOT + i.url
			valid = os.path.isfile(path)
			print(i.url,path,valid)
			if valid == False:
				crsr.execute("DELETE FROM almanak_imageelementrelation WHERE image_id =" + str(i.id) + " ")
				crsr.execute("DELETE FROM almanak_image WHERE id =" + str(i.id) + " ")

			else:
				img = Image.objects.get(pk=i.id)
				if len(img.elements_in_image.get_queryset()) > 0:
					el = img.elements_in_image.get_queryset()[0]
					print(el.element.universe)
					#el = Element.objects.get(pk=e.element.id)
					#print(el.universe)

					img.universe = el.element.universe
					img.save()
					#img.campaigns = el.element.campaigns
					if len(el.element.campaigns_containing.get_queryset()) > 0:
						ci = CampaignImage()
						ci.image = img
						ci.campaign = el.element.campaigns_containing.get_queryset()[0].campaign
						ci.save()

		self.stdout.write(self.style.SUCCESS("Images cleaned"))



#		self.stdout.write(self.style.WARNING("start writing universes to images"))

		# images = Image.objects.exclude(url="gallery/")
		# count = 0
		# for i in images:
		# 	ret = urllib.urlopen(i.url)
		# 	if ret.code == 200:
		# 		print ("Exists!")
		# 		count = count + 1
		# 		print(count)
		# 		print("asfsaffs")

				#path = i.image.url
			#print("URL", path)
			# if i.elements_in_image.get_queryset():
			# 	universe = i.elements_in_image.get_queryset()[0].element.universe
			# 	i.universe = universe
			# 	i.save()



#		self.stdout.write(self.style.SUCCESS("universes written"))
