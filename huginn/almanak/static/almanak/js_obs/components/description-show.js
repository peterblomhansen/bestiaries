var descriptionShow = Vue.component('description-show', {
    template:
      "<article class=\"beskrivelse\" :class=\"inkType\"  >" +
        "<aside class=\"beskrivelse__options rubrik\" style=\"margin-right:10px;text-align:center;\">" +
          "<img v-if=\"loadedData.description.permissions.write == true\" @click=\"loadedData.relevance++\" alt=\"Pil op\" data-toggle=\"tooltip\" data-placement=\"left\" :src=\"st + 'images/icons/relevans_pilop.svg'\" class=\"pilop relevans-pil\" data-original-title=\"Sæt relevansen én op\" width=\"25\">" +
          "<p>{{ loadedData.relevance }}</p>" +
          "<img v-if=\"loadedData.description.permissions.write == true\" @click=\"loadedData.relevance--\" alt=\"Pil ned\" data-toggle=\"tooltip\" data-placement=\"left\" :src=\"st + 'images/icons/relevans_pilned.svg'\" class=\"pilned relevans-pil\" data-original-title=\"Sæt relevansen én ned\" width=\"25\">" +
        "</aside>" +

        "<span class=\"initial\" @click=\"showFull = !showFull\" >{{ loadedData.description.content_stripped.uncial }}</span>" +
        "<div class=\"rest\" :class=\"inkType\" v-html=\"rest\" :style=\"cssHemmeligOffentlig\" ></div>" +

        "<aside class=\"beskrivelse__footer\" >" +
          "<div>" +
            "<p @click=\"showDeltmed = !showDeltmed\" style=\"cursor:pointer\">{{ hemmeligOffentlig }}<span v-if=\"loadedData.description.secret == true\">({{ loadedData.description.shared_with.length }})</span></p>" +
            "<ul v-if=\"loadedData.description.secret == true && showDeltmed == true\">" +
              "<li v-for=\"delt in loadedData.description.shared_with\">" +
                "<a class=\"ink\" :href=\"delt.id_entitet\">{{ delt.present_name }}</a>" +
              "</li>" +
            "</ul>" +
			"<a :href=\"'/almanak/description/show/' + loadedData.description.id\">Full screen</a>" +
          "</div>" +
          "<div style=\"text-align:right;\">" +
            "<p>" +
              "by <span v-if=\"loadedData.description.description_type == 'off_game'\">{{ loadedData.description.author.first_name }} {{ loadedData.description.author.last_name }}</span>" +
              "<span v-if=\"loadedData.description.description_type == 'in_game'\"  >\
			  		<span v-if=\"loadedData.description.ingame_author != null\"><a :href=\"'/almanak/side/' + loadedData.description.ingame_author.id\">{{ loadedData.description.ingame_author.present_name }}</a></span>\
			  		<span v-if=\"loadedData.description.ingame_author == null\">Unknown</span>\
				</span>" +
            "</p>" +
            "<p>" +
              "<span v-if=\"loadedData.description.ingame_age == null\">{{ updatedOffgame.date }}</span>" +
              "<span v-else>{{ ingameTimeComp }}</span>" +
              " <img v-if=\"loadedData.description.permissions.write == true\" style=\"height:1em;cursor:pointer;\" :src=\"st + 'images/icons/pencil.svg'\" @click=\"openEditor(loadedData.description.id)\" />" +
            "</p>" +
          "</div>" +
        "</aside>" +

      "</article>"
    ,
    props: {
        loadedData: Object,
        currentElement: String,
        perms: Object,
        allElements: Array,
    //    st: String,
        ages: Array,
    },
    data: function() {
        return {
            edit: false,
            showDeltmed: false,
            showFull: false
        };
    },
    computed: {

        st: function (){
          return this.$store.state.initial.staticUrl;
        },
        cssHemmeligOffentlig: function() {
            if (this.hemmelig == "3") {
                return "css-hemmelig";
            } else {
                return "";
            }
        },
        permissionEdit: function() {
            return false;
        },
        inkType: function() {
            var cssClass = "";
            if (this.loadedData.description.description_type == "in_game") {
                cssClass = "ink";
            } else if (this.loadedData.description.description_type == "off_game") {
                cssClass = "rubrik";
            }
            if (this.showState == "short" && this.loadedData.description.secret == true) {
                cssClass += " css-hemmelig";
            }
            if (this.loadedData.relevance < 0) {
                cssClass += " irrelevant_bes";
            }
            return cssClass;
        },
        rest: function() {
            if (this.showFull == true) {
                return this.contentFixedLinks(this.loadedData.description.content_full);
            } else if (this.showFull == false) {
                return this.loadedData.description.content_stripped.rest.substring(0, 440) + "&#8230;";
            }
        },
        cssHemmeligOffentlig: function() {
            if (this.loadedData.description.secret == true && this.showFull == false) {
                return "text-decoration: line-through";
            }
        },
        hemmeligOffentlig: function() {
            if (this.loadedData.description.secret == false) {
                return "Public";
            } else if (this.loadedData.description.secret == true) {
                return "Secret";
            }
        },
        CompAuthor: function() {
            if (this.loadedData.type_game == "in_game") {
                return this.loadedData.navn_forfatter;
            } else if (this.loadedData.type_game == "off_game") {
                return this.loadedData.navn_deltager + " " + this.loadedData.efternavn_deltager;
            }
        },
        createdOffgame: function() {
            return this.dateTimeSplit(this.loadedData.created);
        },
        updatedOffgame: function() {
            return this.dateTimeSplit(this.loadedData.description.updated);
        },
        ingameTimeComp: function() {
			console.log("Age",this.loadedData.description.ingame_age);
            return this.ingame_date_str(this.loadedData.description.ingame_date, this.loadedData.description.ingame_month, this.loadedData.description.ingame_year, this.loadedData.description.ingame_age)
        },
    },
    methods: {
		dateTimeSplit: function(dt){
  		  var split = dt.split("T");
  		  var date = split[0];
  		  var time = split[1];

  		  return {
  			  "date": date,
  			  "time": time
  		  }
  	  },
        openEditor: function(data) {

          this.$store.dispatch("openDescriptionEditor",data);

        },
        ingame_date_str: function(day, month, year, age = 0, min = 0, sec = 0) {

            var self = this;

            //console.log(self.$store.state.session);
            var ageIndex = self.$store.state.session.universe.ages.findIndex(function(a) {  return a.id == age});

            var ageComp = "Not defined";
            if (ageIndex > -1) {
                ageComp = self.$store.state.session.universe.ages[ageIndex].name
            }
            dateString = "Not set"
            if (day != 0 && month != 0 && year != 0 && age != null) {
                var monthNames = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
                var mIndex = parseInt(month) - 1;
                dateString = day + " " + monthNames[mIndex] + " " + year + " " + ageComp;
            }
            return dateString;
        },
        contentFixedLinks: function(content) {
            var find = 'href="';
            var re = new RegExp(find, 'g');
            var display = content.replace(re, 'href="/almanak/side/');
            return display;
        },
        updateFromEditor: function(value) {
            console.log("updateFromEditor " + value);
            this.$emit("reloadAjax", true);
        },
        unixDateToString: function(stamp) {
            var a = new Date(stamp * 1000);
            var months = ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'];
            var year = a.getFullYear();
            var month = months[a.getMonth()];
            var date = a.getDate();
            var hour = a.getHours();
            var min = a.getMinutes();
            var sec = a.getSeconds();
            var time = date + ' ' + month + ' ' + year;
            return time;
        },
    }
});
