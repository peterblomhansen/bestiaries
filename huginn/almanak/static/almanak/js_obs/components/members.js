var members = Vue.component('members', {
    template:"\
    <div v-if=\"groupType != null && groupType.id > 0\">\
        <h3 class=\"rubrik\" >Members without a title <span v-if=\"permissions.write == true\" class=\"cursor--pointer\" @click=\"editMembers = !editMembers\" >+</span></h3>\
        <div v-if=\"editMembers == true\" >\
			<autocomplete link-target=\"_blank\" :filter=\"'entitet'\" :buttonMethod=\"'addBelow'\" :buttonText=\"'Add member'\"  ></autocomplete>\
        </div>\
        <div v-if=\"membersWithNoTitles.length > 0\" v-for=\"b in membersWithNoTitles\" >\
            <p<a class=\"ink\" :href=\"'/almanak/side/' + b.below.id\">{{ b.below.present_name }}</a>\
            <strong v-if=\"masterdata.user_permissions.write == true\" class=\"rubrik\" style=\"cursor:pointer\" @click=\"b.delete_question = true\"> &times;</strong></p>\
			<span v-if=\"b.delete_question == true\">Do you want to remove {{ b.below.present_name }} from {{ masterdata.present_name }} <button @click=\"deleteMembership(b)\" >Yes</button> <button @click=\"b.delete_question = false\" >No</button></span>\
        </div>\
		<p v-if=\"membersWithNoTitles.length == 0\">None</p>\
    </div>\
	<div v-else >\
        <h3 class=\"rubrik\" >Members <span v-if=\"permissions.write == true\" class=\"cursor--pointer\" @click=\"editMembers = !editMembers\" >+</span></h3>\
        <div v-if=\"editMembers == true\" >\
			<autocomplete link-target=\"_blank\" :filter=\"'entitet'\" :buttonMethod=\"'addBelow'\" :buttonText=\"'Add member'\"  ></autocomplete>\
        </div>\
        <div v-for=\"b in below\" v-if=\"b.below.type_spec == 'individ' || b.below.type_spec == 'player_character'\" >\
            <p<a class=\"ink\" :href=\"'/almanak/side/' + b.below.id\">{{ b.below.present_name }}</a>\
            <strong v-if=\"masterdata.user_permissions.write == true\" class=\"rubrik\" style=\"cursor:pointer\" @click=\"b.delete_question = true\"> &times;</strong></p>\
			<span v-if=\"b.delete_question == true\">Do you want to remove {{ b.below.present_name }} from {{ masterdata.present_name }} <button @click=\"deleteMembership(b)\" >Yes</button> <button @click=\"b.delete_question = false\" >No</button></span>\
        </div>\
    </div>\
    ",
    data: function() {
        return { 
			editMembers: false,
		}
    },
    methods: {
		deleteMembership: function(m){
			var self = this;
			axios.patch("/api/elementrelation/" + m.id + "/update/",{ deleted: true }).then(function(response){
				console.log(response);
				var i = self.$store.state.element.relations.below.findIndex(function(b) { return b.id == m.id });
				self.$store.state.element.relations.below.splice(i,1);

			}).catch(function(error) {
				console.log(error);
			});
		},

    },
    computed: {
		masterdata: function (){
			return this.$store.state.element.masterdata;
		},
		permissions: function (){
			return this.$store.state.element.masterdata.user_permissions;
		},
		groupType: function() {
			return this.$store.state.element.masterdata.group_type;
		},
        below: function() {
            return this.$store.state.element.relations.below;
        },
		membersWithNoTitles: function (){
			var self = this;
			var withNoTitle = function (m){

				return m.above_title == null && (m.below.type_spec == 'individ' || m.below.type_spec == 'player_character');
			};
			console.log(this.$store.state.element.relations);
			return this.below.filter(withNoTitle);
		},
    }
});

// 	<pre>{{ below }}</pre>\
