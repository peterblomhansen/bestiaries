from django.test import TestCase, SimpleTestCase
import almanak.services as services
from typing import List
import bs4
# Create your tests here.
html = ""

with open('almanak/test/html.html', 'r') as myfile:
    data=myfile.read()
    html = data 

content_string = "<p>sdgdsgdsgdsg</p><a href='2345'>Name</a>"
another_content_string = "<p>sdasgds agdsg we grewgrsgs sgdsg</p><a href='2345'>Name</a>"

class StringCompareTestcase(SimpleTestCase):


    def test_checks_if_strings_are_equal(self):
        self.assertEqual(services.is_description_content_equal(content_string,content_string),True)
        self.assertEqual(services.is_description_content_equal(content_string,another_content_string),False)


class DescriptionSyncTestCase(SimpleTestCase):

    def test_returns_list_of_strings(self):
        self.assertTrue(services.list_links_in_html(html),type(bs4.element.ResultSet))


class LinkListTestCase(SimpleTestCase):


    def test_lists_are_equal_if_data_is_equal(self):
        self.assertTrue(services.is_link_list_equal(content_string,another_content_string))

    def test_lists_are_not_equal_if_number_of_links_is_not_equal(self):
        self.assertFalse(services.is_link_list_equal(content_string,html))


class get_element_ids_from_links(SimpleTestCase):

    def test_get_element_ids_from_links(self):
        self.assertEqual(services.get_element_ids_from_links(html),[1840, 1432, 1399])
