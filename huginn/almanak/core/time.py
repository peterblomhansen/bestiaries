#from almanak.models import Age
from django.apps import apps




#class TimePoint():
class TimePoint:
    def __init__(self, age, year, month, date, hour, minute, second):
        self.age = None
        if age != None:
            self.age = age
        self.year = year
        self.month = month
        self.date = date
        self.hour = hour
        self.minute = minute
        self.second = second

    def name(self):
        return str(self.date) + "." + str(get_month_name(self.month)) + "." + str(self.year) + " " +  str(self.age)

    def __str__(self):
        if self.age != None:
            return str(self.date) + "." + str(self.month) + "." + str(self.year) + " " +  str(self.age)
        else:
            return "Time not specified"

    def get_month_name(self,month_num):
        monthdict = {
            '1':'januar',
            '2':'februar',
            '3':'marts',
            '4':'april',
            '5':'maj',
            '6':'juni',
            '7':'juli',
            '8':'august',
            '9':'september',
            '10':'oktober',
            '11':'november',
            '12':'december',
        }
        return monthdict[str(month_num)]





class Interval:
    def __init__(self,first,last):

        self.valid = False
        self.ages = None
        self.years = 0
        self.months = 0
        self.dates = 0
        self.hours = 0
        self.minutes = 0
        self.seconds = 0

        if isinstance(first, TimePoint) and isinstance(last, TimePoint):

            self.valid = True
            if first.age == None or last.age == None:
                self.ages = None

            elif last.age == first.age:
                self.ages = 0
            else:
                self.ages = last.age.age_number - first.age.age_number

            self.years = last.year - first.year
            self.months = last.month - first.month
            self.dates = last.date - first.date
            self.hours = last.hour - first.hour
            self.minutes = last.minute - first.minute
            self.seconds = last.second - first.second


    def latest(self):

        if self.ages == None:
            return 0
        elif self.ages == 0:

            if self.years == 0:

                if self.months == 0:

                    if self.dates == 0:

                        if self.hours == 0:

                            if self.minutes == 0:

                                if self.seconds == 0:
                                    return 0
                                elif self.seconds < 0:
                                    return 1
                                elif self.seconds > 0:
                                    return 2

                            elif self.minutes < 0:
                                return 1
                            elif self.minutes > 0:
                                return 2

                        elif self.hours < 0:
                            return 1
                        elif self.hours > 0:
                            return 2

                    elif self.dates < 0:
                        return 1
                    elif self.dates > 0:
                        return 2

                elif self.months < 0:
                    return 1
                elif self.months > 0:
                    return 2

            elif self.years < 0:
                return 1
            elif self.years > 0:
                return 2

        elif self.ages < 0:
            return 1
        elif self.ages > 0:
            return 2


        return 0

    def __str__(self):
        if self.valid == False:
            return "Invalid TimePoints"
        # if in different ages
        if self.ages == None:
            return "Time not specified"
        if self.ages != 0:
            if self.ages > 0:
                if self.ages == 1:
                    return str(self.ages) + " age ago"
                else:
                    return str(self.ages) + " ages ago"
            elif self.ages < 0:
                if self.ages == -1:
                    return "in " + str(self.ages * -1) + " age"
                else:
                    return "in " + str(self.ages * -1) + " ages"

        # If in the same age
        if self.years != 0:
            if self.years > 0:
                if self.years == 1:
                    return str(self.years) + " year ago"
                else:
                    return str(self.years) + " years ago"
            elif self.years < 0:
                if self.years == -1:
                    return "in " + str(self.years * -1) + " year"
                else:
                    return "in " + str(self.years * -1) + " year"

        # If in the same year
        if self.months != 0:
            if self.months > 0:
                if self.months == 1:
                    return str(self.months) + " month ago"
                else:
                    return str(self.months) + " months ago"
            elif self.months < 0:
                if self.months == -1:
                    return "in " + str(self.months * -1) + " month"
                else:
                    return "in " + str(self.months * -1) + " months"

        # If in the same month
        if self.dates != 0:
            if self.dates > 0:
                if self.dates == 1:
                    return str(self.dates) + " day ago"
                else:
                    return str(self.dates) + " days ago"
            elif self.dates < 0:
                if self.dates == -1:
                    return "in " + str(self.dates * -1) + " day"
                else:
                    return "in " + str(self.dates * -1) + " day"

        # If in the same day
        if self.hours != 0:
            if self.hours > 0:
                if self.hours == 1:
                    return str(self.hours) + " hour ago"
                else:
                    return str(self.hours) + " hours ago"
            elif self.hours < 0:
                if self.hours == -1:
                    return "in " + str(self.hours * -1) + " hour"
                else:
                    return "in " + str(self.hours * -1) + " hours"

        # If in the same hour
        if self.minutes != 0:
            if self.minutes > 0:
                if self.minutes == 1:
                    return str(self.minutes) + " minute ago"
                else:
                    return str(self.minutes) + " minutes ago"
            elif self.minutes < 0:
                if self.minutes == -1:
                    return "in " + str(self.minutes * -1) + " minute"
                else:
                    return "in " + str(self.minutes * -1) + " minutes"

        # If in the same minute
        if self.seconds != 0:
            if self.seconds > 0:
                if self.seconds == 1:
                    return str(self.seconds) + " second ago"
                else:
                    return str(self.seconds) + " seconds ago"
            elif self.seconds < 0:
                if self.seconds == -1:
                    return "in " + str(self.seconds * -1) + " second"
                else:
                    return "in " + str(self.seconds * -1) + " seconds"


        return "now"









class DateHelper:

    def date_number(self,age,year,month,date,hours = 0,minutes = 0,seconds = 0):

        if age == None or age == 0:
            return None
        else:
            return str(age.age_number) + "-" + str(year) + "-" + str(month) + "-" + str(date) + "-" + str(minutes) + "-" + str(seconds)



    def date_string(self,age,year,month,date):

        monthdict = {
            '0':'not set',
            '1':'januar',
            '2':'februar',
            '3':'marts',
            '4':'april',
            '5':'maj',
            '6':'juni',
            '7':'juli',
            '8':'august',
            '9':'september',
            '10':'oktober',
            '11':'november',
            '12':'december',
        }

        if age == None:
            return False
        else:
            return str(date) + ". " + monthdict[str(month)] + " " + str(year) + " " + age.initials
