var playerCharacters = Vue.component('player-characters', {
    //template: "<div><p class='rubrik'>Roleplayer: <span class='ink'>[[ player.first_name ]] [[ player.last_name ]]</span></p></div>", '\n'
    template: "\
      <div>\
        <h4 class=\"rubrik\">\
          Main Characters\
          <strong v-if=\"masterdata.user_permissions.write == true\" class=\"rubrik\" style=\"cursor:pointer;\" @click=\"editMainCharacters = !editMainCharacters\" >+</strong>\
        </h4>\
		<div v-if=\"editMainCharacters == true\">\
			<autocomplete parent-component=\"participants\" :action-url=\"'entityeventrelation/create/'\" :filter=\"'individ'\" :button-method=\"'createRelation'\" :button-text=\"'Add as participant'\" >\
			</autocomplete>\
		</div>\
        <p v-for='pc in participantsPCs'>\
			<a class=\"rubrik\" >{{ pc.entity.roleplayer.first_name }} {{ pc.entity.roleplayer.last_name }}</a> as <a class=\"ink\" :href=\"'/almanak/side/' + pc.entity.id \">{{ pc.entity.present_name }}</a>\
        </p>\
      </div>\
    ",
	mounted: function() {
		this.$store.dispatch("getElementRelations");
	},
    data: function() {
        return {
            editMainCharacters: false,
            pcParticipantToBe: -1,
        }
    },
    methods: {
        deleteMainCharacterRelation: function(id) {
            axios.patch('/api/entityeventrelation/' + id + '/update/', {
                deleted: true
            }).then(function(response) {
                console.log(response)
                eventHub.$emit('reloadData');
            }).catch(function(error) {
                console.log(error);
            });
        },
        createMainCharacterRelation: function(id) {
            var self = this;
            if (id > 0) {
                axios.post("/api/entityeventrelation/create/", {
                    entity: id,
                    event: self.element.id
                }).then(function(response) {
                    console.log(response)
                    eventHub.$emit('reloadData');
                }).catch(function(error) {
                    console.log(error);
                });
            }
        }
    },
    computed: {
        participantsPCs: function() {
			if(this.$store.state.element.relations.participants) {
				return this.$store.state.element.relations.participants.filter(function(p){
						return p.entity.type_grund == "player_character";
				});
			}
        },
        pcsNotParticipants: function() {
            var self = this;
            var pcs = [];
            _.each(this.playerCharactersInUniverse, function(pc) {
                var isParticipant = false;
                _.each(self.participants, function(p) {
                    if (pc.id == p.entity.id && p.deleted == false) {
                        isParticipant = true;
                    }
                });
                if (isParticipant == false) {
                    pcs.push(pc);
                }
            });
            var data = _.orderBy(pcs, ['present_name'], ['asc']);
            return data;
        },
        masterdata: function(){
            return this.$store.state.element.masterdata;
        },
        element: function(){
            return this.$store.state.element;
        }
    }
});
