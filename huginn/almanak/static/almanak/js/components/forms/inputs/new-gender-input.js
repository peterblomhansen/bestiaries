var newGenderInput = Vue.component('new-gender-input', {
	template: "\
	<div class='grid grid--create-input'>\
		<label for='new-gender'>Gender</label>\
		<select v-model='newGender'>\
			<option value='Female'>Female</option>\
			<option value='Male' >Male</option>\
			<option value='Other' >Other</option>\
		</select>\
	</div>",
	computed: {
		newGender: {
			get: function (){
				return this.$store.state.create.gender;
			},
			set: function (value){
				this.$store.commit("createDataMutate", { key: "gender", value: value })
			}
		}
	}
});
 
