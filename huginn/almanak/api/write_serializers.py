from rest_framework.serializers import ModelSerializer, PrimaryKeyRelatedField
import almanak.models as almanak


class ModelWriteSerializer(ModelSerializer):
	pass


class NameWriteSerializer(ModelWriteSerializer):

	class Meta:
		model = almanak.Name
		fields = ["name","author","secret","name_type"]


class ElementRelationAboveWriteSerializer(ModelWriteSerializer):

	class Meta:
		model = almanak.ElementRelation
		fields = ["above","relation_type"]

class ElementRelationBelowWriteSerializer(ModelWriteSerializer):

	class Meta:
		model = almanak.ElementRelation
		fields = ["below","relation_type"]

# class CampaignElementWriteSerializer(ModelWriteSerializer):
# 	campaign = PrimaryKeyRelatedField(queryset=almanak.Campaign.objects.all(), many=True)

# 	class Meta:
# 		model = almanak.CampaignElement
# 		fields = ["campaign"]




class ElementWriteSerializer(ModelSerializer):
	names = NameWriteSerializer(many=True)
	campaigns = PrimaryKeyRelatedField(queryset=almanak.Campaign.objects.all(),many=True)#CampaignElementWriteSerializer(many=True)
	element_relations_above	= ElementRelationAboveWriteSerializer(many=True)
	element_relations_below	= ElementRelationBelowWriteSerializer(many=True)


	class Meta:
		model = almanak.Element
		fields = "__all__"

	# On Post Requests
	def create(self, validated_data):
		names_data = validated_data.pop('names')
		campaigns_data = validated_data.pop('campaigns')		
		element_relations_above_data = validated_data.pop('element_relations_above')
		element_relations_below_data = validated_data.pop('element_relations_below')		
		element = self.Meta.model.objects.create(**validated_data)

		for name_data in names_data:
			almanak.Name.objects.create(element=element, **name_data)
		for campaign_data in campaigns_data:
			almanak.CampaignElement.objects.create(element=element, campaign=campaign_data)
		for element_relation_above_data in element_relations_above_data:
			almanak.ElementRelation.objects.create(below=element, **element_relation_above_data)
		for element_relation_below_data in element_relations_below_data:
			almanak.ElementRelation.objects.create(above=element, **element_relation_below_data)
		return element




class IndividualWriteSerializer(ElementWriteSerializer):

	class Meta:
		model = almanak.Individual
		fields = "__all__"


class GroupWriteSerializer(ElementWriteSerializer):

	class Meta:
		model = almanak.Group
		fields = "__all__"


class EventWriteSerializer(ElementWriteSerializer):

	class Meta:
		model = almanak.Event
		fields = "__all__"


class RpgSessionWriteSerializer(ElementWriteSerializer):

	class Meta:
		model = almanak.RpgSession
		fields = "__all__"


class PlaceWriteSerializer(ElementWriteSerializer):

	class Meta:
		model = almanak.Place
		fields = "__all__"


class ItemWriteSerializer(ElementWriteSerializer):

	class Meta:
		model = almanak.Item
		fields = "__all__"


class ElementRelationWriteSerializer(ModelSerializer):

	class Meta:
		model = almanak.ElementRelation
		fields = "__all__"


class DescriptionWriteSerializer(ModelSerializer):

	class Meta:
		model = almanak.Description
		fields = "__all__"
