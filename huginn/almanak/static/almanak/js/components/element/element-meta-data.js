// "<div v-if=\"masterdata.length >0\">dfdf {{ masterdata }}\
// <div>\
// 	<p class=\"rubrik\">Bestiary: <span class=\"ink\">{{ masterdata.universe.name }}</span></p>\
// </div> \
// <div>\
// 	<div v-if=\"editCampaigns\">\
// 		<p class=\"rubrik\" ><strong>Edit campaigns:</strong></p>\
// 		<p>\
// 			<select @change=\"addCampaign(campaignSelected)\" v-model=\"campaignSelected\"> {{ addCampaignAjaxStatus }}\
// 				<option value=\"0\">Add campaign</option>\
// 				<option v-for=\"(c,i) in campaignsInUniverse\" v-if=\"campaignsIds.indexOf(c.id) == -1\" :value=\"c.id\">\
// 					{{ c.present_name }}\
// 				</option>\
// 			</select>\
// 		</p>\
// 		<div class=\"ink\" v-for=\"c in masterdata.campaigns\" >\
// 			<p  class=\"ink\" >\
// 				{{ c.campaign.present_name }}\
// 				<strong v-if=\"masterdata.user_permissions.write == true\" class=\"rubrik\" style=\"cursor:pointer\" @click=\"c.campaign.delete_question = !c.campaign.delete_question\"> &times;</strong>\
// 			</p>\
// 			<p class=\"rubrik\" v-if=\"c.campaign.delete_question == true\" >\
// 				Do you want to remove {{ c.present_name }} as a connected campaign <button @click=\"removeCampaign(c)\">Yes</button> <button @click=\"c.campaign.delete_question = !c.campaign.delete_question\">No</button>\
// 			</p>\
// 		</div>\
// 	</div>\
// </div>\
// </div>\
var elementMetaData = Vue.component('element-meta-data', {
	template: 
	"<div v-if=\"!_.isEmpty(masterdata) && !_.isEmpty(permissions)\" >\
		<p class=\"rubrik\">Bestiary: {{ masterdata.universe.name }} {{ isUniverseOwner }}</p>\
		<div>\
			<p class=\"rubrik\" v-if=\"masterdata.campaigns_full.length > 0\">Campaigns:\
				<span class=\"ink\" v-for=\"(c,i) in masterdata.campaigns_full\" >\
					<a :href=\"'/almanak/side/' + c.campaign_full.id\" >{{ c.campaign_full.present_name.name }}</a>\
					<span v-if=\"c.approved == 0\" >(Approval missing)</span>\
					<span v-if=\"c.approved == 1\" >(Reapproval missing)</span>\
					<button class=\"btn-offgame\" v-if=\"c.approved == 0 && campaignIsCurrent(c.campaign.id).approved == 0\" @click=\"approveElement(i,c.id)\">Approve</button>\
					<button class=\"btn-offgame\" v-if=\"c.approved == 1 && campaignIsCurrent(c.campaign.id).approved == 1\" @click=\"approveElement(i,c.id)\" >Reapprove</button>\
				</span>\
				<button v-if=\"permissions.write == true && $store.state.generic.session.base.roleId == 0\" class=\"button--small\" @click=\"$store.commit('toggleEditElementPermissions')\" >edit</button>\
			</p>\
			<p class=\"rubrik\" v-else>\
				Not included in a campaign <button v-if=\"permissions.write == true && $store.state.generic.session.base.roleId == 0\" class=\"button--small\" @click=\"$store.commit('toggleEditElementPermissions')\" >edit</button>\
			</p>\
		</div>\
	</div>",
    data: function() {
        return {
			test: 0,
			editCampaigns: false,
			campaignSelected: 0,
            addCampaignAjaxStatus: "",
        }
    },
    methods: {
		approveElement: function (index,id) {
			var self = this;
			axios.patch("/api/campaignelement/" + id + "/update/", { approved: 2 }).then(function(response) {
				self.masterdata.campaigns[index].approved = 2;
				// self.$store.state.session = response.data;
				// self.$store.state.element.loaded.sessiondata = true;
				// self.$store.state.initial.sessionLoaded = true
			}).catch(function(error) {
				console.log(error);
			});
		},
		loadCampaignsInUniverse: function (){
			var self = this;
			if(campaignId){
				axios.get("/api/session/" + campaignId + "/?d=us1_ages__cs1_universe__us1_name__us1_campaigns&format=json").then(function(response) {
					self.$store.state.session = response.data;
					self.$store.state.element.loaded.sessiondata = true;
					self.$store.state.initial.sessionLoaded = true
				}).catch(function(error) {
					console.log(error);
				});
			}
		},
		addCampaign: function(i) {
            var self = this;
            // self.masterdata.campaigns.push(self.campaignsInUniverse[i]);
            // self.addCampaignAjaxStatus = "adding";
			console.log(i)

            this.updateCampaignsAjax(i);
        },
        removeCampaign: function(c) {

			var self = this;
			axios.delete("/api/campaignelement/" + c.id + "/delete/").then(function(response) {
				self.$store.dispatch("getElement");
			});

        },
        updateCampaignsAjax: function(idCampaign) {
            //console.log("afaf", "afaf");
            var self = this;
			axios.post("/api/campaignelement/create/",{
				"element": self.masterdata.id,
				"campaign": idCampaign,
				"approved":0,
				"canon":0,

			}).then(function(response) {
				self.$store.dispatch("getElement");
			});

        },
		campaignIsCurrent: function(id) {
			console.log("Approve",this.$store.state.generic.session.base.roleId,id);
			if (this.campaignsContainingSession != null && id == this.campaignsContainingSession.campaign) {
				if(this.initial.roleId == "0" && this.initial.campaignId == id) {
					return this.campaignsContainingSession;
				}
				else {
					return false;
				}
			}
			else {
				return false;
			}

		},
	},
	computed: {
		permissions: function (){
			return this.$store.state.element.permissions;			
		},
		static: function (){

            return this.$store.state.generic.staticUrl;

		},		
        masterdata: function(){
			//if(this.$store.state.element.masterdata) {
				return this.$store.state.element.masterdata;
			//}else {
			//	return {};
			//}
        },
        userUniverses: function (){

            return this.$store.state.user.universes;
        },
		campaignsInUniverse: function (){
            return this.$store.state.session.universe.campaigns;
        },
		campaignsContainingSession: function (){
            return this.masterdata.campaigns_containing_session;
        },
		campaignsIds: function() {
            var ids = []
            _.each(this.masterdata.campaigns, function(c) {
                if (c) {
                    ids.push(c.id);
                }
            });
            return ids;
		},
		isUniverseOwner: function() {
			if (this.$store.state.generic.session.full.user_is_universe_owner === true){
				return "(Owner)";
			}else {
				return "";
			}
		},
	},

});
