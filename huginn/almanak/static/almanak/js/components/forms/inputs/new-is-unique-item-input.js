var newIsUniqueItemInput = Vue.component('new-is-unique-item-input', {
	template: "\
	<div class='grid grid--create-input'>\
		<label for='new-name'>Unique Item</label>\
		<input type=\"checkbox\" v-model=\"newUniqueItem\" />\
	</div>",
	computed: {
		newUniqueItem: {
			get: function (){
				return this.$store.state.create.unique_item;
			},
			set: function (value){
				this.$store.commit("createDataMutate", { key: "unique_item", value: value })
			}
		}
	}
});
 
