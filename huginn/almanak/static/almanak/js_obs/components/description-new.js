var descriptionNew = Vue.component('description-new', {
	template:"\
	<img v-tooltip=\"'Create New Description'\" class=\"cursor--pointer icon icon--header\" :src=\"static + '/images/icons/initial.svg'\" @click=\"initializeDescription()\" />\
	",
    data: function() {
        return {
			descriptionInitialData: {
				id: null,
				author: {
					id: this.$store.state.user.id,
					username: this.$store.state.user.username,
					first_name: this.$store.state.user.first_name,
					last_name: this.$store.state.user.last_name,
				},
				campaigns: [{
					 id: this.$store.state.session.id,
					 present_name: this.$store.state.session.present_name,
				}],
			//	created: "0",
			//	updated: "0",
				content: "",
				description_type: "off_game",
				secret: true,
				elements_in_description: [],
				deleted: false,
			}

        }
    },
    methods: {
		initializeDescription: function(){
			this.$store.state.element.editors.description.data = {};
			this.$store.state.element.editors.description.data = this.descriptionInitialData;
			this.$store.state.element.editors.description.open = true;
			this.$store.state.element.editors.description.dataStatus = "Create New Description";
		}
	},
	computed: {
		currentUser:function(){
			return this.$store.state.user;
		},
		static: function (){
			return this.$store.state.initial.staticUrl;
		},
	}

});
