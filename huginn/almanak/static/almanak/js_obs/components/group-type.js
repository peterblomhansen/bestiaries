var groupType = Vue.component('group-type', {
	template: "<div v-if=\"groupType.id\">\
		<h4 class=\"ink\">{{ groupType.name }} <span class=\"faded rubrik\" v-if=\"element.user_permissions.write == true\" @click=\"editGrouptypeDetails = !editGrouptypeDetails\">edit</span></h4>\
		<p class=\"rubrik\">{{ groupType.description }}</p>\
		<div @input=\"updateGroupTypeDetails(groupType.id)\" v-if=\"editGrouptypeDetails == true\" >\
			<p><input type=\"text\" v-model=\"groupType.name\"></p>\
			<textarea rows=\"8\" style=\"width:100%;\" v-model=\"groupType.description\"></textarea>\
		</div>\
		<div>\
			<h4 class=\"ink\">Present Members <span v-if=\"element.user_permissions.write == true\" class=\"rubrik faded cursor--pointer\" @click=\"editMembers = !editMembers\">edit all</span></h4>\
			<p class=\"rubrik faded\" v-if=\"element.user_permissions.write == true && membersWithNoTitles.length > 0\" >! {{ membersWithNoTitles.length }} members of this group have no titles</p>\
			<p class=\"rubrik faded\" v-if=\"element.user_permissions.write == true && membersWithInvalidTitles.length > 0\" >! {{ membersWithInvalidTitles.length }} members of this group have invalid titles</p>\
			<p class=\"rubrik cursor--pointer\" v-if=\"element.user_permissions.write == true\" @click=\"showAddNewRelationTitle = !showAddNewRelationTitle\">add new title</span>\
			<div v-if=\"showAddNewRelationTitle == true\">\
				<div>\
					<p>Name:<br/> <input type=\"text\" v-model=\"newRelationTitle.name\"></p>\
					<p>Name Plural:<br/> <input type=\"text\" v-model=\"newRelationTitle.name_plural\"></p>\
					<p>Preposition:<br/> <input type=\"text\" v-model=\"newRelationTitle.preposition\"></p>\
					<p>Description:</p>\
					<textarea rows=\"8\" style=\"width:100%;\" v-model=\"newRelationTitle.description\"></textarea>\
					<button @click=\"createNewRelationTitle(groupType.id)\" >Create</button>\
				</div>\
				<br/><br/>\
			</div>\
			<div @input=\"updateRelationTitle(t)\" v-for=\"t in groupType.titles\" >\
				<h5 class=\"ink\">{{ t.name }} \
					<span v-if=\"element.user_permissions.write == true && t.ajax_status.show_edit == false\" class=\"rubrik faded cursor--pointer\" @click=\"t.ajax_status.show_edit = !t.ajax_status.show_edit\" >edit title</span>\
					<span v-if=\"element.user_permissions.write == true && t.ajax_status.show_edit == true\" class=\"rubrik cursor--pointer\" @click=\"t.ajax_status.show_edit = !t.ajax_status.show_edit\" >&times;</span> <span v-if=\"t.ajax_status.show_edit == true\" class=\"faded\">{{ t.ajax_status.message }}</span>\
				</h5>\
				<div v-if=\"t.ajax_status.show_edit == false\" >\
					<p class=\"rubrik\">{{ t.description }}</p>\
					<p v-for=\"m in presentMembers\" >\
						<a class=\"ink\" v-if=\"m.above_title && t.id == m.above_title.id\" :href=\"'/almanak/side/' + m.below.id\">- {{ m.below.present_name }}</a>\
					</p>\
				</div>\
				<div v-if=\"element.user_permissions.write == true && t.ajax_status.show_edit == true\" >\
				<p>Name:<br/> <input type=\"text\" v-model=\"t.name\"></p>\
				<p>Name Plural:<br/> <input type=\"text\" v-model=\"t.name_plural\"></p>\
				<p>Preposition:<br/> <input type=\"text\" v-model=\"t.preposition\"></p>\
				<textarea rows=\"8\" style=\"width:100%;\" v-model=\"t.description\"></textarea>\
				</div>\
			</div>\
		</div>\
		<div v-if=\"editGrouptype == true\" class=\"shader--full-screen\" style=\"align-items:flex-start;\">\
			<div style=\"width:80%\" >\
				<div class=\"cursor--pointer\" @click=\"$store.dispatch('getElement');$store.dispatch('getElementRelations');editGrouptype = false\" style=\"font-size:2em;\" >&times;</div>\
				<h3>{{ element.present_name }} is a: \
					<select @change=\"updateGroupType(element.id)\" v-model=\"groupType.id\">\
						<option v-for=\"gt in groupTypesOfUniverse\" :value=\"gt.id\" >{{ gt.name }}</option>\
					</select>\
					 or Create new <input type=\"text\" v-model=\"newGroupTypeName\" /> <button @click=\"createNewGroupType()\" >create</button>\
					 <span class=\"faded\">{{ groupType.ajax_status.message }}</span>\
				</h3>\
				<h3>Edit the Group Type {{ groupTypeCurrent.name }}</h3>\
				<p>Name: <input type=\"text\" v-model=\"groupTypeCurrent.name\"></p>\
				<p>Description:<br/> <textarea rows=\"8\" style=\"width:100%;\" v-model=\"groupTypeCurrent.description\" ></textarea></p>\
				<h4>Titles of {{ groupTypeCurrent.name }}  <span class=\"cursor--pointer\" @click=\"showAddNewRelationTitle = !showAddNewRelationTitle\">+</span></h4>\
				<div v-if=\"showAddNewRelationTitle == true\">\
					<h4>Create a new title in the group type {{ groupTypeCurrent.name }}</h4>\
					<div class=\"row\">\
						<div class=\"column\" >\
							<p>Name:<br/> <input type=\"text\" v-model=\"newRelationTitle.name\"></p>\
							<p>Name Plural:<br/> <input type=\"text\" v-model=\"newRelationTitle.name_plural\"></p>\
							<p>Preposition:<br/> <input type=\"text\" v-model=\"newRelationTitle.preposition\"></p>\
						</div>\
						<div class=\"column\" >\
							<p>Description:</p>\
							<textarea rows=\"8\" style=\"width:100%;\" v-model=\"newRelationTitle.description\"></textarea>\
							<button @click=\"createNewRelationTitle(groupTypeCurrent.id)\" >Create</button>\
						</div>\
					</div>\
					<br/><br/>\
				</div>\
				<div @input=\"updateRelationTitle(t)\" :class=\"t.ajax_status.class\" class=\"\" style=\"margin-bottom:1em;padding:0.5em;border: 1px gray solid\" v-for=\"t in groupTypeCurrent.titles\">\
					<p style=\"text-align:right\">{{ t.ajax_status.message }}</p>\
					<div class=\"row\">\
						<div class=\"column\" >\
							<p>Name:<br/> <input type=\"text\" v-model=\"t.name\"></p>\
							<p>Name Plural:<br/> <input type=\"text\" v-model=\"t.name_plural\"></p>\
							<p>Preposition:<br/> <input type=\"text\" v-model=\"t.preposition\"></p>\
						</div>\
						<div class=\"column\" >\
							<p>Description:</p>\
							<textarea rows=\"8\" style=\"width:100%;\" v-model=\"t.description\"></textarea>\
							{{ t.delete_question }}\
							{{ t.deleted }}\
							<p @click=\"t.delete_question = !t.delete_question\" style=\"text-align:right\">Delete this title</p>\
							<p v-if=\"t.delete_question == true\" >Do you want to delete the title {{ t.name }} for the group type {{ groupTypeCurrent.name }}? <button @click=\"deleteRelationTitle(t)\" >Yes</button> <button @click=\"t.delete_question = !t.delete_question\" >No</button></p>\
						</div>\
					</div>\
				</div>\
			</div>\
		</div>\
		<div v-if=\"editMembers == true\" class=\"shader--full-screen\" style=\"align-items:flex-start;\">\
			<div style=\"width:80%;\">\
				<div class=\"rubrik cursor--pointer\" @click=\"editMembers = false\" style=\"font-size:2em;\" >&times;</div>\
				<div class=\"row\">\
					<div class=\"column\">\
						<h3 class=\"rubrik\" >Edit members and titles for {{ element.present_name }} <span class=\"cursor--pointer\" @click=\"showAddNew = !showAddNew\">+</span></h3>\
						<div v-if=\"showAddNew == true\" style=\"display:flex\">\
							Add Individual as Member: <autocomplete link-target=\"_blank\" :filter=\"'individ'\" :buttonMethod=\"'addBelow'\" :buttonText=\"'Add member'\"  ></autocomplete> <button @click=\"showAddNew = !showAddNew\">Cancel</button>\
							Add Group as Member: <autocomplete link-target=\"_blank\" :filter=\"'gruppe'\" :buttonMethod=\"'addBelow'\" :buttonText=\"'Add member'\"  ></autocomplete> <button @click=\"showAddNew = !showAddNew\">Cancel</button>\
						</div>\
					</div>\
				</div>\
				<div class=\"row\">\
					<div class=\"rubrik column\">\
						Member\
					</div>\
					<div class=\"rubrik column\">\
						Title\
					</div>\
					<div class=\"rubrik column\">\
						Membership start\
					</div>\
					<div class=\"rubrik column\">\
						Membership ended\
					</div>\
				</div>\
				\
				<h3 class=\"rubrik\" >Present Members</h3>\
				<membership-edit v-for=\"b in presentMembers\" :membership=\"b\" :key=\"b.id\" ></membership-edit>\
				<h3 class=\"rubrik\" >Past Members</h3>\
				<membership-edit v-for=\"b in pastMembers\" :membership=\"b\" :key=\"b.id\" ></membership-edit>\
				<h3 class=\"rubrik\" >Future Members</h3>\
				<membership-edit v-for=\"b in futureMembers\" :membership=\"b\" :key=\"b.id\" ></membership-edit>\
				<h3 class=\"rubrik\" >Members Beyond Chronology</h3>\
				<membership-edit v-for=\"b in membersBeyondChronolgy\" :membership=\"b\" :key=\"b.id\" ></membership-edit>\
				<br/><br/>\
			</div>\
		</div>\
	</div>",

    data: function() {
        return {
			editMembers: false,
			showAddNew: false,
			editGrouptype: false,
			editGrouptypeDetails: false,
			editGrouptypeAjaxWait: false,
			showCreateNewGroupType: false,
			newGroupTypeName: "",
			showAddNewRelationTitle: false,
			newRelationTitle: {
				name: "",
				name_plural: "",
				preposition: "",
				description: "",


			},
        }
    },
    methods: {
		createNewGroupType: function() {
			var self = this;
			var newGT = {
				universe: self.$store.state.initial.universeId,
				name: self.newGroupTypeName,
				description: "",
			};
			axios.post("/api/grouptype/create/",newGT).then(function(response){
				self.loadGroupTypesOfUniverse();
				self.createNewGroupType = false;

			}).catch(function(error) {
				console.log(error);
			});
		},
		updateGroupType: function(id) {
			var self = this;
			this.groupType.ajax_status.message = "Updating";

			axios.patch("/api/group/" + id + "/update/", { group_type: self.groupType.id }).then(function(response){
				self.groupType.ajax_status.message = "Changed";
			}).catch(function(error) {
				console.log(error);
			});

		},
		updateGroupTypeDetails: function (id){
			var updatedDetails = {
				"name": this.groupType.name,
				"description": this.groupType.description,
			};

			window.clearTimeout(this.timer);
			this.timer = window.setTimeout(function(){

				axios.patch("/api/grouptype/" + id + "/update/", updatedDetails).then(function(response){
					//self.groupType.ajax_status.message = "Changed";
				}).catch(function(error) {
					console.log(error);
				});

			},1000);

		},
		createNewRelationTitle: function (){
			var self = this;
			self.newRelationTitle.group_type = self.groupType.id;
			axios.post("/api/relationtitle/create/",self.newRelationTitle).then(function(response){
				self.loadGroupTypesOfUniverse();
				self.$store.dispatch("getElement");
				self.showAddNewRelationTitle = false;

				self.newRelationTitle = {
					name: "",
					name_plural: "",
					preposition: "",
					description: "",

				};

			}).catch(function(error) {
				console.log(error);
			});

		},
		updateRelationTitle: function(title){
			title.ajax_status = { "class":"alert alert--warning", "message": "Updating", "show_edit": true};
			console.log(title.id);

			window.clearTimeout(this.timer);
			this.timer = window.setTimeout(function(){

				axios.patch("/api/relationtitle/" + title.id + "/update/", title).then(function(response){
					title.ajax_status =  {"class":"alert alert--success", "message": "Updated", "show_edit": true	};

				}).catch(function(error) {
					console.log(error);
				});

			},1500);


		},
		deleteRelationTitle: function(t){
			axios.patch("/api/relationtitle/" + t.id + "/update/", { deleted: true }).then(function(response){
				title.ajax_status =  {"class":"alert alert--success", "message": "Updated"};

			}).catch(function(error) {
				console.log(error);
			});
		},
		loadGroupTypesOfUniverse: function() {
			var self = this;
			axios.get("/api/session/" + campaignId + "?d=us1_ages__cs1_universe__us1_campaigns__us1_name__us1_users__us1_kins__us1_group_types__gts1_titles&format=json").then(function(response) {
				self.$store.state.session = response.data;

			}).catch(function(error) {
				console.log(error);
			});

		},
		ingame_date_str: function(time){
			return this.$store.getters.ingame_date_str(time);
		},
		compareTime: function (time) {

			return this.$store.getters.ingame_date_interval(time,this.ingameNow);

		},
		compareTimeString: function (time){
			return this.$store.getters.ingame_date_interval_string(this.compareTime(time).data);
		},
		timeStart: function (el) {

			var start = {
				age: el.start_age,
				year: el.start_year,
				month: el.start_month,
				day: el.start_date,
				hour: el.start_hours,
				min: el.start_minutes,
				sec: el.start_seconds,
			};

			return start;

		},
		timeEnd: function (el) {

			var end = {
				age: el.end_age,
				year: el.end_year,
				month: el.end_month,
				day: el.end_date,
				hour: el.end_hours,
				min: el.end_minutes,
				sec: el.end_seconds,
			};

			return end;

		}

	},
	computed: {
		element: function (){
			return this.$store.state.element.masterdata;
		},
		membersWithInvalidTitles: function(){
			var self = this;
			var withInvalidTitles = function(m){
				//var i = -1;
				if(m.above_title != null) {
					var i = self.groupType.titles.findIndex(function(t){
						console.log(t.id, m.above_title.id);
						return t.id == m.above_title.id
					});
				}
				return i == -1;
			}

			return this.below.filter(withInvalidTitles);

	//		return null;
		},
		membersWithNoTitles: function (){

			var withNoTitle = function (m){
				return m.above_title == null && (m.below.type_spec == 'individ' || m.below.type_spec == 'player_character');
			};

			return this.below.filter(withNoTitle);


		},
		groupTypeCurrent: function(){
			var self = this;
			//console.log(self.groupType.id);
			var i = this.groupTypesOfUniverse.findIndex(function(g){ return g.id == self.groupType.id });
			console.log(i);
			return this.groupTypesOfUniverse[i];
		},
		groupTypesOfUniverse: function(){
			return this.$store.state.session.universe.group_types;
		},
		element: function (){
			return this.$store.state.element.masterdata;
		},
		ingameNow: function (){
			return this.$store.getters.ingameNow;
		},
		groupType: function(){

            return this.$store.state.element.masterdata.group_type;
        },
		below: function(){

            return this.$store.state.element.relations.below;
        },
		belowFuture: function (){
			var self = this;
			return this.below.filter(function(m){ self.compareTime(self.timeStart(m)).location == "after" });
		},
		belowPresent: function (){
			var self = this;
			return this.below.filter(function(m){
				console.log(self.compareTime(self.timeStart(m)).location);
				self.compareTime(self.timeStart(m)).location == "before" });
		},
		belowPast: function (){

		},
		belowBeyondChronolgy: function (){

		},
		membersWithTitles: function (){

			function hasTitle(mem) {
			    return mem.above_title != null;
			}

			return this.below.filter(hasTitle);

		},
		presentMembers: function(){
			var self = this;
			function isPresent(mem) {

				var startLoc = self.compareTime(self.timeStart(mem)).location;
				var endLoc = self.compareTime(self.timeEnd(mem)).location;

				var start = self.ingame_date_str(self.timeStart(mem));//self.compareTime(self.timeStart(mem)).location;
				var end = self.ingame_date_str(self.timeEnd(mem));//self.compareTime(self.timeEnd(mem)).location;


				return (startLoc == "now" || startLoc == "before") && (endLoc == "now" || endLoc == "after");
			}

			return this.below.filter(isPresent);

		},
		pastMembers: function(){
			var self = this;
			function isPast(mem) {

				var startLoc = self.compareTime(self.timeStart(mem)).location;
				var endLoc = self.compareTime(self.timeEnd(mem)).location;

				return (endLoc == "before");
			}

			return this.below.filter(isPast);

		},
		futureMembers: function(){
			var self = this;
			function isFuture(mem) {

				var startLoc = self.compareTime(self.timeStart(mem)).location;
				var endLoc = self.compareTime(self.timeEnd(mem)).location;

				return (startLoc == "after");
			}

			return this.below.filter(isFuture);

		},
		membersBeyondChronolgy: function(){
			var self = this;
			function hasNoTime(mem) {

				var start = self.ingame_date_str(self.timeStart(mem));//self.compareTime(self.timeStart(mem)).location;
				var end = self.ingame_date_str(self.timeEnd(mem));//self.compareTime(self.timeEnd(mem)).location;


				return (start == "Not set" && end == "Not set");
			}

			return this.below.filter(hasNoTime);

		}

	}
});



// OLD CODE

// <a v-if=\"element.user_permissions.write == true\" class=\"cursor--pointer rubrik\" @click=\"editMembers = !editMembers\" >Edit members and title of {{ element.present_name }}</a><br/>\
// <div>\
// 	<a v-if=\"element.user_permissions.write == true\" class=\"cursor--pointer rubrik\" @click=\"loadGroupTypesOfUniverse()\" >Edit the group type {{ groupType.name }}</a>\
// </div>\
