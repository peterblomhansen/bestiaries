var names = Vue.component('names', {
	template: "<div>\
	<transition name=\"fade\">\
		<div class=\"placeholder--name\" v-if=\"!presentName\">\
			<div class=\"placeholder--dropcap\" > </div>\
			<div>\
				<div class=\"placeholder--h1\"></div>\
				<br>\
				<div class=\"placeholder--p\"></div>\
				<div class=\"placeholder--p\"></div>\
			</div>\
		</div>\
	</transition >\
	<transition name=\"fade\">\
		<div style=\"display: inline-block;\" v-if=\"presentName\">\
			<h1 class=\"ink initial\">{{ presentName }}</h1>\
			<p class=\"rubrik\" v-if=\"otherNames.length > 0\">Also known as <span class=\"ink\" v-for=\"(n,i) in otherNames\" >{{ n.name }}<span v-if=\"i + 1 < otherNames.length\">, </span> </span> <button @click=\"toggleEditNames()\" class=\"button--small\"  >edit names</button>\</p>\
			<p v-else><button @click=\"toggleEditNames()\" class=\"button--small\"  >edit names</button></p>\
			<transition name=\"fade-fold\"  >\
				<overlay v-if=\"showEditNames.paper == true\"\ >\
					<p style=\"text-align:right;\" ><button @click=\"toggleEditNames()\" class=\"button--close-modal\"  style=\"font-size:200%;\">&times;</button></p>\
					<div class=\"\">\
						<h2 class=\"rubrik\">Edit names of <strong class=\"ink\">{{ presentName.name }}</strong></h2>\
						<edit-name v-for=\"(n,i) in names.results\" :nameObject=\"n\" :key=\"n.id\" v-if=\"n.edit_permissions == true\" ></edit-name>\
						<h2 class=\"rubrik\">Give <strong class=\"ink\">{{ presentName.name }}</strong>  a new name</h2>\
						<edit-name :nameObject=\"newName\"  ></edit-name>\
						<h2 class=\"rubrik\">Other names</h2>\
						<div class=\"border--content\">\
							<div v-if=\"n.edit_permissions == false\" v-for=\"(n,i) in names.results\" >\
								<p class=\"ink\"><strong>{{ n.name }}</strong></p>\
								<p class=\"ink\" ><strong class=\"rubrik\" >Author: </strong>{{ authorName(n.author) }}</p>\
							</div>\
						</div>\
					</div>\
				</overlay>\
			</transition>\
		</div>\
	</transition>\
	</div>",
	mounted: function (){
		//this.$store.commit("getNames");
	},
    data: function() {
        return {
			showEditNames: { "overlay": false, "paper": false},
			newName: new NameModel(),
        }
    },
    methods: {
		authorName: function (id){
			var user = this.$store.getters.getUser(id);
			if (user) {
				return user.first_name + " " + user.last_name;
			}else {
				return "Unknown author";
			}
		},
		toggleEditNames: function () {
			this.showEditNames.overlay = !this.showEditNames.overlay;
			this.showEditNames.paper = !this.showEditNames.paper;
			this.$store.commit("getEmptyNameObject");
		},	
	},
	computed: {
		id: function () {
			return this.$store.state.element.id;
		},
		presentName: function(){
			if(this.$store.state.element.names.count > 0) {
				if (this.$store.state.element.masterdata.present_name) {
					var id = this.$store.state.element.masterdata.present_name.id;
					if(id == 0) {
						return "No name found";
					}
					else {
						var nameFound = _.find(this.$store.state.element.names.results,function(n){ return n.id == id });
						if(nameFound) {
							return nameFound.name;
						}
						else { return "No name found" }
					}
				}
				else if (this.$store.state.element.names.results) { return this.$store.state.element.names.results[0].name; }
			}
			else { return "No names known"; }
		},
		otherNames: function (){
			if(this.$store.state.element.masterdata.present_name) {
				var id = this.$store.state.element.masterdata.present_name.id;
				return _.filter(this.$store.state.element.names.results,function(n){ return n.id != id });			
			}
			if(this.$store.state.element.names.results) {
				var self = this;
				return _.filter(this.$store.state.element.names.results,function(n){ return self.$store.state.element.names.results.indexOf(n) !== 0; });			
			}			
			return [];
		},
		names: {
			get: function(){
				return this.$store.state.element.names;			
			},
			set: function (val) {
				this.$store.state.element.names = val;
			},
		},
		// newName: {
		// 	get: function(){
		// 		return this.$store.state.new.name;			
		// 	},
		// 	set: function (val) {
		// 		this.$store.state.state.new.name = val;
		// 	},
		// },		
	},
});
