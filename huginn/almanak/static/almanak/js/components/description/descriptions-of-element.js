var descriptionsOfElement = Vue.component('descriptions-of-element', {
	template: "\
	<section class=\"column\">\
		<div v-if=\"!$store.state.element.relations.descriptions\" >\
			<loading-spinner></loading-spinner>\
		</div>\
		<div class=\"column column-content\">\
			<transition-group name=\"fade\" tag=\"div\">\
				<div v-for=\"d in descriptionsDistributed.left\" :key=\"d.id\" >\
					<image-show v-if=\"d.description && d.description.url !== null\" :data=\"d\"  ></image-show>\
					<description-show v-if=\"d.description && d.description.url === null\" :data=\"d\"  ></description-show>\
				</div>\
			</transition-group>\
		</div>\
		<div class=\"column column-content\">\
			<transition-group name=\"fade\" tag=\"div\">\
				<div v-for=\"d in descriptionsDistributed.right\" :key=\"d.id\" >\
					<image-show v-if=\"d.description && d.description.url !== null\" :data=\"d\" ></image-show>\
					<description-show v-if=\"d.description && d.description.url === null\" :data=\"d\" ></description-show>\
				</div>\
			</transition-group>\
			<button v-if=\"showGetMore\" @click=\"getMoreDescriptions(page)\">More</button>\
		</div>\
	</section>",
	mounted: function (){
	//
	},
	data: function () {
		return {
			page: 1,
		};
	},
	computed: {
		descriptionsDistributed: function (){
			var desLeft = [];
			var desRight = [];
			var self = this; 
			_.each(this.descriptions,function(d,i){
				if(i % 2 == 0) {
					desLeft.push(d);
				}else if (self.descriptions.length > 1) {
					desRight.push(d);
				}
			});	
			return { left: desLeft, right: desRight };
		},
		descriptions: function (){
			if(this.$store.state.element.relations.descriptions) {
				return this.$store.state.element.relations.descriptions.results;
			}else {
				return [];
			}
		},
		totalDescriptions: function (){
			if(this.$store.state.element.relations.descriptions) {
				return this.$store.state.element.relations.descriptions.count;
			}else {
				return 0;
			}
		},
		showGetMore: function (){
			if(this.page * 10 > this.totalDescriptions) {
				return false;
			}else {
				return true;
			}
		}
	},
	methods: {
		getMoreDescriptions: function() {
			this.page++;
			this.$store.dispatch("getDescriptionsOfElement", { page: this.page });

		},
	}

});

