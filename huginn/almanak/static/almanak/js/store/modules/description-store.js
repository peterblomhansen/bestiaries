var descriptionStore = {
    state: {
        user_can_read: undefined,
        //descriptionFull: {},
        
    },
    actions: {
        getDescription: function(context){
            var self = this;
            axios.get("/api/new/description/" + context.state.id + "?d=author__ingame_author__can_user_edit__ds1ia_present_name__campaigns__ds1c_present_name&format=json")
                .then(function (response) {
                    Vue.set(context.state,"descriptionFull",response.data);
                    context.state.user_can_read = true;
                })
                .catch(function (error) {
                    if(error.response.status == "403") {
                       context.state.user_can_read = false;
                    }
                    console.log(error.errorCode);
                });
        },
        getDescriptionElementRelations: function(context,load){
            var self = this;
            var pageParam = "";
            if(load && load.page) {
                pageParam = "&page=" + load.page
            }  
            axios.get("/api/new/description-element-relation/?description__id=" + context.state.id + "&ordering=-relevance&d=element__ders1e_present_name" + pageParam + "&format=json")
                .then(function (response) {
                    if(load && load.page) {
                        _.each(response.data.results,function(r){
                            context.state.elementRelations.results.push(r);
                        })
                    }else {
                        Vue.set(context.state,"elementRelations",response.data);

                    }

                })
                .catch(function (error) {
                    console.log(error);
                });

        },
    }

}