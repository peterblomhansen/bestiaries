function ElementItemRelationModel(
	data = {
		owner: null,
		owned: null,
		number_of_items: null,
		ingame_age: null,
		ingame_year: null,
		ingame_month: null,
		ingame_date: null,
		ingame_hour: null,
		ingame_minute: null,
		ingame_second: null 
	}

){
	this.owner = data.owner;
	this.owned = data.owned;
	this.number_of_items = data.number_of_items;
	this.ingame_age = data.ingame_age;
	this.ingame_year = data.ingame_year;
	this.ingame_month = data.ingame_month;
	this.ingame_date = data.ingame_date;
	this.ingame_hour = data.ingame_hour;
	this.ingame_minute = data.ingame_minute;
	this.ingame_second = data.ingame_second;

}
