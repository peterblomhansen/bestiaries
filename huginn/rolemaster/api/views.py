from rest_framework.generics import ListAPIView, RetrieveAPIView, UpdateAPIView, RetrieveUpdateAPIView, CreateAPIView,DestroyAPIView
from rest_framework.views import APIView
from rest_framework.fields import SerializerMethodField
from rest_framework.response import Response
from django.db.models import Q
from almanak.models import Individual
from rolemaster.logic.charactersheet import SkillRows
from django_filters.rest_framework import DjangoFilterBackend


from rolemaster.models import (
	ExperiencePoint,
	RolemasterCharacter,
	StatBonusDistribution,
	Skill,
	SkillArea,
	SkillComment,
	DevelopedSkill,
	SkillRow,
	SelectedSkill,
	ProfessionSkillCost,
	SkillRank,
	Profession,
	BodyDevelopment,
	SkillMiscBonus
)



from .serializers import (
	ExperiencePointSerializer,
	BodyDevelopmentSerializer,
	RolemasterCharacterSerializer,
	RolemasterCharacterCreateSerializer,
	StatBonusDistributionSerializer,
	StatsSerializer,
	SkillSerializer,
	SkillAreaSerializer,
	DevelopedSkillSerializer,
	SkillRowSerializer,
	ProfessionSkillCostSerializer,
	ProfessionMinimalSerializer,
	RolemasterCharacterUpdateSerializer,
	SelectedSkillSerializer,
	SkillMiscBonusSerializer,
	SkillCommentSerializer

)
from rest_framework.permissions import (

    AllowAny,
    IsAdminUser,
    IsAuthenticated,
    IsAuthenticatedOrReadOnly

)
from .filters import DevelopedSkillFilter
from rest_framework import viewsets

class DevelopedSkillViewset(viewsets.ModelViewSet):
	model = DevelopedSkill
	queryset = DevelopedSkill.objects.all()
	serializer_class = DevelopedSkillSerializer
	#pagination_class = TenPerPagePagination
	filter_backends = (DjangoFilterBackend,)
	filterset_class = DevelopedSkillFilter


class SkillRowsView(APIView):

	data = {}
	character = {}


	def get_data(self,request, pk):
		u = request.session.get("universe_current")

		self.character = RolemasterCharacter.objects.select_related("profession").prefetch_related("profession__skill_costs","profession__skill_costs__skill","weapon_category_priority").get(id=pk)

		print(self.character.profession)
		#data = {}
		#data.skills =  []
		self.data['skills'] = Skill.objects.prefetch_related("supported_area_types","skill_origin_for","skill_target_for").all()
		self.data['areas'] = SkillArea.objects.select_related("universe","skill").filter(Q(universe__id=u)|Q(universe=None),deleted=False)
		self.data['ranks'] = DevelopedSkill.objects.select_related("character","skill","area").prefetch_related("character","skill","area").filter(character__id=pk)
		self.data['costs'] = []
		if self.character.profession:
			self.data['costs'] = self.character.profession.skill_costs


		return self.data

	def get(self, request, pk,format="json"):
		data = self.get_data(request,pk)

		sr = SkillRows(self.data,self.character)
		skill_rows = sr.generate_skillrows()
		serializer = SkillRowSerializer(skill_rows,many=True)

		#print(self.data['costs'].get_queryset())

		return Response(serializer.data)



class CharacterView(APIView):
	#queryset = RolemasterCharacter.objects.prefetch_related("dev_ranks","names","stats")
	serializer_class = RolemasterCharacterSerializer
	model = RolemasterCharacter

	# def get_object(self,pk):
	# 	return RolemasterCharacter.objects.prefetch_related("dev_ranks","names","stats").get(pk=pk)
	#
	# def get(self,request,pk):
	#
	# 	obj = self.get_object(pk).
	# 	return Response(self.serializer_class(obj).data)

	def get_object(self, pk):
		try:
			return self.model.objects.prefetch_related("dev_ranks","names","stats","selected_skills","skill_misc_bonuses","character_experience","skill_comments","stat_special_bonuses","profession__level_bonuses_of_profession").get(pk=pk)
		except self.model.DoesNotExist:
			raise Http404

	def get(self, request, pk,format="json"):
	    model     = self.get_object(pk)
	    #params = request.GET.get('d', None)
	    serializer = self.serializer_class(model,context={'request': request})

	    return Response(serializer.data)

class StatBonusDistributionView(ListAPIView):
	queryset = StatBonusDistribution.objects.all()
	serializer_class = StatBonusDistributionSerializer


class Race(RetrieveAPIView):
	queryset = StatBonusDistribution.objects.all()
	serializer_class = StatBonusDistributionSerializer


class SkillListView(ListAPIView):
	queryset = Skill.objects.prefetch_related("supported_area_types").all()
	serializer_class = SkillSerializer

class SkillAreaListView(ListAPIView):
	#queryset = SkillArea.objects.all()
	serializer_class = SkillAreaSerializer

	def get_queryset(self):

		u = self.request.session.get("universe_current")

		return SkillArea.objects.filter(Q(universe=u)|Q(universe=None),deleted=False)


class ProfessionListView(ListAPIView):
	queryset = Profession.objects.all()
	serializer_class = ProfessionMinimalSerializer




# CREATE VIEWS

class StatsCreateView(CreateAPIView):

	serializer_class = StatsSerializer

class ProfessionCreateView(CreateAPIView):

	serializer_class = ProfessionMinimalSerializer

class SelectedSkillCreateView(CreateAPIView):

	serializer_class = SelectedSkillSerializer



class RolemasterCharacterCreateView(APIView):


	def post(self,request,*args,**kwargs):


		pk = request.data["id"]
		print(pk)

		individual = Individual.objects.get(id=pk)

		# Create a Rolemaster Character using existing Individual
		rm_character = RolemasterCharacter(individual_ptr=individual)
		rm_character.__dict__.update(individual.__dict__)
		rm_character.save()

		return Response("Character Created")



class DevelopedSkillCreateView(CreateAPIView):
	serializer_class = DevelopedSkillSerializer

class SkillCostCreateView(CreateAPIView):
	serializer_class = ProfessionSkillCostSerializer

class BodyDevelopmentCreateView(CreateAPIView):
	serializer_class = BodyDevelopmentSerializer

class SkillMiscBonusCreateView(CreateAPIView):
	serializer_class = SkillMiscBonusSerializer

class SkillCommentCreateView(CreateAPIView):
	#queryset = SkillComment.objects.all()
	serializer_class = SkillCommentSerializer

class ExperiencePointCreateView(CreateAPIView):
	#queryset = SkillComment.objects.all()
	serializer_class = ExperiencePointSerializer


# UPDATE VIEWS

class BodyDevelopmentUpdateView(UpdateAPIView):
	queryset = BodyDevelopment.objects.all()
	serializer_class = BodyDevelopmentSerializer


class DevelopedSkillUpdateView(UpdateAPIView):
	queryset = DevelopedSkill.objects.all()
	serializer_class = DevelopedSkillSerializer


class SkillCostUpdateView(UpdateAPIView):
	queryset = ProfessionSkillCost.objects.all()
	serializer_class = ProfessionSkillCostSerializer



class RolemasterCharacterUpdateView(UpdateAPIView):
	queryset = RolemasterCharacter.objects.all()
	serializer_class = RolemasterCharacterUpdateSerializer


class SelectedSkillUpdateView(UpdateAPIView):
	queryset = SelectedSkill.objects.all()
	serializer_class = SelectedSkillSerializer

class SkillMiscBonusUpdateView(UpdateAPIView):
	queryset = SkillMiscBonus.objects.all()
	serializer_class = SkillMiscBonusSerializer

class SkillCommentUpdateView(UpdateAPIView):
	queryset = SkillComment.objects.all()
	serializer_class = SkillCommentSerializer

class ExperiencePointUpdateView(UpdateAPIView):
	queryset = ExperiencePoint.objects.all()
	serializer_class = ExperiencePointSerializer

# DELETE VIEWS


# class RolemasterCharacterCreateView(CreateAPIView):
#
# 	serializer_class = RolemasterCharacterCreateSerializer

#class RolemasterCharacterCreateView(APIView):

	# #serializer_class = RolemasterCharacterCreateSerializer
    #
    #
	# def post(self,request):
    #
	# 	serializer = RolemasterCharacterCreateSerializer(data=request.data)
	# 	if serializer.is_valid():
	# 	    serializer.save()
	# 	    return Response(serializer.data, status=status.HTTP_201_CREATED)
	# 	return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
