var tocStore = {
	state: {
		bestiary: {
			counts: {
				elements: null,
				descriptions: null,
				individuals: null,
				groups: null,
				places: null,
				events: null,
				roleplayingCampaigns: null,
				roleplayingSessions: null,
				playerCharacters: null,
			}
		},
		campaign: {
			sessions: null,
		}
	},
	actions: {
		tocGetElementCounts: function (context) {
			
			axios.get("/api/new/element/").then(function(response){
				context.commit("setElementCount", { type: "elements", count: response.data.count });
			});
			axios.get("/api/new/individual/").then(function(response){
				context.commit("setElementCount", { type: "individuals", count: response.data.count });
			})
			axios.get("/api/new/player_character/").then(function(response){
				context.commit("setElementCount", { type: "playerCharacters", count: response.data.count });
			})			
			axios.get("/api/new/group/").then(function(response){
				context.commit("setElementCount", { type: "groups", count: response.data.count });
			});	
			axios.get("/api/new/place/").then(function(response){
				context.commit("setElementCount", { type: "places", count: response.data.count });
			});
			axios.get("/api/new/event/").then(function(response){
				context.commit("setElementCount", { type: "events", count: response.data.count });
			});	
			axios.get("/api/new/rpg_session/").then(function(response){
				context.commit("setElementCount", { type: "roleplayingSessions", count: response.data.count });
			});	
			axios.get("/api/new/campaign/").then(function(response){
				context.commit("setElementCount", { type: "roleplayingCampaigns", count: response.data.count });
			});		
			axios.get("/api/new/description/").then(function(response){
				context.commit("setElementCount", { type: "descriptions", count: response.data.count });
			});		
		},
		tocGetRpgSessionsOfCurrentCampaign: function (context){
            var campaignId = context.rootState.generic.session.base.campaignId;

			axios.get("/api/new/rpg_session/?in_campaign__id=" + campaignId + "&ordering=-offgame_start&d=present_name__participants&format=json").then(function(response){
				context.commit("setRpgSessionsOfCurrentCampaign",response.data);
			});
		},
		tocPlayerCharactersOfSession: function (context,id){
			axios.get("/api/new/player-character/?experiences__event__id__in=" + id + "&d=present_name__roleplayer&format=json").then(function(response){
				context.commit("setRpgSessionParticipation",response.data);
			});			
		},	
	},
	mutations: {
		setElementCount: function (state,payload) {
			Vue.set(state.bestiary.counts,payload.type,payload.count);

		},
		setRpgSessionsOfCurrentCampaign: function (state,payload) {
			Vue.set(state.campaign,"sessions",payload);

		},
		setRpgSessionParticipation: function (state,payload){
			var sessionIndex 
			Vue.set(state.campaign,"sessions",payload);

		},
	},
}