var userStatus = Vue.component('user-status', {
    template: "\
	<div v-if=\"!_.isEmpty($store.state.generic.session.full)\" style=\"text-align:right\" >\
		<p class=\"rubrik\" ><a href=\"/accounts/profile/\">{{ user.first_name }} {{ user.last_name }}</a></p>\
		<p><a class=\"rubrik\" href=\"/logout\">Log Out</a></p>\
	</div>",
	mounted: function (){
		// var self = this;
		// axios.get('/api/session/?d=es1_present_name')	
		// 	.then(function (response) {
		// 		self.$store.state.session = response.data;
		// 		self.$store.state.search.element.scope = response.data.element_search_scope;

		// 	})
		// 	.catch(function (error) {
		// 	  console.log(error);
		// 	});			
	},
	computed: {
		user: function () {
			return this.$store.state.generic.session.full.user_details;
		},
		
	},
	
	
});
