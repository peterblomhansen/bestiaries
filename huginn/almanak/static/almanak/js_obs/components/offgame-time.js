var offgameTime = Vue.component('offgame-time', {
	template: "\
	<div>\
		<p class=\"rubrik\" >\
			<strong>Session played:</strong>\
			{{ sessionTimeSpan }} 			<img v-if=\"element.user_permissions.write == true\" @click=\"editOffgameTime = !editOffgameTime\" :src=\"st + 'images/icons/pencil.svg'\" style=\"cursor:pointer;height:1em;\" />\
			</p>\
		<div v-if=\"editOffgameTime == true\" >\
			<table>\
				<thead>\
					<tr><td></td><td>Date</td><td>Time</td></tr>\
				</thead>\
				<tbody @change=\"updateOffgameDateTime()\">\
					<tr><td>Offgame Start</td><td><input class=\"input--narrow-height\" type=\"date\" v-model=\"startDate\" /></td><td> <input class=\"input--narrow-height\" type=\"text\" v-model=\"startTime\" /></td></tr>\
					<tr><td>Offgame End</td><td><input class=\"input--narrow-height\" type=\"date\" v-model=\"endDate\" /></td><td> <input class=\"input--narrow-height\" type=\"text\" v-model=\"endTime\" /></td></tr>\
				</tbody>\
			</table>\
		</div>\
	</div>\
	",
    data: function() {
         return {
			 editOffgameTime: false,
		 }
    },
	computed: {
		sessionTimeSpan: function () {
			var start = this.dateArray(this.element.offgame_start);
			var end = this.dateArray(this.element.offgame_end);
            console.log(start,end);
			var oneDayEvent = true;
            var timeSpan = "";
            
			if(start.date != end.date || start.month != end.month || start.year != end.year) {
				oneDayEvent = false;
			}
			if(oneDayEvent == true) {
                timeSpan = start.date + " " + this.monthName(start.month) + " " +  start.year + " - " + start.hour + ":" + start.minute + " to " +  end.hour + ":" + end.minute;
			}
			else {
                timeSpan = start.date + " " + this.monthName(start.month) + " " +  start.year + " to " + end.date + " " + this.monthName(end.month) + " " +  end.year;
            }
			
            console.log(oneDayEvent);
			
            return timeSpan;

		},
		element: function () {
			return this.$store.state.element.masterdata;
		},
		st: function (){
			return this.$store.state.initial.staticUrl;
		},
		isEditor: function (){
			return this.$store.state.element.user_permissions.write;
		},
		startDate: {
			get: function (){
				return this.element.offgame_start.split("T")[0];
			//return this.offgameTimeObject(this.element.offgame_start);
			},
			set: function (nv){
				console.log(nv);
				this.element.offgame_start = nv + "T" + this.element.offgame_start.split("T")[1];
			},
		},
		startTime: {
			get: function (){
				return this.element.offgame_start.split("T")[1].split("Z")[0];
			//return this.offgameTimeObject(this.element.offgame_start);
			},
			set: function (nv){
				console.log(nv);
				this.element.offgame_start = this.element.offgame_start.split("T")[0] + "T" + nv + "Z";
			},
		},
		endDate: {
			get: function (){
				return this.element.offgame_end.split("T")[0];
			//return this.offgameTimeObject(this.element.offgame_start);
			},
			set: function (nv){
				console.log(nv);
				this.element.offgame_end = nv + "T" + this.element.offgame_end.split("T")[1];
			},
		},
		endTime: {
			get: function (){
				return this.element.offgame_end.split("T")[1].split("Z")[0];
			//return this.offgameTimeObject(this.element.offgame_start);
			},
			set: function (nv){
				console.log(nv);
				this.element.offgame_end = this.element.offgame_end.split("T")[0] + "T" + nv + "Z";
			},
		},
		end: function (){
			return this.offgameTimeObject(this.element.offgame_end);
		},
	},
    methods: {
		updateOffgameDateTime: function () {
			var self = this;
			var offgameTime = { 
				"offgame_end": this.element.offgame_end, 
				"offgame_start": this.element.offgame_start
				
			};
			setTimeout(function() {
				axios.patch("/api/rpg_element/" + self.element.id + "/update/",offgameTime).then(function(response){

				});
			},1000);
		},
		monthName: function(monthNum){
			monthNumInt = parseInt(monthNum);
			var months = [
				"None",
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December",
			];

			return months[monthNumInt];

		},
		dateArray: function(dt) {
			if (dt != null) {
				var date = dt.split("T")[0];
				var time = dt.split("T")[1];

				return {
					date: date.split("-")[2],
					month: date.split("-")[1],
					year: date.split("-")[0],
					hour: time.split(":")[0],
					minute: time.split(":")[1],
					second: time.split(":")[2],
				};

			}
			else {
				return {
					date: null,
					month: null,
					year: null,
					hour: null,
					minute: null,
					second: null,
				};
			}

		},
// 		offgameTimeString: function (obj) {
// 			console.log(obj);
// 			var dataTimeString = "";
// 			dataTimeString = obj.date.year + "-" + obj.date.month + "-" + obj.date.day + "T" + obj.time.hours + ":" + obj.time.minutes + ":" + obj.time.seconds + "Z";
// 
// 			return dateTimeSting;
// 
// 		},
// 		offgameTimeObject: function (dateTimeString) {
// 
// 			var time = {
// 				hours: parseInt(dateTimeString.split("T")[1].split(":")[0]),
// 				minutes: parseInt(dateTimeString.split("T")[1].split(":")[1]),
// 				seconds: parseInt(dateTimeString.split("T")[1].split(":")[2].split("Z")[0]),
// 			}
// 
// 			var date = {
// 				year: parseInt(dateTimeString.split("T")[0].split("-")[0]),
// 				month: parseInt(dateTimeString.split("T")[0].split("-")[1]),
// 				day: parseInt(dateTimeString.split("T")[0].split("-")[2]),
// 			}
// 
// 			return { "date": date, "time": time };
// 		},
//         timestampToValues: function(ts) {
//             var datevalues = false;
//             if (ts > 0) {
//                 date = new Date(ts * 1000), datevalues = {
//                     year: date.getFullYear(),
//                     month: date.getMonth() + 1,
//                     date: date.getDate(),
//                     hour: date.getHours(),
//                     minute: date.getMinutes(),
//                     second: date.getSeconds(),
//                 };
//             }
//             return datevalues;
//         },
//         timeToHuman: function(ts) {
//             var dateString = "Not set";
//             var to = this.timestampToValues(ts);
//             if (to != false) {
//                 var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
//                 var mIndex = parseInt(to.month) - 1;
//                 var ds = to.date + "th";
//                 if (to.date == 1) {
//                     ds = to.date + "st";
//                 }
//                 if (to.date == 2) {
//                     ds = to.date + "nd";
//                 }
//                 if (to.date == 3) {
//                     ds = to.date + "rd";
//                 }
//                 dateString = monthNames[mIndex] + " " + ds + " " + " " + to.year;
//             }
//             return dateString;
//         },
//         updateOffgameTime: function() {}
    }
});
