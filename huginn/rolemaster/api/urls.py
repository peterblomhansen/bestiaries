from django.conf.urls import url
from django.contrib import admin

from .views import(
	ExperiencePointCreateView,
	ExperiencePointUpdateView,
	BodyDevelopmentCreateView,
	BodyDevelopmentUpdateView,
	CharacterView,
	StatBonusDistributionView,
	StatsCreateView,
	SkillListView,
	SkillAreaListView,
	SkillMiscBonusCreateView,
	SkillMiscBonusUpdateView,
	RolemasterCharacterCreateView,
	DevelopedSkillCreateView,
	DevelopedSkillUpdateView,
	SkillRowsView,
	SkillCostCreateView,
	SkillCostUpdateView,
	SkillCommentCreateView,
	SkillCommentUpdateView,
	ProfessionListView,
	ProfessionCreateView,
	RolemasterCharacterUpdateView,
	SelectedSkillCreateView,
	SelectedSkillUpdateView,
	DevelopedSkillViewset
)
from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

router = DefaultRouter()

router.register(r'devskill', DevelopedSkillViewset)


urlpatterns = [

    url(r'^viewset/', include(router.urls)),

	# GET VIEWS
	url(r'^character/(?P<pk>[0-9]+)/$', CharacterView.as_view(),name="characterview" ),
	url(r'^statbonusdistribution/$', StatBonusDistributionView.as_view(),name="statsview" ),
	url(r'^skills/$', SkillListView.as_view(),name="skills_list_view" ),
	url(r'^skillareas/$', SkillAreaListView.as_view(),name="skill_area_list_view" ),
	url(r'^skillrows/(?P<pk>[0-9]+)/$', SkillRowsView.as_view(),name="skill_area_list_view" ),
	url(r'^profession/list/$', ProfessionListView.as_view(),name="professions_list_view" ),

	# CREATE VIEWS
	url(r'^stats/create/$', StatsCreateView.as_view(),name="stats_create_view" ),
	url(r'^rm_character/create/$', RolemasterCharacterCreateView.as_view(),name="rolemaster_character_create_view" ),
	url(r'^developedskill/create/$', DevelopedSkillCreateView.as_view(),name="rolemaster_character_create_view" ),
	url(r'^skillcost/create/$', SkillCostCreateView.as_view(),name="skill_cost_create_view" ),
	url(r'^profession/create/$', ProfessionCreateView.as_view(),name="skill_cost_create_view" ),
	url(r'^bodydevelopment/create/$', BodyDevelopmentCreateView.as_view(),name="body_development_create_view" ),
	url(r'^selectedskill/create/$', SelectedSkillCreateView.as_view(),name="selected_skill_create_view" ),
	url(r'^skillmiscbonus/create/$', SkillMiscBonusCreateView.as_view(),name="skill_misc_bonus_create_view" ),
	url(r'^skillcomment/create/$', SkillCommentCreateView.as_view(),name="skill_comment_create_view" ),
	url(r'^experiencepoint/create/$', ExperiencePointCreateView.as_view(),name="experience_points_create_view" ),



#	url(r'^charactersheet/(?P<pk>[0-9]+)/(?P<lvl>[0-9]+)/pdf/$', CharacterSheetPdfView.as_view(),name="charactersheetPDF" )

	# UPDATE VIEWS
	url(r'^developedskill/(?P<pk>[0-9]+)/update/$', DevelopedSkillUpdateView.as_view(),name="rolemaster_character_create_view" ),
	url(r'^skillcost/(?P<pk>[0-9]+)/update/$', SkillCostUpdateView.as_view(),name="skill_cost_update_view" ),
	url(r'^rm_character/(?P<pk>[0-9]+)/update/$', RolemasterCharacterUpdateView.as_view(),name="rolemaster_character_update_view" ),
	url(r'^bodydevelopment/(?P<pk>[0-9]+)/update/$', BodyDevelopmentUpdateView.as_view(),name="body_development_update_view" ),
	url(r'^selectedskill/(?P<pk>[0-9]+)/update/$', SelectedSkillUpdateView.as_view(),name="selected_skill_update_view" ),
	url(r'^skillmiscbonus/(?P<pk>[0-9]+)/update/$', SkillMiscBonusUpdateView.as_view(),name="skill_misc_bonus_update_view" ),
	url(r'^skillcomment/(?P<pk>[0-9]+)/update/$', SkillCommentUpdateView.as_view(),name="skill_comment_update_view" ),
	url(r'^experiencepoint/(?P<pk>[0-9]+)/update/$', ExperiencePointUpdateView.as_view(),name="experience_points_update_view" ),

]
