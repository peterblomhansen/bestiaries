from django.db import models
from almanak.models import Kin, Individual, Element, Universe, Event

#############################################################
## MODELS FOR ROLEMASTER CHARACTERSHEET FOR BESTIARIES.ORG ##
#############################################################


## -- MODELS INHERITITED FROM BESTIARIES.ALMANAK MODELS -- ##

# ROLEMASTER abstraction of the Bestiaries Individual Model Class
class RolemasterCharacter(Individual):
	level 			= models.IntegerField(default=0)
	race			= models.ForeignKey("Race",related_name="rm_characters_of_race",blank=True,null=True, on_delete=models.PROTECT)
	profession		= models.ForeignKey("Profession",related_name='characters_of_profession',blank=True,null=True, on_delete=models.PROTECT)

# ROLEMASTER abstraction of the Bestiaries Kin Model Class
class Race(Kin):
	hit_dice 		= models.IntegerField(default=8)
	luck 			= models.IntegerField(verbose_name="Luck",default=0)
	agility 		= models.IntegerField(verbose_name="Agility",default=0)
	constitution 	= models.IntegerField(verbose_name="Constitution",default=0)
	memory 			= models.IntegerField(verbose_name="Memory",default=0)
	reasoning 		= models.IntegerField(verbose_name="Reasoning",default=0)
	self_discipline = models.IntegerField(verbose_name="Self Discipline",default=0)
	strength 		= models.IntegerField(verbose_name="Strength",default=0)
	quickness 		= models.IntegerField(verbose_name="Quickness",default=0)
	presence 		= models.IntegerField(verbose_name="Presence",default=0)
	empathy 		= models.IntegerField(verbose_name="Empathy",default=0)
	intuition		= models.IntegerField(verbose_name="Intuition",default=0)




## -- MODELS IMPORTED REDIRECTLY FROM ROLEMASTER RULEBOOKS --##


# Rolemaster 2nd edition skill categories according to Rolemaster Companion II
ACADEMIC = 'Academic'
ANIMAL = 'Animal'
ATHLETIC = 'Athletic'
BODY_DEVELOPMENT = 'Body_development'
COMBAT = 'Combat'
CONCENTRATION = 'Concentration'
DEADLY = 'Deadly'
EVALUATION = 'Evaluation'
GENERAL = 'General'
GYMNASTIC = 'Gymnastic'
LINGUISTIC = 'Linguistic'
MAGICAL = 'Magical'
BASE_SPELL_CASTING = 'Base_spell_casting'
DIRECTED_SPELL = 'Directed_spell'
MEDICAL = 'Medical'
PERCEPTION = 'Perception'
SOCIAL = 'Social'
SUBTERFUGE = 'Subterfuge'
SURVIVAL = 'Survival'
CATEGORY_CHOICES = (
    (ACADEMIC,'Academic'),
    (ANIMAL,'Animal'),
    (ATHLETIC,'Athletic'),
    (BODY_DEVELOPMENT,'Body Development'),
    (COMBAT,'Combat'),
    (CONCENTRATION,'Concentration'),
    (DEADLY,'Deadly'),
    (EVALUATION,'Evaluation'),
    (GENERAL,'General'),
    (GYMNASTIC,'Gymnastic'),
    (LINGUISTIC,'Linguistic'),
    (MAGICAL,'Magical'),
    (BASE_SPELL_CASTING,'Base Spell Casting'),
    (DIRECTED_SPELL,'Directed Spell'),
    (MEDICAL,'Medical'),
    (PERCEPTION,'Perception'),
    (SOCIAL,'Social'),
    (SUBTERFUGE,'Subterfuge'),
    (SURVIVAL,'Survival'),
)


LU = "LU"
AG = "AG"
CO = "CO"
ME = "ME"
RE = "RE"
SD = "SD"
ST = "ST"
QU = "QU"
PR = "PR"
EM = "EM"
IN = "IN"

STAT_CHOICES = (
	(LU ,"Luck"),
	(AG, "Agility"),
	(CO, "Constitution"),
	(ME, "Memory"),
	(RE, "Reasoning"),
	(SD, "Self Discipline"),
	(ST, "Strength"),
	(QU, "Quickness"),
	(PR, "Presence"),
	(EM, "Empathy"),
	(IN, "Intuition"),
)

WEAPON_CATEGORY_CHOICES = (
	("polearm" ,"Polearm"),
	("missile" ,"Missile"),
	("1hslashing" ,"One Hand Slashing"),
	("1hconcussion" ,"One Hand Concussion"),
	("2h" ,"Two Handed"),
	("thrown" ,"Thrown"),
)


class StatBonusDistribution(models.Model):
	stat 			= models.IntegerField()
	bonus			= models.IntegerField()
	dev_points		= models.DecimalField(verbose_name="Development Points",max_digits=5,decimal_places=2)
	spell_points	= models.DecimalField(verbose_name="Spell Points",max_digits=5,decimal_places=2)


	def __str__(self):
		return str(self.stat) + " (" + str(self.bonus) + ")"

	class Meta:
		ordering = ("-stat",)


class ArmorType(models.Model):
	armor_type 		= models.IntegerField()
	min_mm			= models.IntegerField()
	max_mm			= models.IntegerField()
	missile_penalty	= models.IntegerField()
	armor_quickness_penalty = models.IntegerField()
	skill			= models.ForeignKey("Skill",related_name="covered_armor_types",blank=True,null=True, on_delete=models.PROTECT)


class Profession(models.Model):
	name			= models.CharField(max_length=50)
	prime_stat_1	= models.CharField(max_length=50,blank=True,null=True)
	prime_stat_2	= models.CharField(max_length=50,blank=True,null=True)


	def __str__(self):
		return self.name

	class Meta:
		ordering = ("name",)

class ProfessionLevelBonus(models.Model):
	profession		= models.ForeignKey("Profession",related_name="level_bonuses_of_profession", on_delete=models.PROTECT)
	category 		= models.CharField(max_length=20,choices=CATEGORY_CHOICES)
	bonus 			= models.IntegerField()

	def __str__(self):
		return str(self.profession) + " - " + str(self.category) + " - " + str(self.bonus)


class Stats(models.Model):
	character		= models.ForeignKey("RolemasterCharacter",related_name="stats", on_delete=models.PROTECT)
	level			= models.IntegerField()
	stat_type		= models.CharField(max_length=5)
	luck 			= models.IntegerField(verbose_name="Luck",default=0)
	agility 		= models.IntegerField(verbose_name="Agility",default=0)
	constitution 	= models.IntegerField(verbose_name="Constitution",default=0)
	memory 			= models.IntegerField(verbose_name="Memory",default=0)
	reasoning 		= models.IntegerField(verbose_name="Reasoning",default=0)
	self_discipline = models.IntegerField(verbose_name="Self Discipline",default=0)
	strength 		= models.IntegerField(verbose_name="Strength",default=0)
	quickness 		= models.IntegerField(verbose_name="Quickness",default=0)
	presence 		= models.IntegerField(verbose_name="Presence",default=0)
	empathy 		= models.IntegerField(verbose_name="Empathy",default=0)
	intuition		= models.IntegerField(verbose_name="Intuition",default=0)

	def __str__(self):
		return str(self.character) + " " + str(self.level) + " " + str(self.stat_type)



class StatSpecialBonus(models.Model):
	stat			= models.CharField(max_length=20,choices=STAT_CHOICES,blank=True,null=True)
	character		= models.ForeignKey("RolemasterCharacter",related_name="stat_special_bonuses", on_delete=models.PROTECT)
	bonus			= models.IntegerField()

	def __str__(self):
		return str(self.character) + " - " + str(self.stat) + " - " + str(self.bonus)



class Skill(models.Model):
	name 			= models.CharField(max_length=256)
	category 		= models.CharField(max_length=20,choices=CATEGORY_CHOICES)
	stat_1			= models.CharField(max_length=20,choices=STAT_CHOICES,blank=True,null=True)
	stat_2			= models.CharField(max_length=20,choices=STAT_CHOICES,blank=True,null=True)
	stat_3			= models.CharField(max_length=20,choices=STAT_CHOICES,blank=True,null=True)
	area_support	= models.BooleanField()

	def __str__(self):
		return self.name

	class Meta:
		ordering = ("category","name")


class SkillArea(models.Model):
	name 			= models.CharField(max_length=256)
	area_type		= models.CharField(max_length=50)
	skill			= models.ForeignKey("Skill",related_name="areas",blank=True,null=True, on_delete=models.PROTECT)
	almanak_page	= models.ForeignKey(Element,related_name="related_skill_areas",blank=True,null=True, on_delete=models.PROTECT)
	weapon_category	= models.CharField(max_length=50,choices=WEAPON_CATEGORY_CHOICES,blank=True,null=True)
	stat_1			= models.CharField(max_length=20,choices=STAT_CHOICES,blank=True,null=True)
	stat_2			= models.CharField(max_length=20,choices=STAT_CHOICES,blank=True,null=True)
	stat_3			= models.CharField(max_length=20,choices=STAT_CHOICES,blank=True,null=True)
	universe		= models.ForeignKey(Universe,related_name="areas_of_universe",blank=True,null=True, on_delete=models.PROTECT)
	deleted			= models.BooleanField(default=False)


	def __str__(self):
		un = "Cross Universe Area"
		if self.universe != None:
			un = self.universe.name
		return self.name + " - " + un


	class Meta:
		ordering = ["name"]


class SkillAreaType(models.Model):
	skill 			= models.ForeignKey("Skill",related_name="supported_area_types", on_delete=models.PROTECT)
	area_type		= models.CharField(max_length=50)


class ProfessionSkillCost(models.Model):
	profession 		= models.ForeignKey("Profession",related_name="skill_costs", on_delete=models.PROTECT)
	skill 			= models.ForeignKey("Skill", on_delete=models.PROTECT)
	cost_1			= models.IntegerField()
	cost_2			= models.IntegerField(blank=True,null=True)
	cost_3			= models.IntegerField(blank=True,null=True)


class DevelopedSkill(models.Model):
	skill 			= models.ForeignKey("Skill", on_delete=models.PROTECT)
	area			= models.ForeignKey("SkillArea",blank=True,null=True, on_delete=models.PROTECT)
	character 		= models.ForeignKey("RolemasterCharacter",related_name="dev_ranks", on_delete=models.PROTECT)
	level 			= models.IntegerField()
	num_ranks		= models.IntegerField()
	development_type = models.CharField(max_length=10,choices=(("dev","Developed"),("hobby","Hobby"),("gm","Gamemaster"),))


class BodyDevelopment(models.Model):
	character 		= models.ForeignKey("RolemasterCharacter",related_name="body_development_of_character", on_delete=models.PROTECT)
	level			= models.IntegerField()
	hits			= models.IntegerField()


class CharacterWeaponCategoryPriority(models.Model):
	character 		= models.ForeignKey("RolemasterCharacter",related_name="weapon_category_priority", on_delete=models.PROTECT)
	skill			= models.ForeignKey("Skill",related_name="character_weapon_priorities", on_delete=models.PROTECT)
	category 		= models.CharField(max_length=20,choices=WEAPON_CATEGORY_CHOICES)

	def __str__(self):
		return str(self.character) + " - " + str(self.skill) + " " + str(self.category)


class SelectedSkill(models.Model):
	skill 			= models.ForeignKey("Skill", on_delete=models.PROTECT)
	area			= models.ForeignKey("SkillArea",blank=True,null=True, on_delete=models.PROTECT)
	character 		= models.ForeignKey("RolemasterCharacter",related_name="selected_skills", on_delete=models.PROTECT)
	selected 		= models.BooleanField()


class SkillMiscBonus(models.Model):
	skill 			= models.ForeignKey("Skill", on_delete=models.PROTECT)
	area			= models.ForeignKey("SkillArea",blank=True,null=True, on_delete=models.PROTECT)
	character 		= models.ForeignKey("RolemasterCharacter",related_name="skill_misc_bonuses", on_delete=models.PROTECT)
	bonus 			= models.IntegerField()
	bonus_type		= models.CharField(max_length=50,default="misc")
	deleted			= models.BooleanField(default=False)

	def __str__(self):
		return str(self.character) + " - " + str(self.bonus) + " - " + str(self.skill) + " - " + str(self.area)


class SkillComment(models.Model):
	comment			= models.TextField()
	skill 			= models.ForeignKey("Skill",on_delete=models.PROTECT)
	area			= models.ForeignKey("SkillArea",blank=True,null=True,on_delete=models.PROTECT)
	character 		= models.ForeignKey("RolemasterCharacter",related_name="skill_comments",on_delete=models.PROTECT)
	deleted			= models.BooleanField(default=False)


	def __str__(self):
		return str(self.character) + " - " + str(self.comment) + " - " + str(self.skill) + " - " + str(self.area)


class SkillAreaSimilarity(models.Model):
	origin			= models.ForeignKey("SkillArea",related_name="area_origin_for", on_delete=models.PROTECT)
	target			= models.ForeignKey("SkillArea",related_name="area_target_for", on_delete=models.PROTECT)
	ratio 			= models.CharField(max_length=5)


	def __str__(self):
		return self.origin.name + " -> " + self.target.name + " (" + self.ratio + ")"


	class Meta:
		ordering = ["origin","target"]


class SkillSkillSimilarity(models.Model):
	origin			= models.ForeignKey("Skill",related_name="skill_origin_for", on_delete=models.PROTECT)
	target			= models.ForeignKey("Skill",related_name="skill_target_for", on_delete=models.PROTECT)
	ratio 			= models.CharField(max_length=5)

	def __str__(self):
		return self.origin.name + " -> " + self.target.name + " (" + self.ratio + ")"

	class Meta:
		ordering = ["origin","target"]



class ExperiencePoint(models.Model):
	amount 				= models.IntegerField()
	character 			= models.ForeignKey("RolemasterCharacter",related_name="character_experience",on_delete=models.PROTECT)
	comment				= models.TextField(blank=True)
	approved 			= models.IntegerField() # 0: default / klar til godkendelse; 1: tidligere godkendt men nu opdateret; 2: godkendt. Hvis beskrivelsen forkastes, bliver realtionen helt slettet.
	deleted				= models.BooleanField(default=False)
	created 			= models.DateTimeField(auto_now_add=True,blank=True,null=True)
	updated 			= models.DateTimeField(auto_now=True,blank=True,null=True)

class SanityLoss(models.Model):
	amount 				= models.IntegerField()
	character 			= models.ForeignKey("RolemasterCharacter",related_name="character_sanity_loss",on_delete=models.PROTECT)
	level 				= models.IntegerField()
	comment				= models.TextField()
	related_event		= models.ForeignKey(Event,blank=True, null=True, related_name="event_sanity_losses",on_delete=models.PROTECT)
	created 			= models.DateTimeField(auto_now_add=True,blank=True,null=True)
	updated 			= models.DateTimeField(auto_now=True,blank=True,null=True)


###########################################
# Non Database Objects For CharacterSheet #
###########################################

class SkillRow:

	def __init__(self,id,area,category,name,area_name,area_support,stat_1,stat_2,stat_3,cost,gm_ranks,hobby_ranks,dev_ranks,similarity = None, area_similarity = None, selected = False):

		self.id = id
		self.area = area
		self.category = category
		self.name = name
		self.area_name = area_name
		self.stat_1 = stat_1
		self.stat_2 = stat_2
		self.stat_3 = stat_3
		self.cost = cost
		self.gm_ranks = gm_ranks
		self.hobby_ranks = hobby_ranks
		self.dev_ranks = dev_ranks
		self.similarity = similarity
		self.area_similarity = area_similarity
		self.area_support = area_support
		self.selected = selected


	def __str__(self):
		return str(self.name) + " - " + str(self.area)


class SkillRank:

	def __init__(self, id = None, skill = 0, area = 0, character = None, level = 0, num_rank = 0, development_type = "",dev_points_used = 0):

		self.id 				= id
		self.skill 				= skill
		self.area				= area
		self.character 			= character
		self.level 				= level
		self.num_ranks			= num_rank
		self.development_type	= development_type
		self.dev_points_used	= dev_points_used





class ComputedSkillSkillSimilarity:

	def __init__(self, origin, target, ratio, area = None):

		self.origin		= origin
		self.target		= target
		self.ratio		= ratio
		self.area 		= area



class ComputedAreaSimilarity:

	def __init__(self, origin, target, ratio, skill):

		self.origin		= origin
		self.target		= target
		self.ratio		= ratio
		self.skill 		= skill
