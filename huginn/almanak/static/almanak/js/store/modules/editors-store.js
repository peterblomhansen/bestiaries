
// const editorStatus = {
//     UNCHAM: 'summer',
//     WINTER: 'winter',
//     SPRING: 'spring',
//     AUTUMN: 'autumn'
// }

var editorsStore = {
    state: {
        editorActive: null,
        descriptionEditor: {
            contentStatus: "passive", //   
            relationStatus: "passive", //
            //statusText: "No Changes yet",
        },
        imageEditor: {

        },
        newElementEditor: {

        },
    },
    mutations: {
        activateEditor: function (state,editor){
            Vue.set(state,"editorActive",editor);
        },
        loadDescriptionEditorData: function (state,load){
            Vue.set(state.descriptionEditor,"data",load);
            Vue.set(state,"editorActive","description");            
        },      
        updateDescriptionContent: function (state,load) {
            Vue.set(state.descriptionEditor.data,"content",load);
            Vue.set(state.descriptionEditor,"contentStatus","response");

        },
    },
    actions: {
        editorUpdateDescriptionContent: function(context, load) {

            axios.post("/api/sync-description-links/" + load.id + "/",{ content: load.content })
            .then(function(response){
                console.log(response);
                if(response.status === 202) { 
                    context.dispatch("editorGetDescriptionElementRelations", {id: load.id});
                }
            })
            .catch(function (error) {
                context.state.editorStatusKey = "error";
                console.log(error);
            });            

            axios.patch("/api/new/description/" + load.id + "/",{ content: load.content })
            .then(function(response){
                context.commit("updateDescriptionContent",response.data.content);
            })
            .catch(function (error) {
                context.state.editorStatusKey = "error";
                console.log(error);
            });

        },
        lockDescription: function(context,load) {
            var self = this;
            context.commit("loadDescriptionEditorData",null);
            Vue.set(context.state.descriptionEditor,"relations",null);
            axios.patch("/api/new/description/" + load.id + "/",{ locked_by_user: load.userId })
            .then(function(response){
            })
            .catch(function (error) {
                console.log(error);
            });
        },
        unloadDescriptionData: function(context,id) {
            axios.patch("/api/new/description/" + id + "/",{ locked_by_user: null})
            .then(function(response){
                Vue.set(context.state.descriptionEditor,"data",null);
                Vue.set(context.state,"editorActive",null);                

            });
        },        
        loadDescriptionData: function (context,load){
            axios.get("/api/new/description/" + load.id + "/?d=\
author__\
ingame_author__ds1ia_present_name__\
can_user_edit__\
shared_with__ds1sw_present_name__\
campaigns__ds1c_present_name&format=json"
                    )
            .then(function(response){
                context.commit("loadDescriptionEditorData",response.data);
                //context.dispatch("editorGetDescriptionElementRelations")
                //context.state.editorActive = "description";
            });
        },
        editorGetDescriptionElementRelations: function(context,load){
            var self = this;
            var pageParam = "";
            if(load && load.page) {
                pageParam = "&page=" + load.page
            }  
            Vue.set(context.state.descriptionEditor,"relationStatus","request");
            axios.get("/api/new/description-element-relation/?description__id=" + load.id + "&ordering=-relevance,element__names__name&d=element__ders1e_present_name" + pageParam + "&page_size=1000&format=json")
                .then(function (response) {
                    context.state.descriptionEditor.relationStatus = "response";
                        Vue.set(context.state.descriptionEditor,"relationStatus","response");
                        Vue.set(context.state.descriptionEditor,"relations",response.data);

                    // if(load && load.page) {
                    //     _.each(response.data.results,function(r){
                    //         context.state.descriptionEditor.relations.results.push(r);
                    //     })
                    // }else {
                    //     Vue.set(context.state.descriptionEditor,"relationStatus","response");
                    //     Vue.set(context.state.descriptionEditor,"relations",response.data);

                    // }

                })
                .catch(function (error) {
                    console.log(error);
                });

        },
        editorUpdateDescriptionElementRelation: function (context, load) {
            debugger;
            //context.state.descriptionEditor.relationStatus = "request";
            axios.patch("/api/new/description-element-relation/" + load.id + "/", load.data )
            .then(function (response){  
                var updatedRelation = _.find(context.state.descriptionEditor.relations,function(r){ return r.id == load.id })
                updatedRelation = response.data;
                context.state.descriptionEditor.relationStatus = "response";

            })
            .catch(function (error) {
                console.log(error);
                context.state.descriptionEditor.relationStatus = "error";
            });            
        },        
        updateDescription: function (context, load) {

            //context.state.descriptionEditor.contentStatus = "request";
            axios.patch("/api/new/description/" + load.id + "/", load)
            .then(function (response){
                context.dispatch("loadDescriptionData",load);

                //loadDescriptionData
                // console.log(load);
                // _.each(load,function(value,key){
                //     if(key !== 'id') {
                //         Vue.set(context.state.descriptionEditor.data,key,response.data.key);
                //     }//console.log(v,i);
                // });
                //context.state.descriptionEditor.data['content'] = response.data['content'];
                // context.state.descriptionEditor.contentStatus = "response";
                //return "updateDescription then";
            });
        }
    },
}