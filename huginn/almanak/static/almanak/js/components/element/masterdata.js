var masterdata = Vue.component('masterdata', {
	template: "<div>\
		<div v-if=\"canReadElement == false\" class=\"rubrik\" >You do not have permission to see details of this page</div>\
		<div v-if=\"$store.state.element.masterdata.id && canReadElement == true\">\
			<p class=\"rubrik\"><strong>\
				{{ publicSecretString }}  {{ element.masterdata.type_spec_obj.text.charAt(0).toUpperCase() + element.masterdata.type_spec_obj.text.slice(1) }}\
			</strong></p>\
			<p class=\"ink\"><strong class=\"rubrik\">\
				Author: </strong> <a :href=\"'/accounts/user/' + masterdata.author_full.id\" >{{ masterdata.author_full.first_name }} {{ masterdata.author_full.last_name }}</a> \
				<button v-if=\"element.permissions.write== true\" class=\"button--small\" @click=\"$store.commit('toggleEditElementPermissions')\" >edit permissions</button>\
			</p>\
			<p>Birth: {{ masterdata.birth_string }}</p>\
			<p>Death: {{ masterdata.death_string }}</p>\
		</div>\
	</div>",
	mounted: function (){
		//this.$store.commit("getMasterdata");
			
	},
	methods: {
		// getUsersOfUniverse: function (){
		// 	var self = this;
		// 	axios.get("/api/new/user/?d=bus1_universes&universes__universe=" + this.$store.state.ingame.universeId)
		// 	.then(function (response) {
		// 		Vue.set(self.$store.state.session,"users",response.data);
        //     })
        //     .catch(function (error) {
        //       console.log(error);
        //     });
			
		// },
	},
	computed: {
		id: function(){
			return this.$store.state.element.id;
		},
		element: function() {
			return this.$store.state.element;
		},
		publicSecretString: function (){
			return this.element.masterdata.secret === true ? "Secret" : "Public";
		},		
		masterdata: {
			get: function(){
				return this.$store.state.element.masterdata;
			},
			set: function (val) {
				this.$store.state.element.masterdata = val;
			},
		},
		canReadElement: function(){
			return this.$store.state.element.permissions.read;
		},		
	}

});
