var descriptionFull = Vue.component('description-full', {
	template: "\
	<div class=\"shader--full-screen\" style=\"top:40px;\">\
		<div style=\"padding-top:1em;width:100%;display:flex;flex-direction:row;align-self:flex-start\">\
		<div style=\"display:flex;flex-direction:column;justify-content:center;width:100%;\">\
		   <div class=\"beskrivelse\" style=\"align-self:center;box-shadow:rgba(0,0,0,0.3) 0 0 2px 2px;max-width:800px\" >\
			   <div style=\"padding:2em\">\
			   		<div class=\"row\">\
						<div class=\"column\">\
							<div v-if=\"data.ingame_age != null\" class=\"ink\">{{ ingameTimeComp }}</div>\
						</div>\
					    <div class=\"column\" style=\"text-align:right;\">\
			   			   <div class=\"rubrik\" >{{ hemmeligOffentlig }} Description</div>\
						   <div class=\"rubrik\">written by {{ data.author.first_name }} {{ data.author.last_name }}</div>\
						   <div v-if=\"data.ingame_author\" ><span class=\"rubrik\">as</span> <a class=\"ink\" :href=\"'/almanak/side/' + data.ingame_author.id\">{{ data.ingame_author.present_name }}</a></div>\
						   <div v-if=\"data.campaigns.length > 0\" ><span class=\"rubrik\">for</span> <a class=\"ink\" :href=\"'/almanak/side/' + c.id\" v-for=\"c in data.campaigns\">{{ c.present_name }}</a></div><br/>\
					   </div>\
				   </div>\
				   <div style=\"font-size:150%;line-height:1;overflow:auto;padding-bottom:1em;\">\
					   <span :class=\"inkType\" class=\"initial\" >{{ data.content_stripped.uncial }}</span>\
					   <div class=\"rest\" :class=\"inkType\" v-html=\"rest\" ></div>\
				   </div>\
				   <div style=\"width:100%;text-align: center\">\
					   <img class=\"icon--breaker\" :src=\"static + 'images/icons/breaker.svg'\" />\
				   </div>\
				   <div>\
					   <h3 style=\"font-size:150%;margin-bottom:0.5em\" class=\"rubrik\">Pages mentioned in Description</h3>\
					   <p  style=\"font-size:150%;\" class=\"ink\" v-for=\"e in elementsInDescription\"><a :href=\"'/almanak/side/' + e.element.id\"> - {{ e.element.present_name }}</a></p>\
				   </div>\
			   </div>\
		   </div>\
		</div>\
		</div>\
	</div>\
	",
    data: function() {
        return {
			showFull: true,
        }
    },
    methods: {
		contentFixedLinks: function(content) {
            var find = 'href="';
            var re = new RegExp(find, 'g');
            var display = content.replace(re, 'href="/almanak/side/');
            return display;
        },
		ingame_date_str: function(day, month, year, age = 0, min = 0, sec = 0) {

            var self = this;

            //console.log(self.$store.state.session);
            var ageIndex = self.$store.state.session.universe.ages.findIndex(function(a) {  return a.id == age});

            var ageComp = "Not defined";
            if (ageIndex > -1) {
                ageComp = self.$store.state.session.universe.ages[ageIndex].name
            }
            dateString = "Not set"
            if (day != 0 && month != 0 && year != 0 && age != null) {
                var monthNames = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
                var mIndex = parseInt(month) - 1;
                dateString = day + ". " + monthNames[mIndex] + " " + year + " " + ageComp;
            }
            return dateString;
        },
	},
	computed: {
		data: function(){
			return this.$store.state.descriptionFull.data;
		},
		ingameTimeComp: function() {

            return this.ingame_date_str(this.data.ingame_date, this.data.ingame_month, this.data.ingame_year, this.data.ingame_age)
        },
		hemmeligOffentlig: function() {
            if (this.data.secret == false) {
                return "Public";
            } else if (this.data.secret == true) {
                return "Secret";
            }
        },
		elementsInDescription: function (){

			return _.sortBy(this.data.elements_in_description, [function(o) { return o.element.present_name; }]);
			//return this.data.elements_in_description;
		},
		static: function (){

            return this.$store.state.initial.staticUrl;

        },
		rest: function() {
			var self = this;
            // if (this.showFull == true) {
            return this.contentFixedLinks(self.data.content_full);
            // } else if (this.showFull == false) {
            //     return this.data.content_stripped.rest.substring(0, 440) + "&#8230;";
            // }
        },
		cssHemmeligOffentlig: function() {
    		if (this.hemmelig == "3") {
                return "css-hemmelig";
            } else {
                return "";
            }
        },
        permissionEdit: function() {
            return false;
        },
        inkType: function() {
            var cssClass = "";
            if (this.data.description_type == "in_game") {
                cssClass = "ink";
            } else if (this.data.description_type == "off_game") {
                cssClass = "rubrik";
            }
            // if (this.data.secret == true) {
            //     cssClass += " css-hemmelig";
            // }
            // if (this.loadedData.relevance < 0) {
            //     cssClass += " irrelevant_bes";
            // }
            return cssClass;
        },
	},
});
