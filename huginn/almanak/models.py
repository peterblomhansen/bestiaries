from django.db import models
from django.contrib.auth.models import AbstractUser
from almanak.core.namehelper import PresentName, ChooseName
from almanak.core.time import DateHelper
from almanak.core.description_helper import DescriptionHelper

# Bestiaries Muninn on Django Smoke Migration


# DELTAGER


class BestiariesUser(AbstractUser):
	default_universe = models.ForeignKey("Universe",blank=True,null=True, on_delete=models.PROTECT)

	class Meta:
		ordering = ("first_name","last_name")


	def get_links(self,request):
		links = {}
		links["name_user"] 	= "<a class='rubrik' href='/accounts/profile/" + str(self.id) + "'>" + str(self.username) + "</a>"
		links["name_real"]	= "<a class='rubrik' href='/accounts/profile/" + str(self.id) + "'>" + str(self.first_name) + " " + str(self.last_name) + "</a>"
		links["name_combi"] = "<a class='rubrik' href='/accounts/profile/" + str(self.id) + "'>" + str(self.first_name) + " " + str(self.last_name) + " (" + str(self.username) + ")</a>"
		return links

# UNIVERSE

class Universe(models.Model):
	name 		= models.CharField(max_length=256)
	slug 		= models.SlugField()
	users 		= models.ManyToManyField(BestiariesUser,through="UniverseMembership",related_name="user_memberships")
	created 	= models.DateTimeField(auto_now_add=True,blank=True,null=True)
	updated 	= models.DateTimeField(auto_now=True,blank=True,null=True)

	def __str__(self):
	    return self.name

class UniverseMembership(models.Model):
	universe 			= models.ForeignKey("Universe",related_name="participants", on_delete=models.PROTECT)
	participant 		= models.ForeignKey("BestiariesUser", related_name="universes", on_delete=models.PROTECT)
	universe_owner 		= models.BooleanField()
	default_view 		= models.ForeignKey("PlayerCharacter", blank=True, null=True, on_delete=models.PROTECT)
	default_campaign 	= models.ForeignKey("Campaign", blank=True, null=True, on_delete=models.PROTECT)
	created 			= models.DateTimeField(auto_now_add=True,blank=True,null=True)
	updated 			= models.DateTimeField(auto_now=True,blank=True,null=True)

	class Meta:
		ordering = ("universe"),

	def __str__(self):
	    return self.universe.name + " - " + str(self.default_view) + " - " + str(self.participant.first_name) + " " + str(self.participant.last_name)

class Age(models.Model):
	universe 			= models.ForeignKey("Universe", related_name="ages", on_delete=models.PROTECT)
	num_years 			= models.IntegerField(blank=True,null=True)
	initials 			= models.CharField(max_length=10)
	name 				= models.CharField(max_length=50)
	num_months 			= models.IntegerField()
	age_number 			= models.IntegerField()
	created 			= models.DateTimeField(auto_now_add=True,blank=True,null=True)
	updated 			= models.DateTimeField(auto_now=True,blank=True,null=True)

	def __str__(self):
	    return self.name




# ELEMENT


class Name(models.Model):
	name 			= models.TextField()
	element 		= models.ForeignKey("Element",related_name='names', on_delete=models.PROTECT)
	name_type 		= models.CharField(max_length=50,blank=True,null=True)
	created 		= models.DateTimeField(auto_now_add=True,blank=True,null=True)
	updated		 	= models.DateTimeField(auto_now=True,blank=True,null=True)
	deleted 		= models.BooleanField(default=False)
	secret 			= models.BooleanField(default=False)
	shared_with 	= models.ManyToManyField("Entity",related_name='names_shared',blank="True")
	author 			= models.ForeignKey("BestiariesUser",related_name='authored_names',blank=True, null=True, on_delete=models.PROTECT)
	start_age 		= models.ForeignKey("Age",related_name="name_start_age",blank=True,null=True, on_delete=models.PROTECT)
	start_year 		= models.IntegerField(default=0)
	start_month 	= models.IntegerField(default=0)
	start_date 		= models.IntegerField(default=0)
	start_hour	 	= models.IntegerField(default=0)
	start_minute 	= models.IntegerField(default=0)
	start_second 	= models.IntegerField(default=0)
	end_age 		= models.ForeignKey("Age",related_name="name_end_age",blank=True,null=True, on_delete=models.PROTECT)
	end_year 		= models.IntegerField(default=0)
	end_month 		= models.IntegerField(default=0)
	end_date 		= models.IntegerField(default=0)
	end_hour 		= models.IntegerField(default=0)
	end_minute	 	= models.IntegerField(default=0)
	end_second 		= models.IntegerField(default=0)

	class Meta:
		ordering = ("name",)
		indexes = [
			models.Index(fields=['name','element','deleted','secret','author'])
		]

	def __str__(self):
		return self.name

	def start(self):
		return str(self.start_year) + ":" + str(self.start_month) + ":" + str(self.start_date)





class Element(models.Model):
	universe 		= models.ForeignKey("Universe",related_name='elements_in_universe', on_delete=models.PROTECT)
	universe_canon	= models.IntegerField(default=0)
	published		= models.BooleanField(default=False)
	campaigns 		= models.ManyToManyField("Campaign", through="CampaignElement",blank="True")
	created 		= models.DateTimeField(auto_now_add=True,blank=True,null=True)
	updated 		= models.DateTimeField(auto_now=True,blank=True,null=True)
	deleted 		= models.BooleanField(default=False)
	secret 			= models.BooleanField(default=False)
	shared_with 	= models.ManyToManyField("BestiariesUser",related_name='elements_shared',blank="True")
	author 			= models.ForeignKey("BestiariesUser",related_name='authored_elements', on_delete=models.PROTECT)
	editors 		= models.ManyToManyField("BestiariesUser",related_name='editor_for',blank="True")
	element_type 	= models.CharField(max_length=100,blank=True,null=True)
	type_grund 		= models.CharField(max_length=100,blank=True,null=True)
	type_spec 		= models.CharField(max_length=100,blank=True,null=True)
	birth_age 		= models.ForeignKey("Age",related_name="element_birth_age",blank=True,null="True", on_delete=models.PROTECT)
	birth_year 		= models.IntegerField(default=0)
	birth_month 	= models.IntegerField(default=0)
	birth_date 		= models.IntegerField(default=0)
	birth_hour 		= models.IntegerField(default=0)
	birth_minute 	= models.IntegerField(default=0)
	birth_second 	= models.IntegerField(default=0)
	death_age 		= models.ForeignKey("Age",related_name="element_death_age",blank=True,null="True", on_delete=models.PROTECT)
	death_year 		= models.IntegerField(default=0)
	death_month 	= models.IntegerField(default=0)
	death_date 		= models.IntegerField(default=0)
	death_hour	 	= models.IntegerField(default=0)
	death_minute 	= models.IntegerField(default=0)
	death_second 	= models.IntegerField(default=0)

	class Meta:
		indexes = [
			models.Index(fields=['universe','deleted','secret','author'])
		]

	def __str__(self):
		if len(self.names.get_queryset()) > 0:
			return self.names.get_queryset()[0].name + " (" + str(self.id) + ")" + " [" + self.universe.name + "]"
		else:
			return "No name"

	def save(self,*args, **kwargs):
		for ce in self.campaigns_containing.get_queryset():
			if ce.approved == 2:
				ce.approved = 1
				ce.save()
		super(Element,self).save(*args, **kwargs)

	def get_present_name(self,request):
		#if request.session.get("campaign_current") != None:
		#	campaign_current_id = request.session.get("campaign_current")
		n = ChooseName(self,request)
		#else:
		#	n = ChooseName(self,0)

		return { "name": n.get_present_name_str(), "id": n.get_present_name() }


	def get_type_spec_obj(self):
		type_spec_obj = {}
		def text(x):
			return {
				'artefakt'			: "item",
				'gruppe'			: "group",
				'individ'			: "individual",
				'sted'				: "place",
				'begivenhed'		: "event",
				'race'				: "kin",
				'kampagne'			: "roleplaying campaign",
				'rpg_session'		: "roleplaying session",
				'player_character'	: "player character",
				'unika'				: "unique item",
				'artefakt'			: "item",
			}.get(x, x)
		type_spec_obj["text"] = text(self.type_spec)
		return type_spec_obj

	def get_links(self,request):
		links = {}
		links["basic"] = "<a class='ink' href='/almanak/side/" + str(self.id) + "'>" + str(self.get_present_name(request)) + "</a>"
		links["type_spec"]	= "<a class='ink' href='/almanak/side/" + str(self.id) + "'>" + str(self.get_present_name(request)) + "</a>" + " <span style='font-size:10px'>(" + str(self.get_type_spec_obj()["text"]) + ")</span>"
		if(self.secret):
			links["type_spec_long"]		= "<a class='ink' href='/almanak/side/" + str(self.id) + "'>" + str(self.get_present_name(request)) + "</a>" + " <span style='font-size:10px'>("  + "secret " + str(self.get_type_spec_obj()["text"]) + ")</span>"
			links["type_spec_long_br"]	= "<a class='ink' href='/almanak/side/" + str(self.id) + "'>" + str(self.get_present_name(request)) + "</a>" + "<br><span style='font-size:10px'>("  + "secret " + str(self.get_type_spec_obj()["text"]) + ")</span>"
		else:
			links["type_spec_long"]		= "<a class='ink' href='/almanak/side/" + str(self.id) + "'>" + str(self.get_present_name(request)) + "</a>" + " <span style='font-size:10px'>("  + "public " + str(self.get_type_spec_obj()["text"]) + ")</span>"
			links["type_spec_long_br"]	= "<a class='ink' href='/almanak/side/" + str(self.id) + "'>" + str(self.get_present_name(request)) + "</a>" + "<br><span style='font-size:10px'>("  + "public " + str(self.get_type_spec_obj()["text"]) + ")</span>"
		return links

	@property
	def birth_number(self):
		d = DateHelper()
		return d.date_number(self.birth_age,self.birth_year,self.birth_month,self.birth_date,self.birth_hour,self.birth_minute,self.birth_second)


class CampaignImage(models.Model):
	image				= models.ForeignKey("Image", related_name="campaigns_containing", on_delete=models.PROTECT)
	campaign			= models.ForeignKey("Campaign", related_name="images_in_campaigns", on_delete=models.PROTECT)
	approved			= models.IntegerField(default=0)	# 0: default / klar til godkendelse; 1: tidligere godkendt men nu opdateret; 2: godkendt. Hvis beskrivelsen forkastes, bliver realtionen helt slettet.
	canon				= models.IntegerField(default=0)
	created 			= models.DateTimeField(auto_now_add=True,blank=True,null=True)
	updated 			= models.DateTimeField(auto_now=True,blank=True,null=True)

class CampaignDescription(models.Model):
	description			= models.ForeignKey("Description", related_name="campaigns_containing", on_delete=models.PROTECT)
	campaign			= models.ForeignKey("Campaign", related_name="descriptions_in_campaigns", on_delete=models.PROTECT)
	approved			= models.IntegerField(default=0)	# 0: default / klar til godkendelse; 1: tidligere godkendt men nu opdateret; 2: godkendt. Hvis beskrivelsen forkastes, bliver realtionen helt slettet.
	canon				= models.IntegerField(default=0)
	created 			= models.DateTimeField(auto_now_add=True,blank=True,null=True)
	updated 			= models.DateTimeField(auto_now=True,blank=True,null=True)


class CampaignElement(models.Model):
	element				= models.ForeignKey("Element", related_name="campaigns_containing", on_delete=models.PROTECT)
	campaign			= models.ForeignKey("Campaign", related_name="elements_in_campaigns", on_delete=models.PROTECT)
	approved			= models.IntegerField(default=0)	# 0: default / klar til godkendelse; 1: tidligere godkendt men nu opdateret; 2: godkendt. Hvis elementet forkastes, bliver realtionen helt slettet.
	canon				= models.IntegerField(default=0)
	created 			= models.DateTimeField(auto_now_add=True,blank=True,null=True)
	updated 			= models.DateTimeField(auto_now=True,blank=True,null=True)

	def __str__(self):
		return str(self.campaign.id) + " | " + str(self.element.id)



class Description(models.Model):
	campaigns 			= models.ManyToManyField("Campaign", through="CampaignDescription",blank=True)
	description_type 	= models.CharField(max_length=15)
	content 			= models.TextField(blank=True,null=True)
	title 				= models.CharField(max_length=250,blank=True,null=True)		
	url 				= models.ImageField(upload_to="gallery/", height_field="height", width_field="width",blank=True,null=True)
	height 				= models.IntegerField(blank=True,null=True)
	width 				= models.IntegerField(blank=True,null=True)
	created 			= models.DateTimeField(auto_now_add=True,blank=True,null=True)
	updated 			= models.DateTimeField(auto_now=True,blank=True,null=True)
	secret 				= models.BooleanField(default=False)
	shared_with 		= models.ManyToManyField("Entity",related_name='descriptions_shared',blank="True")
	deleted 			= models.BooleanField(default=False)
	author 				= models.ForeignKey("BestiariesUser",related_name="authored_descriptions", on_delete=models.PROTECT)
	ingame_author 		= models.ForeignKey("Individual",related_name="ingame_descriptions",blank=True,null=True, on_delete=models.PROTECT)
	ingame_age 			= models.ForeignKey("Age",related_name="descriptions_in_age",blank=True,null=True, on_delete=models.PROTECT)
	ingame_year 		= models.IntegerField(default=0)
	ingame_month 		= models.IntegerField(default=0)
	ingame_date 		= models.IntegerField(default=0)
	ingame_hour 		= models.IntegerField(default=0)
	ingame_minute 		= models.IntegerField(default=0)
	ingame_second 		= models.IntegerField(default=0)
	universe 			= models.ForeignKey("Universe",related_name='descriptions_in_universe',blank=True,null=True, on_delete=models.PROTECT)
	locked_by_user		= models.ForeignKey("BestiariesUser",related_name='locked_descriptions',blank=True,null=True, on_delete=models.PROTECT)

	# def save(self, force_insert=False, force_update=False, using=None):
	# 	for ce in self.campaigns_containing.get_queryset():
	# 		if ce.approved == 2:
	# 			ce.approved = 1
	# 			ce.save()

	# 	super(Description,self).save()

	# 	if self.content and len(self.content) > 0 and not self.url.name:
	# 		dh = DescriptionHelper(self)
	# 		dh.sync_relations()

	def __str__(self):
		if self.content:
			return self.content[:40]
		elif self.title:
			return self.title


class Image(models.Model):
	url 				= models.ImageField(upload_to="gallery/", height_field="height", width_field="width",blank=True,null=True)
	title 				= models.CharField(max_length=150,blank=True,null=True)
	image_type 			= models.CharField(max_length=15)
	height 				= models.IntegerField(blank=True,null=True)
	width 				= models.IntegerField(blank=True,null=True)
	created 			= models.DateTimeField(auto_now_add=True,blank=True,null=True)
	updated 			= models.DateTimeField(auto_now=True,blank=True,null=True)
	secret 				= models.BooleanField(default=False)
	shared_with 		= models.ManyToManyField("Entity",related_name='images_shared',blank="True")
	deleted 			= models.BooleanField(default=False)
	author 				= models.ForeignKey("BestiariesUser",related_name="authored_images", on_delete=models.PROTECT)
	ingame_author 		= models.ForeignKey("Individual",related_name="ingame_images",blank=True,null=True, on_delete=models.PROTECT)
	campaigns 			= models.ManyToManyField("Campaign", through="CampaignImage",blank=True)
	#campaigns 			= models.ManyToManyField("Campaign",related_name="images_in_campaign",blank=True)
	ingame_age 			= models.ForeignKey("Age",related_name="images_in_age",blank=True,null=True, on_delete=models.PROTECT)
	ingame_year 		= models.IntegerField(default=0)
	ingame_month 		= models.IntegerField(default=0)
	ingame_date 		= models.IntegerField(default=0)
	ingame_hour 		= models.IntegerField(default=0)
	ingame_minute 		= models.IntegerField(default=0)
	ingame_second 		= models.IntegerField(default=0)
	universe 			= models.ForeignKey("Universe",related_name='images_in_universe',blank=True,null=True, on_delete=models.PROTECT)


	def __str__(self):
		return str(self.url) + " - " + str(self.title)



# EVENTS

class Event(Element):
    event_type = models.CharField(max_length=20,blank=True,null=True)




# RPG ELEMENTS

class RpgElement(Event):
    offgame_start = models.DateTimeField(blank=True,null=True)
    offgame_end = models.DateTimeField(blank=True,null=True)
    gamemasters = models.ManyToManyField("BestiariesUser",related_name="gamemaster_for",blank=True)
    rpg_type = models.CharField(max_length=20,blank=True,null=True)


class Campaign(RpgElement):
	ingame_now_age 		= models.ForeignKey("Age",related_name="ingame_now_age",blank=True,null=True, on_delete=models.PROTECT)
	ingame_now_year 	= models.IntegerField(default=0)
	ingame_now_month 	= models.IntegerField(default=0)
	ingame_now_date 	= models.IntegerField(default=0)
	ingame_now_hour		= models.IntegerField(default=0)
	ingame_now_minute 	= models.IntegerField(default=0)
	ingame_now_second	= models.IntegerField(default=0)


	# def save(self,*args, **kwargs):
	# 	cc = CampaignElement
	# 	cc.campaign = self
	# 	cc.element = self
	# 	cc.save(self,*args, **kwargs)
	# 	super(Campaign,self).save(*args, **kwargs)




class RpgSession(RpgElement):
	in_campaign = models.ForeignKey("Campaign",related_name="sessions", on_delete=models.PROTECT)
	session_number = models.IntegerField()

	# class Meta:
	# 	ordering: ['-session_number']


# ENTITIES

class Entity(Element):
    entity_type = models.CharField(max_length=50,blank=True,null=True)


class Group(Entity):
	group_type = models.ForeignKey("GroupType",related_name="groups",null=True,blank=True, on_delete=models.PROTECT)
	is_kin = models.BooleanField(default=False)

	def save(self,*args, **kwargs):
		if self.group_type:
			self.type_spec = self.group_type.name
		else:
			self.type_spec = "gruppe"

		super(Group,self).save(*args, **kwargs)


class GroupType(models.Model):
	name 				= models.CharField(max_length=250)
	name_plural 		= models.CharField(max_length=250)
	description 		= models.TextField(blank=True,null=True)
	universe			= models.ForeignKey("Universe",related_name="group_types", on_delete=models.PROTECT)
	deleted				= models.BooleanField(default=False)
	created 			= models.DateTimeField(auto_now_add=True,blank=True,null=True)
	updated 			= models.DateTimeField(auto_now=True,blank=True,null=True)

	class Meta:
		ordering = ["name"]


class RelationTitle(models.Model):
	name 				= models.CharField(max_length=250)
	name_plural 		= models.CharField(max_length=250)
	preposition 		= models.CharField(max_length=10)
	description 		= models.TextField(blank=True,null=True)
	title_type			= models.CharField(max_length=250,blank=True,null=True)
	group_type			= models.ForeignKey("GroupType",related_name="titles",blank=True, on_delete=models.PROTECT)
	deleted				= models.BooleanField(default=False)
	created 			= models.DateTimeField(auto_now_add=True,blank=True,null=True)
	updated 			= models.DateTimeField(auto_now=True,blank=True,null=True)
	#place 		= models.ForeignKey("Place",related_name="titles_of_place",blank=True,null=True)

	class Meta:
		ordering = ["name"]





class Kin(Group):
	pass

class Individual(Entity):
	individual_type 	= models.CharField(max_length=20,blank=True,null=True)
	kin 				= models.ForeignKey("Kin",blank=True,null=True,related_name="individuals_of_kin", on_delete=models.PROTECT)
	gender 				= models.CharField(max_length=20,blank=True,null=True)
    
	@property
	def child_relations(self):
		return self.element_relations_below.filter(relation_type="familie",deleted=False)
		
	@property
	def parent_relations(self):
		return self.element_relations_above.filter(relation_type="familie",deleted=False)

class PlayerCharacter(Individual):
    roleplayer = models.ForeignKey("BestiariesUser",related_name="player_characters", on_delete=models.PROTECT)


# PLACES

class Place(Element):
    pass


# ITEMS

class Item(Element):
	unique_item = models.BooleanField(default=False)

	def save(self,*args, **kwargs):
		if self.unique_item == True:
			self.type_spec = "unika"

		super(Item,self).save(*args, **kwargs)

# RELATIONS

class ElementRelation(models.Model):
	above 				= models.ForeignKey("Element",related_name='element_relations_below', on_delete=models.PROTECT)
	below 				= models.ForeignKey("Element",related_name='element_relations_above', on_delete=models.PROTECT)
	secret 				= models.BooleanField(default=False)
	deleted 			= models.BooleanField(default=False)
	relation_type		= models.CharField(max_length=50,blank=True,null=True)
	above_title 		= models.ForeignKey("RelationTitle",related_name='below_titles',blank=True,null=True, on_delete=models.PROTECT)
	below_title 		= models.ForeignKey("RelationTitle",related_name='elements_titles',blank=True,null=True, on_delete=models.PROTECT)
	start_age 			= models.ForeignKey("Age",related_name="element_relation_start_age",blank=True,null=True, on_delete=models.PROTECT)
	start_year 			= models.IntegerField(default=0)
	start_month 		= models.IntegerField(default=0)
	start_date 			= models.IntegerField(default=0)
	start_hour	 		= models.IntegerField(default=0)
	start_minute 		= models.IntegerField(default=0)
	start_second 		= models.IntegerField(default=0)
	end_age 			= models.ForeignKey("Age",related_name="element_relation_end_age",blank=True,null=True, on_delete=models.PROTECT)
	end_year 			= models.IntegerField(default=0)
	end_month		 	= models.IntegerField(default=0)
	end_date 			= models.IntegerField(default=0)
	end_hour 			= models.IntegerField(default=0)
	end_minute	 		= models.IntegerField(default=0)
	end_second	 		= models.IntegerField(default=0)
	created 			= models.DateTimeField(auto_now_add=True,blank=True,null=True)
	updated 			= models.DateTimeField(auto_now=True,blank=True,null=True)


	class Meta:
		ordering = ("above","below",)

class EventPlaceRelation(models.Model):
	event 				= models.ForeignKey("Event", related_name="occured", on_delete=models.PROTECT)
	place 				= models.ForeignKey("Place", related_name="history", on_delete=models.PROTECT)
	secret 				= models.BooleanField(default=False)
	deleted 			= models.BooleanField(default=False)
	start_age 			= models.ForeignKey("Age",related_name="event_place_relation_start_age",blank=True,null=True, on_delete=models.PROTECT)
	start_year 			= models.IntegerField(default=0)
	start_month 		= models.IntegerField(default=0)
	start_date 			= models.IntegerField(default=0)
	start_hour	 		= models.IntegerField(default=0)
	start_minute 		= models.IntegerField(default=0)
	start_second 		= models.IntegerField(default=0)
	end_age 			= models.ForeignKey("Age",related_name="event_place_relation_end_age",blank=True,null=True, on_delete=models.PROTECT)
	end_year 			= models.IntegerField(default=0)
	end_month 			= models.IntegerField(default=0)
	end_date 			= models.IntegerField(default=0)
	end_hour 			= models.IntegerField(default=0)
	end_minute	 		= models.IntegerField(default=0)
	end_second 			= models.IntegerField(default=0)
	created 			= models.DateTimeField(auto_now_add=True,blank=True,null=True)
	updated 			= models.DateTimeField(auto_now=True,blank=True,null=True)




class EntityEventRelation(models.Model):
	event 				= models.ForeignKey("Event",related_name="participants", on_delete=models.PROTECT)
	entity 				= models.ForeignKey("Entity",related_name="experiences", on_delete=models.PROTECT)
	secret 				= models.BooleanField(default=False)
	deleted 			= models.BooleanField(default=False)
	start_age 			= models.ForeignKey("Age",related_name="entity_event_relation_start_age",blank=True,null=True, on_delete=models.PROTECT)
	start_year 			= models.IntegerField(default=0)
	start_month 		= models.IntegerField(default=0)
	start_date 			= models.IntegerField(default=0)
	start_hours 		= models.IntegerField(default=0)
	start_minutes 		= models.IntegerField(default=0)
	start_seconds 		= models.IntegerField(default=0)
	end_age 			= models.ForeignKey("Age",related_name="entity_event_relation_end_age",blank=True,null=True, on_delete=models.PROTECT)
	end_year 			= models.IntegerField(default=0)
	end_month 			= models.IntegerField(default=0)
	end_date 			= models.IntegerField(default=0)
	end_hours 			= models.IntegerField(default=0)
	end_minutes 		= models.IntegerField(default=0)
	end_seconds 		= models.IntegerField(default=0)
	created 			= models.DateTimeField(auto_now_add=True,blank=True,null=True)
	updated 			= models.DateTimeField(auto_now=True,blank=True,null=True)


class ElementItemRelation(models.Model):
	owner 				= models.ForeignKey("Element",related_name="owned_relations", on_delete=models.PROTECT)
	owned 				= models.ForeignKey("Element",related_name="owner_relations", on_delete=models.PROTECT)
	secret 				= models.BooleanField(default=False)
	deleted 			= models.BooleanField(default=False)
	number_of_items 	= models.IntegerField(default=1)
	ingame_age 			= models.ForeignKey("Age",related_name="item_element_relation_age",blank=True,null=True, on_delete=models.PROTECT)
	ingame_year 		= models.IntegerField(default=0)
	ingame_month 		= models.IntegerField(default=0)
	ingame_date 		= models.IntegerField(default=0)
	ingame_hour 		= models.IntegerField(default=0)
	ingame_minute 		= models.IntegerField(default=0)
	ingame_second 		= models.IntegerField(default=0)
	created 			= models.DateTimeField(auto_now_add=True,blank=True,null=True)
	updated 			= models.DateTimeField(auto_now=True,blank=True,null=True)


	def __str__(self):
		return str(self.owner) + " > < " + str(self.owned) + ": " + str(self.ingame_age) + "-" +  str(self.ingame_year) + "-" + str(self.ingame_month) + "-" + str(self.ingame_date)




class ElementUniqueItemRelation(models.Model):
	owner 				= models.ForeignKey("Element",related_name="inventory_unique", on_delete=models.PROTECT)
	owned 				= models.ForeignKey("Element",related_name="owners_unique", on_delete=models.PROTECT)
	secret 				= models.BooleanField(default=False)
	deleted 			= models.BooleanField(default=False)
	start_age 			= models.ForeignKey("Age",related_name="element_item_relation_start_age",blank=True,null=True, on_delete=models.PROTECT)
	start_year 			= models.IntegerField(default=0)
	start_month 		= models.IntegerField(default=0)
	start_date 			= models.IntegerField(default=0)
	start_hour	 		= models.IntegerField(default=0)
	start_minute 		= models.IntegerField(default=0)
	start_second 		= models.IntegerField(default=0)
	end_age 			= models.ForeignKey("Age",related_name="element_item_relation_end_age",blank=True,null=True, on_delete=models.PROTECT)
	end_year 			= models.IntegerField(default=0)
	end_month 			= models.IntegerField(default=0)
	end_date 			= models.IntegerField(default=0)
	end_hour 			= models.IntegerField(default=0)
	end_minute 			= models.IntegerField(default=0)
	end_second	 		= models.IntegerField(default=0)
	created 			= models.DateTimeField(auto_now_add=True,blank=True,null=True)
	updated 			= models.DateTimeField(auto_now=True,blank=True,null=True)


class DescriptionElementRelation(models.Model):
	description 		= models.ForeignKey("Description",related_name="element_relations", on_delete=models.PROTECT)
	element 			= models.ForeignKey("Element",related_name="description_relations", on_delete=models.PROTECT)
	relevance 			= models.IntegerField(default=0)
	created 			= models.DateTimeField(auto_now_add=True,blank=True,null=True)
	updated 			= models.DateTimeField(auto_now=True,blank=True,null=True)

	class Meta:
		indexes = [
			models.Index(fields=['description','element','relevance'])
		]

	def __str__(self):
		return str(self.element.id) + "-" + str(self.description.id) 


class ImageElementRelation(models.Model):
	image 				= models.ForeignKey("Image",related_name="elements_in_image", on_delete=models.PROTECT)
	element 			= models.ForeignKey("Element",related_name="images_of_element", on_delete=models.PROTECT)
	relevance 			= models.IntegerField(default=0)
	coordinates 		= models.TextField(blank=True,null=True)
	created 			= models.DateTimeField(auto_now_add=True,blank=True,null=True)
	updated 			= models.DateTimeField(auto_now=True,blank=True,null=True)
