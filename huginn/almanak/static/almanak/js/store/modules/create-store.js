var createStore = { 
	state: {
		// Async states used for state of the create request
		isCreating: false,
		hasCreated: false,
		hasError: false,

		// Mandatory properties of all elements
		name: "",
		secret: false,
		element_type: "",
		type_grund: "",
		type_spec: "",
		campaign: null,
		universe: null,
		author: null,

		// General element properties
		element_relations_above: [],
		element_relations_below: [],

		// Type specific properties
		// Individual
		gender: null,
		
		// rpg_session
		session_number: 0,
		in_campaign: null,

		// item
		unique_item: false,

	},
	getters: {
		validateCreateNewElementData: function (state){
			var validate = false;
			if(
				state.name.length > 0 &&
				state.element_type.length > 0 &&
				state.type_grund.length > 0 &&
				state.type_spec.length > 0
			) {
				validate = true;
			}

			return validate;
		},
	},
	mutations: {
		createDataMutate: function (state, load) {
			Vue.set(state,load.key,load.value);
		},
		setElementTypes: function (state, types) {
			Vue.set(state,"element_type",types.element_type);
			Vue.set(state,"type_grund",types.type_grund);
			Vue.set(state,"type_spec",types.type_spec);
		},
		createDataPushToArray: function (state, data) {
			var newArray = data.location[data.array];
			newArray.push(data.entry);
			Vue.set(data.location,data.array,newArray);
		},
		createDataPopFromArray: function (state, data) {
			var newArray = data.location[data.array];
			newArray.pop(data.index);
			Vue.set(data.location,data.array,newArray);
		},
		createProposeNewSessionNumber: function (state, data) {
			Vue.set(state,"session_number",state.rpgSessionNumber + 1);
		},	
		createSetInCampaign: function (state,rootState) {
			Vue.set(state,"in_campaign",rootState.generic.session.base.campaignId);
		},				
	},
	actions: {
		createGetCampaignDataForRpgSession: function (context,campaignId){
			axios.get('/api/new/rpg_session/?in_campaign__id=' + campaignId)	
			.then(function (response) {
				var currentCount = parseInt(response.data.count); 
				context.commit("createDataMutate",
					{ 	
						key: "rpgSessionNumber",
						value: currentCount + 1
					}
				);
				context.commit("createDataMutate",
					{ 	
						key: "in_campaign",
						value: campaignId
					}
				);				
			})
			.catch(function (error) {
				console.log(error);
			});				
			
		},
		createNewElement: function (context,type) {
			console.log("createNewElement",type);
			var createData = {
				names: [
					{
						name: context.state.name,
						author: context.state.author,
						name_type: "prim",
					},
				],
				element_type: context.state.element_type,
				type_grund: context.state.type_grund,
				type_spec: context.state.type_spec,
				secret: context.state.secret,
				campaigns: [context.state.campaign],
				universe: context.state.universe,
				author: context.state.author,
				element_relations_above: context.state.element_relations_above,
				element_relations_below: context.state.element_relations_below,

			}

			if (type === "individual") {
				createData.gender = context.state.gender;
			}

			if (type === "rpg_session") {
				createData.session_number = context.state.session_number;
				createData.in_campaign = context.state.in_campaign;
			}

			if (type === "item") {
				createData.unique_item = context.state.unique_item;
			}


			console.log(createData);
			Vue.set(context.state, "isCreating",true);
			axios.post("/api/new/" + type + "/",createData)
			.then(function(response){
				if(response.data.id) {
					Vue.set(context.state, "isCreating",false);
					Vue.set(context.state, "hasCreated",true);
					Vue.set(context.state, "id",response.data.id);				
				}
				else {
					Vue.set(context.state, "hasError",true);					
				}
			})
			.catch(function (error) {
				console.log(error);
				Vue.set(context.state, "hasError",true);

			});
		}
	}

} 