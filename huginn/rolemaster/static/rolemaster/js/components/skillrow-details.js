
var skillrowDetails = Vue.component('skillrow-details', {
	template: "\
	<div class=\"shader--full-screen\">\
		<div class=\"alert--center\" style=\"width:90%;height:80%;\">\
			<div style=\"overflow:auto;height:100%\">\
				<div style=\"display:flex;flex-direction:row\">\
					<div @click=\"closeDetails()\" class=\"cursor--pointer rubrik\" style=\"font-size:2em;width:50%;\" >&times;</div>\
					<div style=\"text-align:right;width:50%;align-self:flex-end\"><p >{{ character.names[0].name }} Level {{ $store.state.initial.level.viewed }} ({{ character.profession.name }})</p></div>\
				</div>\
				<h2>{{ skillData.name }} <span v-if=\"skillData.area_name != null\"> - {{ skillData.area_name }}</span></h2>\
				<h3>\
					<span v-if=\"skillData.stat_1 != null\">{{ skillData.stat_1 }}</span>\
					<span v-if=\"skillData.stat_2 != null\"> / {{ skillData.stat_2 }}</span>\
					<span v-if=\"skillData.stat_3 != null\"> / {{ skillData.stat_3 }}</span>\
				</h3>\
				<h3 >\
					<span v-if=\"skillData.change_cost == false\" >Cost: {{ $store.getters.skillCost(skillData) }} </span>\
					<span v-else>Add cost for {{ character.profession.name }}\
						<input class=\"skill__cost-input\"  type=\"text\" v-model=\"skillData.cost.cost_1\" /> / \
						<input class=\"skill__cost-input\"  type=\"text\" v-model=\"skillData.cost.cost_2\"  /> / \
						<input class=\"skill__cost-input\"  type=\"text\" v-model=\"skillData.cost.cost_3\"  />\
						<button @click=\"updateSkillCost(skillData)\" >Update Skill Cost</button>\
					</span>\
				</h3>\
				<h3>Category (Level Bonus): {{ skillData.category }} ({{ levelBonusProfession }})</h3>\
				<div>\
					<h3>Ranks <span v-if=\"similarRanks.name != null\" >(from {{ similarRanks.name }} - {{ similarRanks.ratio }} ranks)</span></h3>\
					<table class=\"table__skillcategory\">\
						<tr>\
							<th class=\"skill__num-rank\" >GM</th>\
							<th class=\"skill__num-rank\" >Hobby</th>\
							<th class=\"skill__num-rank\" v-for=\"l in levels\" >{{ l }}</th>\
							<th class=\"skill__num-rank\" >Total ranks</th>\
						</tr>\
						<tr>\
							<td class=\"skill__num-rank\">\
								<input :class=\"$store.getters.ranksValidated(skillData.gm_ranks,skillData)\" type=\"text\" v-model=\"skillData.gm_ranks.num_ranks\" @blur=\"updateSkillRanks(skillData,skillData.gm_ranks,0)\" />\
							</td>\
							<td class=\"skill__num-rank\">\
								<input :class=\"$store.getters.ranksValidated(skillData.hobby_ranks,skillData)\" type=\"text\" v-model=\"skillData.hobby_ranks.num_ranks\" @blur=\"updateSkillRanks(skillData,skillData.hobby_ranks,0)\" />\
							</td>\
							<td class=\"skill__num-rank\" v-for=\"(l,i) in levels\" >\
								<input :class=\"$store.getters.ranksValidated(skillData.dev_ranks[i],skillData)\" type=\"text\" v-model=\"skillData.dev_ranks[i].num_ranks\" @blur=\"updateSkillRanks(skillData,skillData.dev_ranks[i],i)\" />\
							</td>\
							<td class=\"skill__num-rank table-cell--emphasis\" >{{ skillRanksTotal }}</td>\
						</tr>\
					</table>\
				</div>\
				<div>\
					<h3>Bonus</h3>\
					<table class=\"table__skillcategory\">\
						<tr>\
							<th class=\"skill__num-rank\" >Rank Bonus</th>\
							<th class=\"skill__num-rank\" >Stat Bonus</th>\
							<th class=\"skill__num-rank\" >Level Bonus</th>\
							<th class=\"skill__num-rank\" >Misc Bonus</th>\
							<th class=\"skill__num-rank\" >Total Bonus</th>\
						</tr>\
						<tr>\
							<td class=\"skill__num-rank table-cell--emphasis\" >{{ rankBonus }}</td>\
							<td class=\"skill__num-rank table-cell--emphasis\" >{{ statBonus }}</td>\
							<td class=\"skill__num-rank table-cell--emphasis\" >{{ levelBonus }}</td>\
							<td class=\"skill__num-rank table-cell--emphasis\" >{{ misc_bonus_sum }}</td>\
							<td class=\"skill__num-rank table-cell--emphasis\" >{{ skillTotalBonus }}</td>\
						</tr>\
					</table>\
					<div class=\"row\">\
						<div class=\"column\" >\
							<h4>Misc Modifiers</h4>\
							<table v-if=\"misc_bonuses.length > 0 \">\
								<tr>\
									<th>Modifier</th>\
								</tr>\
								<tr v-for=\"mb in misc_bonuses\" >\
									<td><input type=\"text\" v-model=\"mb.bonus\"  @blur=\"updateMiscBonus(mb)\" /> <span class=\"cursor--pointer\" @click=\"deleteMiscBonus(mb.id)\" >&times;</span></td>\
								<tr>\
							</table>\
							<p v-else>No Misc Modifiers</p>\
							<br/><button @click=\"newMiscBonus.show = !newMiscBonus.show\" >Add New Misc Modifier</button>\
							<table v-if=\"newMiscBonus.show == true \">\
								<tr>\
									<th>Modifier</th>\
								</tr>\
								<tr >\
									<td><input type=\"text\" v-model=\"newMiscBonus.bonus\" /></td>\
								<tr>\
								<button @click=\"addMiscBonus()\" >Add</button>\
							</table>\
						</div>\
						<div class=\"column\" >\
							<h4>Comments</h4>\
							<div v-if=\"skillComments.length > 0\" >\
								<div v-for=\"c in skillComments\" ><textarea style=\"width:90%\" v-model=\"c.comment\" @blur=\"updateSkillComment(c)\" ></textarea> <span @click=\"c.delete_question = !c.delete_question\" >&times;</span>\
									<p v-if=\"c.delete_question == true\">Do you want to remove this comment? <button @click=\"deleteSkillComment(c)\" >Yes</button> <button>No</button></p>\
								</div>\
							</div>\
							<p v-else>No Comments</p>\
							<br/><button @click=\"newSkillComment.show = !newSkillComment.show\" >Add a New Comment</button>\
							<p v-if=\"newSkillComment.show == true\" ><br/><textarea style=\"width:90%\" v-model=\"newSkillComment.comment\"></textarea>\
								<br/><button @click=\"addSkillComment()\">Add</button>\
							</p>\
						</div>\
					</div>\
				</div>\
				<div>\
				</div>\
			</div>\
		</div>\
	</div>\
	",
    data: function() {
		return {
			//skillCostEditShow: false,
			newMiscBonus: {
				show: false,
				bonus_type: "",
				bonus: 0,
			},
			newSkillComment: {
				show: false,
				comment: "",
			},
		}
    },
	mounted: function (){
		//console.log(this.$store.getters.skillCost(this.skillData).length);
		if(this.$store.getters.skillCost(this.skillData).length === 0) {
			this.skillData.change_cost = true;
		}

	},
    methods: {
		deleteSkillComment: function (c) {
			var self = this;
			axios.patch("/rm/api/skillcomment/" + c.id + "/update/",{ deleted: true }).then(function(response){
				var i = self.character.skill_comments.findIndex(function(m){ return m.id == c.id });
				self.character.skill_comments.splice(i, 1);
			}).catch(function(error) {
					console.log(error);
			});
		},
		updateSkillComment: function (c){
			var self = this;

			axios.patch("/rm/api/skillcomment/" + c.id + "/update/",c).then(function(response){
				// var i = self.character.skill_misc_bonuses.findIndex(function(m){ return m.id == id });
				// self.character.skill_misc_bonuses.splice(i, 1);
			}).catch(function(error) {
					console.log(error);
			});

		},
		addSkillComment: function (){
			var self = this;
			var newSkillComment = {
				comment: this.newSkillComment.comment,
				character: this.$store.state.initial.characterId,
				skill: this.skillData.id,
				area: this.skillData.area,
			};
			axios.post("/rm/api/skillcomment/create/",newSkillComment).then(function(response){
				self.$store.state.character.skill_comments.push(response.data);
				self.newSkillComment.comment = "";

			}).catch(function(error) {
					console.log(error);
			});

		},
		deleteMiscBonus: function (id){
			var self = this;
			console.log(id);
			axios.patch("/rm/api/skillmiscbonus/" + id + "/update/",{ deleted: true }).then(function(response){
				var i = self.character.skill_misc_bonuses.findIndex(function(m){ return m.id == id });
				self.character.skill_misc_bonuses.splice(i, 1);
			}).catch(function(error) {
					console.log(error);
			});

		},
		updateMiscBonus: function (mb){
			var self = this;

			axios.patch("/rm/api/skillmiscbonus/" + mb.id + "/update/",mb).then(function(response){
				// var i = self.character.skill_misc_bonuses.findIndex(function(m){ return m.id == id });
				// self.character.skill_misc_bonuses.splice(i, 1);
			}).catch(function(error) {
					console.log(error);
			});

		},
		addMiscBonus: function (){
			var self = this;
			var newMiscBonus = {
				bonus_type: "misc"	,
				bonus: this.newMiscBonus.bonus,
				character: this.$store.state.initial.characterId,
				skill: this.skillData.id,
				area: this.skillData.area,
			};
			axios.post("/rm/api/skillmiscbonus/create/",newMiscBonus).then(function(response){
				self.$store.state.character.skill_misc_bonuses.push(response.data);
				self.newMiscBonus.bonus_type = "";
				self.newMiscBonus.bonus = 0;
			}).catch(function(error) {
					console.log(error);
			});

		},
		updateSkillCost: function (skillData) {
			console.log("update skill cost");

			var updated = this.$store.commit("updateSkillCost",{"skill": skill});


		},
		updateSkillRanks: function (skill,rankObj,lvlIndex){

			this.$store.commit("updateSkillRanks",{"skill": skill, "rankObj": rankObj, "lvlIndex": lvlIndex});

		},
		closeDetails: function() {
			this.$store.state.loaded.skillrowDetails = false;
		},
		bonusOfStat: function (statName){
			var statNameTranslater = {
				"LU": "luck",
				"AG": "agility",
				"CO": "constitution",
				"RE": "reasoning",
				"ME": "memory",
				"SD": "self_discipline",
				"ST": "strength",
				"QU": "quickness",
				"PR": "presence",
				"EM": "empathy",
				"IN": "intuition"
			};
			var statTranslated = statNameTranslater[statName];
			return this.$store.getters.getStatTotalBonus(statTranslated);
		},


	},
	computed: {
		numGmRanks: function(){
			if(this.skillData.gm_ranks != null) {
				return this.skillData.gm_ranks.num_ranks;
			}else {
				return 0;
			}

		},
		index: function (){
			return this.$store.state.loaded.skillrowDetails
		},
		statBonus: function () {
			var statList = [this.skillData.stat_1,this.skillData.stat_2,this.skillData.stat_3];
			var bonusList = [];
			var self = this;
			_.each(statList,function (s,i){
				if(s != null) {
					bonusList.push(self.bonusOfStat(s));
				}
			});
			return Math.round(bonusList.reduce((a, b) => a + b, 0) / bonusList.length);

		},
		levelBonusProfession: function (){
			var self = this;
			var catMatch = this.character.profession.level_bonuses_of_profession.filter(function(l){ return l.category == self.skillData.category });
			if(catMatch.length == 1) {
				return catMatch[0].bonus;
			}else {
				return 0;
			}
		},
		levelBonus: function (){
			var self = this;
			var catMatch = this.character.profession.level_bonuses_of_profession.filter(function(l){ return l.category == self.skillData.category });
			if(catMatch.length == 1) {
				return catMatch[0].bonus * self.$store.state.initial.level.viewed;
			}else {
				return 0;
			}
		},
		rankBonus: function (){
			return this.$store.getters.getRankBonus(this.skillRanksTotal);
		},
		misc_bonus_sum: function () {
			var sum = 0;

			if(this.misc_bonuses.length > 0) {
				_.each(this.misc_bonuses,function(b){
					if(b.bonus && b.deleted != true) {
						sum = sum + parseInt(b.bonus);
					}
				});
			}

			return sum;
		},
		skillTotalBonus: function (){
			return this.rankBonus + this.statBonus + this.levelBonus + this.misc_bonus_sum;
		},
		skillData: function (){
			return this.$store.state.skillrows[this.index];
		},
		nativeRanks: function(){
			return this.$store.getters.getSkillNativeRanks(this.skillData);
		},
		similarRanks: function (){
			return this.$store.getters.getSkillSimilarRanks(this.skillData);
		},
		skillRanksTotal: function (){

			if(this.nativeRanks > this.similarRanks.ranks || this.similarRanks.ranks == 0) {
				this.ranksCssClass = "";
				return this.nativeRanks;
			}else {
				this.fromSimilar = " (" + this.similarRanks.ratio + " from " + this.similarRanks.name + ")";
				this.ranksCssClass = "alert alert--warning";
				//this.ranksCssClass = "num-ranks--similar";
 				return Math.round(this.similarRanks.ranks);
			}
		},
		character: function (){
			return this.$store.state.character;
		},
		levels: function (){
			var levels = [];
			n = 1;
			while (n <= this.$store.state.initial.level.viewed) {
				levels.push(n);
				n++;
			}
			return levels;
		},
		misc_bonuses: function (){
			var self = this;
			return this.character.skill_misc_bonuses.filter(function(b){
				return b.skill == self.skillData.id && b.area == self.skillData.area && b.deleted != true;
			});
		},
		skillComments: function (){
			var self = this;
			return this.$store.state.character.skill_comments.filter(function(s){ return self.skillData.id == s.skill && self.skillData.area == s.area });
		},
	}
});
