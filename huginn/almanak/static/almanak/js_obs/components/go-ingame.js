var goIngame = Vue.component('go-ingame', {
    template: "\
    <div class=\"shader--full-screen\" style=\"z-index:1000000\">\
        <div class=\"alert--center\">\
			<div style=\"text-align:right;\">\
				<p class=\"rubrik\"><a href=\"/logout\">Log Out</a></p>\
			</div>\
            <h3 class=\"ink\">You are currently not ingame</h3>\
            <h2 class=\"rubrik\">Go Ingame</h2>\
            <div v-for=\"u in user.universes\">\
                <b>{{ u.universe.name }}</b>\
                <div class=\"rubrik\" v-for=\"pc in user.player_characters\" v-if=\"pc.universe == u.universe.id && pc.campaigns.length > 0\" style=\"cursor:pointer;\" @click=\"goIngame(pc.universe,pc.campaigns[0].campaign,pc.id)\">\
                    as the Character <strong class=\"ink\">{{ pc.present_name }}</strong>\
                </div>\
                <div class=\"rubrik\" v-for=\"cm in user.gamemaster_for\" v-if=\"cm.universe == u.universe.id && cm.type_grund == 'kampagne'\" style=\"cursor:pointer;\" @click=\"goIngame(cm.universe,cm.id,0)\">\
                    as Gamemaster for <strong class=\"ink\">{{ cm.present_name }}</strong>\
                </div>\
            </div>\
        </div>\
    </div>",
    props: {
        player: Object
    },
    data: function() {
        return {
            user: {}
        }
    },
    mounted: function(){

        this.getUserPointOfViews(userId);

    },
    methods: {
        getUserPointOfViews: function (id){
            var self = this;
            //console.log("in here");
            axios.get('/api/user/' + id + '/?d=bus1_universes__bus1_player_characters__bus1_gamemaster_for__es1_campaigns')
            .then(function (response) {
                self.user = response.data;
              //console.log(response);
            })
            .catch(function (error) {
              console.log(error);
            });

        },
        goIngame: function(id_universe, id_campaign, id_role) {

          axios.post('/api/go_ingame/', { universe: id_universe, campaign: id_campaign, role: id_role } )

            .then(function (response) {
              console.log(response.data)
              if(response.data = "is_ingame") {
            //    console.log("asfsafsa")
                window.location.reload();
              }

            })
            .catch(function (error) {
              console.log(error);
            });


        },

    },
    computed: {
        static: function(){

            return this.$store.state.initial.staticUrl;
        },




    }
});
