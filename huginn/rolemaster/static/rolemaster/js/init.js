
axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
axios.defaults.xsrfCookieName = "csrftoken";



var rmCharacterSheet = new Vue({
  el: "#rm-charactersheet",
  store: store,

  mounted:function (){
	  var self = this;

	  axios.get("/rm/api/profession/list/").then(function(response){
		  self.$store.state.professions = response.data;
		  //self.$store.state.loaded.skills = true;

	  });

	  if(characterId) {

	  		this.$store.dispatch("getStats");
	  		this.$store.dispatch("getSkills");

			axios.get("/rm/api/statbonusdistribution/").then(function(response){
			  self.$store.state.rolemasterTables.statBonusDistribution = response.data;

			});
			axios.get("/rm/api/skills/").then(function(response){
			  self.$store.state.skills = response.data;

			});
			axios.get("/rm/api/skillareas/").then(function(response){
			  self.$store.state.skillareas = response.data;

			});


		}
  },
  computed: {
	  id: function() {
		  return this.$store.state.initial.characterId;
	  },
  }


})
