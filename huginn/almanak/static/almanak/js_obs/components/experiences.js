var experiences = Vue.component('experiences', {

    template: "\
        <div>\
            <h3 v-if=\"experiences\" class=\"rubrik\">Experiences <strong v-if=\"masterdata.user_permissions.write\" class=\"cursor--pointer\" @click=\"addExperience = !addExperience\">+</strong></h3>\
            <h3 v-else >Loading Experiences</h3>\
            <div v-if=\"addExperience == true\">\
                <autocomplete parent-component=\"experiences\" :action-url=\"'entityeventrelation/create/'\" filter=\"begivenhed\" :button-method=\"'createRelation'\" :button-text=\"'Add to Experiences'\" >\
                </autocomplete>\
            </div>\
			<ul class=\"tome__list\">\
            <li v-for=\"e in experiences\" v-if=\"e.event.user_permissions.read == true\" >\
                \
                    <span v-if=\"e.event.birth_string != false\" class=\"rubrik\">{{ e.event.birth_string }}</span>\
                    <span v-else class=\"rubrik\">Unknown</span>\
                     - <a class=\"ink\" :href=\"'/almanak/side/' + e.event.id\">{{ e.event.present_name }}</a>\
                     <strong v-if=\"masterdata.user_permissions.write\" class=\"rubrik cursor--pointer\" @click=\"e.event.delete_question = !e.event.delete_question\">&times;</strong>\
                \
                 <div v-if=\"e.event.delete_question == true\" >\
                    <p class=\"rubrik\">\
                        Do you want to remove\
                        <a class=\"ink\" :href=\"'/almanak/side/' + e.event.id\">\
                            {{ e.event.present_name }}\
                        </a>\
                         as an experience of \
                        <a class=\"ink\" :href=\"'/almanak/side/' + masterdata.id\">\
                            {{ masterdata.present_name }}\
                        </a>\
                        <button @click=\"removeExperience(e.id)\">Yes</button>\
                        <button @click=\"e.event.delete_question = !e.event.delete_question\" >No</button>\
                    </p>\
                 </div>\
            </li>\
			</ul>\
        </div>\
    ",
    data: function() {
        return {
            addExperience: false,
        }
    },
    methods: {
        removeExperience: function(id) {
                var data = {
                    "id": id,
                    "view": "entityeventrelation",
                    "updateObject": {
                        "deleted": true,
                    }

                };
                console.log(data);
                this.$store.dispatch("updateRelation",data);
        },
    },
    computed: {
		ingame_now: function (){
			return this.$store.state.session;
		},
        experiences: function(){
            return this.$store.state.element.relations.experiences;
        },
        masterdata: function(){
            return this.$store.state.element.masterdata;
        },
    }
});
