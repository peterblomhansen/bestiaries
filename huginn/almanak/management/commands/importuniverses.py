from django.core.management.base import BaseCommand, CommandError
from django.db import connections
from collections import namedtuple
from almanak.models import Universe, BestiariesUser, Age
#from polls.models import Question as Poll

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def namedtuplefetchall(cursor):
        #"Return all rows from a cursor as a namedtuple"
        desc = cursor.description
        nt_result = namedtuple('Result', [col[0] for col in desc])
        return [nt_result(*row) for row in cursor.fetchall()]


    def handle(self, *args, **options):

        st = ""
        crsr = connections['legacy'].cursor()


        def namedtuplefetchall(cursor):
            #"Return all rows from a cursor as a namedtuple"
            desc = cursor.description
            nt_result = namedtuple('Result', [col[0] for col in desc])
            return [nt_result(*row) for row in cursor.fetchall()]

        crsr.execute("SELECT * FROM univers")
        uni = namedtuplefetchall(crsr)
        for r in uni:
            e = Universe(pk=r.id, name=r.navn, slug=r.slug )
            e.save()

        crsr.execute("SELECT * FROM alder")
        ages = namedtuplefetchall(crsr)
        for a in ages:
            u = Universe.objects.get(pk=a.id_univers)
            age = Age(pk=a.id,universe=u, num_years=a.antal_aar, initials=a.forkortelse, name=a.navn, num_months=a.antal_maaneder, age_number=a.nummer)
            age.save()




    # universe = models.ForeignKey("Universe")
    # num_years = models.IntegerField()
    # initials = models.CharField(max_length=10)
    # name =  models.CharField(max_length=50)
    # num_months = models.IntegerField()
    # age_number = models.IntegerField()













        self.stdout.write(self.style.SUCCESS("Universes and ages importet"));
