var elementAllocation = Vue.component('element-allocation', {
    template: "\
    <div>\
        <h4 class=\"rubrik\">Relations</h4>\
        <ul class=\"tome__list\">\
            <li v-if=\"a.deleted == false && a.type_spec != 'rpg_session' && a.above.deleted == false\" v-for=\"a in above\">\
              <a class=\"ink\" :href=\"'/almanak/side/' + a.above.id\">{{ a.above.present_name }} </a>\
               <strong v-if=\"masterdata.user_permissions.write == true\" class=\"rubrik\" style=\"cursor:pointer\" @click=\"a.delete_question = true\"> &times;</strong><br/>\
               <span v-if=\"a.delete_question == true\">Do you want to delete this relation <button @click=\"deleteRelation(a.id)\" >Yes</button> <button @click=\"a.delete_question = false\" >No</button></span>\
            </li>\
            <ul>\
                <li class=\"ink\">\
                    <strong>\
                        {{ masterdata.present_name }} <span v-if=\"masterdata.user_permissions.write == true\" class=\"rubrik\" style=\"cursor:pointer\" @click=\"addBelowChooser = true\">+</span>\
                    </strong>\
                      <span v-if=\"addBelowChooser == true\" ><br/>\
                      <input type=\"radio\" v-model=\"relationAllocation\" value=\"above\" /> Add a page above {{ masterdata.present_name }}<br/>\
                      <input type=\"radio\" v-model=\"relationAllocation\" value=\"below\" /> Add a page below {{ masterdata.present_name }}<br/>\
                      <input v-model=\"addBelowQuery\" @keyup=\"getResults()\"> <button @click=\"addBelowChooser = false\">Cancel</button>\
                        <div v-if=\"addBelowShowList == true\" class=\"typeahead__list\">\
                          <p v-for=\"r in addBelowResult\" v-if=\"matchType(r)\" >\
                            <img style=\"height:20px\" :src=\"st + 'images/icons/type/rubrik/' + r.type_grund + '.svg'\" />\
                             <a class=\"ink\" :href=\"'/almanak/side/' + r.element.id\">{{ r.name }}</a>\
                              <button @click=\"createRelationBelow(r)\">Relate</button>\
                           </p>\
                        </div>\
                      </span>\
                    </li>\
                    <ul v-if=\"below.length > 0\">\
                      <li v-if=\"b.deleted == false && b.type_spec != 'rpg_session' && b.below.type_grund != 'individ' && b.below.type_grund != 'player_character' && b.below.user_permissions.read == true\" v-for=\"b in below\"><a class=\"ink\" :href=\"'/almanak/side/' + b.below.id\">\
                        <span >{{ b.below.present_name }} </span>\
                      </a>\
                      <strong class=\"rubrik\" v-if=\"masterdata.user_permissions.write == true\" style=\"cursor:pointer\" @click=\"b.delete_question = true\"> &times;</strong><br/>\
                      <span v-if=\"b.delete_question == true\">Do you want to delete this relation <button @click=\"deleteRelation(b.id)\" >Yes</button> <button @click=\"b.delete_question = false\" >No</button></span>\
                      </li>\
                    </ul>\
            </ul>\
        </ul>\
    </div>\
    ",
    props: {
        //above: Array,
        //below: Array,
        names: Array,
        //element: Object,
        //st: String,
        isEditor: Boolean,
    },
    created: function() {},
    mounted: function() {
        this.$store.dispatch("getElementRelations");
        //this.getElementRelations();
    },
    data: function() {
        return {
            data: "data",
            addBelowChooser: false,
            addBelowQuery: "",
            addBelowShowList: false,
            addBelowResult: [],
			relationAllocation: "below",
            types: {
                element_type: this.$store.state.element.masterdata.element_type,
                type_grund: this.$store.state.element.masterdata.type_grund,
                type_spec: this.$store.state.element.masterdata.type_spec,
            }
        }
    },
    computed:{
        static: function(){
            return staticUrl;
        },
		st: function (){
			return this.$store.state.initial.staticUrl;
		},
        // relations: function(){
        //     return this.$store.state.element.relations
        // },
        above: function(){

            return this.$store.state.element.relations.above;

        },
        below: function(){
            if(this.$store.state.element.relations.below) {
                return this.$store.state.element.relations.below;
            }else {
                return [];
            }

        },
        masterdata: function() {

            return this.$store.state.element.masterdata;

        },

    },
    methods: {
        deleteRelation: function(id) {
            var self = this;
            axios.patch('/api/elementrelation/' + id + '/update/', {
                deleted: true
            }).then(function(response) {
                console.log(response)
                self.$store.commit("getElementRelations");

            }).catch(function(error) {
                console.log(error);
            });
        },
        getResults: function() {
            var self = this;
            var results = [];
            if (this.addBelowQuery.length > 2) {
                axios.get('/api/element_by_name/?search=' + this.addBelowQuery).then(function(response) {
                    console.log(response.data)
                    self.addBelowResult = response.data;
                    self.addBelowShowList = true;
                }).catch(function(error) {
                    console.log(error);
                });
            }
            console.log(results);
            return results;
        },
        matchType(el) {
            var match = false;
            if (el.element_type == this.types.element_type) {
                match = true;
            }
            return match;
        },
        createRelationBelow: function(el) {
            var self = this;
            var relationData = {};
            if(this.relationAllocation == "below") {
				relationData = {
					below: el.element,
					above: self.masterdata.id,
					relation_type: "normal"
				};
			}
            if(this.relationAllocation == "above") {
				relationData = {
					below: self.masterdata.id,
					above: el.element,
					relation_type: "normal"
				};
			}
            axios.post("/api/elementrelation/create/", relationData).then(function(response) {
                self.$store.commit("getElementRelations");
                self.addBelowQuery = "";
                self.addBelowShowList = false;
            }).catch(function(error) {
                console.log(error);
            });
        }
    },
});
