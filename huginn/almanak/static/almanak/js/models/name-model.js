function NameModel(
    id= null,
    author = null,
    delete_question = false,
    element = null,
    end_age = null,
    end_date = 0,
    end_hours = 0,
    end_month = 0,
    end_minutes = 0,
    end_seconds = 0,
    end_year = 0,
    name = "",
    name_type = "prim",
    secret = false,
    shared_with = [],
    shared_with_full = [],
    start_age = null,
    start_date = 0,
    start_hours = 0,
    start_minutes = 0,
    start_month = 0,
    start_seconds = 0,
    start_year = 0,
    edit_permissions = true
)
{
    this.id = id;
    this.author = author;
    this.delete_question = delete_question;
    this.element = element;
    this.end_age = end_age;
    this.end_date = end_date;
    this.end_hours = end_hours;
    this.end_minutes = end_minutes;
    this.end_month = end_month;
    this.end_seconds = end_seconds;
    this.end_year = end_year;
    this.name = name;
    this.name_type = name_type;
    this.secret = secret;
    this.shared_with = shared_with;
    this.shared_with_full = shared_with_full;    
    this.start_age = start_age;
    this.start_date = start_date; 
    this.start_hours = start_hours;
    this.start_minutes = start_minutes;
    this.start_month = start_month;
    this.start_seconds = start_seconds;
    this.start_year = start_year
    this.edit_permissions = edit_permissions;
}

// var newNameModel = {
//     id: null,
//     author: {},
//     delete_question:false,
//     element: null,
//     end_age:null,
//     end_date:0,
//     end_hours:0,
//     end_minutes:0,
//     end_month:0,
//     end_seconds:0,
//     end_year:0,
//     name:"",
//     name_type:"prim",
//     secret:false,
//     shared_with:[],
//     start_age: null,
//     start_date:0,
//     start_hours:0,
//     start_minutes:0,
//     start_month:0,
//     start_seconds:0,
//     start_year:0,
//     edit_permissions: true,

// };