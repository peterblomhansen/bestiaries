var elementMasterData = Vue.component('element-master-data', {
     template: "\
     <div>\
         <p class=\"rubrik\">\
            <strong>{{ $store.getters.elementTypeTranslated(masterdata.type_spec) }}</strong> ({{ elementIsSecret }} <img v-if=\"masterdata.user_permissions.write == true\" :src=\"static + 'images/icons/pencil.svg'\" style=\"cursor:pointer;height:1em;\" @click=\"showSharings = !showSharings\" />)\
			<a target=\"_blank\" v-if=\"masterdata.user_permissions.write == true && (masterdata.type_spec == 'individ' || masterdata.type_spec == 'player_character') \" :href=\"'/rm/charactersheet/' + masterdata.id + '/'\"  class=\"rubrik cursor--pointer\" >charactersheet</a>\
			<span class=\"rubrik cursor--pointer\" v-if=\" masterdata.type_grund == 'gruppe' && masterdata.user_permissions.write == true\" @click=\"loadGroupTypesOfUniverse();assignGroupTypeQuestion = !assignGroupTypeQuestion\" >Assign Group Type</span>\
	  		<span v-if=\"$store.state.element.masterdata.type_spec == 'rpg_session'\" ><span class=\"rubrik\"> #{{ masterdata.session_number }} in </span><a class=\"ink\" :href=\"'/almanak/side/' + masterdata.in_campaign.id\">{{ masterdata.in_campaign.present_name }}</a></span>\
         </p>\
		 <div  v-if=\"assignGroupTypeQuestion == true && masterdata.type_grund == 'gruppe'\" >\
		 	<p class=\"rubrik\" ><strong>Assign group Type from Dropdown</strong></p>\
			 <select @change=\"updateGroupType(masterdata.id)\" v-model=\"groupType.id\">\
				 <option :value=\"null\" >None Group Type</option>\
				 <option v-for=\"gt in groupTypesOfUniverse\" :value=\"gt.id\" >{{ gt.name }}</option>\
			 </select>\
			  or Create new <input type=\"text\" v-model=\"newGroupTypeName\" /> <button @click=\"createNewGroupType()\" >create</button>\
		 </div>\
         <div v-if=\"showSharings == true\">\
            <p class=\"rubrik\"><strong>Secrecy and Sharing</strong></p>\
            <p> Secret: <input type=\"checkbox\" v-model=\"masterdata.secret\" @click=\"toggleSecrecy()\"> </p>\
         </div>\
         <div v-if=\"masterdata.secret == true\">\
            <p class=\"rubrik\" ><strong>Shared with: </strong><span style=\"cursor:pointer\" @click=\"loadUsersInUniverse();shareWithAnotherUser = !shareWithAnotherUser\">+</span></p>\
                <div v-if=\"shareWithAnotherUser == true && users != null\">\
              <select v-if=\"users != null\" @change=\"ShareWithUser(userToShare)\" v-model=\"userToShare\">\
                <option value=\"0\">Share with user</option>\
                <option v-for=\"(a,i) in users\" :value=\"i\" >\
                  {{ a.first_name }} {{ a.last_name }}\
                </option>\
              </select>\
            </div>\
\
            <ul class=\"tome__list\">\
              <li v-for=\"se in masterdata.shared_with\">\
                {{ se.first_name }} {{ se.last_name }}\
                <strong v-if=\"masterdata.user_permissions.write == true\" class=\"rubrik\" style=\"cursor:pointer\" @click=\"se.unshare_question = !se.unshare_question\"> &times;</strong>\
                <p v-if=\"se.unshare_question == true\">Do you want stop sharing {{ masterdata.present_name }} with {{ se.first_name }} {{ se.last_name }} <button @click=\"unshareWithUser(se)\" >Yes</button> <button>No</button></p>\
              </li>\
            </ul>\
          </div>\
          <p class=\"rubrik\">\
            <strong>Author:</strong> <span class=\"ink\">{{ masterdata.author.first_name}} {{ masterdata.author.last_name }}</span>\
            <img v-if=\"masterdata.user_permissions.write == true\" :src=\"static + 'images/icons/pencil.svg'\" style=\"cursor:pointer;height:1em;\" @click=\"loadUsersInUniverse();editWriters = !editWriters\" />\
          </p>\
          <div v-if=\"editWriters == true\" class=\"box--border\">\
            <p class=\"rubrik\"><strong>Writers</strong></p>\
            <p class=\"rubrik\">Author: <span class=\"ink\">{{ masterdata.author.first_name}} {{ masterdata.author.last_name }}</span></p>\
            <p class=\"rubrik\">Gamemasters:</p>\
            <p class=\"ink\" v-for=\"gm in allGamemasters\" >{{ gm.first_name}} {{ gm.last_name }}</p>\
            <p class=\"rubrik\">Editors</p>\
            <div v-for=\"e in masterdata.editors\">\
              <p>\
                {{ e.first_name}} {{ e.last_name }}\
                <strong v-if=\"masterdata.user_permissions.write == true\" class=\"rubrik\" style=\"cursor:pointer\" @click=\"e.unshare_question = !e.unshare_question\"> &times;</strong>\
              </p>\
              <p class=\"rubrik\" v-if=\"e.unshare_question == true\" >\
                Do you want to remove {{ e.first_name}} {{ e.last_name }} as editor for {{ e.present_name }}? <button @click=\"removeEditor(e)\">Yes</button> <button @click=\"e.unshare_question = !e.unshare_question\">No</button>\
              </p>\
            </div>\
            <select @click=\"addEditor(editorToAdd)\" v-model=\"editorToAdd\">\
              <option value=\"-1\">Add Editor</option>\
              <option v-for=\"(a,i) in $store.state.session.universe.users\" :value=\"i\" >\
                {{ a.first_name }} {{ a.last_name }}\
              </option>\
            </select>\
\
          </div>\
     </div>",
    data: function() {
        return {
            editCampaigns: false,
            campaignSelected: 0,
            addCampaignAjaxStatus: "",
            showSharings: false,
            shareWithAnotherUser: false,
            userToShare: -1,
            editWriters: false,
            editorToAdd: -1,
			// Group type
			newGroupTypeName: "",
			assignGroupTypeQuestion: false,
        }
    },
    mounted: function (){
        //this.getMasterdata();
    },
    methods: {
		loadCampaignsInUniverse: function (){
			var self = this;
			if(campaignId){
				axios.get("/api/session/" + campaignId + "/?d=us1_ages__cs1_universe__us1_name__us1_campaigns&format=json").then(function(response) {
					self.$store.state.session = response.data;
					self.$store.state.element.loaded.sessiondata = true;
					self.$store.state.initial.sessionLoaded = true
				}).catch(function(error) {
					console.log(error);
				});
			}
		},
        addCampaign: function(i) {
            var self = this;
            // self.masterdata.campaigns.push(self.campaignsInUniverse[i]);
            // self.addCampaignAjaxStatus = "adding";
			console.log(i)

            this.updateCampaignsAjax(i);
        },
        removeCampaign: function(c) {

			var self = this;
			axios.delete("/api/campaignelement/" + c.id + "/delete/").then(function(response) {
				self.$store.dispatch("getElement");
			});

        },
        updateCampaignsAjax: function(idCampaign) {
            //console.log("afaf", "afaf");
            var self = this;
			axios.post("/api/campaignelement/create/",{
				"element": self.masterdata.id,
				"campaign": idCampaign,
				"approved":0,
				"canon":0,

			}).then(function(response) {
				self.$store.dispatch("getElement");
			});

        },
        toggleSecrecy() {
            var self = this;
            this.masterdata.secret = !this.masterdata.secret;
            axios.patch('/api/element/' + this.masterdata.id + '/update/', {
                secret: self.masterdata.secret
            }).then(function(response) {
                console.log(response)
                //eventHub.$emit('reloadData');
            }).catch(function(error) {
                console.log(error);
            });
        },
		loadUsersInUniverse: function() {
			var self = this;
			if(campaignId){
				axios.get("/api/session/" + campaignId + "/?d=us1_ages__cs1_universe__us1_name__us1_users&format=json").then(function(response) {
					self.$store.state.session = response.data;
					self.$store.state.element.loaded.sessiondata = true;
					self.$store.state.initial.sessionLoaded = true
				}).catch(function(error) {
					console.log(error);
				});
			}
		},
        ShareWithUser: function(i) {
            var self = this;
            self.masterdata.shared_with.push(this.$store.state.session.universe.users[i]);
            self.addCampaignAjaxStatus = "adding";
            this.updateSharedWithAjax();
        },
        unshareWithUser: function(c) {
            console.log("chosen", c)
            var self = this;
            var i = _.findIndex(self.masterdata.shared_with, function(o) {
                return o.id == c.id;
            });
            console.log("index", i);
            this.masterdata.shared_with.splice(i, 1);
            this.updateSharedWithAjax();
        },
        updateSharedWithAjax: function() {
            var self = this;
            axios.patch('/api/element/' + this.masterdata.id + '/update/', {
                shared_with: self.sharedWithIds
            }).then(function(response) {
                console.log(response)
                //eventHub.$emit('reloadData');
            }).catch(function(error) {
                console.log(error);
            });
        },
        addEditor: function(i) {
            var self = this;
            if (i > -1) {
                self.masterdata.editors.push(this.$store.state.session.universe.users[i]);
                this.updateEditorsAjax();
            }
        },
        removeEditor: function(c) {
            console.log("chosen", c)
            var self = this;
            var i = _.findIndex(self.masterdata.editors, function(o) {
                return o.id == c.id;
            });
            console.log("index", i);
            this.masterdata.editors.splice(i, 1);
            this.updateEditorsAjax();
        },
        updateEditorsAjax: function() {
            var self = this;
            axios.patch('/api/element/' + this.masterdata.id + '/update/', {
                editors: self.editorsIds
            }).then(function(response) {
                console.log(response)
                self.$store.dispatch("getElement");
            }).catch(function(error) {
                console.log(error);
            });
        },
		// Groups
		loadGroupTypesOfUniverse: function() {
			var self = this;
			axios.get("/api/session/" + campaignId + "?d=us1_ages__cs1_universe__us1_campaigns__us1_name__us1_users__us1_kins__us1_group_types__gts1_titles&format=json").then(function(response) {
				self.$store.state.session = response.data;
				self.$store.state.element.loaded.sessiondata = true;
				self.$store.state.initial.sessionLoaded = true;
				self.editGrouptype = true;

			}).catch(function(error) {
				console.log(error);
			});


		},
		updateGroupType: function(id) {
			var self = this;
			//this.groupType.ajax_status.message = "Updating";
			console.log(self.groupType.id);

			axios.patch("/api/group/" + id + "/update/", { group_type: self.groupType.id }).then(function(response){
				//self.groupType.ajax_status.message = "Changed";
				self.$store.dispatch("getElement");
			}).catch(function(error) {
				console.log(error);
			});

		},
		createNewGroupType: function() {
			var self = this;
			var newGT = {
				universe: self.$store.state.initial.universeId,
				name: self.newGroupTypeName,
				description: "",
			};
			axios.post("/api/grouptype/create/",newGT).then(function(response){
				self.groupType.id = response.data.id;
				self.updateGroupType(self.masterdata.id)
				self.loadGroupTypesOfUniverse();
				//self.createNewGroupType = false;

			}).catch(function(error) {
				console.log(error);
			});
		},
    },
    computed: {
        static: function (){

            return this.$store.state.initial.staticUrl;

        },
        masterdata: function(){

            return this.$store.state.element.masterdata;
        },
        userUniverses: function (){

            return this.$store.state.user.universes;
        },
        campaignsInUniverse: function (){
            return this.$store.state.session.universe.campaigns;
        },
        allGamemasters: function() {
            var gms = [];
            _.each(this.masterdata.campaigns, function(c) {
                _.each(c.gamemasters, function(gm) {
                    gms.push(gm);
                });
            });
            return gms;
        },
		users: function (){
			return this.$store.state.session.universe.users;
		},
        // usersInUniverse: function() {
        //     var self = this;
        //     console.log("unis", self.userUniverses);
        //     console.log("current", self.currentUniverse);
        //     var i = _.findIndex(self.userUniverses, function(o) {
        //         return o.universe.id == universeId;
        //     });
        //     console.log("index", i)
        //     var users = this.userUniverses[i].universe.users;
        //     var array = [];
        //     _.each(users, function(u) {
        //         array.push(u)
        //     });
        //     return array;
        // },
        campaignsIds: function() {
            var ids = []
            _.each(this.masterdata.campaigns, function(c) {
                if (c) {
                    ids.push(c.id);
                }
            });
            return ids;
        },
        sharedWithIds: function() {
            var ids = []
            _.each(this.masterdata.shared_with, function(c) {
                if (c) {
                    ids.push(c.id);
                }
            });
            return ids;
        },
        editorsIds: function() {
            var ids = []
            _.each(this.masterdata.editors, function(c) {
                if (c) {
                    ids.push(c.id);
                }
            });
            return ids;
        },
        elementIsSecret: function() {
            if (this.masterdata.secret == true) {
                return "Secret";
            } else {
                return "Public";
            }
        },
		groupTypeCurrent: function(){
			var self = this;
			//console.log(self.groupType.id);
			var i = this.groupTypesOfUniverse.findIndex(function(g){ return g.id == self.groupType.id });
			console.log(i);
			return this.groupTypesOfUniverse[i];
		},
		groupTypesOfUniverse: function(){
			return this.$store.state.session.universe.group_types;
		},
		groupType: function(){

            return this.$store.state.element.masterdata.group_type;
        },

    }
});
