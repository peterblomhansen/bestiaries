
// <pre>{{ dataEditor }}</pre>\
var now = Math.round((new Date()).getTime() / 1000);


var descriptionEdit = Vue.component('description-edit', {
  template: "\
  <div class=\"beskrivelse__editor\" style=\"overflow:auto\">\
  	<div :class=\"updatingDataClass\" >\
  		<div style=\"padding:1em;\">\
			<div class=\"row\">\
				<div class=\"column\">\
			  		<div v-if=\"canCloseEditor == true\" class=\"cursor--pointer\" @click=\"closeEditor()\"><strong>Close Editor</strong></div>\
			  		<div v-if=\"canCloseEditor == false\" class=\"faded\" >Close</div>\
					{{ updatingData }}\
				</div>\
				<div class=\"column\" style=\"text-align:right;\">\
					<p v-if=\"dataEditor.id\" class=\"cursor--pointer\" @click=\"delete_question = !delete_question;updatingDataClass = 'alert alert--danger'\" ><strong>Delete</strong></a>\
					<div v-if=\"delete_question == true\" >Do you want to delete this description? <button @click=\"deleteDescription()\">Yes</button> <button @click=\"delete_question = !delete_question;updatingDataClass = 'alert alert--success'\">No</button> </div>\
					<p >Written by {{ dataEditor.author.first_name }} {{ dataEditor.author.last_name }}</p>" +
		    		"<p v-if=\"dataEditor.campaigns.length != 0\"  >for <a class=\"alert--link\" target=\"_blank\" :href=\"'/almanak/side/' + k.id\" v-for=\"k in dataEditor.campaigns\">{{ k.present_name }} </a></p>" +
		    		"<p v-if=\"dataEditor.created\">Created: {{ createdSplit.date }} at {{ createdSplit.time.substring(0,5) }}</p>" +
		    		"<p v-if=\"dataEditor.updated\">Last updated: {{ updatedSplit.date }} at {{ updatedSplit.time.substring(0,5) }}</p>" +
		 		"</div>" +
			"</div>\
 		</div>" +
 	"</div>" +
 	"<p class=\"alert alert--danger\" style=\"padding:1em\" v-show=\"hasNoLinks\">This description has no links, and therefore will not be shown anywhere.</p>" +
 	"<div style=\"display:flex;flex-wrap:wrap;padding:0.5em 1em 0.5em 1em;margin:0.1em 0\">" +
     	"<div style=\"flex-grow:1;flex:1;\">\
			<p>\
       			<button :class=\"classButtonOffgame\" @click=\"setTypeGame('off_game')\" >Offgame Note</button> \
       			<button :class=\"classButtonIngame\" @click=\"setTypeGame('in_game')\" >Ingame Description</button>\
			</p>\
		</div>\
		<div style=\"flex-grow:1;flex:1;\" v-if=\"dataEditor.description_type == 'in_game'\">\
			<p class=\"rubrik\" style=\"text-align:right;\">Ingame Author: \
				<span v-if=\"dataEditor.ingame_author\">\
					<a class=\"ink\" target=\"_blank\" href=\"'/almanak/side/' + dataEditor.ingame_author.id\">\
						{{ dataEditor.ingame_author.present_name }}\
					</a>\
				</span>\
				<span v-else >Not set</span>\
				<img class=\"cursor--pointer\" @click=\"showChangeIngameAuthor = !showChangeIngameAuthor\" :src=\"st + '/images/icons/pencil.svg'\" style=\"cursor: pointer; height: 1em;\">\
			</p>\
			<p v-if=\"showChangeIngameAuthor == true\" ><strong class=\"rubrik\">Change ingame author: </strong>\
				<autocomplete link-target=\"_blank\" admin=\"true\" :filter=\"'individ'\" :buttonMethod=\"'descriptionChangeIngameAuthor'\" :buttonText=\"'Change Ingame Author'\" >\
				</autocomplete>\
			</p>\
		</div>\
  	</div>\
	<div style=\"display:flex;padding:0.5em 1em 0.5em 1em;margin:0.1em 0;flex-wrap:wrap;\">\
		<div style=\"flex-grow:1;flex:1;\" >\
			<p>\
       			<button :class=\"classButtonHemmelig\" @click=\"setSecrecy(true)\" >Secret</button>\
       			<button :class=\"classButtonOffentlig\" @click=\"setSecrecy(false)\" >Public</button>\
			</p>\
   		</div>\
       <div style=\"flex-grow:1;flex:1;text-align:right;\" v-if=\"dataEditor.secret == true\">\
	   	<p class=\"rubrik\">Shared with: <strong class=\"cursor--pointer\" @click=\"showAddSharedWith = !showAddSharedWith\" >+</strong></p>\
		<p v-if=\"showAddSharedWith == true\">\
			<strong class=\"rubrik\">Share with:</strong>\
     		<autocomplete :filter=\"'individ'\" :buttonMethod=\"'shareDescriptionWith'\" :buttonText=\"'Add'\" >\
        	</autocomplete>\
		</p>\
	   </div>\
	</div>\
	<div style=\"display:flex;padding:0.5em 1em 0.5em 1em;margin:0.1em 0;flex-wrap:wrap;\">\
		<div @change=\"updateIngameTime()\"  >\
			Ingame Time: \
			date: <input class=\"input--narrow\" v-model.number=\"dataEditor.ingame_date\" type=\"number\" :disabled=\"ingameTimeNotDefined == true ? true : false\" />\
			month:\
			<select v-model=\"dataEditor.ingame_month\" :disabled=\"ingameTimeNotDefined == true ? true : false\" >\
			  <option value=\"0\">-</option>\
			  <option v-for=\"m in months.options\" :value=\"m.value\">{{ m.text }}</option>\
			</select>\
			year: <input class=\"input--narrow\" v-model.number=\"dataEditor.ingame_year\" type=\"number\" :disabled=\"ingameTimeNotDefined == true ? true : false\" />\
			age:\
			<select v-model=\"dataEditor.ingame_age\">\
				<option :value=\"null\">-</option>\
				<option v-for=\"a in ages\" :value=\"a.id\" >{{ a.initials }}</option>\
			</select>\
		</div>\
	</div>\
	<div>\
         <ul v-if=\"dataEditor.secret == true\" class=\"beskrivelse__link-tags\" style=\"display:flex;align-items: flex-end;flex-direction:row-reverse;margin:0 1em 0 1em;\"  >" +
           "<li class=\"ink--background\" v-for=\"d in dataEditor.shared_with\"><a class=\"ink--background\" :href=\"'/almanak/side/' + d.id\"  >{{ d.present_name }} </a><span style=\"cursor:pointer;color:#fff;\" @click=\"unshare(d.id)\">&times;</span>\
		   </li>\
	   	</ul>" +

	"</div>" +


	"<div style=\"margin:1em\">\
    	<div style=\"flex-grow:1;\" id=\"toolbar\"></div>\
	    <div >\
	    	<div id=\"content\" contenteditable=\"true\"\
	         	v-html=\"editorContent\"\
	         	@input=\"onDivInput($event)\"\
	         	class=\"beskrivelse__wysiwyg\"\
	    	>\
			</div>\
	    </div>\
	</div>\
	<div>\
	    <input style=\"margin-left:1em\" type=\"text\" v-model=\"query\" @input=\"getResults()\" placeholder=\"Search for pages in the bestiary\" >\
	    <ul v-if=\"showList\" class=\"beskrivelse__link-tags beskrivelse--link-chooser\">\
	      <li v-for=\"e in json\" >\
	        <button v-if=\"e.user_permissions.read == true\" @click=\"createElementLink(e)\" :class=\"inkBackgroundType\" class=\"ink--background\" style=\"color:#fff;cursor:pointer;\" >\
	          <img :src=\"elementTypeIcon(e.element_type,e.type_grund,e.type_spec)\" style=\"height:20px;\" />\
	            {{ e.name }} {{ e.element.id }}\
	          </button>\
	        </li>\
	    </ul>\
	</div>\
    <ul class=\"beskrivelse__link-tags\">\
      <li v-for=\"lnk in dataEditor.elements_in_description\" :class=\"inkBackgroundType\">\
        <img :src=\"elementTypeIcon(lnk.element.element_type,lnk.element.type_grund,lnk.element.type_spec)\" style=\"height:20px;\" />\
        <a :href=\"'/almanak/side/' + lnk.element.id\">{{ lnk.element.present_name }}</a>\
        <input type=\"text\" v-model=\"lnk.relevance\" @input=\"updateRelevance(lnk.id,lnk.relevance)\" />\
      </li>\
    </ul>\
    <pre>{{  }}</pre>\
  </div>",
  data: function () {
    return {
      // DATA
	  linksObsolete: this.$store.state.element.editors.description.data.elements_in_description,
	  elements_in_description: this.$store.state.element.editors.description.data.elements_in_description,
      editorContent: this.$store.state.element.editors.description.data.content, //this.editData,
      dataOrigin: this.editData,
      dataSession: this.session,
      dataIndivider: [],
      tekstTemp: "",
      updatedTemp: false, //this.editData.updated,
      authorsIngame: {
        selected: 1,//this.editData.author.id,
        options: [],
      },
      deltMedPotential: {
        selected: "",
        options: [],
      },
      months: {
        selected: 0, //this.editData.ingame_tid_array.maaned,
        options: [
          { text: "januar", value: "1"},
          { text: "februar", value: "2"},
          { text: "marts", value: "3"},
          { text: "april", value: "4"},
          { text: "maj", value: "5"},
          { text: "juni", value: "6"},
          { text: "juli", value: "7"},
          { text: "august", value: "8"},
          { text: "september", value: "9"},
          { text: "oktober", value: "10"},
          { text: "november", value: "11"},
          { text: "december", value: "12"},
        ]
      },
      tidsaldrer: {
          selected: 0, //this.editData.ingame_tid_array.alder,
        options: [],
      },
      disableIngameTime: false,
      linksNew: [],
      linksObsolete: [],
      // STATES
      showState: "",
      updatingAjax: false,
      confirmDelete: false,
      // LINK VARIABLES
      searchLink: false,
      customLinkSearch: false,
      linkSearchString: "",
      alterKey: "",
      json: [],
      showList: false,
      query: "",
      // MESSAGES
      timer: null,
	  showChangeIngameAuthor: false,
	  showAddSharedWith: false,
	  delete_question: false,

    };

  },
  created: function() {
          //this.dataEditor = this.$store.state.element.editors.description.data;
  },
  mounted: function(){



    var wysiwyg = require('wysiwyg');
    var events  = require('component-event');

    var toolbar = document.getElementById('toolbar');
    var content = document.getElementById('content');
    var ignore  = {html: true, img: true};

    for (var name in wysiwyg.commands){
      if (!ignore[name]){
        toolbar.appendChild(makeCommandButton(name));
      }
    }

    function makeCommandButton(name){
      var command = wysiwyg.commands[name];
      var checkState = wysiwyg.state[name];
      var button = document.createElement("button");

      button.setAttribute("name", name);
      if (button.textContent === ""){
        button.textContent = name;
      } else {
        button.innerText = name;
      }

      events.bind(button, "click", function(){
        command();
        content.focus();
        if (checkState) updateState();
      });

      if (checkState){
        events.bind(content, "keyup", updateState);
        events.bind(content, "mouseup", updateState);
      }

      var timer = null;
      var timeout = 100;
      function updateState(){
        clearTimeout(timer);
        timer = setTimeout(function(){
          button.setAttribute("class", checkState() ? "active" : "");
        },timeout);
      }

      return button;
    }


    var self = this;
    var individer = [];


    this.tekstTemp = this.dataEditor.content;

  },
  methods: {
	  deleteDescription: function() {
		axios.patch("/api/description/" + this.dataEditor.id + "/update/", { deleted: true  })
		  .then(function(response) {
			  console.log("asfsafs");
			  window.location.reload();
		  }).catch(function(error) {
			  console.log(error);
	  	});



	  },
	  dateTimeSplit: function(dt){
			if(dt.indexOf("T") != -1) {
				var split = dt.split("T");
				var date = split[0];
				var time = split[1];
			}else {
				date = "Not set";
				time = "";
			}
			return {
				"date": date,
				"time": time
			}
	  },
	  elementTypeIcon: function (type,type_grund,type_spec) {
        //console.log(type + " - " + type_grund + " - " + type_spec);

        var filename = type;
      //
        if(type == "begivenhed") {
          if(type_spec == "kampagne" || type_spec == "rpg_session"){
            filename = type_spec;
          }
        }
     //    }else if(type == "sted") {
      //
       if (type == "entitet") {
         filename = type_grund;
         if(type_grund == "player_character") {
           filename = "individ"
         }
       }
      //
    //
       return this.st + "images/icons/type/white/" + filename + ".svg";

    },

    addCampaign: function (idCampaign, nameCampaign){

      console.log("SESS",this.dataSession.ingame);

      console.log(idCampaign, nameCampaign);

      this.editData.kampagner.push({ id:idCampaign, navn: nameCampaign });
      this.updatingData = "Updating";
      this.updatingDataClass = "alert alert--warning";
      console.log(this.dataEditor.kampagner);

          this.updateToDB([this.toDbAddCampaign(idCampaign)]);




    },
    setTypeGame: function (type_game) {

      this.dataEditor.description_type = type_game;
      this.updatingData = "Updating";
      this.updatingDataClass = "alert alert--warning";


      this.updateToDB([this.toDbTypeGame(type_game)]);

    },
    setAuthorIngame: function(idIndivid) {

      console.log("setAuthorIngame");

      this.updatingData = "Updating";
      this.updatingDataClass = "alert alert--warning";

      this.updateToDB([this.toDbAuthorIngame(idIndivid)]);

    },
    setDateIngame: _.debounce(function() {
      var aar = 0; //parseInt(this.dataEditor.ingame_tid_array.aar);
      var dag = 0; //parseInt(this.dataEditor.ingame_tid_array.dag);
      var time = 0; //parseInt(this.dataEditor.ingame_tid_array.time);
      var minut = 0;
      var alder = 0;
      console.log("setDateIngame");
      if(this.disableIngameTime == true) {

      }else if (this.disableIngameTime == false) {
        var aar = parseInt(this.dataEditor.ingame_tid_array.aar);
        var dag = parseInt(this.dataEditor.ingame_tid_array.dag);
        var time = parseInt(this.dataEditor.ingame_tid_array.time);
        var minut = parseInt(this.dataEditor.ingame_tid_array.minut);
        var alder = parseInt(this.dataEditor.ingame_tid_array.alder);

      }


      console.log(aar);
      console.log(dag);
      console.log(time);
      console.log(minut);




      if(aar >= 0 && dag >= 0 && dag < 32 && time >= 0 && time < 24 && minut >= 0 && minut < 61) {
        this.updatingData = "Updating";
        this.updatingDataClass = "alert alert--warning";

        this.updateToDB([this.toDbDateIngame()]);
      }else {
        this.updatingData = "Invalid dato";

        this.updatingDataClass = "alert alert--danger";

      }
    },1000),
    setHemmelig: function(val) {

      this.dataEditor.hemmelig = val;
      this.updatingData = "Updating";
      this.updatingDataClass = "alert alert--warning";
      this.updateToDB([this.toDbHemmelig()]);

    },
    searchLinkFromButton: function() {

      var html = "";
      if (typeof window.getSelection != "undefined") {
          var sel = window.getSelection();
          if (sel.rangeCount) {
              var container = document.createElement("div");
              for (var i = 0, len = sel.rangeCount; i < len; ++i) {
                  container.appendChild(sel.getRangeAt(i).cloneContents());
              }
              html = container.innerHTML;
          }
      } else if (typeof document.selection != "undefined") {
          if (document.selection.type == "Text") {
              html = document.selection.createRange().htmlText;
          }
      }
      console.log(html);
      this.linkSearchString = html;
      //this.getElements(this.linkSearchString);


    },
    updateDataFromEditor: function (event) {
      console.log("updateDataFromEditor");
      this.$emit('updateData', true)
    },
    closeAndUpdate: function() {
      eventHub.$emit('updatePermissions', true);
      this.closeEditor();
      console.log("closed");
      this.updateDataFromEditor();
      console.log("data sent");

    },
    resetEdit: function() {
      console.log("resetting");

      this.dataOrigin.links = [];
      this.editData = this.dataOrigin;

      var app = this;
      this.updatingData = "Updating";
      this.updatingDataClass = "alert alert--warning";

      var content = document.getElementById('content');
      content.innerHTML = this.editData.tekst;
      this.tekstTemp = content.innerHTML;

      console.log(this.dataOrigin.updated);

      this.updateToDB([this.toDbContent(),this.toDbUpdateDateUpdated(this.dataOrigin.updated)]);

      if(this.linksNew.length > 0) {
          console.log("Removing relations");
        _.each(this.linksNew,function(i,d) {
          app.updateToDB([app.toDbRelationElement(i.id, i.relevans)]);
        });

      }
      if(this.linksObsolete.length > 0) {
          console.log("Add relations");
        _.each(this.linksObsolete,function(i,d) {
          app.updateToDB([app.toDbRemoveRelation(i.id)]);
        });
      }

    },
    closeEditor: function(event){
      //this.timerInactivity();
      this.$store.commit("closeDescriptionEditor");
    },
    timerInactivity: function(){
          var app = this;
          console.log("Updating in 3 seconds");
		  //if(app.dataEditor.elements_in_description.length > 0) {

	          window.clearTimeout(this.timer);
	          //var millisecBeforeRedirect = 10000;

	          this.timer = window.setTimeout(function(){

		          	app.updateToDB([app.toDbContent()]);

		            _.each(app.linksToDeletion,function(d,i){
		                app.updateToDB([app.toDbRemoveRelation(d.id)]);
		            });

		            _.each(app.linksToAdd,function(d,i){
		                app.updateToDB([app.toDBCreateElementRelation(d.element.id)]);
		            });


				    },2000);
			//}
			//else {
		//		app.updatingData = "No links in Descriptions";
		//		app.updatingDataClass = "alert alert--warning"
		//	}

    },

    onDivInput: function() {
        //var ev = e;
		var content = document.getElementById("content");
        this.tekstTemp = content.innerHTML;
        //this.tekstTemp = e.target.innerHTML;


        var matches = this.tekstTemp.match(/<a\s+(?:[^>]*?\s+)?href="([^"]*)"/gi);
        // var matchesStripped = [];
        // var currentLinks = [];
        // var obsoleteLinks = [];
        var idsInContent = [];
        //console.log("links",this.dataEditor.elements_in_description);
        // Runs through .elements_in_description to check if a link in content is a new og existing link
        _.each(matches,function(i,d){
          var id = i.replace('<a href="','').replace('"','');
          //console.log("currentLinks",id);
          if(idsInContent.indexOf(id) === -1) {
              idsInContent.push(parseInt(id));
          }

        });


        var app = this;
        var editor = this;
        this.updatingData = "Updating";
        this.updatingDataClass = "alert alert--warning";

		console.log(idsInContent)
        //this.updateToDB([this.toDbContent()]);

		window.clearTimeout(this.timer);
		//var millisecBeforeRedirect = 10000;
		var initial = app.dataEditor.elements_in_description.map(function(e){ return e.element.id })
		var existingLinks = [];
		var newLinks = [];
		var obsoleteLinks = [];
		console.log("Initial",initial);
		//console.log("obsNews",obsoleteLinkObjs);
		this.timer = window.setTimeout(function(){


			var descriptionId = editor.dataEditor.id



      	  if(descriptionId == null) {

      		  var createData = {
      			  author: editor.dataEditor.author.id,
      			  campaigns: [editor.dataEditor.campaigns[0].id],
      			  created: "0",
      			  updated: "0",
      			  content: "",
      			  description_type: "off_game",
      			  secret: true,
      			  elements_in_description: [],
      			  deleted: false,
   			  universe: editor.$store.state.initial.universeId

      		  }


      		  axios.post("/api/description/create/",createData).then(function(response){

   			  axios.post("/api/campaigndescription/create/",{
   				  campaign: editor.dataEditor.campaigns[0].id,
   				  description: response.data.id,


   			  }).then(function(response2){
				  app.$store.state.element.editors.dataStatus = "Updated";
				  app.updatingData = "Updated";
				  app.updatingDataClass = "alert alert--success";
   				  editor.$store.dispatch("openDescriptionEditor",response.data.id);

   			  });



      		  }).catch(function(error){
      			  console.log(error);
      		  });

      	  }else {

			exists = [];
			newIds = [];
			_.each(idsInContent,function(id){
				_.each(app.dataEditor.elements_in_description,function(r){
					if(id == r.element.id && exists.indexOf(r.element.id) == -1) {
						existingLinks.push(r)
						exists.push(r.element.id)
					}

				});
			});
			newLinks = idsInContent.filter(function(i) { return exists.indexOf(i) == -1  })


			_.each(app.dataEditor.elements_in_description,function(r){
				if (existingLinks.findIndex(function(el){ return el.id == r.id }) == -1) {
					obsoleteLinks.push(r.id);
				}
 			});

			function ajaxCall(method,url,load,id) {

		        var self = this;
		        return axios({
					"method": method,
					"url": url,
					"data": load});
		    };

			calls = [];

			_.each(newLinks,function(n){
				var data = {
					description:app.dataEditor.id,
					element:n,
					relevance:0,
				}
				ajaxCall("post","/api/descriptionelementrelation/create/",data);
			});

			_.each(obsoleteLinks,function(o){
				ajaxCall("delete","/api/descriptionelementrelation/" + o + "/delete/",{});
			});


			axios.patch("/api/description/" + app.dataEditor.id + "/update/", { content: app.tekstTemp  }).then(function(response){
				app.$store.state.element.editors.dataStatus = "Updated";
				app.updatingData = "Updated";
				app.updatingDataClass = "alert alert--success";
				app.$store.dispatch("openDescriptionEditor",response.data.id);
			});
		}

		},1000);


        // var wait = false;
        // var updateNeeded = false;
        // var relNoneExistingIds = [];
        // var numDeletedLinks = 0;

        //this.timerInactivity();


    },

    clearInput: function() {
      this.showlist = false
      this.searchString = ""
    //  this.type = ""
      this.json = []
      this.focusList = ""
    },
    changeBackgroundColor: function() {
      console.log("asfsafs debounced  ");
      this.editorBackground = "background:red;";
    },
    unixDateToString: function(stamp) {
      var a = new Date(stamp * 1000);
      var months = ['Jan','Feb','Mar','Apr','Maj','Jun','Jul','Aug','Sep','Okt','Nov','Dec'];
      var year = a.getFullYear();
      var month = months[a.getMonth()];
      var date = a.getDate();
      var hour = a.getHours();
      var min = a.getMinutes();
      var sec = a.getSeconds();
      var time = date + ' ' + month + ' ' + year ;
      return time;
    },
    sletBeskrivelse: function(){

      this.updateToDB([this.toDbSlet()]);

      this.closeAndUpdate();

    },
    createElementLink: function(e) {

      var html = "";
      if (typeof window.getSelection != "undefined") {
          var sel = window.getSelection();
          if (sel.rangeCount) {
              var container = document.createElement("div");
              for (var i = 0, len = sel.rangeCount; i < len; ++i) {
                  container.appendChild(sel.getRangeAt(i).cloneContents());
              }
              html = container.innerHTML;
          }
      } else if (typeof document.selection != "undefined") {
          if (document.selection.type == "Text") {
              html = document.selection.createRange().htmlText;
          }
      }
      console.log(html);
      if(html.length > 0) {
        document.execCommand("CreateLink", false ,e.element);
		this.onDivInput;
        //this.showList = false;
		//this.onDivInput();
        // console.log("Element",e.element);
        //
        // var link = {
        //     id: null,
        //     relevance: 0,
        //     element: {
        //         id: e.element,
        //         present_name: e.name,
        //         element_type:e.element_type,
        //         type_grund:e.type_grund,
        //         type_spec:e.type_spec,
        //
        //
        //     }
        //
        // };
        //
        // this.dataEditor.elements_in_description.push(link);
        //
        // //this.updateToDB([this.toDbRelationElement(element, "0")]);


      }else {

        alert("Du skal markere tekst i beskrivelsen, inden du kan knytte et specifikt link");
      }
    },
    getResults: function() {
        var self = this;
        var filter = "";
        if(this.filter) {
          filter = "&type=" + this.filter;
        }
        var results = [];
        if (this.query.length > 2) {
            axios.get('/api/element_by_name/?search=' + this.query + filter).then(function(response) {
                console.log(response.data)
                self.json = response.data;
                self.showList = true;
            }).catch(function(error) {
                console.log(error);
            });
        } else {
            self.showList = false;
        }
        console.log(results);
    },
    getElements: function (val){

      var self = this;
      this.type = "";
      this.url = "/api/element_by_name/";
     // this.url = "/api/element_data/get_all_elements";
      this.param = "q";

      if (val.length < this.min) return;

      if(this.url != null){

        // Callback Event
        this.onBeforeAjax ? this.onBeforeAjax(val) : null

        var ajax = new XMLHttpRequest();

        var params = ""
        if(this.customParams) {
          Object.keys(this.customParams).forEach(function(key) {
            params += `&${key}=${this.customParams[key]}`
            //console.log(this.customParams[key]);
          })
        }
        //console.log(`${this.url}?q=${val}&type=${this.type}`);
        ajax.open('GET', `${this.url}?q=${val}&type=${this.type}`, true);
//          ajax.open('GET', `${this.url}?${this.param}=${val}${params}`, true);
        ajax.send();

        ajax.addEventListener('progress', function (data) {
          if(data.lengthComputable){

            // Callback Event
            this.onAjaxProgress ? this.onAjaxProgress(data) : null
          }
        });

        ajax.addEventListener('loadend', function (data) {
          var json = JSON.parse(this.responseText);

          // Callback Event
          this.onAjaxLoaded ? this.onAjaxLoaded(json) : null

          self.json = self.process ? self.process(json) : json;

          if(self.json.length > 0) {
            self.showList = true;
          }else {
            self.showList = false;

          }

        });

      }
    },
    unshare: function(id){
      var self = this;

	  var sharedWithIds = []
	  _.each(this.dataEditor.shared_with,function(d,i){
		  sharedWithIds.push(d.id);
	  });
	  var i = sharedWithIds.indexOf(id);
	  sharedWithIds.splice(i,1);

      // _.remove(this.dataEditor.shared_with, {
      //     id
      // });

      this.updatingData = "Updating";
      this.updatingDataClass = "alert alert--warning";

	  this.$store.dispatch(
		  "consumerRequest",
		  {
			method:"patch",
			url :"/api/description/" + self.dataEditor.id + "/update/",
			data: { shared_with: sharedWithIds },
			mutation: { name: "openDescriptionEditor", payload: self.dataEditor.id },
		  }
	  );

      //this.updateToDB([this.toDbUnshare(idEntitet)]);

    },
    shareWith: function () {
      var self = this;
      var selectedObject = _.find(self.deltMedPotential.options, function(o) { return o.value == self.deltMedPotential.selected; });
      console.log(selectedObject);
      this.dataEditor.delt_med.push({ id_entitet: selectedObject.value, navn_entitet: selectedObject.text });

      this.updatingData = "Updating";
      this.updatingDataClass = "alert alert--warning";

      this.updateToDB([this.toDbShareWith(selectedObject.value)]);

    },
    updateRelevance: function(idElement,relevance) {

      this.updatingData = "Updating";
      this.updatingDataClass = "alert alert--warning";

      this.updateToDB([this.toDbRelationElement(idElement,relevance)]);

    },
	updateIngameTime: function () {
		this.updateToDB([this.toDbUpdateIngameTime()]);
	},
    getIndivider: function() {
      return axios.get('/api/element_data/get_all_elements?q=&type=entitet');
    },
	toDbUpdateIngameTime(){
		var date = {
			ingame_date: this.dataEditor.ingame_date,
			ingame_month: this.dataEditor.ingame_month,
			ingame_year: this.dataEditor.ingame_year,
			ingame_age: this.dataEditor.ingame_age,
			ingame_hours: this.dataEditor.ingame_hours,
			ingame_minutes: this.dataEditor.ingame_minutes,
			ingame_seconds: this.dataEditor.ingame_seconds,
		}


		return axios.patch("/api/description/" + this.dataEditor.id + "/update/", date);

    },
    toDbAddCampaign(idCampaign){

      return axios.post("/api/beskrivelse_data/add_campaign", { id: this.dataEditor.id, id_kampagne: idCampaign });

    },
    toDbShareWith: function(idEntitet) {

      return axios.post("/api/beskrivelse_data/share_with", { id_beskrivelse: this.dataEditor.id, id_entitet: idEntitet });

    },
    toDbUnshare: function(idEntitet) {

      return axios.post("/api/beskrivelse_data/unshare", { id_beskrivelse: this.dataEditor.id, id_entitet: idEntitet });

    },
    // toDbContent: function() {
    //
    //   return axios.post("/api/beskrivelse_data/update_beskrivelse_tekst", { id: this.dataEditor.id, content: this.tekstTemp  });
    //
    // },
    toDbContent: function() {

      return axios.patch("/api/description/" + this.dataEditor.id + "/update/", { content: this.tekstTemp  });

    },
    toDBCreateElementRelation: function(id) {
        var self = this;
        return axios.post("/api/descriptionelementrelation/create/",{ "element": id, "description": self.dataEditor.id });
    },
    toDbRelationElement: function(id,elementRelevance) {

      console.log("Adding relation",id);
      return axios.patch("/api/descriptionelementrelation/" + id+ "/update/", { relevance: elementRelevance });

    },
    toDbRemoveRelation: function(id) {
      console.log("Removing relation",id);
      return axios.delete("/api/descriptionelementrelation/" + id + "/delete/",{});
    },
    toDbElementLinks: function(elementIds) {


      return axios.post("/api/beskrivelse_data/sync_element_links", { id: this.dataEditor.id, elementIds: elementIds });

    },
    toDbTypeGame: function(type_game) {

      //return axios.post("/api/beskrivelse_data/change_type_game", { id: this.dataEditor.id, type_game: type_game });
      return axios.patch("/api/description/" + this.dataEditor.id + "/update/", { description_type: type_game  });

    },
    toDbAuthorIngame: function(idIndivid) {

      console.log("toDbAuthorIngame");

      return axios.post("/api/beskrivelse_data/set_author_ingame", { id: this.dataEditor.id, idIndivid: this.authorsIngame.selected });

    },
    toDbDateIngame: function() {

      console.log("toDbAuthorIngame");


        return axios.post("/api/beskrivelse_data/set_date_ingame", { id: this.dataEditor.id, date: this.dateArrayForDb });

    },
    toDbUpdateDateUpdated: function(date) {

      console.log(date);
      this.updatedTemp = date;

      return axios.post("/api/beskrivelse_data/update_date", { id: this.dataEditor.id, date: date, field_db: 'updated' });

    },
    toDbHemmelig: function() {

      return axios.post("/api/beskrivelse_data/update_hemmelig", { id: this.dataEditor.id, hemmelig: this.dataEditor.hemmelig  });

    },
    toDbSlet: function() {

      return axios.post("/api/beskrivelse_data/slet_beskrivelse", { id: this.dataEditor.id });

    },
    setSecrecy:function(isSecret) {

      this.updateDescriptionAjax({ secret: isSecret })
      this.dataEditor.secret = isSecret;

    },
    updateDescriptionAjax: function(changeData) {
      var self = this;
      axios.patch("/api/description/" + this.dataEditor.id + "/update/", changeData ).then(function(response) {
        self.$store.state.element.editors.dataStatus = "Updated";
      })

    },
    updateToDB: _.debounce(function(calls){


	 var editor = this;
         var descriptionId = editor.dataEditor.id



   	  if(descriptionId == null) {

   		  var createData = {
   			  author: editor.dataEditor.author.id,
   			  campaigns: [editor.dataEditor.campaigns[0].id],
   			  created: "0",
   			  updated: "0",
   			  content: "",
   			  description_type: "off_game",
   			  secret: true,
   			  elements_in_description: [],
   			  deleted: false,
			  universe: editor.$store.state.initial.universeId

   		  }


   		  axios.post("/api/description/create/",createData).then(function(response){

			  axios.post("/api/campaigndescription/create/",{
				  campaign: editor.dataEditor.campaigns[0].id,
				  description: response.data.id,


			  }).then(function(response2){
				  editor.$store.dispatch("openDescriptionEditor",response.data.id);

			  });



   		  }).catch(function(error){
   			  console.log(error);
   		  });

   	  }else {


   	      editor.updatingData = "Updating";
   	      editor.updatingDataClass = "alert alert--warning"

   	      axios.all(calls)
   	        .then(axios.spread(function (call1,call2,call3,call4) {
   			  editor.$store.state.element.editors.dataStatus = "Updated";
   	          editor.updatingData = "Updated";
   	          editor.updatingDataClass = "alert alert--success"

   	          console.log(descriptionId);
   	          editor.$store.commit("openDescriptionEditor",descriptionId);

	   	        })

   	      ).catch(function (error) {
   	            editor.updatingData = "Error";
   	            editor.updatingDataClass = "alert alert--danger"
   	            console.log(error);
             });
   		}

	},300),



    validateLinkRef: function (id) {

          return !isNaN(id);

    },


    updateRettigheder: function (){

      axios.get('/api/rettighed_data/update_alle_rettigheder')
        .then(function (response) {
          console.log(response);
        })
        .catch(function (error) {
          console.log(error);
        });


    }

  },


  computed: {
	  ages: function (){
		  return this.$store.state.session.universe.ages;
	  },
	  ingameTimeNotDefined: function (){
		  var notDef = true;
		  	if(this.dataEditor.ingame_age != null) {
				notDef = false;
			}

		  return notDef;
	  },
	  updatingData: {
		  	get() {
		  		return this.$store.state.element.editors.description.dataStatus;
	  		},
			set(value) {
				this.$store.state.element.editors.description.dataStatus = value;
			}
	  },
	  updatingDataClass: {
		  get() {
			  return this.$store.state.element.editors.description.dataStatusClass;
		  },
		  set(value) {
			  this.$store.state.element.editors.description.dataStatusClass = value;
		  }
	  },
	  createdSplit: function (){
		  return this.dateTimeSplit(this.dataEditor.created)
	  },
	  updatedSplit: function (){
		  return this.dateTimeSplit(this.dataEditor.updated);
	  },
	  canCloseEditor: function(){

		if(this.updatingData == "Updated" || this.updatingData == "Not edited" || this.updatingData == "Not Changed" || this.updatingData == "Create New Description") {
			return true;
		}
		else {
			return false;
		}
	  },

    dataStatus: function(){
      return this.$store.state.element.editors.description.dataStatus;
    },
    st: function(){
        return this.$store.state.initial.staticUrl;
    },
    editData: function(){

        return this.$store.state.element.editors.description.data;

    },
    dataEditor: function(){

        return this.$store.state.element.editors.description.data;

    },
    classButtonHemmelig: function() {

		var type = "";
		var active = "";

		if(this.dataEditor.description_type == "in_game") {

			type = "btn-ingame";
			if(this.dataEditor.secret == true) {
				active = "btn-ingame--active";
			}

        }else if(this.dataEditor.description_type == "off_game") {

			type = "btn-offgame";
			if(this.dataEditor.secret == true) {
				active = "btn-offgame--active";
			}

        }

		return type + " " + active;

    },
    classButtonOffentlig: function () {

		var type = "";
		var active = "";

		if(this.dataEditor.description_type == "in_game") {

			type = "btn-ingame";
			if(this.dataEditor.secret == false) {
				active = "btn-ingame--active";
			}
//          return "btn-ingame";

        }else if(this.dataEditor.description_type == "off_game") {

			type = "btn-offgame";
			if(this.dataEditor.secret == false) {
				active = "btn-offgame--active";
			}

        }

		return type + " " + active;

    },
    dateArrayForDb: function() {

      var date = {};
      if(this.disableIngameTime == false) {
  		    date.id_ingame_tid_alder = this.tidsaldrer.selected;
      }else if(this.disableIngameTime == true)  {
          date.id_ingame_tid_alder = "0";
      }

  		date.ingame_tid_aar = this.dataEditor.ingame_tid_array.aar;
  		date.ingame_tid_maaned = this.months.selected;
  		date.ingame_tid_dag = this.dataEditor.ingame_tid_array.dag;
  		date.ingame_klokken = this.dataEditor.ingame_tid_array.time + ":" + this.dataEditor.ingame_tid_array.minut + ":00";

      return date;

    },

    classButtonIngame: function () {

       if(this.dataEditor.description_type == "in_game") {

         return "btn-ingame btn-ingame--active";

       }else if(this.dataEditor.description_type == "off_game") {

         return "btn-ingame btn-ingame";

       }


    },
    classButtonOffgame: function () {

      if(this.dataEditor.description_type == "in_game") {

        return "btn-offgame";

      }else if(this.dataEditor.description_type == "off_game") {

        return "btn-offgame btn-offgame--active";

      }

    },
    hasNoLinks: function() {

      if(this.dataEditor.elements_in_description.length == 0) {
        return true;
      }else {
        return false;
      }


    },
    inkBackgroundType: function() {

      var cssClass = "";

      if(this.dataEditor.description_type == "in_game") {
        cssClass =  "ink--background";
      }
      else if(this.dataEditor.description_type == "off_game") {
          cssClass = "rubrik--background";
      }

      return cssClass;

    },
    ajaxActive: function (){

        if(this.linksToDeletion.length > 0) {
            console.log("there are link to be deleted");
        }
        if(this.linksToAdd.length > 0) {
            console.log("there are link to be added");
        }


        return false;
    },
    linksToDeletion: function(){

        return _.differenceBy(this.dataEditor.elements_in_description,this.currentLinks,"id");

    },
    linksToAdd: function(){

        return this.dataEditor.elements_in_description.filter(function(e){ return e.id == null});
        //return _.differenceBy(this.currentLinks,this.dataEditor.elements_in_description,"id");

    },
    // currentLinks: function () {
    //
    //    var app = this;
    //    var matches = this.tekstTemp.match(/<a\s+(?:[^>]*?\s+)?href="([^"]*)"/gi);
    //    var matchesStripped = [];
    //    var currentLinks = [];
    //    var obsoleteLinks = [];
    //    var idsInContent = [];
    //
    //    // Runs through .elements_in_description to check if a link in content is a new or existing link
	//    var currentElementRelations = []
    //    _.each(matches,function(i,d){
    //
	// 	   	var id = i.replace('<a href="','').replace('"','');
	// 		idsInContent.push(parseInt(id));
    //
    //
    //
	// 	});
    //
    //
	// 	_.each(app.dataEditor.elements_in_description,function(b,j){
    //
	// 	   if(idsInContent.indexOf(b.element.id) != -1) {
	// 		   currentElementRelations.push(b);
	// 	   }
    //
	// 	});
    //
	// 	function onlyUnique(value, index, self) {
	// 	   return self.indexOf(value) === index;
	// 	}
	// 	var trimmed = idsInContent.filter(onlyUnique);
    //
	// 	console.log(trimmed);
	// 	console.log(currentElementRelations);
	// 	console.log(app.dataEditor.elements_in_description);
    //
    //
    //
	// 	return currentElementRelations;
    //
    //
	// }


}

});
