from almanak.api.serializers import NameSerializer
#from almanak.api.permissions import PermissionCheck

from rolemaster.models import (
	RolemasterCharacter,
	Stats,
	StatBonusDistribution,
	Race,
	DevelopedSkill,
	Skill, SkillArea,
	SelectedSkill,
	Profession,
	ProfessionSkillCost,
	SkillAreaType,
	SkillRow,
	SkillSkillSimilarity,
	StatSpecialBonus,
	BodyDevelopment,
	SkillComment,
	SkillMiscBonus,
	ExperiencePoint,
	ProfessionLevelBonus
)

from rest_framework.serializers import (
	BooleanField,
	CharField,
	DateTimeField,
	HyperlinkedRelatedField,
	ImageField,
	IntegerField,
	ModelSerializer,
	PrimaryKeyRelatedField,
	RelatedField,
	SerializerMethodField,
	StringRelatedField,
	ValidationError,
	Serializer,
)


class BodyDevelopmentSerializer(ModelSerializer):

	class Meta:
		model = BodyDevelopment
		fields = "__all__"


class StatsSerializer(ModelSerializer):

	class Meta:
		model = Stats
		fields = "__all__"


class RaceSerializer(ModelSerializer):

	present_name = SerializerMethodField()

	class Meta:
		model = Race
		fields = "__all__"

	def get_present_name(self,obj):
		return obj.names.get_queryset()[0].name



class DevelopedSkillSerializer(ModelSerializer):
	ajax_lock = SerializerMethodField()
	changed = SerializerMethodField()
#	skill = SerializerMethodField()
#	area = SerializerMethodField()

	class Meta:
		model = DevelopedSkill
		fields = "__all__"


	def get_ajax_lock(self,obj):
		return False

	def get_changed(self,obj):
		return False

	# def get_area(self,obj):
	# 	if obj.area != None:
	# 		return obj.area.name
	# 	else:
	# 		return None

class ProfessionSkillCostSerializer(ModelSerializer):

	class Meta:
		model = ProfessionSkillCost
		fields = "__all__"

class ProfessionLevelBonusSerializer(ModelSerializer):

	class Meta:
		model = ProfessionLevelBonus
		fields = "__all__"


class ProfessionSerializer(ModelSerializer):
	skill_costs = ProfessionSkillCostSerializer(many=True)
	level_bonuses_of_profession = ProfessionLevelBonusSerializer(many=True)

	class Meta:
		model = Profession
		fields = "__all__"

class ProfessionMinimalSerializer(ModelSerializer):
	#skill_costs = ProfessionSkillCostSerializer(many=True)

	class Meta:
		model = Profession
		fields = ("id","name",)


class SelectedSkillSerializer(ModelSerializer):

	class Meta:
		model = SelectedSkill
		fields = ("id","skill","area","selected","character")


class ExperiencePointSerializer(ModelSerializer):

	class Meta:
		model = ExperiencePoint
		fields = "__all__"


class SkillMiscBonusSerializer(ModelSerializer):
	delete_question = SerializerMethodField()

	class Meta:
		model = SkillMiscBonus
		fields = "__all__"
		
	def get_delete_question(self,obj):
		return False


class StatSpecialBonusSerializer(ModelSerializer):

	class Meta:
		model = StatSpecialBonus
		fields = "__all__"


class SkillCommentSerializer(ModelSerializer):
	delete_question = SerializerMethodField()

	class Meta:
		model = SkillComment
		fields = "__all__"


	def get_delete_question(self,obj):
		return False


class RolemasterCharacterSerializer(ModelSerializer):
	stats = StatsSerializer(many=True)
	race = RaceSerializer()
	names = NameSerializer(many=True)
	dev_ranks = SerializerMethodField()
	character_experience = ExperiencePointSerializer(many=True)
	skill_misc_bonuses = SkillMiscBonusSerializer(many=True)
	stat_special_bonuses = StatSpecialBonusSerializer(many=True)
	skill_comments = SkillCommentSerializer(many=True)
	profession = ProfessionSerializer()
	body_development_of_character = BodyDevelopmentSerializer(many=True)
	#user_permissions = SerializerMethodField()
	selected_skills = SelectedSkillSerializer(many=True)

	class Meta:
		model = RolemasterCharacter
		fields = ("id","stats","race","names","campaigns","dev_ranks","profession","body_development_of_character","selected_skills","character_experience","skill_misc_bonuses","skill_comments","stat_special_bonuses")


	def get_dev_ranks(self,obj):

		q = DevelopedSkill.objects.filter(character=obj.id)
		s = DevelopedSkillSerializer(q,many=True)
		#print(len(q))
		return s.data





class StatBonusDistributionSerializer(ModelSerializer):

	class Meta:
		model = StatBonusDistribution
		fields = "__all__"


class SkillAreaTypeSerializer(ModelSerializer):

	class Meta:
		model = SkillAreaType
		fields = "__all__"


class SkillSerializer(ModelSerializer):
	supported_area_types = SkillAreaTypeSerializer(many=True)
	area = SerializerMethodField()

	class Meta:
		model = Skill
		fields = "__all__"

	def get_area(self,obj):
		return None
	# def get_supported_area_types(self,obj):
	# 	if obj.area_support == True:
	# 		return obj.supported_area_types.get_queryset()





class SkillAreaSerializer(ModelSerializer):

	class Meta:
		model = SkillArea
		fields = "__all__"



class SkillCostSerializer(Serializer):
	id = IntegerField(allow_null=True)
	cost_1 = IntegerField()
	cost_2 = IntegerField()
	cost_3 = IntegerField()



class SkillRankSerializer(Serializer):
	id 				= IntegerField(allow_null=True)
	skill 			= IntegerField()
	area			= IntegerField()
	character 		= IntegerField()
	level 			= IntegerField()
	num_ranks		= IntegerField()
	num_ranks_init	= SerializerMethodField()
	development_type = CharField(allow_blank=True, max_length=100)
	dev_points_used = IntegerField()


	def get_num_ranks_init(self,obj):
		return obj.num_ranks



class SkillSkillSimilaritySerializer(Serializer):
	factor = SerializerMethodField()
	area = SerializerMethodField()

	class Meta:
		model = SkillSkillSimilarity
		fields = "__all__"


	def get_factor(self,obj):
		factor = obj.ratio.split("/")
		return int(factor[0]) / int(factor[1])

	def get_area(self,obj,area):
		return obj.area



class ComputedSkillSkillSimilaritySerializer(Serializer):
	origin = SerializerMethodField()
	target = SerializerMethodField()
	area = IntegerField()
	ratio = CharField(allow_blank=True, max_length=100)
	factor = SerializerMethodField()

	def get_origin(self,obj):
		return obj.origin.id

	def get_target(self,obj):
		return obj.target.id

	def get_factor(self,obj):
		factor = obj.ratio.split("/");
		return int(factor[0]) / int(factor[1])



class SkillRowSerializer(Serializer):
	id 				= IntegerField()
	area 			= CharField(allow_blank=True, max_length=100)
	category 		= CharField(allow_blank=True, max_length=100)
	name 			= CharField(allow_blank=True, max_length=100)
	area_name 		= CharField(allow_blank=True, max_length=100)
	area_support	= BooleanField()
	stat_1			= CharField(allow_blank=True, max_length=100)
	stat_2			= CharField(allow_blank=True, max_length=100)
	stat_3			= CharField(allow_blank=True, max_length=100)
	cost			= SkillCostSerializer(allow_null=True)
	gm_ranks		= SkillRankSerializer(read_only=True)
	hobby_ranks		= SkillRankSerializer(read_only=True)
	dev_ranks		= SkillRankSerializer(many=True,read_only=True)
	change_cost 	= SerializerMethodField()
	similarity		= ComputedSkillSkillSimilaritySerializer(many=True)  #SkillSkillSimilaritySerializer(many=True)

	#selected		= BooleanField()
    #
	# class Meta:
	# 	model = SkillRow

	def get_change_cost(self,obj):
		return False


	# def get_similarity(self,obj):
    #
	# 	#q = None
	# 	#if obj.similarity:
	# 	#	print(obj.similarity)
	# 	#	q = obj.similarity
	# 	serializer = ComputedSkillSkillSimilaritySerializer(q,many=True)
	# 	#print(serializer.is_valid())
	# 	#if serializer.is_valid():
	# 	return obj.data  #""serializer.initial_data


class RolemasterCharacterCreateSerializer(ModelSerializer):

	class Meta:
		model = RolemasterCharacter
		fields = ("pk","level","race","profession")


class ProfessionSkillCostSerializer(ModelSerializer):

	class Meta:
		model = ProfessionSkillCost
		fields = "__all__"



class RolemasterCharacterUpdateSerializer(ModelSerializer):


	class Meta:
		model = RolemasterCharacter
		fields = "__all__"
#
# class StatsCreateSerializer(ModelSerializer):
#
# 	class Meta:
# 		model = StatBonusDistribution
# 		fields = "__all__"
#
