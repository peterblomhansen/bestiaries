from typing import List
from bs4 import BeautifulSoup, element
from django.db.models import QuerySet
from almanak.models import DescriptionElementRelation, Description, Element
import re


def is_description_content_equal(content_current: str , content_new: str) -> bool:   
    
    return content_current == content_new


def list_links_in_html(html: str) -> element.ResultSet:

    soup = BeautifulSoup(html, 'html.parser')
    return soup.find_all('a')


def is_link_list_equal(content_current: str , content_new: str) -> bool:

    return get_element_ids_from_links(content_current) == get_element_ids_from_links(content_new)


def get_element_ids_from_links(content: str) -> List[int]:

    links = list_links_in_html(content)

    def get_href_from_tag(tag) -> int:
        return int(re.findall('[0-9]+', tag['href'])[0])

    return list(set(map(get_href_from_tag,links)))


def ids_from_list_a_not_in_list_b(list_a: List[int], list_b: List[int]) -> List [int]:

    def is_id_not_in_list(id):
        if id not in list_b:
            return id

    return list(filter(is_id_not_in_list,list_a))


def sync_description_element_relations(description_id: int,ids_content_request: List[int],ids_content_db: List[int]) -> None:
    new_ids = ids_from_list_a_not_in_list_b(ids_content_request,ids_content_db)
    obsolete_ids = ids_from_list_a_not_in_list_b(ids_content_db,ids_content_request)

    print("new ids",new_ids)
    print("obsolete_ids",obsolete_ids)
    for element_id in obsolete_ids:
        remove_description_element_relation(description_id,element_id)

    for element_id in new_ids:
        create_description_element_relation(description_id,element_id)



def remove_description_element_relation(description_id: int, element_id: int) -> bool:

    try:
        relation_to_be_removed = DescriptionElementRelation.objects.get(description__id=description_id,element__id=element_id)
    except DescriptionElementRelation.DoesNotExist:
        relation_to_be_removed = None

    if relation_to_be_removed:
        relation_to_be_removed.delete()

def create_description_element_relation(description_id: int, element_id: int) -> bool:

    relations_to_be_created = DescriptionElementRelation.objects.filter(description__id=description_id,element__id=element_id).exists()

    if relations_to_be_created:
        print("relations already exists",relations_to_be_created)
        #return False
    else:
        print("relations does not exist - Creating")

        related_element = Element.objects.get(pk=element_id)
        related_description = Description.objects.get(pk=description_id)
        if related_element and related_description:
            relation_new = DescriptionElementRelation(
                description=related_description,
                element=related_element
            )
            relation_new.save()
            print("Relation created")








