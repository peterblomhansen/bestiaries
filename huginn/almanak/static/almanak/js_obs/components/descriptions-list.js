var id_current = 0;
var descriptionsList = Vue.component('descriptions-list', {
    template: "<div style=\"min-height:200px;\">" +

                "<description-edit v-if=\"$store.state.element.editors.description.open == true\"></description-edit>" +
				"<image-edit v-if=\"$store.state.element.editors.image.open == true\"></image-edit>" +


                "<div v-if=\"$store.state.element.loaded.descriptionsImages == false\" style=\"margin-top:2em;display:flex;flex-direction:column;align-items:center;height:100%;width:100%;\">" +
                  "<div class=\"loading\">" +
                    "<div class=\"spinner\">" +
                      "<div class=\"mask\">" +
                        "<div class=\"maskedCircle\"></div>" +
                      "</div>" +
                    "</div>" +
                  "</div>" +
                "</div>" +

                "<div class=\"row\">" +

                  "<div class=\"column\" style=\"margin:1em 0.5em 2em 0.2em;\">" +

                    "<div v-for=\"m in leftColumnComp\" >" +
						"<image-show v-if=\"m.image\" :loaded-data=\"m\" >" +
						"</image-show >" +

						"<description-show v-if=\"m.description\" :loaded-data=\"m\" >" +

						"</description-show >" +

                    "</div>" +
                  "</div>" +

                  "<div class=\"column\" style=\"margin:1em 0.5em 2em 0.2em;\">" +

                    "<div v-for=\"m in rightColumnComp\" >" +
						"<image-show v-if=\"m.image\" :loaded-data=\"m\" >" +
						"</image-show >" +

						"<description-show v-if=\"m.description\" :loaded-data=\"m\" >" +

						"</description-show >" +

                    "</div>" +
                  "</div>" +

                "</div>" +

              //  "<description-show v-for=\"f in filteredData\" :key=\"f.id\"  ></description-show>" +
              //  "<div v-for=\"f in filteredData\">{{ f.id }}</div>" +
                //"<pre>{{ vuexdata }}</pre>" +
              "</div>",
    props: {
        element: Object,
        ingame: Object,
        st: String,
        ages: Array,
    },
    data: function() {
        return {
            descriptionsLoaded: false,
            descriptions: [],
            images: [],
            filterKey: "",
            dataLoading: 1,
            merged: [],
            leftColumn: [],
            rightColumn: [],
            activeInEdtior: [],
            elementId: Number,
            rettigheder: {
                seEle: [],
                seBes: [],
                redEle: [],
                redBs: [],
                seBld: [],
                redBld: [],
            },
            allElements: [],
        }
    },
    beforeDestroy: function() {
        eventHub.$off('updatePermissions', this.getUpdatedData)
    },
    created: function() {
        eventHub.$on('updatePermissions', this.getUpdatedData);
        //console.log("xddgd");
        this.dataLoading = true;
        //console.log(this);
    },
    mounted: function() {
        //console.log("descriptions");
        this.$store.commit('ajaxDescriptionsImages');

      //  this.getDescriptionsImages();
    },
    computed: {
        vuexdata: function(){

          return this.$store.state.element.descriptions;


        },
        filteredData: function() {

			//var array = [];

			//_.each(this.$store.state.element.descriptions, function(d,i){
			//	console.log(d.description.permissions.read);
			//});

			//return this.$store.state.element.descriptions;
			if(this.$store.state.element.descriptions.length > 0) {
	            return this.$store.state.element.descriptions.filter(
					function(e) {
						if(e.description) {
							return e.description.permissions.read == true
						}
						if(e.image) {
							return e.image.permissions.read == true
						}


					}
				);
			}

			return [];
            // var filterKey = this.filterKey && this.filterKey.toLowerCase();
            // var data = this.$store.getters.getDescriptionsImages;
            // if (filterKey) {
            //     data = data.filter(function(row) {
            //         if (row.description) {
            //             return Object.keys(row.description).some(function(key) {
            //                 return String(row.description[key]).toLowerCase().indexOf(filterKey) > -1
            //             })
            //         }
            //     })
            // }
            // return data
        },
        leftColumnComp: function() {
            var self = this;
            var half = Math.round(this.filteredData.length / 2);
            var left = [];
            var count = 0;
            // _.each(self.filteredData, function(d, i) {
            //     if (count < half) {
            //         left.push(d)
            //     } else {}
            //     count++;
            // });
            if(self.filteredData) {
				_.each(self.filteredData, function(d,i){
                //self.filteredData.forEach(function(d, i) {
                    if (count < half) {
                        left.push(d)
                    } else {}
                    count++;

                });
            }
            return left;
        },
        rightColumnComp: function() {
            var self = this;
            var half = Math.round(this.filteredData.length / 2);
            var right = [];
            var count = 0;
            if (self.filteredData && self.filteredData.length > 1) {
                self.filteredData.forEach(function(d, i) {

                    if (count < half) {} else {
                        right.push(d)
                    }
                    count++;
                });
            }
            return right;
        },
    },
    methods: {
        getUpdatedData: function(emitEvent) {
            this.dataLoading = 1;
        },
        getDescriptionsImages() {
            var self = this;
            axios.get('/api/descriptionsimagesofelement/' + this.element.id + '/').then(function(response) {
                console.log(response)
                self.descriptions = response.data.descriptions_of_element;
                self.images = response.data.images_of_element;
                self.mergeAndSort(self.images, self.descriptions);
                self.descriptionsLoaded = true;
            }).catch(function(error) {
                console.log(error);
            });
        },
        mergeAndSort: function(bld, bes) {
            var self = this;
            var mergedTemp = _.concat(bld, bes);
            self.merged = mergedTemp;
            self.merged.sort(function(a, b) {
                return parseFloat(b.relevance) - parseFloat(a.relevance);
            });
            self.dataLoading = 0;
        },
        getUserPermissions: function() {
            return axios.get('/api/rettighed_data/get_user_permissions');
        },
        getBeskrivelser: function() {
            return axios.get('/api/beskrivelse_data/beskrivelser_per_element?element=' + id_current);
        },
        getAllElements: function() {
            return axios.get('/api/element_data/get_all_elements');
        },
        getBilleder: function() {
            return axios.get('/api/billede_data/billeder_per_element?element=' + id_current);
        },
        updatePermissions: function() {
            return axios.get('/api/billede_data/billeder_per_element?element=' + id_current);
        },
        toDbUpdateRettigheder: function() {
            return axios.get('/api/rettighed_data/update_alle_rettigheder');
        },
    },
});
