var descriptionEditor = Vue.component('description-editor', { 
    template: "\
    <transition name=\"fade-fold\">\
        <overlay>\
                <div class=\"description-editor__status\" :class=\"editorStatus.cssClass\" >\
                    <div>\
                        <p class=\"cursor--pointer\" @click=\"$store.dispatch('unloadDescriptionData',descriptionData.id)\" >\
                            <strong>Close editor</strong>\
                        </p>\
                        <p>{{ editorStatus.message }}</p>\
                        <p>created {{ getDateOnly(descriptionData.created) }}</p>\
                        <p>updated {{ getDateOnly(descriptionData.updated) }}</p>\
                    </div>\
                    <div style=\"text-align:right\">\
                        <p>\
                            <span v-if=\"delete_question === true\" >\
                                Do you want to delete this description \
                                <button class=\"button--small\" @click=\"deleteDescription()\">Yes</button> \
                                <button class=\"button--small\" @click=\"delete_question = false\">No</button>\
                            </span>\
                            <strong v-else @click=\"delete_question = true\" class=\"cursor--pointer\" >Delete</strong></p>\
                        <p>{{ secretPublic }} {{ offGameInGame }} Description </p>\
                        <p>written by {{ descriptionData.author_full.first_name }} {{ descriptionData.author_full.last_name }}</p>\
                        <p><strong>{{ forCampaignsText }}</strong></p>\
                    </div>\
                </div>\
                <div class=\"description-editor__main\" >\
                    <div  >\
                        <fieldset-wrapper legend=\"Author\">\
                            <p>User: <span v-if=\"!isUserGM\" >{{ author.first_name }} {{ author.last_name }}</span> <user-select :selected-id=\"author.id\" :on-selection=\"changeAuthor\" v-if=\"isUserGM\" /> </p>\
                            <p v-if=\"descriptionData.description_type === 'in_game'\">\
                                Ingame Author: {{ ingameAuthor }} \
                                <button @click=\"editIngameAuthorToggle = !editIngameAuthorToggle\" class=\"button--small\" >edit</button>\
                                <autocomplete v-if=\"editIngameAuthorToggle\" \
                                    :resetOnButtonClick=\"true\" \
                                    filter=\"&type=individual__player_character&user_can_edit_element=true\"\
                                    placeholder-text=\"Change Ingame Author\"\
                                    button-text=\"Make Ingame Author\"\
                                    :on-button-click=\"setIngameAuthor\"\
                                ></autocomplete>\
                            </p>\
                        </fieldset-wrapper>\
                        <edit-ingame-time :obj=\"descriptionData\" field=\"ingame\" title=\"Ingame time\" />\
                    </div>\
                    <div >\
                        <fieldset-wrapper legend=\"Type\">\
                            <button @click=\"$store.dispatch('updateDescription',{ id: descriptionData.id, description_type: 'in_game' });\" class=\"button--ink\" :class=\"typeGameButtonIsActive('in_game')\" >Ingame Description</button>\
                            <button @click=\"$store.dispatch('updateDescription',{ id: descriptionData.id, description_type: 'off_game' });\" class=\"button--rubrik\" :class=\"typeGameButtonIsActive('off_game')\" >Offgame Note</button>\
                            <button class=\"button--rubrik\"  ><input type=\"checkbox\" v-model=\"isSecret\" > Secret</button>\
                        </fieldset-wrapper>\
                        <fieldset-wrapper v-if=\"secretBinary === true\" legend=\"Shared with\">\
                            <autocomplete\
                                :on-button-click=\"addSharedWith\"\
                                placeholder-text=\"Share with\"\
                                button-text=\"Share with\"\
                                filter=\"&type=individual__player_character\"\
                            ></autocomplete>\
                            <p v-for=\"el in descriptionData.shared_with_full\" >\
                                {{ el.present_name.name  }} \
                                <span @click=\"removeSharedWith(el.id)\" >&times;</span>\
                            </p>\
                        </fieldset-wrapper>\
                    </div>\
                    <div >\
                        <fieldset-wrapper legend=\"Editor\" style=\"display:grid;grid-template-columns:auto;grid-template-rows:auto;\" >\
                            <quill-editor v-model=\"editorData\"\
                                style=\"grid-row: 1 / 2;grid-column: 1/ 2;\"\
                                :class=\"'ql-editor--'+ descriptionData.description_type\" \
                                ref=\"quillEditorA\"\
                                :options=\"editorOption\"\
                                @blur=\"onEditorBlur($event)\"\
                                @focus=\"onEditorFocus($event)\"\
                                @ready=\"onEditorReady($event)\"\
                            >\
                            </quill-editor>\
                            <div v-if=\"isLinkOverlayVisible === true\"\
                                style=\"grid-row: 1 / 2;grid-column: 1/ 2;background:#fff;z-index:3;\"\
                            >\
                                <h3>Insert Bestiaries Link</h3>\
                                <div v-html='selectionNearContext' ></div>\
                                <autocomplete \
                                    :initialQuery=\"selectionRange.text\"\
                                    :resetOnButtonClick=\"true\" \
                                    placeholder-text=\"Find page to link to\"\
                                    button-text=\"Create Link\"\
                                    :on-button-click=\"createSelectionLink\"\
                                ></autocomplete>\
                            </div>\
                        </fieldset-wrapper>\
                    </div>\
                    <div >\
                        <div>{{ relationStatusKey }}</div>\
                        <transition name=\"fade\" >\
                            <fieldset-wrapper :legend=\"'Pages mentioned loading...'\"  v-if=\"!$store.state.editors.descriptionEditor.relations\" >\
                            </fieldset-wrapper>\
                        </transition>\
                        <transition name=\"fade\" >\
                            <fieldset-wrapper :legend=\"'Pages mentioned (' + relations.count + ')'\"  v-if=\"$store.state.editors.descriptionEditor.relations\" >\
                                <input type=\"text\" v-model=\"linkSearchQuery\" placeholder=\"Search for pages\" />\
                                <transition name=\"fade\" >\
                                    <div v-if=\"elementSearchResult.results && elementSearchResult.results.length > 0\" >\
                                        <p v-for=\"n in elementSearchResult.results\" class=\"description-editor__element-relation\" :class=\"relationBackground\" >\
                                                {{ n.name }}\
                                                <img class=\"description-editor__element-relation-icon\" :src=\"$store.getters.elementTypeIcon(n.element_type,n.type_grund,n.type_spec)\" /> \
                                            <button @click=\"insertBestiariesLink(n.element_id)\" class=\"button--small\" >link</button>\
                                        </p>\
                                    </div>\
                                </transition>\
                                <description-editor-relation v-for=\"rel in relations.results\" :relation-background=\"relationBackground\" :relation=\"rel\" :key=\"rel.id\" ></description-editor-relation>&nbsp;\
                            </fieldset-wrapper>\
                        </transition>\
                    </div>\
                </div>\
        </overlay>\
    </transition>",
    mounted: function(){

        var self = this;
        this.customButton = document.querySelector('.ql-bslink');
        this.customButton.addEventListener('click', function(value) {
            if (self.selectionRange.length > 0) {
                self.isLinkOverlayVisible = true;
            }
            else {
                alert("Select at least one word");
            }

            
        });
        this.editorA.on('text-change', function(delta, oldDelta, source) {
            self.syncContent();
        });


        this.editorA.on('selection-change', function(range, oldRange, source) {      
            if(range) { 
                var selectedText = range.length > 0 ? self.editorA.getText(range.index, range.length) : null;
                Vue.set(self,"selectionRange",
                    {
                    index: range.index,
                    length: range.length,
                    text: selectedText,                  
                    }
                )
            }
        });
            // self.editorA.formatText( self.selectionRange.index, self.selectionRange.length, "background","#444");
           // self.editorA.formatText( self.selectionRange.index, self.selectionRange.length, "color","#fff");    

            // if(self.selectionRange.index,self.selectionRange.length) {
            //     self.editorA.removeFormat( self.selectionRange.index, self.selectionRange.length)
            // }
            // if (range) {
            //   if (range.length == 0) {
            //     self.selectionRange = {
            //         index: range.index,
            //         length: 0,
            //         text: null,
            //     };

            //   } else {
            //     self.selectionRange = {
            //         index: range.index,
            //         length: range.length,
            //         text: self.editorA.getText(range.index, range.length),
            //     };                
            //   }
            // } else {                
            //     self.editorA.formatText( self.selectionRange.index, self.selectionRange.length, "background","#000");
            //     self.editorA.formatText( self.selectionRange.index, self.selectionRange.length, "color","#fff");

            // }
        //});

        //this.toolbarOptions =  

    },  
    data: function () {
        return {
            isLinkOverlayVisible: false,
            editIngameAuthorToggle: false,
            contentSyncFreeze: true,
            delete_question: false,
            filterApplied: null,            
            showAutocomplete: false,
            elementId: 0,
            selectionRange: {
                index: null,
                length: null,
                text: null,
            },
            customButton: null,
            linkSearchQuery: "",
            elementSearchResult: {},
            editorOption: {
                theme: 'snow',
                modules: {
                    toolbar: {
                        container: [
                            ['bslink','link'],
                            [{ align: '' }, { align: 'center' }, { align: 'right' }],
                            [{ 'list': 'ordered'}, { 'list': 'bullet' }],['bold', 'italic', 'underline']
                        ],
                    },

                }   
            }

        }
    },
    watch: {
        linkSearchQuery: function (newVal){
                var self = this;
                this.pageNumberCurrent = 1;
                window.clearTimeout(this.timer);
                this.timer = window.setTimeout(function(){
                    if( newVal.length > 1) {
                        axios.get("/api/new/name/?format=json&search=" + newVal + self.scopeFilter)
                            .then(function (response) {
                                Vue.set(self,"elementSearchResult",response.data);
                            })
                            .catch(function (error) {
                                console.log(error);
                            });
                    }
                    else {
                        Vue.set(self,"elementSearchResult",{});
                    }                
                
                },300)

        }
    }, 
    computed: {
        selectionNearContext: function (){
            nearCxtIndex = this.selectionRange.index - 20;
            nearCxtlength = this.selectionRange.length + 40;
                       
            var text = this.editorA.getText(nearCxtIndex, nearCxtlength);
            return text.replace(this.selectionRange.text,"<span style='background:red;color:white;'>" + this.selectionRange.text + "</span>");
            
        },
        isUserGM: function (){
            
            return this.descriptionData.campaigns.length > 0 ?
                this.$store.getters.isUserGamemasterOfCampaign(this.descriptionData.campaigns_full[0].id): false;
        },
        author: function (){
            return this.descriptionData.author_full;
        },
        ingameAuthor: function (){
            return this.descriptionData.ingame_author ? this.descriptionData.ingame_author_full.present_name.name :  "Not set";
            //if(this.descriptionData.description_type === "in_game") {
               // return this.descriptionData.ingame_author;
            //}
            //return null; 
        },
        isSecret: {
            get: function (){
                return this.$store.state.editors.descriptionEditor.data.secret;
            },
            set: function (value) {
                self.$store.dispatch("updateDescription",{ id: this.descriptionData.id, secret: value })
            },
        },
        editorStatusKey: {
            get: function (){
                return this.$store.state.editors.descriptionEditor.contentStatus;
            },
            set: function (value){
                this.$store.state.editors.descriptionEditor.contentStatus = value;
            },
        },
        relationStatusKey: {
            get: function (){
                return this.$store.state.editors.descriptionEditor.relationStatus;
            },
            set: function (value){
                this.$store.state.editors.descriptionEditor.relationStatus = value;
            },
        },        
        relations: function (){ 
            return this.$store.state.editors.descriptionEditor.relations; 
        },
        editorData: {
            get: function (){
                return this.$store.state.editors.descriptionEditor.data.content;
            },  
            set: function (value) {
                this.$store.commit("updateDescriptionContent",value);
            },
        },
        secretBinary: {
            get: function (){
                return this.$store.state.editors.descriptionEditor.data.secret;
            },
            set: function (value){
                var self = this;
                var load = { id: self.descriptionData.id, secret: value };
                self.$store.dispatch("updateDescription",load)
                .then(function(response){
//                    Vue.set(self.$store.state.editors.descriptionEditor.data,"secret",value);
 //                   self.editorStatusKey = "response";
                                       
                });
            },            
            
        },
        descriptionData: function (){
            return this.$store.state.editors.descriptionEditor.data;
        },
        editorA: function () {
            return this.$refs.quillEditorA.quill
        },
		scopeFilter: function (){
			if(this.scope == "campaign") {
				return "&element__campaigns__id__in=" + this.$store.state.generic.session.base.campaignId;
			}else {
				return "";
			}
		},
        forCampaignsText: function (){
            // if(this.descriptionData.campaigns_full.length > 0) {
            //     var text = "for ";
            //     _.each(this.descriptionData.campaigns_full,function(cmp){
            //             text += cmp.present_name.name;
            //         });
            //     return text;
            // }
            // else {
                return "No campaigns";
            //}
        },
        relationUpdateStatus: function (){
            return this.$store.state.editors.descriptionEditor.relationStatus;       
        },
        contentUpdateStatus: function (){
            return this.$store.state.editors.descriptionEditor.contentStatus;       
        },        
        editorStatus: function () {
            var status = {
                cssClass: "",
                message: "",
            };
            if(this.$store.state.editors.descriptionEditor.contentStatus == "passive") {
                status.cssClass = "description-editor__status--success"
                status.message = "No Changes yet";
            }
            else if(this.$store.state.editors.descriptionEditor.contentStatus == "request") {
                status.cssClass = "description-editor__status--warning"
                status.message = "Writing to Bestiary"
            }  
            else if(this.$store.state.editors.descriptionEditor.contentStatus == "response") {
                status.cssClass = "description-editor__status--success"
                status.message = "All changes written to bestiary"
            }                      
            else if(this.$store.state.editors.descriptionEditor.contentStatus == "error") {
                status.cssClass = "description-editor__status--error"
                status.message = "Broken pen, ";
            }   

            return status
        },
        secretPublic: function (){
            return this.$store.state.editors.descriptionEditor.data.secret === true ? "Secret" : "Public";
        },
        offGameInGame: function () {
            return this.descriptionData.description_type == "off_game" ? "Offgame" : "Ingame";
        },
        relationBackground: function (){
            return this.descriptionData.description_type == "off_game" ? "description-editor__element-relation--rubrik" : "description-editor__element-relation--ink";
        },

       
    },
    methods: {
        syncContent: function () {
           Vue.set(this,"editorStatusKey","request");
           // Vue.set(this,"relationStatusKey","request");


            var self = this;
            var load = { id: this.descriptionData.id, content: this.editorData };
            window.clearTimeout(self.timer);
            self.timer = window.setTimeout(function(){ 
                self.$store.dispatch("editorUpdateDescriptionContent",load);
            },1500);
        },
        createSelectionLink: function(nameObject) {
            this.editorA.formatText(this.selectionRange.index,this.selectionRange.length,'link',"/almanak/element/" + nameObject.element_id)

            Vue.set(this,"selectionRange", {
                index: null,
                length: null,
                text: null,
            });
            this.isLinkOverlayVisible = false;            
        },
        removeSharedWith: function (id){
            var index = this.descriptionData.shared_with.indexOf(id);
            console.log(index);
            index !== -1 ? this.descriptionData.shared_with.splice(index,1) : false;
            console.log(this.descriptionData.shared_with);
            this.$store.dispatch("updateDescription",{ id: this.descriptionData.id, shared_with: this.descriptionData.shared_with });
        },
        addSharedWith: function (n) {
            //console.log("setIngameAuthor",n.element);
            console.log(this.descriptionData.shared_with);
            this.descriptionData.shared_with.push(n.element);
            this.$store.dispatch("updateDescription",{ id: this.descriptionData.id, shared_with: this.descriptionData.shared_with });
        },        
        setIngameAuthor: function (n) {
            this.$store.dispatch("updateDescription",{ id: this.descriptionData.id, ingame_author: n.element });
            
        },
        typeGameButtonIsActive: function (identifier){
             return identifier === this.descriptionData.description_type ? "button--active" : "";
        },
        changeAuthor: function(userId) {
            this.$store.dispatch("updateDescription",{ id: this.descriptionData.id, author: userId });
        },
        insertBestiariesLink: function (id) {
            this.editorA.formatText(this.selectionRange.index,this.selectionRange.length,'link',"/almanak/element/" + id)

            Vue.set(this,"selectionRange", {
                index: null,
                length: null,
                text: null,
            });            
        },
        deleteDescription: function (){
            alert("DELELETE")
        },     
        onEditorBlur(quill) {
            //console.log('editor blur!', quill)
            //console.log('selection', this.selectionRange);
           // this.contentSyncFreeze = true;
            
        },
        onEditorFocus(quill) {
            //console.log('editor focus!', quill)
            //this.contentSyncFreeze = false;
            // if(!_.isEmpty(this.selectionRange)){
            //     this.editorA.removeFormat(this.selectionRange.index,this.selectionRange.length);
            // }

        },
        onEditorReady(quill) {
            //console.log('editor ready!', quill)
            //this.quillEditor = quill;
        },
        updateContent: function(event) {
            //console.log(event.target.innerHTML);
            //this.$store.commit("UpdateDescriptionContent",event.target.innerHTML);
        },
        getDateOnly: function (date){
            return date.split("T")[0];
        },


    }

});
