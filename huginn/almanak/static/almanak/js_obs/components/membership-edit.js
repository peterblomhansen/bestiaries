var membershipEdit = Vue.component('membership-edit', {
	template: "\
	<div @change=\"updateMembership(b)\">\
		<div class=\"row\" >\
			<div class=\"column\">\
			<span class=\"rubrik cursor--pointer\" @click=\"b.delete_question = !b.delete_question \">&times;</span>\
				<span class=\"ink\">{{ b.below.present_name }}</span>\
				<span class=\"rubrik\" v-if=\"invalidTitle.length == 0\">! Invalid Title</span>\
			</div>\
			<div class=\"column\">\
				<select v-if=\"b.below.type_grund == 'individ' || b.below.type_grund == 'player_character'\" v-model=\"currentTitle\">\
					<option :value=\"null\" >No title</option>\
					<option v-for=\"t in availableTitles\" :value=\"t.id\">{{ t.name }}</option>\
				</select>\
			</div>\
			<div class=\"column\">\
				<input class=\"input--narrow\" v-model.number=\"b.start_date\" type=\"number\" :disabled=\"false\" />\
				<select v-model=\"b.start_month\" :disabled=\"isBirthNotDefined == true ? true : false\" >\
				  <option value=\"0\">-</option>\
				  <option v-for=\"m in months\" :value=\"m.value\">{{ m.text }}</option>\
				</select>\
				<input class=\"input--narrow\" v-model.number=\"b.start_year\" type=\"number\" :disabled=\"false\" />\
				<select v-model=\"b.start_age\">\
					<option :value=\"null\">-</option>\
					<option v-for=\"a in ages\" :value=\"a.id\" >{{ a.initials }}</option>\
				</select>\
			</div>\
			<div class=\"column\">\
				<input class=\"input--narrow\" v-model.number=\"b.end_date\" type=\"number\" :disabled=\"false\" />\
				<select v-model=\"b.end_month\" :disabled=\"isBirthNotDefined == true ? true : false\" >\
				  <option value=\"0\">-</option>\
				  <option v-for=\"m in months\" :value=\"m.value\">{{ m.text }}</option>\
				</select>\
				<input class=\"input--narrow\" v-model.number=\"b.end_year\" type=\"number\" :disabled=\"false\" />\
				<select v-model=\"b.end_age\">\
					<option :value=\"null\">-</option>\
					<option v-for=\"a in ages\" :value=\"a.id\" >{{ a.initials }}</option>\
				</select>\
			</div>\
		</div>\
		<div v-if=\"b.delete_question == true\">\
			<p>Do you want to delete {{ b.below.present_name }} as a member of {{ element.present_name }}?\
			<button @click=\"deleteMembership(b)\" >Yes</button>\
			<button @click=\"b.delete_question = !b.delete_question \" >No</button></p>\
		</div>\
	</div>\
	",
    props: {
        membership: Object
    },
    data: function() {
        return {
            hello: "Hello World",
            please: "sfdsgdsgsgsd"
        }
    },
    methods: {
		deleteMembership: function(m){
			var self = this;
			axios.patch("/api/elementrelation/" + m.id + "/update/",{ deleted: true }).then(function(response){
				console.log(response);
				var i = self.$store.state.element.relations.below.findIndex(function(b) { return b.id == m.id });
				self.$store.state.element.relations.below.splice(i,1);

			}).catch(function(error) {
				console.log(error);
			});
		},
		updateMembership: function(m){
			console.log("Update Membership",m);
			var title = null;
			if(m.above_title) {
				title = m.above_title.id;
			}

			var load = {
				above_title: m.above_title.id,
				start_age: m.start_age,
				start_year: m.start_year,
				start_month: m.start_month,
				start_date: m.start_date,
				end_age: m.end_age,
				end_year: m.end_year,
				end_month: m.end_month,
				end_date: m.end_date,
			}


			axios.patch("/api/elementrelation/" + m.id + "/update/",load).then(function(response){
				console.log(response);
			}).catch(function(error) {
				console.log(error);
			});
		},
		ingame_date_str: function(time){
			return this.$store.getters.ingame_date_str(time);
		},
		compareTime: function (time) {

			return this.$store.getters.ingame_date_interval(time,this.ingameNow);

		},
		compareTimeString: function (time){
			return this.$store.getters.ingame_date_interval_string(this.compareTime(time).data);
		},
		timeStart: function (el) {

			var start = {
				age: el.start_age,
				year: el.start_year,
				month: el.start_month,
				day: el.start_date,
				hour: el.start_hours,
				min: el.start_minutes,
				sec: el.start_seconds,
			};

			return start;

		},
		timeEnd: function (el) {

			var end = {
				age: el.end_age,
				year: el.end_year,
				month: el.end_month,
				day: el.end_date,
				hour: el.end_hours,
				min: el.end_minutes,
				sec: el.end_seconds,
			};

			return end;

		}


	},
	computed: {
		element: function (){
			return this.$store.state.element.masterdata;
		},
		currentTitle: {
			get() {
				var current = null;
				if (this.b.above_title != null) {
					current = this.b.above_title.id;
				}
				return current;
			},
			set(value){
				if (value != null) {
					console.log(value);
					var i = this.availableTitles.findIndex(function(t){ return t.id == value });
					this.b.above_title = this.availableTitles[i];
					//current = this.b.above_title.id;
				}
				else {
					this.b.above_title = null;
				}
				//	return current;
			}

		},
		availableTitles: function (){
			return this.$store.state.element.masterdata.group_type.titles;
		},
		invalidTitle: function (){
			var self = this;


			return this.availableTitles.filter(function(t){

				return self.membership.above_title == null || self.membership.above_title.id == t.id;
			});

		},
		ages: function () {
			return this.$store.state.session.universe.ages;
		},
		b: function () {
			return this.membership;
		},
		isBirthNotDefined: function () {
			return false;
		},
		months: function (){
			return [
				{text: "jan", value: 1 },
				{text: "feb", value: 2 },
				{text: "mar", value: 3 },
				{text: "apr", value: 4 },
				{text: "may", value: 5 },
				{text: "jun", value: 6 },
				{text: "jul", value: 7 },
				{text: "aug", value: 8 },
				{text: "sep", value: 9 },
				{text: "okt", value: 10 },
				{text: "nov", value: 11 },
				{text: "dec", value: 12 },
			];
		}
	}
});
