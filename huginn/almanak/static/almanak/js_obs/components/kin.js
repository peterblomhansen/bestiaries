	var kin = Vue.component('kin', {
    template: "\
    <div>\
      <p class=\"rubrik\">\
        Kin:\
        <a v-if=\"element.kin != null\" class=\"ink\" :href=\"'/almanak/side/' + element.kin.id\">\
          {{ element.kin.present_name }}\
        </a>\
        <img v-if=\"element.user_permissions.write == true\" style=\"height:1em;cursor:pointer;\" :src=\"st + 'images/icons/pencil.svg'\" @click=\"loadKins();editKin = !editKin\" />\
      </p>\
      <div v-if=\"editKin == true\">\
        <p class=\"rubrik\"><strong>Change Kin:</strong> <span class=\"ajax__update-text\">{{ changeKinAjaxMessage }}</span></p>\
        <select @change=\"changeKin()\" v-model=\"selectedKin\">\
          <option v-for=\"k in kinsInUniverse\" :value=\"k.id\">{{ k.present_name }}</option>\
        </select>\
      </div>\
    </div>\
    ",
    props: {
    //    element: Object,
    //    kinsInUniverse: Array,
    //    st: String,
    },
    data: function() {
        return {
            editKin: false,
            changeKinAjaxMessage: "",
//            selectedKin: this.$store.state.element.masterdata.kin.id,
        }
    },
    computed: {
        element: function(){
            return this.$store.state.element.masterdata;
        },
        kinsInUniverse: function(){
            return this.$store.state.session.universe.kins;
        },
        st: function (){
            return this.$store.state.initial.staticUrl;
        },
        // selectedKin: function(){
        //     return this.element.kin.id;
        // },
    },
    methods: {
		selectedKin: function (){
			if(this.$store.state.element.masterdata.kin) {
				return this.$store.state.element.masterdata.kin.id
			}
			else {
				return null;
			}
		},
		loadKins: function(){
			var self = this;
			console.log("load kins");
			if(campaignId){
				axios.get("/api/session/" + campaignId + "/?d=us1_ages__cs1_universe__us1_campaigns__us1_name__us1_users__us1_kins&format=json").then(function(response) {
					self.$store.state.session = response.data;
					self.$store.state.element.loaded.sessiondata = true;
					self.$store.state.initial.sessionLoaded = true
				}).catch(function(error) {
					console.log(error);
				});
			}
		},
        changeKin: function() {
            var self = this;
            self.changeKinAjaxMessage = "updating";
            axios.patch('/api/individual/' + this.element.id + '/update/', {
                kin: self.selectedKin
            }).then(function(response) {

                self.changeKinAjaxMessage = "updated";
                self.$store.commit("getElement");

            }).catch(function(error) {
                console.log(error);
                if (error.response.status == 403) {
                    self.names[i].ajax_status = "permission denied: no changes was written to the Bestiary";
                }
            });
        }
    }
});
