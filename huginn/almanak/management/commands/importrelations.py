from django.core.management.base import BaseCommand, CommandError
from django.db import connections
from collections import namedtuple
from django.contrib.auth.models import AbstractUser
from almanak.models import BestiariesUser


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'




    def handle(self, *args, **options):

        def namedtuplefetchall(cursor):
            #"Return all rows from a cursor as a namedtuple"
            desc = cursor.description
            nt_result = namedtuple('Result', [col[0] for col in desc])
            return [nt_result(*row) for row in cursor.fetchall()]

        def importElementRelations():
            dbleg = connections['legacy'].cursor()
            dbnew = connections['default'].cursor()
            dbleg.execute("SELECT * FROM relation")

            el_rel = namedtuplefetchall(dbleg)
            for n in el_rel:
                        if n.hemmelig == 1:
                            sec = True
                        else:
                            sec = False
                        if n.slettet == 1:
                            rem = True
                        else:
                            rem = False

                        if n.id_start_alder == 0:
                            age_start = None
                        else:
                            age_start = n.id_start_alder

                        if n.id_slut_alder == 0:
                            age_end = None
                        else:
                            age_end = n.id_slut_alder

                        if n.id_over_titel == 0:
                            over_titel = None
                        else:
                            over_titel = n.id_over_titel

                        if n.id_under_titel == 0:
                            under_titel = None
                        else:
                            under_titel = n.id_under_titel

                        over = False
                        under = False
                        dbleg2 = connections['legacy'].cursor()
                        dbleg2.execute("SELECT id FROM element")
                        els = namedtuplefetchall(dbleg2)
                        for e in els:
                            if e.id == n.over_id:
                                over = True
                            if e.id == n.under_id:
                                under = True

                        if under == True and over == True:
                            dbnew.execute("""INSERT INTO almanak_elementrelation (id, secret,deleted, relation_type, below_id, above_id, start_age_id, start_year, start_month, start_date, start_hours, start_minutes,start_seconds, end_age_id, end_year, end_month, end_date,end_hours,end_minutes,end_seconds,above_title_id,below_title_id) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",
                                (n.id, sec,rem, n.type, n.under_id, n.over_id,age_start, n.start_aar, n.start_maaned, n.start_dag,0,0,0,age_start, n.slut_aar,n.slut_maaned,n.slut_dag,0,0,0,under_titel,over_titel))

            print(str(len(el_rel)) + " Element Relations Imported")



        def importEntityEvent():
            dbleg = connections['legacy'].cursor()
            dbnew = connections['default'].cursor()
            dbleg.execute("SELECT * FROM begivenhed_entitet")

            entity_event = namedtuplefetchall(dbleg)
            for n in entity_event:
                        if n.hemmelig == 1:
                            sec = True
                        else:
                            sec = False

                        if n.id_start_alder == 0:
                            age_start = None
                        else:
                            age_start = n.id_start_alder

                        if n.id_slut_alder == 0:
                            age_end = None
                        else:
                            age_end = n.id_slut_alder
                        event = False
                        entity = False
                        dbleg2 = connections['legacy'].cursor()
                        dbleg2.execute("SELECT id FROM element")
                        els = namedtuplefetchall(dbleg2)
                        for e in els:
                            if e.id == n.id_begivenhed:
                                event = True
                            if e.id == n.id_entitet:
                                entity = True

                        if event == True and entity == True:
                            dbnew.execute("""INSERT INTO almanak_entityeventrelation (id, secret, deleted, event_id, entity_id, start_age_id, start_year, start_month, start_date, start_hours, start_minutes,start_seconds, end_age_id, end_year, end_month, end_date,end_hours,end_minutes,end_seconds) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",
                                (n.id, sec,False, n.id_begivenhed, n.id_entitet,age_start, n.start_aar, n.start_maaned, n.start_dag,0,0,0,age_start, n.slut_aar,n.slut_maaned,n.slut_dag,0,0,0))

            print(str(len(entity_event)) + " participants/experiences (entity event) Imported")


        def importEventPlace():
            dbleg = connections['legacy'].cursor()
            dbnew = connections['default'].cursor()
            dbleg.execute("SELECT * FROM begivenhed_sted")

            place_event = namedtuplefetchall(dbleg)
            for n in place_event:
                        if n.hemmelig == 1:
                            sec = True
                        else:
                            sec = False

                        if n.id_start_alder == 0:
                            age_start = None
                        else:
                            age_start = n.id_start_alder

                        if n.id_slut_alder == 0:
                            age_end = None
                        else:
                            age_end = n.id_slut_alder
                        event = False
                        place = False
                        dbleg2 = connections['legacy'].cursor()
                        dbleg2.execute("SELECT id FROM element")
                        els = namedtuplefetchall(dbleg2)
                        for e in els:
                            if e.id == n.id_begivenhed:
                                event = True
                            if e.id == n.id_sted:
                                place = True

                        if event == True and place == True:
                            dbnew.execute("""INSERT INTO almanak_eventplacerelation (id, secret, deleted, event_id, place_id, start_age_id, start_year, start_month, start_date, start_hours, start_minutes,start_seconds, end_age_id, end_year, end_month, end_date,end_hours,end_minutes,end_seconds) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",
                                (n.id, sec,False, n.id_begivenhed, n.id_sted,age_start, n.start_aar, n.start_maaned, n.start_dag,0,0,0,age_start, n.slut_aar,n.slut_maaned,n.slut_dag,0,0,0))

            print(str(len(place_event)) + " occurred history (place event) Imported")

        def importElementItem():
            dbleg = connections['legacy'].cursor()
            dbnew = connections['default'].cursor()
            dbleg.execute("SELECT * FROM artefakt_element")

            element_item = namedtuplefetchall(dbleg)
            for n in element_item:
                        # if n.hemmelig == 1:
                        #     sec = True
                        # else:
                        #     sec = False

                        # if n.id_start_alder == 0:
                        #     age_start = None
                        # else:
                        #     age_start = n.id_start_alder
                        #
                        # if n.id_slut_alder == 0:
                        #     age_end = None
                        # else:
                        #     age_end = n.id_slut_alder
                        owner = False
                        item = False
                        dbleg2 = connections['legacy'].cursor()
                        dbleg2.execute("SELECT id FROM element")
                        els = namedtuplefetchall(dbleg2)
                        for e in els:
                            if e.id == n.id_ejer:
                                owner = True
                            if e.id == n.id_artefakt:
                                item = True

                        if owner == True and item == True:
                            dbnew.execute("""INSERT INTO almanak_elementitemrelation (id, secret, deleted, owner_id, owned_id, ingame_age_id, ingame_year, ingame_month, ingame_date, ingame_hours, ingame_minutes,ingame_seconds,number_of_items) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",
                                (n.id, False,False, n.id_ejer, n.id_artefakt,None, 0, 0,0,0,0,0,n.antal))

            print(str(len(element_item)) + " ownership inventory (element item) Imported")

        def importElementUniqueItem():
            dbleg = connections['legacy'].cursor()
            dbnew = connections['default'].cursor()
            dbleg.execute("SELECT * FROM unika_element")

            element_uniqueitem = namedtuplefetchall(dbleg)
            for n in element_uniqueitem:
                        if n.hemmelig == 1:
                            sec = True
                        else:
                            sec = False

                        if n.id_start_alder == 0:
                            age_start = None
                        else:
                            age_start = n.id_start_alder

                        if n.id_slut_alder == 0:
                            age_end = None
                        else:
                            age_end = n.id_slut_alder
                        owner = False
                        item = False
                        dbleg2 = connections['legacy'].cursor()
                        dbleg2.execute("SELECT id FROM element")
                        els = namedtuplefetchall(dbleg2)
                        for e in els:
                            if e.id == n.id_ejer:
                                owner = True
                            if e.id == n.id_unika:
                                item = True

                        if owner == True and item == True:
                            dbnew.execute("""INSERT INTO almanak_elementuniqueitemrelation (id, secret, deleted, owner_id, owned_id, start_age_id, start_year, start_month, start_date, start_hours, start_minutes,start_seconds, end_age_id, end_year, end_month, end_date,end_hours,end_minutes,end_seconds) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",
                                (n.id, sec,False, n.id_ejer, n.id_unika,age_start, n.start_aar, n.start_maaned, n.start_dag,0,0,0,age_start, n.slut_aar,n.slut_maaned,n.slut_dag,0,0,0))

            print(str(len(element_uniqueitem)) + " ownership inventory_unique (element unique item) Imported")

        def importDescriptionElement():
            dbleg = connections['legacy'].cursor()
            dbnew = connections['default'].cursor()
            dbleg.execute("SELECT * FROM element_beskrivelse")

            ele_bes = namedtuplefetchall(dbleg)
            for e in ele_bes:
                    elem = False
                    bes = False
                    dbleg3 = connections['legacy'].cursor()
                    dbleg3.execute("SELECT id FROM beskrivelse")
                    beskrivelser = namedtuplefetchall(dbleg3)
                    for i in beskrivelser:
                        if i.id == e.id_beskrivelse:
                            bes = True
                    dbleg2 = connections['legacy'].cursor()
                    dbleg2.execute("SELECT id FROM element")
                    els = namedtuplefetchall(dbleg2)
                    for s in els:
                        if s.id == e.id_element:
                            elem = True
                    if elem == True and bes == True:
                        dbnew.execute("""INSERT INTO almanak_descriptionelementrelation (description_id,element_id,relevance) VALUES (%s,%s,%s)""",(e.id_beskrivelse, e.id_element,e.relevans))

            print(str(len(ele_bes)) + " Description Element Relations Imported")


        def importElementImage():
            dbleg = connections['legacy'].cursor()
            dbnew = connections['default'].cursor()
            dbleg.execute("SELECT * FROM billede_relation")

            ele_bes = namedtuplefetchall(dbleg)
            for e in ele_bes:
                    elem = False
                    img = False
                    dbleg3 = connections['legacy'].cursor()
                    dbleg3.execute("SELECT id FROM billede")
                    imgs = namedtuplefetchall(dbleg3)
                    for i in imgs:
                        if i.id == e.id_billede:
                            img = True
                    dbleg2 = connections['legacy'].cursor()
                    dbleg2.execute("SELECT id FROM element")
                    els = namedtuplefetchall(dbleg2)
                    for s in els:
                        if s.id == e.id_element:
                            elem = True
                    if elem == True and img == True:
                        dbnew.execute("""INSERT INTO almanak_imageelementrelation (image_id,element_id,relevance,coordinates) VALUES (%s,%s,%s,%s)""",(e.id_billede, e.id_element,e.relevans,e.coordinates))

            print(str(len(ele_bes)) + " Image Element Relations Imported")


        def importUniverseRegistration():
            dbleg = connections['legacy'].cursor()
            dbnew = connections['default'].cursor()
            dbleg.execute("SELECT * FROM deltager_univers")

            uni_reg = namedtuplefetchall(dbleg)
            for u in uni_reg:
                if u.id_deltager is not 0:
                    uni_owner = False
                    if u.universejer == 1:
                        uni_owner = True
                    view = u.type_aktuel
                    if u.type_aktuel == 0:
                        view = None

                    dbnew.execute("""INSERT INTO almanak_universemembership (universe_owner,participant_id,universe_id,default_view_id,default_campaign_id) VALUES (%s,%s,%s,%s,%s)""",(uni_owner,u.id_deltager,u.id_univers,view,u.kampagne_aktuel))

            print(str(len(uni_reg)) + " UniverseMemberships Imported")


        def importElementCampaignRelations():
            dbleg = connections['legacy'].cursor()
            dbnew = connections['default'].cursor()
            dbleg.execute("SELECT * FROM element_kampagne")

            el_kam = namedtuplefetchall(dbleg)
            for k in el_kam:

                if k.id_kampagne is not 0:

                    dbnew.execute("""INSERT INTO almanak_campaignelement (element_id,campaign_id,approved,canon) VALUES (%s,%s,%s,%s)""",(k.id_element,k.id_kampagne,0,0))

            print(str(len(el_kam)) + " Element Campaign Relations")



        def importGamemasterRpgElementRelations():
            dbleg = connections['legacy'].cursor()
            dbnew = connections['default'].cursor()
            dbleg.execute("SELECT * FROM rpg_element_gm")

            el_kam = namedtuplefetchall(dbleg)
            for k in el_kam:

                if k.id_gm is not 0:
                    dbnew.execute("""INSERT INTO almanak_rpgelement_gamemasters (bestiariesuser_id,rpgelement_id) VALUES (%s,%s)""",(k.id_gm,k.id_rpg_element))

            print(str(len(el_kam)) + " Gamemaster RpgElement Relations")


        def importEditors():
            dbleg = connections['legacy'].cursor()
            dbnew = connections['default'].cursor()
            dbleg.execute("SELECT * FROM admin_element")

            el_kam = namedtuplefetchall(dbleg)
            for k in el_kam:
                elem = False
                user = False
                dbleg2 = connections['legacy'].cursor()
                dbleg2.execute("SELECT id FROM element")
                els = namedtuplefetchall(dbleg2)
                for s in els:
                    if k.id_element == s.id:
                        elem = True
                dbleg3 = connections['legacy'].cursor()
                dbleg3.execute("SELECT id FROM deltager")
                users = namedtuplefetchall(dbleg3)
                for u in users:
                    if u.id == k.id_deltager:
                        user = True
                # uni_owner = False
                # if u.universejer == 1:
                #     uni_owner = True
                # view = u.type_aktuel
                # if u.type_aktuel == 0:
                #     view = None
                if elem == True and user == True:
                    dbnew.execute("""INSERT INTO almanak_element_editors (bestiariesuser_id,element_id) VALUES (%s,%s)""",(k.id_deltager,k.id_element))

            print(str(len(el_kam)) + " Editors Relations")



        def importDescriptionsSharings():
            dbleg = connections['legacy'].cursor()
            dbnew = connections['default'].cursor()
            dbleg.execute("SELECT * FROM deling_beskrivelse")

            des_share = namedtuplefetchall(dbleg)
            for k in des_share:
                des = False
                ent = False
                dbleg2 = connections['legacy'].cursor()
                dbleg2.execute("SELECT id FROM beskrivelse")
                descriptions = namedtuplefetchall(dbleg2)
                for s in descriptions:
                    if k.id_beskrivelse == s.id:
                        des = True
                dbleg3 = connections['legacy'].cursor()
                dbleg3.execute("SELECT id FROM entitet")
                entities = namedtuplefetchall(dbleg3)
                for u in entities:
                    if u.id == k.id_entitet:
                        ent = True

                if des == True and ent == True:
                    dbnew.execute("""INSERT INTO almanak_description_shared_with (description_id,entity_id) VALUES (%s,%s)""",(k.id_beskrivelse,k.id_entitet))

            print(str(len(des_share)) + " Description Sharings")


        def importDescriptionsCampaignReg():
            dbleg = connections['legacy'].cursor()
            dbnew = connections['default'].cursor()
            dbleg.execute("SELECT * FROM beskrivelse_kampagne")

            des_kam = namedtuplefetchall(dbleg)
            for k in des_kam:
                des = False
                kmp = False
                dbleg2 = connections['legacy'].cursor()
                dbleg2.execute("SELECT id FROM beskrivelse")
                descriptions = namedtuplefetchall(dbleg2)
                for s in descriptions:
                    if k.id_beskrivelse == s.id:
                        des = True
                dbleg3 = connections['legacy'].cursor()
                dbleg3.execute("SELECT id FROM kampagne")
                kampagner = namedtuplefetchall(dbleg3)
                for u in kampagner:
                    if u.id == k.id_kampagne:
                        kmp = True
                if des == True and kmp == True:
                    dbnew.execute("""INSERT INTO almanak_campaigndescription (description_id,campaign_id,approved,canon) VALUES (%s,%s,%s,%s)""",(k.id_beskrivelse,k.id_kampagne,0,0))

            print(str(len(des_kam)) + " Description Campaign Occurences")





        importElementRelations()
        importEntityEvent()
        importEventPlace()
        importElementItem()
        importElementUniqueItem()
        importDescriptionElement()
        importElementImage()
        importUniverseRegistration()
        importElementCampaignRelations()
        importGamemasterRpgElementRelations()
        importEditors()
        importDescriptionsSharings()
        importDescriptionsCampaignReg()



        self.stdout.write(self.style.SUCCESS("Relations imported"));
