from django.conf.urls import url
from django.contrib import admin

# Test viewset Routers
from django.conf.urls import url, include
from django.urls import include, path, re_path
from rest_framework.routers import DefaultRouter
from almanak.api import views

router = DefaultRouter()

# Model Class Viewset mapping
router.register(r'campaign-element-relation', views.CampaignElementViewSet)

router.register(r'description', views.DescriptionViewSet)
router.register(r'description-element-relation', views.DescriptionElementRelationViewSet)

router.register(r'element', views.ElementViewSet)
router.register(r'element-relation', views.ElementRelationViewSet)
router.register(r'element-item-relation', views.ElementItemRelationViewSet)
router.register(r'element-unique-item-relation', views.ElementUniqueItemRelationViewSet)
router.register(r'entity-event-relation', views.EntityEventRelationViewSet)
router.register(r'event-place-relation', views.EventPlaceRelationViewSet)


router.register(r'name', views.NameViewSet)

router.register(r'user', views.UserViewSet)

router.register(r'individual', views.IndividualViewSet)
router.register(r'player_character', views.PlayerCharacterViewSet)
router.register(r'group', views.GroupViewSet)
router.register(r'event', views.EventViewSet)
router.register(r'rpg_session', views.RpgSessionViewSet)
router.register(r'campaign', views.CampaignViewSet)
router.register(r'place', views.PlaceViewSet)
router.register(r'item', views.ItemViewSet)
router.register(r'race', views.RaceViewSet)


# Non ViewSet urls
urlpatterns = [
    re_path(r'^sync-description-links/(?P<pk>[0-9]+)/$', views.SyncDescriptionLinksView.as_view(), name="sync_description_links_view"),
    url(r'^go_ingame/$', views.GoIngameView.as_view(), name="go_ingame_view"),
    url(r'^session/$', views.SessionView.as_view(), name="session_view"),
    url(r'^new/', include(router.urls)),
]
