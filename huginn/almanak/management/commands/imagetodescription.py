from django.core.management.base import BaseCommand, CommandError
from django.db import connections
from collections import namedtuple
from django.contrib.auth.models import AbstractUser
from almanak.models import BestiariesUser, Image, Description, CampaignDescription, DescriptionElementRelation, ImageElementRelation, CampaignImage


class Command(BaseCommand):
	help = 'Migrates data from the Image model to Description'




	def handle(self, *args, **options):

		def namedtuplefetchall(cursor):
		    #"Return all rows from a cursor as a namedtuple"
		    desc = cursor.description
		    nt_result = namedtuple('Result', [col[0] for col in desc])
		    return [nt_result(*row) for row in cursor.fetchall()]


		def images_to_descriptions():

			images = Image.objects.all()

			for i in images:
				des = Description()
				des.__dict__.update(i.__dict__)
				des.id = None
				des.save()
				old_id = i.id
				new_id = des.id
				rel = ImageElementRelation.objects.filter(image__id=old_id)
				for r in rel:
					#rel_ele = r.element
					new_rel = DescriptionElementRelation()
					new_rel.description = des
					new_rel.element = r.element
					new_rel.relevance = r.relevance
					new_rel.save()

				cam = CampaignImage.objects.filter(image__id=old_id)
				for c in cam:
					new_campdes = CampaignDescription()
					new_campdes.description = des
					new_campdes.campaign = c.campaign



		images_to_descriptions()
