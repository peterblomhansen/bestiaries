var editName = Vue.component('edit-name', {
    template: "\
    <div class=\"edit-name\">\
        <div class=\"grid--two-column\">\
            <div v-if=\"nameObject.delete_question == true\" class=\"popover--delete-name\">\
                <p>Are you sure you wants to remove this name? <button @click=\"updateName({ deleted: true })\">Yes</button>  <button @click=\"nameObject.delete_question = !nameObject.delete_question\" >No</button></p>\
            </div>\
            <div>\
                <fieldset>\
                    <legend>Name</legend>\
                    <input type=\"text\" v-model=\"nameObject.name\">\
                </fieldset>\
                <edit-ingame-time title=\"Start\" field=\"start\" :obj=\"nameObject\" ></edit-ingame-time>\
                <edit-ingame-time title=\"End\" field=\"end\" :obj=\"nameObject\" ></edit-ingame-time>\
            </div>\
            <div>\
                <fieldset>\
                    <legend>Permissions</legend>\
                    <p class=\"rubrik\" v-if=\"nameObject.edit_permissions == true && $store.state.generic.session.full.role_current == 0\">\
                    <strong class=\"ink\" >Author: </strong>\
                        <select v-model=\"nameObject.author\">\
                            <option :value=\"null\" >No Author</option>\
                            <option v-for=\"u in $store.state.generic.session.full.universe.users\" :value=\"u.id\" >{{ u.first_name }} {{ u.last_name }}</option>\
                        </select>\
                    </p>\
                    <p class=\"rubrik\" ><strong class=\"ink\" >Author: </strong>{{ authorName }}</p>\
                    <div style=\"display:flex;\">\
                        <div style=\"flex:1;\">\
                        <p class=\"ink\"><strong><label>Important Name</label>:</strong> <input type=\"checkbox\" v-model=\"isPrimary\" /></p>\
                        <p class=\"ink\"><strong><label>Secret Name</label>:</strong> <input type=\"checkbox\" v-model=\"nameObject.secret\" /></p>\
                        </div>\
                        <div style=\"flex:1;\" v-if=\"nameObject.secret == true\">\
                            <p class=\"rubrik\" >Shared with: </p>\
                            <ul>\
                                <li class=\"ink\" v-for=\"s in nameObject.shared_with_full\">\
                                    {{ s.present_name.name }} <span class=\"cursor--pointer rubrik\" @click=\"s.delete_question = !s.delete_question\" >&times;</span>\
                                    <div v-if=\"s.delete_question == true\" class=\"popover--delete-name\" >\
                                        <p class=\"rubrik\">Are you sure you wants hide the secret name <strong class=\"ink\">{{ nameObject.name }}</strong> from <strong class=\"ink\">{{ s.present_name.name }}</strong> <button @click=\"removeShareWith(s)\">Yes</button>  <button @click=\"s.delete_question = !s.delete_question\" >No</button></p>\
                                    </div>\
                                </li>\
                                <li><autocomplete name=\"share_name_with_search\" filter=\"&element__element_type=entitet\" placeholder-text=\"Share with\" button-text=\"Share with\" :button-action=\"{ type: 'nameShareWith', data: nameObject }\" ></autocomplete></li>\
                            </ul>\
                        </div>\
                    </div>\
                </fieldset>\
                <div style=\"text-align:right;\">\
                    <strong v-if=\"ajaxResponseMessage.length > 0\" class=\"alert alert--success\" style=\"padding:0.4em\" >{{ ajaxResponseMessage }}</strong> \
                    <button v-if=\"nameObject.id != null\" @click=\"nameObject.delete_question = !nameObject.delete_question\" >Delete</button> \
                    <button @click=\"updateName(nameObject)\" :disabled=\"!validated\" v-if=\"nameObject.id > 0\" >Update</button>\
                    <button @click=\"createName()\" :disabled=\"!validated\" v-else >Create</button>\
                </div>\
            </div>\
        </div>\
    </div>\
    ",
    props: {
        nameObject: Object,
    },
    data: function(){
        return {
            ajaxResponseMessage: "",
        };
    },
    methods: { 
        removeShareWith: function (entity) {
            var index = this.nameObject.shared_with_full.findIndex(function(s){ return s.id == entity.id });
            this.nameObject.shared_with_full.splice(index,1);
        },      
		updateName: function(data){
            var self = this;
            if(data.shared_with_full) {
                var sharedWithIds = data.shared_with_full.map(function(el){ return parseInt(el.id)});
                data.shared_with = sharedWithIds;
            }
			axios.patch("/api/new/name/" + self.nameObject.id + "/",data)	
				.then(function (response) {
                    self.$store.commit("getNames");
                    self.$store.commit("getMasterdata");
                    self.ajaxResponseMessage = "Name Updated";

				})
				.catch(function (error) {
				  console.log(error);
				});				

		},
		createName: function(){
			var self = this;

            var sharedWithIds = self.nameObject.shared_with_full.map(function(el){ return parseInt(el.id)});
            self.nameObject.shared_with = sharedWithIds;            

			axios.post("/api/new/name/",self.nameObject)	
				.then(function (response) {
                    self.$store.commit("getNames");
                    self.$store.commit("getEmptyNameObject");

                })
				.catch(function (error) {
				  console.log(error);
				});				
		},	        
    },
    computed: {
        isPrimary: {
			get: function(){
				return true ? this.nameObject.name_type == "prim" : false; 		
			},
			set: function (val) {
                console.log(val);
                if(val == true){
                    this.nameObject.name_type = "prim";
                }
                else {
                    this.nameObject.name_type = "sec";
                }
			},            
        },
        updateOrCreate: function() {
            if(this.nameObject.id == null) {
                return "Create";
            }
            else { return "Update"; }
        },
        authorName: function (){
            if(this.nameObject.author === null) {
                return "No Author";
            }else {
                var self = this;
                var user = _.find(this.$store.state.generic.session.full.universe.users, function(u){ return u.id == self.nameObject.author });
                return user.first_name + " " + user.last_name;
            }
        },
        validated: function(){
            var validated = false
            if (this.nameObject.name.length > 0) {
                validated = true; 
            } 
            return validated;
        }

    }


});