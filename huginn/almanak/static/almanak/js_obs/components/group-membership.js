var groupMembership=Vue.component('group-membership',{
    template: "<div>\
        <h3 class=\"rubrik\">\
            Memberships and Titles\
            <span v-if=\"masterdata.user_permissions.write == true\" class=\"rubrik\" style=\"cursor:pointer\" @click=\"groupMembershipChooser = !groupMembershipChooser\">+</span>\
        </h3>\
        <div v-if=\"groupMembershipChooser == true\" >\
            <h4 class=\"rubrik\" >Add <span class=\"ink\">{{ masterdata.present_name }}</span> as member of a group:</h4>\
			<div>\
				<autocomplete link-target=\"_blank\" :filter=\"'gruppe'\" :buttonMethod=\"'addAbove'\" :buttonText=\"'Add Membership'\" >\
				</autocomplete>\
			</div>\
        \
        </div>\
        <div v-if=\"a.group_type_titles != 0\" v-for=\"a in above\" class=\"rubrik\" >\
            <p>\
                <a class=\"ink\" href=\"\" v-if=\"a.above_title.id != 0\">{{ a.above_title.name }}</a><a v-else>No title</a> in <a class=\"ink\" :href=\"'/almanak/side/' + a.above.id\">\
                    {{ a.above.present_name }}\
                </a>\
                <strong v-if=\"masterdata.user_permissions.write == true\" class=\"rubrik\" style=\"cursor:pointer\" @click=\"a.edit_question = true\"> ?</strong>\
                <strong v-if=\"masterdata.user_permissions.write == true\" class=\"rubrik\" style=\"cursor:pointer\" @click=\"a.delete_question = true\"> &times;</strong><br/>\
            \
            </p>\
            <span v-if=\"a.delete_question == true\">Do you want to remove {{ masterdata.present_name }} as {{ a.above_title.name }} of {{ a.above.present_name }} <button @click=\"deleteRelation(a.id)\" >Yes</button> <button @click=\"p.delete_question = false\" >No</button></span>\
            <div v-if=\"a.edit_question == true\">\
                <p class=\"rubrik\">\
                    <select @change=\"changeRelationTitle(a.id,'above',a.above_title.id)\" v-model=\"a.above_title.id\">\
                        <option :value=\"0\">No title</option>\
                        <option v-for=\"gt in a.group_type_titles\" :value=\"gt.id\">{{ gt.name }}</option>\
                    </select>\
                </p>\
            </div>\
        </div>\
        <div v-if=\"a.group_type_titles == 0 && a.relation_type != 'familie' && a.above.type_spec != 'race'\" v-for=\"a in above\" class=\"rubrik\" >\
            <p><a class=\"ink\" :href=\"'/almanak/side/' + a.above.id\" >{{ a.above.present_name }}</a>\
			<strong v-if=\"masterdata.user_permissions.write == true\" class=\"rubrik\" style=\"cursor:pointer\" @click=\"a.delete_question = true\"> &times;</strong><br/>\
			<span v-if=\"a.delete_question == true\">Do you want to remove {{ masterdata.present_name }} as {{ a.above_title.name }} of {{ a.above.present_name }} <button @click=\"deleteRelation(a.id)\" >Yes</button> <button @click=\"p.delete_question = false\" >No</button></span>\
			</p>\
        </div>\
    </div>",
    data:function(){
        return {
            groupMembershipChooser:false,
            chosenTitle: 0,
        }
    },
    methods:{
        deleteRelation: function(id) {
            this.$store.commit('deleteElementRelation',id);
        },
        changeRelationTitle(relation_id,title_location,title_id){
            var payLoad = {};
			var titleId = null
			if(title_id != 0){
				titleId = title_id;
			}
            if(title_location == "above") {
                payLoad = { above_title: titleId };
            }
            if(title_location == "below") {
                payLoad = { below_title: titleId };
            }

            var self = this;
            axios.patch('/api/elementrelation/' + relation_id + '/update/', payLoad ).then(function(response) {
                console.log(response)
                self.$store.commit("getElementRelations");

            }).catch(function(error) {
                console.log(error);
            });
        },

    },
    computed: {
        masterdata: function() {
            return this.$store.state.element.masterdata;
        },
        above: function(){
            if(this.$store.state.element.relations.above) {
                return this.$store.state.element.relations.above.filter( function(o) { return o.above.type_grund == "gruppe" });
            }
        },
    }
});
