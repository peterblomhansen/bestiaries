var editIngameTime = Vue.component('edit-ingame-time', {
    template:"\
    <fieldset>\
        <legend>{{ title}}</legend>\
        <div class=\"edit-ingame-time rubrik\" >\
                <div>Date</div>\
                <div>Month</div>\
                <div>Year</div>\
                <div style=\"align-self:center\">Time</div>\
                <div>Age</div>\
                <div></div>\
                <div>\
                    <input v-model=\"date\" type=\"text\" />\
                </div>\
                <div>\
                    <select v-model=\"month\" >\
                        <option value=\"0\">-</option>\
                        <option v-for=\"m in monthsOfYear\" :value=\"m.value\" >{{ m.text }}</option>\
                    </select>\
                </div>\
                <div>\
                    <input v-model=\"year\" type=\"text\" />\
                </div>\
                <div>\
                    <input v-model=\"hours\" class=\"input--time\" type=\"text\" />:\
                    <input v-model=\"minutes\" class=\"input--time\" type=\"text\" />:\
                    <input v-model=\"seconds\" class=\"input--time\" type=\"text\" />\
                </div>\
                <div>\
                    <select v-model=\"age\" >\
                        <option :value=\"null\" >-</option>\
                        <option v-for=\"a in ageInUniverse\" :value=\"a.id\" >{{ a.initials }}</option>\
                    </select>\
                </div>\
                <div></div>\
        </div>\
    </fieldset>\
    ",
    data: function(){
        return {
            active: false,
        };
    },
    props: ["obj","field","title"],
    computed: {
        monthsOfYear: function () {
            return this.$store.getters.monthsOfYear;
        },
        ageInUniverse: function () {
            return this.$store.getters.ageInUniverse;
        },
        age: {
            get: function (){
                var field = this.field
                if (this.obj[field + "_age"] > 0) {
                    this.active = true;
                }  
                return this.obj[field + "_age"];
            },
            set: function (val) {
                var field = this.field
                this.obj[field + "_age"] = val;               
            }
        },
        year: {
            get: function (){
                var field = this.field
                return this.obj[field + "_year"];
            },
            set: function (val) {
                var field = this.field
                this.obj[field + "_year"] = val;               
            }
        },
        month: {
            get: function (){
                var field = this.field
                return this.obj[field + "_month"];
            },
            set: function (val) {
                var field = this.field
                this.obj[field + "_month"] = val;               
            }
        },
        date: {
            get: function (){
                var field = this.field
                return this.obj[field + "_date"];
            },
            set: function (val) {
                var field = this.field
                this.obj[field + "_date"] = val;               
            }
        },
        hours: {
            get: function (){
                var field = this.field
                return this.obj[field + "_hours"];
            },
            set: function (val) {
                var field = this.field
                this.obj[field + "_hours"] = val;               
            }
        },
        minutes: {
            get: function (){
                var field = this.field
                return this.obj[field + "_minutes"];
            },
            set: function (val) {
                var field = this.field
                this.obj[field + "_minutes"] = val;               
            }
        },        
        seconds: {
            get: function (){
                var field = this.field
                return this.obj[field + "_seconds"];
            },
            set: function (val) {
                var field = this.field
                this.obj[field + "_seconds"] = val;               
            }
        },
    },





});