var elementMetaData = Vue.component('element-meta-data', {
	template: "<div>\
		<div>\
		  <p class=\"rubrik\">Bestiary: <span class=\"ink\">{{ masterdata.universe.name }}</span></p>\
		</div> \
		<div>\
			  <div v-if=\"masterdata.type_grund == 'kampagne' \">\
				  <p class=\"rubrik faded\">Campaign: {{ masterdata.present_name }}</p>\
			  </div>\
			  <div v-else>\
				  <p class=\"rubrik\" v-if=\"masterdata.campaigns.length > 0\">Campaigns:\
					<span class=\"ink\" v-for=\"(c,i) in masterdata.campaigns\" >\
					  <a :href=\"'/almanak/side/' + c.campaign.id\" >{{ c.campaign.present_name }}</a>\
					  <span v-if=\"c.approved == 0\" >(Approval missing)</span>\
					  <span v-if=\"c.approved == 1\" >(Reapproval missing)</span>\
					  <button class=\"btn-offgame\" v-if=\"c.approved == 0 && campaignIsCurrent(c.campaign.id).approved == 0\" @click=\"approveElement(i,c.id)\">Approve</button>\
					  <button class=\"btn-offgame\" v-if=\"c.approved == 1 && campaignIsCurrent(c.campaign.id).approved == 1\" @click=\"approveElement(i,c.id)\" >Reapprove</button>\
					</span>\
					 <img v-if=\"masterdata.user_permissions.write == true && $store.state.initial.roleId == 0\" :src=\"static + 'images/icons/pencil.svg'\" style=\"cursor:pointer;height:1em;\" @click=\"loadCampaignsInUniverse();editCampaigns = !editCampaigns\" />\
				  </p>\
				  <p class=\"rubrik\" v-else>\
					Not included in a campaign <img v-if=\"masterdata.user_permissions.write == true && $store.state.initial.roleId == 0\" :src=\"static + 'images/icons/pencil.svg'\" style=\"cursor:pointer;height:1em;\" @click=\"loadCampaignsInUniverse();editCampaigns = !editCampaigns\" />\
				  </p>\
			  </div>\
			  <div v-if=\"editCampaigns\">\
				<p class=\"rubrik\" ><strong>Edit campaigns:</strong></p>\
				<p>\
				  <select @change=\"addCampaign(campaignSelected)\" v-model=\"campaignSelected\"> {{ addCampaignAjaxStatus }}\
					<option value=\"0\">Add campaign</option>\
					<option v-for=\"(c,i) in campaignsInUniverse\" v-if=\"campaignsIds.indexOf(c.id) == -1\" :value=\"c.id\">\
					  {{ c.present_name }}\
					</option>\
				  </select>\
				</p>\
				<div class=\"ink\" v-for=\"c in masterdata.campaigns\" >\
				  <p  class=\"ink\" >\
					{{ c.campaign.present_name }}\
					<strong v-if=\"masterdata.user_permissions.write == true\" class=\"rubrik\" style=\"cursor:pointer\" @click=\"c.campaign.delete_question = !c.campaign.delete_question\"> &times;</strong>\
				  </p>\
				  <p class=\"rubrik\" v-if=\"c.campaign.delete_question == true\" >\
					Do you want to remove {{ c.present_name }} as a connected campaign <button @click=\"removeCampaign(c)\">Yes</button> <button @click=\"c.campaign.delete_question = !c.campaign.delete_question\">No</button>\
				  </p>\
				</div>\
				<br/>\
			  </div>\
			</div>\
	\
	</div>",
    data: function() {
        return {
			editCampaigns: false,
			campaignSelected: 0,
            addCampaignAjaxStatus: "",
        }
    },
    methods: {
		approveElement: function (index,id) {
			var self = this;
			axios.patch("/api/campaignelement/" + id + "/update/", { approved: 2 }).then(function(response) {
				self.masterdata.campaigns[index].approved = 2;
				// self.$store.state.session = response.data;
				// self.$store.state.element.loaded.sessiondata = true;
				// self.$store.state.initial.sessionLoaded = true
			}).catch(function(error) {
				console.log(error);
			});
		},
		loadCampaignsInUniverse: function (){
			var self = this;
			if(campaignId){
				axios.get("/api/session/" + campaignId + "/?d=us1_ages__cs1_universe__us1_name__us1_campaigns&format=json").then(function(response) {
					self.$store.state.session = response.data;
					self.$store.state.element.loaded.sessiondata = true;
					self.$store.state.initial.sessionLoaded = true
				}).catch(function(error) {
					console.log(error);
				});
			}
		},
		addCampaign: function(i) {
            var self = this;
            // self.masterdata.campaigns.push(self.campaignsInUniverse[i]);
            // self.addCampaignAjaxStatus = "adding";
			console.log(i)

            this.updateCampaignsAjax(i);
        },
        removeCampaign: function(c) {

			var self = this;
			axios.delete("/api/campaignelement/" + c.id + "/delete/").then(function(response) {
				self.$store.dispatch("getElement");
			});

        },
        updateCampaignsAjax: function(idCampaign) {
            //console.log("afaf", "afaf");
            var self = this;
			axios.post("/api/campaignelement/create/",{
				"element": self.masterdata.id,
				"campaign": idCampaign,
				"approved":0,
				"canon":0,

			}).then(function(response) {
				self.$store.dispatch("getElement");
			});

        },
		campaignIsCurrent: function(id) {
			console.log("Approve",this.initial.roleId,this.initial.campaignId,id);
			if (this.campaignsContainingSession != null && id == this.campaignsContainingSession.campaign) {
				if(this.initial.roleId == "0" && this.initial.campaignId == id) {
					return this.campaignsContainingSession;
				}
				else {
					return false;
				}
			}
			else {
				return false;
			}

		},
	},
	computed: {
		initial: function (){
			return this.$store.state.initial;
		},
		static: function (){

            return this.$store.state.initial.staticUrl;

        },
        masterdata: function(){

            return this.$store.state.element.masterdata;
        },
        userUniverses: function (){

            return this.$store.state.user.universes;
        },
		campaignsInUniverse: function (){
            return this.$store.state.session.universe.campaigns;
        },
		campaignsContainingSession: function (){
            return this.masterdata.campaigns_containing_session;
        },
		campaignsIds: function() {
            var ids = []
            _.each(this.masterdata.campaigns, function(c) {
                if (c) {
                    ids.push(c.id);
                }
            });
            return ids;
        },
	},

});
