#!/bin/bash
# Start containers and django developement server
echo ""
echo "--<< Starting application containers >>--"
docker-compose up -d
echo ""
echo "--<< Running Django Tests >>--"
docker-compose exec web python manage.py test