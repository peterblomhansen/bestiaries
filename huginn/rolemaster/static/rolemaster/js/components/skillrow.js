var skillrow = Vue.component('skillrow', {
	template: "<tr>\
		<td style=\"text-align:center;\"><input type=\"checkbox\" v-on:click=\"updateSelected()\" v-model=\"selectedSkill.selected\" ></td>\
		<td v-if=\"skill.area_name == null\" >\
			<a v-if=\"skill.area_support == false\" @click=\"loadSkillrowDetails(index)\" v-html=\"' <strong>' + skill.name + '</strong> ' + fromSimilar\" class=\"cursor--pointer ink\" ></a>\
			<span v-else style=\"opacity:0.7\" v-html=\"'<strong>' + skill.name + '</strong> ' + fromSimilar\" ></span> <span v-if=\"hasAdditionalInformation.length > 0\">{{hasAdditionalInformation}}</span>\
		</td>\
		<td v-else ><a  @click=\"loadSkillrowDetails(index)\" v-html=\"'&nbsp;- <i>' + skill.area_name +  '</i> ' + hasAdditionalInformation + fromSimilar\" class=\"cursor--pointer ink\" ></a></td>\
		<td >{{ stats }}</td>\
		<td v-if=\"skill.area == null\" ><span>{{ skillCost }} </span>\
			<button v-if=\"skillCost.length == 0 && $store.state.character.profession\" @click=\"skill.change_cost = !skill.change_cost\" 			>\
				Add Cost\
			</button>\
			<button v-if=\"skillCost.length != 0 && $store.state.character.profession\" @click=\"skill.change_cost = !skill.change_cost\">\
				Change Cost\
			</button>\
			<div class=\"skill__add-cost\" v-if=\"skill.change_cost == true\">\
				<input class=\"skill__cost-input\"  type=\"text\" v-model=\"skill.cost.cost_1\" /> / \
				<input class=\"skill__cost-input\"  type=\"text\" v-model=\"skill.cost.cost_2\"  /> / \
				<input class=\"skill__cost-input\"  type=\"text\" v-model=\"skill.cost.cost_3\"  />\
				<button @click=\"updateSkillCost()\" >Update</button>\
			</div>\
		</td>\
		<td v-else >{{ skillCost }}</td>\
		<td v-if=\"skill.gm_ranks != null && allowed != false\" :class=\"ranksCssClass\" class=\"skill__num-rank\">{{ skillRanksTotal }}</td>\
		<td v-if=\"skill.gm_ranks != null && allowed != false\" class=\"skill__num-rank\" >\
			<input :class=\"ranksValidated(skill.gm_ranks)\" type=\"text\" v-model=\"skill.gm_ranks.num_ranks\" @blur=\"updateRanks(skill.gm_ranks)\" />\
		</td>\
		<td v-if=\"skill.hobby_ranks != null && allowed != false\" class=\"skill__num-rank\" >\
			<input :class=\"ranksValidated(skill.hobby_ranks)\" type=\"text\" v-model=\"skill.hobby_ranks.num_ranks\" @blur=\"updateRanks(skill.hobby_ranks)\" />\
		</td>\
		<td class=\"skill__num-rank\" v-for=\"(l,i) in levels\" v-if=\"skill.dev_ranks != null && allowed != false\" >\
			<input :class=\"ranksValidated(skill.dev_ranks[i])\" type=\"text\" v-model=\"skill.dev_ranks[i].num_ranks\" @blur=\"updateRanks(skill.dev_ranks[i],i)\" />\
		</td>\
	</tr>",
	props: {
		index: Number,
	},
	data: function (){
			return {
				ranksArray: [],
				fromSimilar: "",
				ranksCssClass: "",

		}
	},
	methods: {
		loadSkillrowDetails: function (i){
			this.$store.state.loaded.skillrowDetails = i;
		},
		updateSelected: function (){
			var self = this;
			console.log(this.selectedSkill.selected);
			if(this.selectedSkill.selected == false) {
				this.selectedSkill.selected = true;
			}
			else {
				this.selectedSkill.selected = false;
			}
			if(this.selectedSkill.id == null) {
				axios.post("/rm/api/selectedskill/create/",this.selectedSkill).then(function(response){
					self.$store.state.character.selected_skills.push(response.data);
				}).catch(function(error) {
						console.log(error);
				});
			}
			else if(this.selectedSkill.id > 0) {
				axios.patch("/rm/api/selectedskill/" + this.selectedSkill.id + "/update/",this.selectedSkill).then(function(response){
					self.selectedSkill = response.data;
				}).catch(function(error) {
						console.log(error);
				});
			}
			console.log(this.selectedSkill.selected);

		},
		// Moved to $store.getters
		ranksValidated: function (rankObj){
			if (rankObj.num_ranks == 0) {
				return "ranks--zero";
			}
			else if (rankObj.num_ranks <= this.allowed || rankObj.development_type == "gm") {
				return "ranks--valid";
			}
			else {
				return "ranks--invalid";
			}

		},
		// Moved to $store.mutations
		updateRanks: function (rankObj,lvlIndex) {
			var self = this;
			if(rankObj.num_ranks != rankObj.num_ranks_init) {

				if(rankObj.num_ranks <= this.allowed || rankObj.development_type == "gm") {
					console.log("Valid");
	 				if(rankObj.id == null) {
						axios.post("/rm/api/developedskill/create/",rankObj).then(function(response){
							console.log(response.data);
							rankObj.id = response.data.id;
							rankObj.num_ranks = response.data.num_ranks;
							rankObj.num_ranks_init = response.data.num_ranks;
							rankObj.dev_points_used = self.devPrice(response.data.num_ranks);
						}).catch(function(error) {
				            console.log(error);
				        });

					}
					else if (rankObj.id > 0) {
						axios.patch("/rm/api/developedskill/" + rankObj.id + "/update/",rankObj).then(function(response){
							rankObj.id = response.data.id;
							rankObj.num_ranks = response.data.num_ranks;
							rankObj.num_ranks_init = response.data.num_ranks;
							rankObj.dev_points_used = self.devPrice(response.data.num_ranks);
						}).catch(function(error) {
				            console.log(error);
				        });

					}

				}
				else {
					console.log("Invalid");
					rankObj.num_ranks = rankObj.num_ranks_init;
				}
			}
			else {
				console.log("Not Changed")
			}
		},
		updateSkillCost: function(){

			var self = this;
			costObj = this.skill.cost;
			costObj.profession = this.$store.state.character.profession.id;
			costObj.skill = this.skill.id;
			costObj.cost_2 = costObj.cost_2.toString().replace("*","-1");
			costObj.cost_3 = costObj.cost_3.toString().replace("*","-1");

			console.log(costObj);

			if(costObj.id == null) {
				axios.post("/rm/api/skillcost/create/",costObj).then(function(response){

					if(self.skill.area_support == false) {
						self.skill.cost = {
							id: response.data.id,
							cost_1: response.data.cost_1,
							cost_2: response.data.cost_2,
							cost_3: response.data.cost_3,
						};
						// costObj.id = response.data.id;
						// costObj.cost_1 = response.data.cost_1;
						// costObj.cost_2 = response.data.cost_2;
						// costObj.cost_3 = response.data.cost_3;
					}
					else {
						var areas = self.$store.state.skillrows.filter(function(r) {
							return parseInt(r.id) == parseInt(self.skill.id);
						});
						console.log(areas);
						_.each(areas,function(a){
							//delete a.cost;
							a.cost = {
								id: response.data.id,
								cost_1: response.data.cost_1,
								cost_2: response.data.cost_2,
								cost_3: response.data.cost_3,
							};
							// a.cost.id = response.data.id;
							// a.cost.cost_1 = response.data.cost_1;
							// a.cost.cost_2 = response.data.cost_2;
							// a.cost.cost_3 = response.data.cost_3;
						});
					}
					self.skill.change_cost = false;

				}).catch(function(error) {
					console.log(error);
				});
			}
			else if (costObj.id > 0) {
				axios.patch("/rm/api/skillcost/" + costObj.id + "/update/",costObj).then(function(response){

					self.skill.change_cost = false;

				}).catch(function(error) {
						console.log(error);
				});

			}
		},
		// moved to $store.getters
		devPrice: function (num_ranks){
			var cost = this.skill.cost;
			var price = 0;

			if (num_ranks == 1) {
				price = cost.cost_1
			}
			else if (num_ranks == 2){
				if (cost.cost_2 == -1) {
					price = cost.cost_1 * num_ranks
				}
				else {
					price = cost.cost_1 + cost.cost_2
				}
			}
			else if (num_ranks == 3) {
				if (cost.cost_2 == -1) {
					price = cost.cost_1 * num_ranks;
				}
				else if (cost.cost_3 == -1) {
					price = cost.cost_1 + cost.cost_2 + cost.cost_3
				}
			}

			else if (num_ranks > 3) {
				if (cost.cost_2 == -1) {
					price = cost.cost_1 * num_ranks
				}
				else if (cost.cost_3 == -1) {
					price = cost.cost_1 + cost.cost_2 * (num_ranks - 1)
				}
			}
			return price;
		},
		// MOVED TO $store.getters
		calcutedTotalRanks: function(skill) {

			var totalRanks = 0;
			var gmRanks = 0;
			var hobbyRanks = 0;

			if(skill.gm_ranks) {
				gmRanks = parseInt(skill.gm_ranks.num_ranks);
			}
			if(skill.hobby_ranks) {
				hobbyRanks = parseInt(skill.hobby_ranks.num_ranks);
			}

			_.each(skill.dev_ranks,function(r){
				totalRanks = totalRanks + parseInt(r.num_ranks);
			});

			totalRanks = totalRanks + gmRanks + hobbyRanks;

			return totalRanks;

		}


	},
	computed: {
		hasAdditionalInformation: function (){
			var self = this;
			var marks = "";
			var misc = this.character.skill_misc_bonuses.filter(function(b){
							return b.skill == self.skill.id && b.area == self.skill.area && b.deleted != true;
						});
			var comments = this.$store.state.character.skill_comments.filter(function(s){ return self.skill.id == s.skill && self.skill.area == s.area });
			_.each(misc,function(){
				marks = marks + "*";
			});
			_.each(comments,function(){
				marks = marks + "*";
			});

			return marks;

		},
		selectedSkill: {
			get: function() {

				var self = this;
				var i = this.$store.state.character.selected_skills.findIndex(function(ss){
					return ss.skill == self.skill.id && ss.area == self.skill.area;
				});
				if(i != -1) {
					return this.$store.state.character.selected_skills[i];
				}else {
					return { id: null, skill: self.skill.id, area: self.skill.area, selected: false, character: this.$store.state.character.id }
				}
			},
			set: function(newValue) {
				var i = this.$store.state.character.selected_skills.findIndex(function(ss){
					return ss.skill == self.skill.id && ss.area == self.skill.area;
				});
				this.$store.state.character.selected_skills[i] = newValue;
			}

		},
		skill: function (){
			return this.$store.state.skillrows[this.index];
		},
		character: function (){
			return this.$store.state.character;
		},
		skillRanksTotal: function (){

		//	if(this.skill.area_support == false) {
				if(this.calcutedTotalRanks(this.skill) > this.similarRanksTotal.ranks || this.similarRanksTotal.ranks == 0) {
					this.ranksCssClass = "";
					return this.calcutedTotalRanks(this.skill)
				}else {
					this.fromSimilar = " (" + this.similarRanksTotal.ratio + " from " + this.similarRanksTotal.name + ")";
					this.ranksCssClass = "alert alert--warning";
					//this.ranksCssClass = "num-ranks--similar";
	 				return Math.round(this.similarRanksTotal.ranks);
				}

				//return this.calcutedTotalRanks(this.skill) + " " + this.similarRanksTotal;
	//		}else {
	//				return this.calcutedTotalRanks(this.skill);
	//		}
		},
		// MOVED TO $store.getters.ranksValidated()
		similarRanksTotal: function (){
			if(this.skill.similarity) {
		//	if(this.skill.similarity && this.skill.area == null) {

				var self = this;
				var simRankArray = [];
				var max = 0;
				_.each(this.skill.similarity,function(s){
				 	var skillRowIndex = self.$store.state.skillrows.findIndex(function(r){ return s.origin == r.id && r.area == s.area});
				 	//simRankArray.push(skillRowIndex);
					if(skillRowIndex > -1) {
						var similarranks = self.calcutedTotalRanks(self.$store.state.skillrows[skillRowIndex]) * s.factor;
						var simName = self.$store.state.skillrows[skillRowIndex].name;
						if(similarranks > 0.5) {
							simRankArray.push({ "name": simName, "ranks": similarranks, "ratio": s.ratio});
						}
					}
				});
				if(simRankArray.length > 0 ) {
					//console.log(this.skill.id,simRankArray);
					max = _.maxBy(simRankArray, function(o) { return o.ranks; });
				}
				if(max.ranks) {

					return max;
				}
				else {
					return { "name": null, "ranks": 0, "ratio": 0};
				}
			}
			else {

				return 0;

			}


		},
		stats: function(){
			var statsString = "";
			statsString = this.skill.stat_1 + "/" + this.skill.stat_2 + "/" + this.skill.stat_3;
			if(this.skill.stat_1 == "0" || this.skill.stat_1 == null) {
				statsString = "None";
			}
			else if (this.skill.stat_2 == null){
				statsString = this.skill.stat_1;
			}
			else if (this.skill.stat_3 == null) {
				statsString = this.skill.stat_1 + "/" + this.skill.stat_2;
			}
			else {
				statsString = this.skill.stat_1 + "/" + this.skill.stat_2 + "/" + this.skill.stat_3;

			}

			return statsString;

		},
		// Moved to $store.getters
		skillCost: function (){
			var self = this;
			var skillCost = ""
			// var skillCostObj = this.professionSkillCosts.find(function(c){
            //
			// 	return c.skill == self.skill.id;
			// })
			var skillCostObj = this.skill.cost;
			if(skillCostObj) {
				skillCost = skillCostObj;

				//var statsString = "";
				//statsString = this.skill.stat_1 + "/" + this.skill.stat_2 + "/" +this.skill.stat_3;
				if(skillCostObj.cost_1 == 0) {
					skillCost = "";
				}
				else if (skillCostObj.cost_2 == 0){
					skillCost = skillCostObj.cost_1;
				}
				else if (skillCostObj.cost_2 == -1) {
					skillCost = skillCostObj.cost_1 + "/*";
				}
				else if (skillCostObj.cost_3 == 0 || skillCostObj.cost_3 == null) {
					skillCost = skillCostObj.cost_1 + "/" + skillCostObj.cost_2;
				}
				else {
					skillCost = skillCostObj.cost_1 + "/" + skillCostObj.cost_2 + "/" + skillCostObj.cost_3;

				}
			}
			else {
				this.skill.cost = {
					id: null,
					cost_1:	0,
					cost_2:	0,
					cost_3:	0,
				}
			}

			return skillCost;

		},
		levels: function (){
			var levels = [];
			n = 1;
			while (n <= this.$store.state.initial.level.viewed) {
				levels.push(n);
				n++;
			}
			return levels;
		},
		// Moved inside $store.getters.
		allowed: function (){
			var self = this;
			var allowed = 0;
			var skillCostObj = this.skill.cost;

			if(!skillCostObj) {
				return false;
			}
			var c1 = parseInt(skillCostObj.cost_1);
			var c2 = parseInt(skillCostObj.cost_2);
			var c3 = parseInt(skillCostObj.cost_3);

			if(c1 > 0 && c2 == 0 && c3 == 0) {
				//console.log("Allowed 1");
				allowed = 1;
			}
			else if(c1 > 0 && c2 > 0 && c3 == 0) {
				//console.log("Allowed 2");
				allowed = 2;
			}
			else if(c1 > 0 && c2 > 0 && c3 > 0) {
				//console.log("Allowed 3");
				allowed = 3;
			}
			else if(c1 > 0 && c2 > 0 && c3 == -1) {
				//console.log("Allowed infinit");
				allowed = 100;
			}
			else if(c1 > 0 && c2 == -1 ) {
				//console.log("Allowed infinit");
				allowed = 100;
			}

			return allowed;
		},
	}



 });
