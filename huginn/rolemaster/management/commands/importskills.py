from django.core.management.base import BaseCommand, CommandError
from django.db import connections
from collections import namedtuple
from rolemaster.models import STAT_CHOICES, RolemasterCharacter




class Command(BaseCommand):
	help = 'Closes the specified poll for voting'

	def handle(self, *args, **options):

		dbleg = connections['legacy'].cursor()
		dbnew = connections['default'].cursor()


		print("Rolemaster import script starting")

		def namedtuplefetchall(cursor):
			#"Return all rows from a cursor as a namedtuple"
			desc = cursor.description
			nt_result = namedtuple('Result', [col[0] for col in desc])
			return [nt_result(*row) for row in cursor.fetchall()]


		def importSkills():

			dbleg = connections['legacy'].cursor()
			dbnew = connections['default'].cursor()

			dbleg.execute("SELECT * FROM rm_skill")

			skills = namedtuplefetchall(dbleg)

			for s in skills:
				self.stdout.write(self.style.WARNING("Importing " + s.name))
				areaSupport = False
				if s.specify_area == 1:
					areaSupport = True

				dbnew.execute("""INSERT INTO rolemaster_skill (id,name,category,stat_1,stat_2,stat_3,area_support) VALUES (%s,%s,%s,%s,%s,%s,%s)""",(s.id,s.name,s.category,s.stat1,s.stat2,s.stat3,areaSupport))

				self.stdout.write(self.style.SUCCESS(s.name + " Imported"))

			print(str(len(skills)) + " Skills Imported")

		def importSkillAreas():

			dbleg = connections['legacy'].cursor()
			dbnew = connections['default'].cursor()

			dbleg.execute("SELECT * FROM rm_skill_area")

			areas = namedtuplefetchall(dbleg)

			for a in areas:
				self.stdout.write(self.style.WARNING("Importing " + a.navn))
				#areaSupport = False
				#if s.specify_area == 1:
				#	areaSupport = True
				almanakId = a.almanak_id
				if a.almanak_id == 0:
					almanakId = None

				skillId = a.skill_id
				if a.skill_id == 0:
					skillId = None

				universeId = a.id_univers
				if a.id_univers == 0:
					universeId = None

				deletedBool = False
				if a.deactivated == 1:
					deletedBool = True

				dbnew.execute("""INSERT INTO rolemaster_skillarea (id,name,area_type,weapon_category,stat_1,stat_2,stat_3,deleted,almanak_page_id,skill_id,universe_id) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",(a.id,a.navn,a.type,a.type_weapon,a.stat1,a.stat2,a.stat3,deletedBool,almanakId,skillId,universeId))

				self.stdout.write(self.style.SUCCESS(a.navn + " Imported"))

			print(str(len(areas)) + " Skill Areas Imported")


		def importArmorTypes():


			dbleg.execute("SELECT * FROM rm_armor_type")

			at = namedtuplefetchall(dbleg)

			for a in at:
				skill = None
				if a.id_skill != 0:
					skill = a.id_skill

				dbnew.execute("""INSERT INTO rolemaster_armortype (armor_type,min_mm,max_mm,missile_penalty,armor_quickness_penalty,skill_id) VALUES (%s,%s,%s,%s,%s,%s)""",(a.armor_type,a.min_mm,a.max_mm,a.missile_penalty,a.armor_quickness_penalty,skill))

			print(str(len(at)) + " Armortypes Imported")
			##print(at);


		def importProfessions():

			dbleg.execute("SELECT * FROM profession")

			prof = namedtuplefetchall(dbleg)

			for p in prof:


				dbnew.execute("""INSERT INTO rolemaster_profession (id,name,prime_stat_1,prime_stat_2) VALUES (%s,%s,%s,%s)""",(p.id, p.navn, p.prime_stat_1,p.prime_stat_2))

			print(str(len(prof)) + " Professions Imported")


		def importRaces():

			dbleg.execute("SELECT * FROM race")

			race = namedtuplefetchall(dbleg)

			#print(race)

			for r in race:

				dbnew.execute("""INSERT INTO rolemaster_race (kin_ptr_id,luck,agility,constitution,memory,reasoning,self_discipline,strength,quickness,presence,empathy,intuition,hit_dice) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",(r.id,0,r.ag,r.co,r.me,r.re,r.sd,r.st,r.qu,r.pr,r.em,r.intu,r.hit_dice))

			print(str(len(race)) + " Races Imported")



		def importIndividuals():

			dbleg.execute("SELECT * FROM individ")

			ind = namedtuplefetchall(dbleg)

			#print(ind)

			for i in ind:
				if i.profession != 0 and i.race != 0 :

					dbnew.execute("""INSERT INTO rolemaster_rolemastercharacter (individual_ptr_id,level,profession_id,race_id) VALUES (%s,%s,%s,%s)""",(i.id,i.level,i.profession,i.race))

			print(str(len(ind)) + " Individuals Imported")


		def importBodyDevelopment():

			dbleg.execute("SELECT * FROM rm_body_development")

			bd = namedtuplefetchall(dbleg)

			for b in bd:
				dbnew.execute("""INSERT INTO rolemaster_bodydevelopment (id,level,hits,character_id) VALUES (%s,%s,%s,%s)""",(b.id,b.level,b.hits,b.id_individ))

			print(str(len(bd)) + " levels of body development Imported")


		def importWeaponCategoryPriorities():

			dbleg.execute("SELECT * FROM rm_entitet_weapon_cat")

			weapcat = namedtuplefetchall(dbleg)

			for w in weapcat:

				dbnew.execute("""INSERT INTO rolemaster_characterweaponcategorypriority (id,category,character_id,skill_id) VALUES (%s,%s,%s,%s)""",(w.id,w.name_weapon_cat,w.id_entitet,w.id_weapon_cat))

			print(str(len(weapcat)) + " Weapon category priorities Imported")


		def importDevelopedSkills():

			dbleg.execute("SELECT * FROM rm_karakterark_developed_skill")

			devskills = namedtuplefetchall(dbleg)

			for d in devskills:

				area = None
				if d.id_area != 0:
					area = d.id_area

				dbnew.execute("""INSERT INTO rolemaster_developedskill (id,level,num_ranks,development_type,area_id,character_id,skill_id) VALUES (%s,%s,%s,%s,%s,%s,%s)""",(d.id,d.level,d.num_ranks,d.type,area,d.id_entitet,d.id_skill))

			print(str(len(devskills)) + " skill developments Imported")


		def importSelectedSkills():

			dbleg.execute("SELECT * FROM rm_karakterark_selected_skill")

			selskills = namedtuplefetchall(dbleg)

			for s in selskills:

				selected = False
				if s.selected == 1:
					selected = True

				area = None
				if s.id_area > 0:
					area = s.id_area

				dbnew.execute("""INSERT INTO rolemaster_selectedskill (selected,area_id,character_id,skill_id) VALUES (%s,%s,%s,%s)""",(selected,area,s.id_individ,s.id_skill))


		def importLevelBonuses():

			dbleg.execute("SELECT * FROM rm_profession_level_bonus")

			lvlbonuses = namedtuplefetchall(dbleg)

			for l in lvlbonuses:


				dbnew.execute("""INSERT INTO rolemaster_professionlevelbonus (id,category,profession_id,bonus) VALUES (%s,%s,%s,%s)""",(l.id,l.category,l.id_profession,l.bonus))



		def importSkillCosts():

			dbleg.execute("SELECT * FROM rm_profession_skill_cost")

			skillCosts = namedtuplefetchall(dbleg)

			for c in skillCosts:

				if c.id_profession > 0:

					dbnew.execute("""INSERT INTO rolemaster_professionskillcost (profession_id,skill_id,cost_1,cost_2,cost_3) VALUES (%s,%s,%s,%s,%s)""",(c.id_profession,c.id_skill,c.cost_1,c.cost_2,c.cost_3))




		def importSkillBonuses():

			dbleg.execute("SELECT * FROM rm_skill_item_bonus")

			itembonus = namedtuplefetchall(dbleg)

			for i in itembonus:

				area = None
				if i.id_area != 0:
					area = i.id_area

				dbnew.execute("""INSERT INTO rolemaster_skillmiscbonus (bonus,area_id,character_id,skill_id,bonus_type) VALUES (%s,%s,%s,%s,%s)""",(i.bonus,area,i.id_entitet,i.id_skill,"item"))

			dbleg.execute("SELECT * FROM rm_skill_misc_bonus")

			miscbonus = namedtuplefetchall(dbleg)

			for m in miscbonus:

				area = None
				if m.id_area != 0:
					area = m.id_area

				dbnew.execute("""INSERT INTO rolemaster_skillmiscbonus (bonus,area_id,character_id,skill_id,bonus_type) VALUES (%s,%s,%s,%s,%s)""",(m.bonus,area,m.id_entitet,m.id_skill,"misc"))



		def importSimilarities():

			dbleg.execute("SELECT * FROM rm_skill_sim_area")

			areasim = namedtuplefetchall(dbleg)

			for s in areasim:

				dbnew.execute("""INSERT INTO rolemaster_skillareasimilarity (ratio,origin_id,target_id) VALUES (%s,%s,%s)""",(s.ratio,s.id_area_org,s.id_area_target))


			dbleg.execute("SELECT * FROM rm_skill_sim_skill")

			areasim = namedtuplefetchall(dbleg)

			for s in areasim:

				dbnew.execute("""INSERT INTO rolemaster_skillskillsimilarity (ratio,origin_id,target_id) VALUES (%s,%s,%s)""",(s.ratio,s.id_skill_org,s.id_skill_target))


		def importStatDist():


			dbleg.execute("SELECT * FROM rm_stat_bonus_distribution_table")

			dist = namedtuplefetchall(dbleg)

			for d in dist:

				dbnew.execute("""INSERT INTO rolemaster_statbonusdistribution (stat,bonus,dev_points,spell_points) VALUES (%s,%s,%s,%s)""",(d.stat,d.bonus,d.dev_pt,d.spell_pt))


		def importStats():

			dbleg.execute("SELECT * FROM rm_karakterark_stats")

			stats = namedtuplefetchall(dbleg)

			for s in stats:
				pc = RolemasterCharacter.objects.filter(id=s.id_entitet)
				if pc:
#																			s.id_entitet,s.type,s.level,s.Lu,s.Co,s.Ag,s.Re,s.Me,s.SD,s.St,s.Qu,s.Em,s.Pr,s.In
					dbnew.execute("""INSERT INTO rolemaster_stats (character_id,stat_type,level,luck,constitution,agility,memory,reasoning,self_discipline,strength,quickness,empathy,presence,intuition) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",(s.id_entitet,s.type,s.level,s.Lu,s.Co,s.Ag,s.Re,s.Me,s.SD,s.St,s.Qu,s.Em,s.Pr,s.In))


		def importSkillAreaTypes():

			dbleg.execute("SELECT * FROM rm_skill_type_area")

			areatypes = namedtuplefetchall(dbleg)

			for a in areatypes:

				dbnew.execute("""INSERT INTO rolemaster_skillareatype (area_type,skill_id) VALUES (%s,%s)""",(a.type_area,a.id_skill))

		importSkills()
		importProfessions()
		importRaces()
		importIndividuals()
		importArmorTypes()
		importSkillAreas()
		importBodyDevelopment()
		importWeaponCategoryPriorities()
		importDevelopedSkills()
		importSelectedSkills()
		importLevelBonuses()
		importSkillCosts()
		importSkillBonuses()
		importSimilarities()
		importStatDist()
		importStats()
		importSkillAreaTypes()
