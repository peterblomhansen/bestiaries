var gender = Vue.component('gender', {
	template: "\
	<p class=\"ink\">\
        <strong class=\"rubrik\" >Gender: </strong>{{gender }}\
	</p>",
	computed: {
		gender: function () {
            return this.$store.state.element.masterdata.gender;
		}
	}
});