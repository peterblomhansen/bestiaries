var ingameNow = Vue.component('ingame-now', {
    props: {
        element: Object,
        ages: Array,
        isEditor: Boolean,
        st: String,
    },
    data: function() {
        return {
            editIngameNow: false,
            ingameNowMessage: "",
            months: {
                select: "",
                options: [{
                    text: "jan",
                    value: 1
                }, {
                    text: "feb",
                    value: 2
                }, {
                    text: "mar",
                    value: 3
                }, {
                    text: "apr",
                    value: 4
                }, {
                    text: "may",
                    value: 5
                }, {
                    text: "jun",
                    value: 6
                }, {
                    text: "jul",
                    value: 7
                }, {
                    text: "aug",
                    value: 8
                }, {
                    text: "sep",
                    value: 9
                }, {
                    text: "oct",
                    value: 10
                }, {
                    text: "nov",
                    value: 11
                }, {
                    text: "dec",
                    value: 12
                }, ]
            },
        }
    },
    methods: {
        changeIngameNow: function(bd) {
            this.ingameNowMessage = "Updating";
            this.changeIngameNowAjax();
        },
        changeIngameNowAjax: _.debounce(function() {
            var self = this;
            var ingame_now_age = null;
            if (this.element.ingame_now_age != null && this.element.ingame_now_age == parseInt(this.element.ingame_now_age, 10)) {
                ingame_now_age = this.element.ingame_now_age;
            }
            axios.patch('/api/campaign/' + self.element.id + '/update/', {
                ingame_now_age: ingame_now_age,
                ingame_now_year: self.element.ingame_now_year,
                ingame_now_month: self.element.ingame_now_month,
                ingame_now_date: self.element.ingame_now_date,
            }).then(function(response) {
                console.log(response.data)
                self.ingameNowMessage = "Updated";
            }).catch(function(error) {
                console.log(error);
                if (error.response.status == 403) {}
            });
        }, 500),
        ingame_date_str: function(day, month, year, age = 0, min = 0, sec = 0) {
            var self = this;
            var ageIndex = _.findIndex(self.ages, function(o) {
                return o.id == age;
            });
            var ageComp = "Not defined";
            if (ageIndex > -1) {
                ageComp = this.ages[ageIndex].name
            }
            dateString = "Not set"
            if (day != 0 && month != 0 && year != 0 && age != null) {
                var monthNames = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
                var mIndex = parseInt(month) - 1;
                dateString = day + " " + monthNames[mIndex] + " " + year + " " + ageComp;
            }
            return dateString;
        },
    },
    computed: {
        ingameNowNotDefined: function() {
            def = false;
            if (this.element.ingame_now_age == null) {
                def = true;
            }
            return def;
        },
    }
});
