var overlay = Vue.component('overlay', {
    template: "\
    <transition name=\"fade-fold\">\
		<div class=\"overlay\" style=\"overflow:auto\">\
            <div class=\"border--padding\">\
                <div class=\"border--content\" :class=\"gridClass\">\
                    <slot></slot>\
                </div>\
            </div>\
        </div>\
    </transition>",
    props: {
        showOverlay: Boolean,
        gridClass: String,
    },
    data: function() {
        return { 
            show: false,
        }
    },
    methods: {},
    computed: {
        so: {
            get: function() {
                return this.show = this.showOverlay
            },
            set: function (value) {
                this.show = value;
            }
        }
    }

});
