from rest_framework import permissions
from almanak.models import Element, BestiariesUser, UniverseMembership, RpgElement, PlayerCharacter, ElementRelation, EntityEventRelation


class ElementPermissionRoles:

	def user_is_universe_owner(self,request,obj):
		#if universe owner
		if request.session.get("user_is_universe_owner") == True and obj.universe.id == request.session.get("universe_current"):
			return True

		return False

	def user_is_author(self,request,obj):
		#if author
		if request.user.id == obj.author.id:
			return True

		return False

	def user_is_roleplayer(self,request,obj):
		# if roleplayer
		if obj.type_spec == "player_character" and obj.id == request.session.get('role_current'):
			return True

		return False

	def user_is_gm(self,request,obj):
		# if gm for element
		if request.session.get("role_current") == 0:
			campaigns = [c.id for c in obj.campaigns.get_queryset()]
			gm_for = [rpg_el.id for rpg_el in request.user.gamemaster_for.get_queryset()]
			if request.session.get("campaign_current") in set(campaigns).intersection(gm_for):
				return True

		return False


	def element_is_shared_with_user(self,request,obj):
		# if shared with
		if obj.id in [el.id for el in request.user.elements_shared.get_queryset()]:
			return True

		return False


	def user_is_editor(self,request,obj):
		# if editor
		if obj.id in [el.id for el in request.user.editor_for.get_queryset()]:
			return True

		return False




class AlmanakPermission:

	def user_can_edit_name(request,obj):

		if obj.element.universe.id == request.session.get("universe_current"):

			if obj.author != None and obj.author.id == request.user.id:
				return True


			elif request.session.get("role_current") == 0:
				if len(request.session.get("gamemaster_for")) > 0:
					gm_for = request.session.get("gamemaster_for")
					if obj.element.type_grund == 'kampagne':
							if obj.element.id in gm_for:
								return True
					else:
						for gc in gm_for:
						#for gc in user.gamemaster_for.get_queryset():
							for oc in obj.element.campaigns.get_queryset():
								if (gc == oc.id):
									return True

			elif request.session.get("role_current") > 0:

				# If the object is a PlayerCharacter, is the user the roleplayer
				if obj.element.type_spec == "player_character": #hasattr(obj,"roleplayer"):
					if obj.element.id == request.session.get('role_current'):
						return True



		return False


	def user_can_read_element(request,obj):

		if obj.universe.id == request.session.get("universe_current"):

			# print("Universe Owner",request.session.get("user_is_universe_owner"))
			# print("Author",request.user.id == obj.author.id)
			# print("Roleplayer",obj.type_spec == "player_character" and obj.id == request.session.get('role_current'))
			# print("Session GM", request.session.get("role_current") == 0)
			# print("object's campaigns",[c.id for c in obj.campaigns.get_queryset()])
			# print("GM for object's campaign",request.session.get("campaign_current") in set([c.id for c in obj.campaigns.get_queryset()]).intersection([rpg_el.id for rpg_el in request.user.gamemaster_for.get_queryset()]))

			if obj.secret == False:
				return True

			elif obj.secret == True:

				#if universe owner
				if request.session.get("user_is_universe_owner") == True:
					return True

				#if author
				elif request.user.id == obj.author.id:
					return True

				# if roleplayer
				if obj.type_spec == "player_character" and obj.id == request.session.get('role_current'):
					return True

				# if gm for element
				elif request.session.get("role_current") == 0:
					campaigns = [c.id for c in obj.campaigns.get_queryset()]
					gm_for = [rpg_el.id for rpg_el in request.user.gamemaster_for.get_queryset()]
					if request.session.get("campaign_current") in set(campaigns).intersection(gm_for):
						return True

				# if shared with
				elif obj.id in [el.id for el in request.user.elements_shared.get_queryset()]:
					return True

				# if editor
				elif obj.id in [el.id for el in request.user.editor_for.get_queryset()]:
					return True

				return False

		return False


	def user_can_edit_element(request,obj):

		if obj.universe.id == request.session.get("universe_current"):

			#if universe owner
			if request.session.get("user_is_universe_owner") == True:
				return True

			#if author
			elif request.user.id == obj.author.id:
				return True

			# if roleplayer
			if obj.type_spec == "player_character" and obj.id == request.session.get('role_current'):
				return True

			# if gm for element
			elif request.session.get("role_current") == 0:
				campaigns = [c.id for c in obj.campaigns.get_queryset()]
				gm_for = [rpg_el.id for rpg_el in request.user.gamemaster_for.get_queryset()]
				if request.session.get("campaign_current") in set(campaigns).intersection(gm_for):
					return True

			# if editor
			elif obj.id in [el.id for el in request.user.editor_for.get_queryset()]:
				return True

			return False

		return False


	def user_can_edit_element_relation(request,element_1,element_2):
			permission_1 = AlmanakPermission.user_can_edit_element(request,element_1)
			permission_2 = AlmanakPermission.user_can_edit_element(request,element_2)			
			
			if permission_1 == True or permission_2 == True:
				return True
			return False

	def user_can_read_description(request,obj):

		if obj.universe.id == request.session.get("universe_current"):

			#if user is universe owner
			if request.session.get("user_is_universe_owner") == True:
				return True

			else:
				# the description is public
				if obj.secret is False:
					return True
				
				# the description is public
				else:
					# user is author of the description
					if request.user.id == obj.author.id:
						return True

					elif request.session.get("role_current") == 0 and request.session.get("campaign_current") in [c.id for c in obj.campaigns.get_queryset()]:
						return True

					elif request.session.get("role_current") in [s.id for s in obj.shared_with.get_queryset()]:
						return True 

					else:
						return False

			return False

		return False

	def user_can_edit_description(request,description):



		if request.session.get("user_is_universe_owner") is True:
			return True

		elif description.author.id is request.user.id:
			return True
			
		elif request.session.get("role_current") == 0 and request.session.get("campaign_current") in [c.id for c in description.campaigns.get_queryset()]:
			return True
		
		return False


class NameChangePermission(permissions.BasePermission):

	message = 'You cannot edit this name.'

	def has_object_permission(self, request, view, obj):

		if request.method in permissions.SAFE_METHODS:
			return True
		return AlmanakPermission.user_can_edit_name(request,obj)



class ElementPermission(permissions.BasePermission):

	def has_object_permission(self, request, view, obj):
		if request.method in permissions.SAFE_METHODS:
			return AlmanakPermission.user_can_read_element(request,obj)
		else:
			return AlmanakPermission.user_can_edit_element(request,obj)

	# def has_permission(self, request, view):
	# 	print("has_permission")		
	# 	if request.method in permissions.SAFE_METHODS:
	# 		return AlmanakPermission.user_can_read_element(request,obj)

	# 	elif request.method == "POST":
	# 		return True #return AlmanakPermission.user_can_read_element(request,obj)		

	# 	elif request.method == "PATCH":
	# 		obj = Element.objects.get(pk=view.kwargs.get('pk'))
	# 		return self.has_object_permission(request, view, obj)




class CanEditElement(permissions.BasePermission):

	def has_object_permission(self, request, view, obj):

		if request.method in permissions.SAFE_METHODS:
			return AlmanakPermission.user_can_edit_element(request,obj)

class ElementRelationPermission(permissions.BasePermission):

	def has_object_permission(self, request, view, obj):
		if request.method in permissions.SAFE_METHODS:
			pass
		else:
			return AlmanakPermission.user_can_edit_element_relation(request,obj.above,obj.below)

	def has_permission(self, request, view):
		if request.method in permissions.SAFE_METHODS:
			return True	
			
		elif request.method == "POST":
			above = Element.objects.get(pk=request.data.get("above"))
			below = Element.objects.get(pk=request.data.get("below"))
			return AlmanakPermission.user_can_edit_element_relation(request,above,below)			

		elif request.method == "PATCH":
			obj = ElementRelation.objects.get(pk=view.kwargs.get('pk'))
			return self.has_object_permission(request, view, obj)


class EntityEventRelationPermission(permissions.BasePermission):

	def has_object_permission(self, request, view, obj):
		if request.method in permissions.SAFE_METHODS:
			pass
		else:
			return AlmanakPermission.user_can_edit_element_relation(request,obj.event,obj.entity)

	def has_permission(self, request, view):
		if request.method in permissions.SAFE_METHODS:
			return True	
			
		elif request.method == "POST":
			entity = Element.objects.get(pk=request.data.get("entity"))
			event = Element.objects.get(pk=request.data.get("event"))
			return AlmanakPermission.user_can_edit_element_relation(request,entity,event)			

		elif request.method == "PATCH":
			obj = EntityEventRelation.objects.get(pk=view.kwargs.get('pk'))
			return self.has_object_permission(request, view, obj)

class EventPlaceRelationPermission(permissions.BasePermission):

	def has_object_permission(self, request, view, obj):
		if request.method in permissions.SAFE_METHODS:
			pass
		else:
			return AlmanakPermission.user_can_edit_element_relation(request,obj.event,obj.place)


class ElementItemRelationPermission(permissions.BasePermission):

	def has_object_permission(self, request, view, obj):
		if request.method in permissions.SAFE_METHODS:
			return True
		else:
			#return AlmanakPermission.user_can_edit_element_relation(request,obj.owned,obj.owner)
			if request.method == "PATCH":
				return AlmanakPermission.user_can_edit_element_relation(request,obj.owned,obj.owner)
				#obj = EntityEventRelation.objects.get(pk=view.kwargs.get('pk'))
				#return self.has_object_permission(request, view, obj)


	# 	if request.method in permissions.SAFE_METHODS:
	# 		return True	
			
	# 	elif request.method == "POST":
	# 		entity = Element.objects.get(pk=request.data.get("owned"))
	# 		event = Element.objects.get(pk=request.data.get("owner"))
	# 		return AlmanakPermission.user_can_edit_element_relation(request,entity,event)			

	# 	elif request.method == "PATCH":
	# 		obj = EntityEventRelation.objects.get(pk=view.kwargs.get('pk'))
	# 		return self.has_object_permission(request, view, obj)


class DescriptionPermission(permissions.BasePermission):

	message = ''

	def has_object_permission(self, request, view, obj):

		if request.method in permissions.SAFE_METHODS:
			self.message = 'You do not have permission to read this description'
			return AlmanakPermission.user_can_read_description(request,obj) # 	pass
		else:
			# if request.user.id is not obj.locked_by_user.id:
			# 	self.message = 'This description is currently locked by another user'
			# 	return False
			# else:	
				self.message = 'You do not have permission to edit this description'
				return AlmanakPermission.user_can_edit_description(request,obj)

