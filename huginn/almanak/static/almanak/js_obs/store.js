var store = new Vuex.Store({

    state: {
        // This data is basis for the data-collecting ajax calls on all pages.
        initial: {
            userId: userId,
            universeId: universeId,
            campaignId: campaignId,
            roleId: roleId,
            pageType: pageType,
            staticUrl: staticUrl,
            sessionLoaded: false
        },
        user: {
        },
		error: {
			message: "",
		},
		// Datasource for Description Full
		descriptionFull: {
			dataLoaded: false,
			data: {},
		},

		// Datasource for Image Full
		imageFull: {
			dataLoaded: false,
			data: {},
		},


      // Data source for almanak pages
      element: {
		accessDenied: false,
        id: elementId,
        elementType: elementType,
        masterdata: {},
        relations: {},
        descriptions: [],
        images: [],
        loaded: {
          userdata: false,
          masterdata: false,
          descriptionsImages: false,
          relations: false,
          sessiondata: false,
        },
        editors: {
          createNewElement: {
            open: false,
            data: {
                "name": "",
                "element_type": "",
                "type_grund": "",
                "type_spec": "",
                "type_url": "",
                "universe": universeId,
                "author": userId,
                "above": null,
                "campaigns": [ campaignId ],
				"secret": false,
				typeData: {
					unique: false,
					group_type: null,
					player_character: false,
					roleplayer:null,
					rpg_session_number: 0,

				},
            },
          },
          description: {
            open: false,
            dataStatus: "Not Changed",
			dataStatusClass: "alert alert--success",
            shareWithSelected: 0,
            data: {},

          },
          image: {
			  open: false,
              dataStatus: "Not Changed",
			  dataStatusClass: "alert alert--success",
              shareWithSelected: 0,
              data: {},
			  newImage: {
				  file: null,
				  preview: null,
				  author: this.user,
				  title: "",
				  links: []
			  },
          }
        },
      },
      session: {

      },
      profile: {
		news: {
			campaign: {
				newElements: {
					count:		0,
					allLoaded:	false,
					frame:		"campaign",
					area:		"element",
					type:		"new",
					name:		"newElements",
					loadPath: function(count){
						return '/api/list/element/?d=lav1_filter_in$campaigns$' + campaignId + '$$lav1_orderby$created$rev$$es1_author$$es1_links$$bus1_links$$es1_created_obj$$es1_user_permissions$$es1_user_permissions$$es1_campaigns_containing_session$$lav1_limit$10$' + count + '$$lav1_filter_multi$campaigns_containing__campaign$' + campaignId + '$campaigns_containing__approved$0$$/';
					}
				},
				newDescriptions: {
					count:		0,
					allLoaded:	false,
					frame:		"campaign",
					area:		"description",
					type:		"new",
					name:		"newDescriptions",
					loadPath: function(count){
						return '/api/list/description/?d=lav1_filter_in$campaigns$' + campaignId + '$$lav1_orderby$created$rev$$lav1_limit$10$' + count + '$$bus1_links$$ds1_author$$ds1_created_obj$$ds1_content_stripped$$ds1_user_permissions$$ds1_campaigns_containing_session$$lav1_filter_multi$campaigns_containing__campaign$' + campaignId + '$campaigns_containing__approved$0$$/';
					}
				},
				updatedElements: {
					count:		0,
					allLoaded:	false,
					frame:		"campaign",
					area:		"element",
					type:		"updated",
					name:		"updatedElements",
					loadPath: function(count){
						return '/api/list/element/?d=lav1_filter_in$campaigns$' + campaignId + '$$lav1_orderby$created$rev$$es1_author$$es1_links$$bus1_links$$es1_updated_obj$$es1_user_permissions$$es1_user_permissions$$es1_campaigns_containing_session$$lav1_limit$10$' + count + '$$lav1_filter_multi$campaigns_containing__campaign$' + campaignId + '$campaigns_containing__approved$1$$/';
					}
				},
				updatedDescriptions: {
					count:		0,
					allLoaded:	false,
					frame:		"campaign",
					area:		"description",
					type:		"updated",
					name:		"updatedDescriptions",
					loadPath: function(count){
						return '/api/list/description/?d=lav1_filter_in$campaigns$' + campaignId + '$$lav1_orderby$created$rev$$lav1_limit$10$' + count +'$$bus1_links$$ds1_author$$ds1_updated_obj$$ds1_content_stripped$$ds1_user_permissions$$ds1_campaigns_containing_session$$lav1_filter_multi$campaigns_containing__campaign$' + campaignId + '$campaigns_containing__approved$1$$/';
					}
				}
			},
			universe: {
				newElements: {
					count:		0,
					allLoaded:	false,
					frame:		"universe",
					area:		"element",
					type:		"new",
					name:		"newElements",
					loadPath: function(count){
						return '/api/list/element/?d=lav1_exclude_in$campaigns$' + campaignId + '$$lav1_orderby$created$rev$$es1_author$$es1_links$$bus1_links$$es1_created_obj$$es1_user_permissions$$es1_user_permissions$$es1_campaigns_containing_session$$lav1_limit$10$' + count + '$$/';
					}
				},
				newDescriptions: {
					count:		0,
					allLoaded:	false,
					frame:		"universe",
					area:		"description",
					type:		"new",
					name:		"newDescriptions",
					loadPath: function(count){
						return '/api/list/description/?d=lav1_exclude_in$campaigns$' + campaignId + '$$lav1_orderby$created$rev$$lav1_limit$10$' + count +  '$$bus1_links$$ds1_author$$ds1_created_obj$$ds1_content_stripped$$ds1_user_permissions$$ds1_campaigns_containing_session$$/';
					}
				},
				updatedElements: {
					count:		0,
					allLoaded:	false,
					frame:		"universe",
					area:		"element",
					type:		"updated",
					name:		"updatedElements",
					loadPath: function(count){
						return '/api/list/element/?d=lav1_exclude_in$campaigns$' + campaignId + '$$lav1_orderby$created$rev$$es1_author$$es1_links$$bus1_links$$es1_updated_obj$$es1_user_permissions$$es1_user_permissions$$es1_campaigns_containing_session$$lav1_limit$10$' + count + '$$/';
					}
				},
				updatedDescriptions: {
					count:		0,
					allLoaded:	false,
					frame:		"universe",
					area:		"description",
					type:		"updated",
					name:		"updatedDescriptions",
					loadPath: function(count){
						return '/api/list/description/?d=lav1_exclude_in$campaigns$' + campaignId + '$$lav1_orderby$created$rev$$lav1_limit$10$' + count +'$$bus1_links$$ds1_author$$ds1_updated_obj$$ds1_content_stripped$$ds1_user_permissions$$ds1_campaigns_containing_session$$/';
					}
				}
			}
		}
	  }


  },
  getters: {
	elementTypeTranslated: function(state) {
		//var typeOrig;
		var self = this;
		return function(typeOrig) {
			var typeTrans = "";
			if (typeOrig == "sted") {
			  typeTrans = "Place";
			} else if (typeOrig == "individ") {
			  typeTrans = "Individual";
			} else if (typeOrig == "gruppe") {
			  typeTrans = "Group";
			} else if (typeOrig == "kampagne") {
			  typeTrans = "Roleplaying Campaign";
			} else if (typeOrig == "rpg_session") {
			  typeTrans = "Roleplaying Session";
			} else if (typeOrig == "begivenhed") {
			  typeTrans = "Event";
			} else if (typeOrig == "player_character") {
			  typeTrans = "Player Character";
		  	} else if (typeOrig == "artefakt") {
			  typeTrans = "Item";
		  } else if (typeOrig == "unika") {
			  typeTrans = "Unique Item";
			} else {
			  typeTrans = typeOrig;
			}
			return typeTrans;
		}
	},
    getDescriptionsImages: function (state){
      var self = this;
      var mergedTemp = state.element.descriptions;
      var mergedTemp = state.element.descriptions.concat(state.element.images);
      mergedTemp.sort(function(a, b) {
          return parseFloat(b.relevance) - parseFloat(a.relevance);
      });

      //self.dataLoading = 0;
      return mergedTemp;
  	},
  	ingameNow: function (state) {
		var ingameNow = {
			age: state.session.ingame_now_age.id,
			year: state.session.ingame_now_year,
			month: state.session.ingame_now_month,
			day: state.session.ingame_now_date,
			hour: state.session.ingame_now_hours,
			min: state.session.ingame_now_minutes,
			sec: state.session.ingame_now_seconds,
		};

		return ingameNow;
	},
	ingame_date_str: function (state) {
		var self = this;
		return function(time = {day: 0,month: 0,year: 0,age: 0, hour:0, min: 0, sec: 0 }){
			var ai = state.session.universe.ages.findIndex(function(u) { return u.id == time.age});
			//console.log("STRING",time);
			//console.log("Index",ai);
	        dateString = "Not set";
	        if(time.day != 0 && time.month != 0 && time.year != 0 && time.age != 0 && ai != -1) {
	          var monthNames = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec'];
	          var mIndex = parseInt(time.month) - 1;
	          dateString = time.day + " " + monthNames[mIndex] + " " + time.year + " " + state.session.universe.ages[ai].initials;
	        }


        	return dateString;
		}
    },
	ingame_date_interval: function(state) {
		var self = this;
		return function(start = {day: 0,month: 0,year: 0,age: 0, hour: 0, min: 0, sec: 0}, end = {day: 0,month: 0,year: 0,age: 0, hour: 0, min: 0, sec: 0}) {

	      var startValid = false;
	      var endValid = false;
		  var loc = "now";

	      var interval = {day: 0,month: 0,year: 0,age: 0, min: 0, sec: 0};

		  if(start.day && start.month && end.year && start.age) {
			  if(start.age != 0) {
				 startValid = true;

			  }
	      }else {
	       // console.log("ingame_date_interval","start date not valid",start);
	      }

		  if(end.day && end.month && end.year && end.age) {
		  	if(end.age != 0) {
	        	endValid = true;
			}

	      }else {
	        //console.log("ingame_date_interval","end date not valid", end);
	      }

		  function timeLocation (diff) {
			  if(diff > 0) {
				 return "before";
			  }else {
				  return "after";
			  }
		  }


	      if(startValid === true && endValid === true) {

	        if(start.age == end.age) {

	          if(start.year == end.year) {

	            if(start.month == end.month) {


	            	if(start.day == end.day) {

					}else {
					  interval.day = end.day - start.day;
					  loc = timeLocation(interval.day);
					}


	            }else {
	              interval.month = end.month - start.month;
	              interval.day = 0;
				  loc = timeLocation(interval.month);
	            }


	          }else {
	            interval.year = end.year - start.year;
	            interval.month = 0;
	            interval.day = 0;
			  	loc = timeLocation(interval.year);
	          }

	        }else {
				var startAgeDetail = state.session.universe.ages.indexOf(function(u) { return u.id == start.age});
				var endAgeDetail = state.session.universe.ages.indexOf(function(u) { return u.id == end.age});
				interval.age = endAgeDetail.age_number - startAgeDetail.age_number;
				interval.year = 0;
				interval.month = 0;
				interval.day = 0;
				loc = timeLocation(interval.age);
	        }

	      }

	      return {
			  data: interval,
			  location: loc,
		  };
	  }
	},

	ingame_date_interval_string: function(state) {
		var self = this;
		return function(interval) {

			var valid = false;
			if(interval.day && interval.month && interval.year && interval.age) {
				valid = true;
			}
			if(interval.age != 0) {
				if(interval.age > 0){
					return "in a past age";
				}else {
					return "in a future age";
				}
			}else if (interval.year != 0){
				if(interval.year > 0){
					return interval.year + " years ago";
				}else {
					return "in " + (interval.year * -1) + " years";
				}
			}else if (interval.month != 0){
				if(interval.month > 0){
					return interval.month + " years ago";
				}else {
					return "in " + (interval.month * -1) + " years";
				}
			}else if (interval.day != 0){
				if(interval.day > 0){
					return interval.day + " years ago";
				}else {
					return "in " + (interval.day * -1) + " years";
				}
			}
			else {
				return "today";
			}


		}
	}


  },
	mutations: {
		getNewsArray: function(state,obj){
			axios.get(state.profile.news[obj.frame][obj.type].loadPath(0) + "&format=json").then(function(response) {
				Vue.set(state.profile.news[obj.frame][obj.type],'array', response.data);

				if(response.data.length < 10 && state.profile.news[obj.frame][obj.type].count >= 10){
					state.profile.news[obj.frame][obj.type].allLoaded = true;
				}
				state.profile.news[obj.frame][obj.type].count = response.data.length;
			}).catch(function(error) {
				console.log(error);
			});
		},
		getUser:function (state) {
			axios.get('/api/user/' + userId + '/?d=bus1_universes__bus1_player_characters&format=json').then(function(response) {
			state.user = response.data;
			state.element.loaded.userdata = true;
			}).catch(function(error) {
				console.log(error);
			});
		},
		getSession: function(state){
			if(campaignId){
				axios.get("/api/session/" + campaignId + "/?d=us1_ages__cs1_universe__us1_name&format=json").then(function(response) {
					state.session = response.data;
					state.element.loaded.sessiondata = true;
					state.initial.sessionLoaded = true
				}).catch(function(error) {
					console.log(error);
				});
			}
		},
      getElement: function(state){
            if(elementId && elementType) {
                axios.get('/api/' + elementType + '/' + elementId + "/?d=es1_names__es1_universe_name__es1_campaigns_containing_session__is1_kin__es1_campaigns__es1_author__es1_editors__es1_user_permissions__es1_birthdeath__pcs1_roleplayer__ces1_campaign__gs1_group_type__gts1_titles__cs1_gamemasters__cs1_sessions__rs1_in_campaign__rs1_gamemasters&format=json").then(function(response) {
                    state.element.masterdata = response.data;
                    state.element.loaded.masterdata = true;
                }).catch(function(error) {
                    console.log(error);
					if(error.response.status == 403) {
					  state.element.accessDenied = true;
					  state.error.message = error.response.data.detail;
		  //          console.log(error.response.data.detail);
					}
                });
            }else{
                return true;
            }
      },

      getElementRelations: function(state){
          //console.log("fasfasfsaf");
          axios.get('/api/' + elementType + '/relations/' + elementId + '/?d=es1_user_permissions__pcs1_roleplayer&format=json').then(function(response) {
              state.element.relations = response.data;
              state.element.loaded.relations = true;
          }).catch(function(error) {
              console.log(error);
          });
        },
      getSiblings: function(state) {
          var self = this;
          axios.get('/api/siblings/' + elementId + '/?format=json').then(function(response) {
			  console.log(response.data);
              Vue.set(state.element.relations,'siblings', response.data);
          }).catch(function(error) {
              console.log(error);
          });
      },
	  getDescription: function(state) {
		  var self = this;
          axios.get('/api/description/' + descriptionId + '/?d=ds1_author__ds1_content_stripped__ds1_campaigns__ds1_elements__ders1_element&format=json').then(function(response) {

             state.descriptionFull.data = response.data;
			 state.descriptionFull.dataLoaded = true;

          }).catch(function(error) {
              console.log(error);
          });
	  },
	  getImage: function(state) {
		  var self = this;
          axios.get('/api/image/' + imageId + '/?d=is1_author__is1_campaigns__is1_elements__iers1_element&format=json').then(function(response) {

             state.imageFull.data = response.data;
			 state.imageFull.dataLoaded = true;

          }).catch(function(error) {
              console.log(error);
          });
	  },
      ajaxDescriptionsImages: function(state) {
          var self = this;
          console.log("des ajax");
	          axios.get("/api/descriptionsimagesofelement/" + elementId + "/?d=ders1_description__iers1_image__ds1_shared_with__ds1_author__is1_shared_with__is1_author__is1_campaigns&format=json").then(function(response) {
              //console.log(response.data);
              state.element.descriptions = response.data;
              state.element.loaded.descriptionsImages  = true;
              //Vue.set(state.element,'descriptions', response.data);
          }).catch(function(error) {
              console.log(error);
          });


    },
    openDescriptionEditor: function(state,descriptionId){
		console.log("opening the descriptions editor")
        axios.get('/api/description/'+ descriptionId +'/?d=ds1_elements__ders1_element__ds1_campaigns__ds1_shared_with__ds1_author&format=json').then(function(response) {
              Vue.set(state.element.editors.description,'data', response.data);
              state.element.editors.description.data.elements_in_description = response.data.elements_in_description;
              state.element.editors.description.open = true;
        }).catch(function(error) {
            console.log(error);
        });
    },
    closeDescriptionEditor: function(state){
      state.element.editors.description.open = false;

      state.element.loaded.descriptionsImages = false;
      state.element.descriptions = {};
      this.commit("ajaxDescriptionsImages");

      state.element.editors.description.dataStatus = "Not Changed";
      state.element.editors.description.data = {};


    },
	openImageEditor: function(state,imageId){
		console.log("openining the Image editor");
        axios.get('/api/image/'+ imageId +'/?d=is1_elements__dirs1_element__is1_campaigns__is1_ingame_author__is1_elements_in_image_iers1_element__is1_shared_with__is1_author').then(function(response) {
              Vue.set(state.element.editors.image,'data', response.data);
              state.element.editors.image.open = true;
        }).catch(function(error) {
            console.log(error);
        });
    },
	closeImageEditor: function(state){
      state.element.editors.image.open = false;

      state.element.loaded.descriptionsImages = false;
      state.element.image = {};
      this.commit("ajaxDescriptionsImages");

      state.element.editors.image.dataStatus = "Not Changed";
      state.element.editors.image.data = {};


    },

    deleteElementRelation: function(state,id) {
        var self = this;
        axios.patch('/api/elementrelation/' + id + '/update/', {
            deleted: true
        }).then(function(response) {
            console.log(response)
            self.commit("getElementRelations");

        }).catch(function(error) {
            console.log(error);
        });
    },

  },
  actions: {
    consumerRequest: function(context, load ) {
        var self = this;
      	var cxt = context;
        axios({
            method: load.method,
            url: load.url,
            data: load.data
        }).then(function(response) {

			 if(load.mutation && load.mutation.name) {
				 cxt.commit(load.mutation.name,load.mutation.payload);
			 }


            // console.log(response.data);

        });
        //var descriptionId = this.$store.state.element.editors.description.data.id
        //axios.patch("/api/description/" + descriptionId + "/update/", changeData ).then(function(response) {
        //  self.$store.state.element.editors.description.dataStatus = "Updated";
        //})

    },
    updateRelation: function(context,data) {
      var self = this;
      var cxt = context;
      console.log(data);
      axios.patch('/api/' + data.view + '/' + data.id + '/update/', data.updateObject ).then(function(response) {
          console.log(response)
          cxt.commit("getElementRelations");

      }).catch(function(error) {
          console.log(error);
      });
    },
    createRelation: function(context,data) {
      var self = this;
      var cxt = context;
      console.debug(data);

      axios.post('/api/' + data.view + '/create/', data.createObject ).then(function(response) {
          console.log(response)
          cxt.commit("getElementRelations");

      }).catch(function(error) {
          console.log(error);
      });
    },
    getUser: function (context){
      context.commit('getUser');

    },
    getSession: function (context){
      context.commit('getSession');

    },
	getDescription: function(context){
	      context.commit('getDescription');
	},
	getImage: function(context){
	      context.commit('getImage');
	},
    getElement: function (context){
      context.commit('getElement');

    },
    getElementRelations: function (context){
        context.commit('getElementRelations');

    },
    getSiblings: function (context){
        context.commit('getSiblings');

    },
    getUser: function (context){
      context.commit('getUser');

    },
    init: function (context){
      context.commit('ajaxDescriptionsImages');

    },
    openDescriptionEditor: function (context,descriptionId) {
      context.commit('openDescriptionEditor',descriptionId);

    },
	openImageEditor: function(context,imageId) {
		console.log(3232);
		context.commit('openImageEditor',imageId);
		console.log(imageId);
	},
	getNews: function(context){
		/* campaign */
		context.commit("getNewsArray",{ frame: "campaign", type: "newElements"});
		context.commit("getNewsArray",{ frame: "campaign", type: "newDescriptions"});
		context.commit("getNewsArray",{ frame: "campaign", type: "updatedElements"});
		context.commit("getNewsArray",{ frame: "campaign", type: "updatedDescriptions"});

		// /* universe */
		// context.commit("getNewsArray",{ frame: "universe", type: "newElements"});
		// context.commit("getNewsArray",{ frame: "universe", type: "newDescriptions"});
		// context.commit("getNewsArray",{ frame: "universe", type: "updatedElements"});
		// context.commit("getNewsArray",{ frame: "universe", type: "updatedDescriptions"});
	},

  }



});
