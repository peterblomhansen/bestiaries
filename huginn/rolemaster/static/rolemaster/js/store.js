Vue.config.devtools = true;

var store = new Vuex.Store({

	state: {
		loaded: {
			stats: false,
			skills: false,
			skillrowDetails: false,
		},
		messages: {
			skills: "",
		},
		individual: {
			id: individualId,
			level: 0,
			author: authorId,
			universe: universeId,
			kin: kinId,
		},
		initial: {
			characterId: characterId,
			level: {
				current: characterLevel,
				viewed: characterLevel,
			},
		},
		rolemasterTables: {
			statBonusDistribution: [],

		},
		character: {},
		skills: [],
		skillareas: [],
		skillrows: [],
		professions: [],

	},
	getters: {
		getNearestValidatedLevel: function (state) {
			return function (level) {
				var self = this;
				var lvlInt = parseInt(level);

				function valid(lvlInt)  {
					var vl = false;
					function statsPresent(l,type){
						var found = state.character.stats.filter(function(s) {
							return s.level == l && s.stat_type == type;
						})
						return found.length;

					}
					var temp = statsPresent(lvlInt,"temp");
					var pot = statsPresent(lvlInt,"pot");
					if(temp > 0 && pot > 0) {
						vl = lvlInt
					}

					return vl;
				}

				if(valid(lvlInt)) {
					return valid(lvlInt);
				}else {
					return this.getNearestValidatedLevel(lvlInt - 1);
				}
			}

		},
		getStatTotalBonus: function (state) {
			return function (statName,lvl = this.getNearestValidatedLevel(state.initial.level.viewed)){
				var normalBonus = 0;
				var raceBonus = this.getStatRaceBonus(statName);
				var specialBonus = this.getStatSpecialBonus(statName);

				// Check if the is Temporary Stats for the given level
				var statRow = state.character.stats.filter(function(st){ return st.level == lvl && st.stat_type == "temp" });

				temp = statRow[0][statName];
				if(temp) {

					// Calculate Normal Bonus
					var stat = state.rolemasterTables.statBonusDistribution.find(function(s){ return s.stat == temp });
					if(stat) {
						normalBonus = stat.bonus;
					}




					return normalBonus + raceBonus + specialBonus;


				}
				else {
					return null;
				}
			}
		},
		getStatRaceBonus: function (state) {
			return function (statName) {
				if(state.character.race) {
					return state.character.race[statName];
				}else {
					return 0;
				}
			}
		},
		getStatSpecialBonus: function (state) {
			return function (statName) {
				if(state.character.stat_special_bonuses.length > 0) {
					var statNameTranslater = {
						"luck": "LU",
						"agility":"AG",
						"constitution": "CO",
						"reasoning": "RE",
						"memory": "ME",
						"self_discipline": "SD",
						"strength": "ST",
						"quickness": "QU",
						"presence": "PR",
						"empathy": "EM",
						"intuition": "IN",
					};

					var statMatch = state.character.stat_special_bonuses.find(function(s){ return s.stat == statNameTranslater[statName]});
					if(statMatch) {
						return statMatch.bonus;
					}else {
						return 0;
					}
				}else {
					return 0;
				}
			}
		},
		ranksValidated: function (state) {
			return function (rankObj,skill){


				if (rankObj == null || rankObj.num_ranks == 0) {
					return "ranks--zero";
				}
				else if (rankObj.num_ranks <= this.ranksAllowed(skill) || rankObj.development_type == "gm") {
					return "ranks--valid";
				}
				else {
					return "ranks--invalid";
				}

			}
		},
		ranksAllowed: function (){
			return function (skill) {
				var allowed = 0;
				var skillCostObj = skill.cost;

				if(!skillCostObj) {
					return false;
				}
				var c1 = parseInt(skillCostObj.cost_1);
				var c2 = parseInt(skillCostObj.cost_2);
				var c3 = parseInt(skillCostObj.cost_3);

				if(c1 > 0 && c2 == 0 && c3 == 0) {
					//console.log("Allowed 1");
					allowed = 1;
				}
				else if(c1 > 0 && c2 > 0 && c3 == 0) {
					//console.log("Allowed 2");
					allowed = 2;
				}
				else if(c1 > 0 && c2 > 0 && c3 > 0) {
					//console.log("Allowed 3");
					allowed = 3;
				}
				else if(c1 > 0 && c2 > 0 && c3 == -1) {
					//console.log("Allowed infinit");
					allowed = 1000;
				}
				else if(c1 > 0 && c2 == -1 ) {
					//console.log("Allowed infinit");
					allowed = 1000;
				}

				return allowed;
			}
		},
		getSkillNativeRanks: function(state) {
			return function(skill) {

				var totalRanks = 0;
				var gmRanks = 0;
				var hobbyRanks = 0;

				if(skill.gm_ranks) {
					gmRanks = parseInt(skill.gm_ranks.num_ranks);
				}
				if(skill.hobby_ranks) {
					hobbyRanks = parseInt(skill.hobby_ranks.num_ranks);
				}

				_.each(skill.dev_ranks,function(r){
					totalRanks = totalRanks + parseInt(r.num_ranks);
				});

				totalRanks = totalRanks + gmRanks + hobbyRanks;

				return totalRanks;

			}

		},
		getSkillSimilarRanks: function (state) {
			return function (skill){
				if(skill.similarity) {
			//	if(this.skill.similarity && this.skill.area == null) {

					var self = this;
					var simRankArray = [];
					var max = 0;
					_.each(skill.similarity,function(s){
					 	var skillRowIndex = state.skillrows.findIndex(function(r){ return s.origin == r.id && r.area == s.area});
					 	//simRankArray.push(skillRowIndex);
						if(skillRowIndex > -1) {
							var similarranks = self.getSkillNativeRanks(state.skillrows[skillRowIndex]) * s.factor;
							var simName = state.skillrows[skillRowIndex].name;
							if(similarranks > 0.5) {
								simRankArray.push({ "name": simName, "ranks": similarranks, "ratio": s.ratio});
							}
						}
					});
					if(simRankArray.length > 0 ) {
						//console.log(this.skill.id,simRankArray);
						max = _.maxBy(simRankArray, function(o) { return o.ranks; });
					}
					if(max.ranks) {

						return max;
					}
					else {
						return { "name": null, "ranks": 0, "ratio": 0};
					}
				}
				else {

					return 0;

				}


			}
		},
		getRankBonus: function (state){
			return function(numRanks){
				var bonus = -25;
				if(numRanks > 0 && numRanks <= 10) {
					bonus = numRanks * 5;
				}
				else if (numRanks > 10 && numRanks <= 20) {
					bonus = 50 + (numRanks - 10) * 3;
				}
				else if (numRanks > 20 && numRanks <= 30) {
					bonus = 80 + (numRanks - 20) * 2;
				}
				else if (numRanks > 30 && numRanks <= 40) {
					bonus = 100 + (numRanks - 30) * 1;
				}
				else if (numRanks > 40 && numRanks <= 50) {
					bonus = 110 + (numRanks - 40) * 0.5;
				}
				else if (numRanks > 50) {
					bonus = 115 + (numRanks - 50) * 0.25;

				}
				return bonus;

			}

		},
		skillCost: function (state){
			return function(skill) {
				var self = this;
				var skillCost = ""
				// var skillCostObj = this.professionSkillCosts.find(function(c){
	            //
				// 	return c.skill == self.skill.id;
				// })
				var skillCostObj = skill.cost;
				if(skillCostObj) {
					skillCost = skillCostObj;

					//var statsString = "";
					//statsString = this.skill.stat_1 + "/" + this.skill.stat_2 + "/" +this.skill.stat_3;
					if(skillCostObj.cost_1 == 0) {
						skillCost = "";
					}
					else if (skillCostObj.cost_2 == 0){
						skillCost = skillCostObj.cost_1;
					}
					else if (skillCostObj.cost_2 == -1) {
						skillCost = skillCostObj.cost_1 + "/*";
					}
					else if (skillCostObj.cost_3 == 0 || skillCostObj.cost_3 == null) {
						skillCost = skillCostObj.cost_1 + "/" + skillCostObj.cost_2;
					}
					else {
						skillCost = skillCostObj.cost_1 + "/" + skillCostObj.cost_2 + "/" + skillCostObj.cost_3;

					}
				}
				else {
					skill.cost = {
						id: null,
						cost_1:	0,
						cost_2:	0,
						cost_3:	0,
					}
				}

				return skillCost;
				}
		},
		skillDevPrice: function(state) {
			return function (skill,num_ranks){
				var cost = skill.cost;
				var price = 0;

				if (num_ranks == 1) {
					price = cost.cost_1
				}
				else if (num_ranks == 2){
					if (cost.cost_2 == -1) {
						price = cost.cost_1 * num_ranks
					}
					else {
						price = cost.cost_1 + cost.cost_2
					}
				}
				else if (num_ranks == 3) {
					if (cost.cost_2 == -1) {
						price = cost.cost_1 * num_ranks;
					}
					else if (cost.cost_3 == -1) {
						price = cost.cost_1 + cost.cost_2 + cost.cost_3
					}
				}

				else if (num_ranks > 3) {
					if (cost.cost_2 == -1) {
						price = cost.cost_1 * num_ranks
					}
					else if (cost.cost_3 == -1) {
						price = cost.cost_1 + cost.cost_2 * (num_ranks - 1)
					}
				}
				return price;
			}
		},
		// isGameMaster: function (state){
		// 	return state.character.campaigns.indexOf(campaignId) != -1;
        //
		// }
	},
	mutations: {
		// params = { skill,  }
		updateSkillCost: function(state,params){

			var self = this;
			var skill = params.skill;
			var costObj = skill.cost;
			costObj.profession = this.$store.state.character.profession.id;
			costObj.skill = skill.id;
			costObj.cost_2 = costObj.cost_2.toString().replace("*","-1");
			costObj.cost_3 = costObj.cost_3.toString().replace("*","-1");

			console.log(costObj);

			if(costObj.id == null) {
				axios.post("/rm/api/skillcost/create/",costObj).then(function(response){

					if(skill.area_support == false) {
						skill.cost = {
							id: response.data.id,
							cost_1: response.data.cost_1,
							cost_2: response.data.cost_2,
							cost_3: response.data.cost_3,
						};

					}
					else {
						var areas = self.$store.state.skillrows.filter(function(r) {
							return parseInt(r.id) == parseInt(skill.id);
						});
						console.log(areas);
						_.each(areas,function(a){
							//delete a.cost;
							a.cost = {
								id: response.data.id,
								cost_1: response.data.cost_1,
								cost_2: response.data.cost_2,
								cost_3: response.data.cost_3,
							};

						});
					}
					skill.change_cost = false;

				}).catch(function(error) {
					console.log(error);
				});
			}
			else if (costObj.id > 0) {
				axios.patch("/rm/api/skillcost/" + costObj.id + "/update/",costObj).then(function(response){

					self.skill.change_cost = false;

				}).catch(function(error) {
						console.log(error);
				});

			}
		},
		updateSkillRanks: function (state,params) {
			var skill = params.skill;
			var rankObj = params.rankObj;
			var lvlIndex = params.lvlIndex;
			var self = this;
			if(rankObj.num_ranks != rankObj.num_ranks_init) {

				if(rankObj.num_ranks <= this.getters.ranksAllowed(skill) || rankObj.development_type == "gm") {
					console.log("Valid");
	 				if(rankObj.id == null) {
						axios.post("/rm/api/developedskill/create/",rankObj).then(function(response){
							console.log(response.data);
							rankObj.id = response.data.id;
							rankObj.num_ranks = response.data.num_ranks;
							rankObj.num_ranks_init = response.data.num_ranks;
							rankObj.dev_points_used = self.getters.skillDevPrice(skill,response.data.num_ranks);
						}).catch(function(error) {
				            console.log(error);
				        });

					}
					else if (rankObj.id > 0) {
						axios.patch("/rm/api/developedskill/" + rankObj.id + "/update/",rankObj).then(function(response){
							rankObj.id = response.data.id;
							rankObj.num_ranks = response.data.num_ranks;
							rankObj.num_ranks_init = response.data.num_ranks;
							rankObj.dev_points_used = self.getters.skillDevPrice(skill,response.data.num_ranks);
						}).catch(function(error) {
				            console.log(error);
				        });

					}

				}
				else {
					console.log("Invalid");
					rankObj.num_ranks = rankObj.num_ranks_init;
				}
			}
			else {
				console.log("Not Changed")
			}

		},

	},
	actions: {
		getStats: function (context) {
			axios.get("/rm/api/character/" + this.state.initial.characterId + "/?format=json").then(function(response){
				context.state.character = response.data;
				context.state.loaded.stats = true;

			});
		},
		getSkills: function (context) {
			console.log("Loading skills");
			axios.get("/rm/api/skillrows/" + this.state.initial.characterId + "/?format=json").then(function(response){
				context.state.skillrows = response.data;
				context.state.loaded.skills = true;

			});

		}
	}


})
;
