var familyTreeDescendants = Vue.component('family-tree-descendants', {
    template: "\
	<div style='margin: 0 0 0 10px;'>\
		<div v-for=\"d in descendantsShown\">\
			{{d.present_name.name}}\
			<button type='button' @click='addDescendants(d)'>fold ud</button>\
			<div v-if=\"typeof d.descendants != 'undefined'\">\
				der er børn!\
			</div>\
		</div>\
	</div>\
    ",
    
    props: {
        nameObject: Object,
        descendants: Array,
    },
    
    data: function(){
        return {
			test: true
        };
    },
    
    methods: { 
		addDescendants: function(object){
			console.debug(object);
			Vue.set(object,"descendants", [
				{navn: 'harry', id: 4},
				{navn: 'irma', id: 5}
			]);
		}
	},
	
    mounted: function(){
		
	},
	
    computed: {
		descendantsShown: function(){
			
			var descendantsShown = [];
			
			var lastParents = [];
			
			_.each(this.descendants,function(entry){
				console.debug(entry);
				
				descendantsShown.push(entry.below_full);
			});
			
			return descendantsShown;
		}
    }


});
