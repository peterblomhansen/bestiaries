var newsUniverse = Vue.component('news-universe', {
    template: '\
    <div>\
		<div v-if="initial.sessionLoaded">\
			<h1 class="rubrik">Other news from {{session.universe.name}}</h1>\
		</div>\
		<br>\
		<news-block :news-object="news.universe.newElements" heading="New pages"></news-block>\
		<news-block :news-object="news.universe.newDescriptions" heading="New descriptions"></news-block>\
		<news-block :news-object="news.universe.updatedElements" heading="Updated pages"></news-block>\
		<news-block :news-object="news.universe.updatedDescriptions" heading="Updated descriptions"></news-block>\
	</div>\
    ',
    computed: {
		initial: function(){
			return this.$store.state.initial;
		},
		news: function(){
			return this.$store.state.profile.news;
		},
		session: function(){
			return this.$store.state.session;
		}
	}
});
