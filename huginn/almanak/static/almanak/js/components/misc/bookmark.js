var bookmark = Vue.component('bookmark', {
	template: "\
		<div @click='openCreateNewElementForm()' class='bookmark--passive'>\
			<p style='color:white;text-align:center;margin-bottom: .2rem;'>New</p>\
		</div>\
	",
	methods: {
		openCreateNewElementForm: function (){
			this.$store.commit("activateEditor","newElement");
		},
		openMenu: function () {
			this.$store.commit("activateEditor","menu");			
		},
	}
});