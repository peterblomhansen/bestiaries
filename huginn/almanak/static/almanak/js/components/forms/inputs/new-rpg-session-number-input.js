var newRpgSessionNumberInput = Vue.component('new-rpg-session-number-input', {
	template: "\
	<div class='grid grid--create-input'>\
		<label for='new-gender'>Session Number</label>\
		<input type=\"number\" v-model=\"newRpgSessionNumber\" />\
	</div>",
	computed: {
		newRpgSessionNumber: {
			get: function (){
				return this.$store.state.create.rpgSessionNumber;
			},
			set: function (value){
				this.$store.commit("createDataMutate", { key: "rpgSessionNumber", value: value })
			}
		}
	}
});
 
