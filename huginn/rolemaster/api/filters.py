
from django_filters import rest_framework as filters
import rolemaster.models as models
from django.db.models import Q

class DevelopedSkillFilter(filters.FilterSet):
	#experiences__event__id__in = NumberInFilter(field_name='experiences__event__id', lookup_expr='in')

	class Meta:
		model = models.DevelopedSkill
		fields = { 
			"character":['exact'],
		}