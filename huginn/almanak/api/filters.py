from django_filters import rest_framework as filters
import almanak.models as models
from django.db.models import Q


class NumberInFilter(filters.BaseInFilter, filters.NumberFilter):
    pass


class ElementFilter(filters.FilterSet):
	campaigns__id__in = NumberInFilter(field_name='campaigns__id', lookup_expr='in')

	class Meta:
		model = models.Element
		fields = { 
			"campaigns__id__in":['in'],
		}

class NameFilter(filters.FilterSet):
	element__type_grund__not = filters.CharFilter(field_name='element__type_grund', method='filter_element__type_grund__not')
	element__campaigns__id__in = NumberInFilter(field_name='element__campaigns__id', lookup_expr='in')
	user_can_edit_element = filters.CharFilter(method='filter_user_can_edit_element')


	class Meta:
		model = models.Name
		fields = { #["element__id","element__element_type","element__type_grund","element__type_spec","element__type_grund__not"] #{
			"element__campaigns__id__in":['in'],
			"element__type_grund__not":['exact'],
			"element__id":['exact'],
			"element__element_type":['exact'],
			"element__type_grund":['exact',],
			"element__type_spec":['exact',],
		}

	def user(request):
		return request.user

	def filter_element__type_grund__not(self, queryset, name, value):
		return queryset.exclude(element__type_grund=value)

	def filter_user_can_edit_element(self, queryset, name, value):
		return queryset.filter(
			Q(element__author=self.request.user)|
			Q(element__campaigns__gamemasters__in=[self.request.user.id])|
			Q(element__id__in=self.request.user.player_characters.get_queryset())
		)

class UserFilter(filters.FilterSet):
	universes__universe = filters.NumberFilter(field_name="universes__universe", lookup_expr='exact')

	class Meta:
		model = models.BestiariesUser
		fields = ('universes__universe',)

class ElementRelationFilter(filters.FilterSet):
	#above_title = filters.BooleanFilter(field_name="above_title", lookup_expr='isnull')

	class Meta:
		model = models.ElementRelation
		fields = {
		"above__id": ['exact'],
		"below__id": ['exact'],
		"above__deleted": ['exact'],
		"below__deleted": ['exact'],
		"relation_type": ['exact'],
		"above_title": ['isnull'],
		}

class IndividualFilter(filters.FilterSet):
	experiences__event__id__in = NumberInFilter(field_name='experiences__event__id', lookup_expr='in')

	class Meta:
		model = models.Individual
		fields = { 
			"experiences__event__id__in":['in'],
		}


class RolePlayingSessionFilter(filters.FilterSet):
	#campaigns__id__in = NumberInFilter(field_name='campaigns__id', lookup_expr='in')

	class Meta:
		model = models.RpgSession
		fields = { 
			"in_campaign__id":['exact'],
		}
 
class DescriptionFilter(filters.FilterSet):
	pass
	# class Meta:
	# 	model = models.Description


class DescriptionElementRelationFilter(filters.FilterSet):
	description__url__not = filters.CharFilter(field_name='description__url', method='filter_description__url__not')

	class Meta:
		model = models.DescriptionElementRelation
		fields =  {
		"element__id": ['exact'],
		"description__id": ['exact'],
		"description__description_type": ['exact'],
		"description__url__not": ['exact'],
		}


	def filter_description__url__not(self, queryset, name, value):
		return queryset.exclude(description__url__isnull=value)


class EntityEventRelationFilter(filters.FilterSet):
#	entity__id = filters.CharFilter(field_name='entity__id', method='filter_entity__id')

	class Meta:
		model = models.EntityEventRelation
		fields = {
		"event__id": ['exact'],
		"entity__id": ['exact'],
		"event__deleted": ['exact'],
		"entity__deleted": ['exact'],
		}
		
#	def filter_entity__id(self, queryset, name, value):
#		return queryset.filter(entity__id=value,event__names__secret=False)
class EventPlaceRelationFilter(filters.FilterSet):

	class Meta:
		model = models.EventPlaceRelation
		fields = {
		"event__id": ['exact'],
		"place__id": ['exact'],
		"event__deleted": ['exact'],
		"place__deleted": ['exact'],
		}


class ElementItemRelationFilter(filters.FilterSet):
	distinct = filters.CharFilter(method='filter_distinct')


	class Meta:
		model = models.ElementItemRelation
		fields = {
			
		"owner__id": ['exact'],
		"owned__id": ['exact'],
		"owner__deleted": ['exact'],
		"owned__deleted": ['exact'],
		"distinct": ['exact'],
		}

	def filter_distinct(self, queryset, name, value):
		if value == "latest":
			return queryset.order_by(
							"-owned__id",
							"-owner__id",
							"-ingame_age__age_number",
							"-ingame_year",
							"-ingame_month",
							"-ingame_date",
							"-ingame_hour",
							"-ingame_minute",
							"-ingame_second",
							"-created"
							).distinct("owned__id","owner__id")
		else:
			return queryset


class ElementUniqueItemRelationFilter(filters.FilterSet):
	
	class Meta:
		model = models.ElementUniqueItemRelation
		fields = {
		"owner__id": ['exact'],
		"owned__id": ['exact'],
		"owner__deleted": ['exact'],
		"owned__deleted": ['exact'],
		}
