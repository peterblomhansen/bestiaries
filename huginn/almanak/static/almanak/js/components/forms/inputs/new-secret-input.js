var newSecretInput = Vue.component('new-secret-input', {
	template: "\
	<div class='grid grid--create-input' >\
		<label>Secret</label>\
		<input type='checkbox' name='new-secret' v-model='nameSecret' />\
	</div>",
	computed: {
		nameSecret: {
			get: function (){
				return this.$store.state.create.secret;
			},
			set: function (value){
				//console.log(value);
				this.$store.commit("createDataMutate", { key: "secret", value: value })
			}
		}
	}
});
 
