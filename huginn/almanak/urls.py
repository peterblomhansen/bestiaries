from django.conf.urls import url
from django.contrib import admin
from django.urls import path, re_path
from django.views.generic import RedirectView


from .views import(
    TemplateTest,
    ElementDetailView,
    OtherUserDetailView,
	DescriptionDetailView,
	ImageDetailView,
    SideRedirectView,
    MarkdownView,
    TableOfContentView
)

urlpatterns = [
    url(r'^$', TemplateTest.as_view(),name="base" ),
    re_path(r'^side/(?P<pk>[0-9]+)/$', RedirectView.as_view(url='/almanak/element/%(pk)s'), name="side_redirect_view"),    
    url(r'^element/(?P<pk>[0-9]+)/$', ElementDetailView.as_view(),name="base" ),
    url(r'^user/(?P<pk>[0-9]+)/$', OtherUserDetailView.as_view(),name="base" ),
    re_path(r'^description/show/(?P<pk>[0-9]+)/$', RedirectView.as_view(url='/almanak/description/%(pk)s'), name="description_show_redirect_view" ),
    re_path(r'^image/show/(?P<pk>[0-9]+)/$', RedirectView.as_view(url='/almanak/image/%(pk)s'), name="image_show_redirect_view" ),
    url(r'^description/(?P<pk>[0-9]+)/$', DescriptionDetailView.as_view(),name="description_show_view" ),
    url(r'^image/(?P<pk>[0-9]+)/$', ImageDetailView.as_view(),name="image_show_view" ),
    path('markdown/', MarkdownView.as_view(), name="mdtest"),
    path('about/', MarkdownView.as_view(), {'file': 'about'}, name="mdabout"),
    path('table-of-content/',TableOfContentView.as_view(),name="tableofcontent")
]

# Der findes endnu en url: Accounts/profile, som er en del af djangos eget tutelut. Se overordnet (projekt)url-fil: bestiaries/bestiaries/urls.py.
