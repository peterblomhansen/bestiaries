var goIngame = Vue.component('go-ingame', {
    template: "\
	<transition name=\"fade\" >\
		<overlay v-if=\"show === true\" >\
			<div style=\"text-align:right;\">\
				<p class=\"rubrik\"><a href=\"/logout\">Log Out</a></p>\
			</div>\
			<h3 class=\"ink\">You are currently not ingame</h3>\
			<h2 class=\"rubrik\">Go Ingame</h2>\
			<div v-for=\"u in user.universe_memberships\" >\
				<h3 class=\"go-ingame__universe-header ink\" >{{ u.universe.name }} Bestiary</h3>\
				<div class=\"go-ingame__universe\" >\
					<button class=\"button--text-large\" v-for=\"pc in user.player_characters\" v-if=\"pc.universe == u.universe.id && pc.campaigns.length > 0\" style=\"cursor:pointer;\" @click=\"goIngame(pc.universe,pc.campaigns_full[0].campaign_full.id ,pc.id)\">\
						{{ pc.present_name.name }} in <span v-if=\"pc.campaigns.length > 0\">{{ pcCampaignName(pc) }}</span>\
					</button>\
					<button class=\"button--text-large\" v-for=\"cm in user.gamemaster_for\" v-if=\"cm.universe == u.universe.id && cm.type_grund == 'kampagne'\" style=\"cursor:pointer;\" @click=\"goIngame(cm.universe,cm.id,0)\">\
						Gamemaster for {{ cm.present_name.name }}\
					</button>\
				</div>\
			</div>\
		</overlay>\
		<overlay v-else-if=\"!show\" style=\"height:100%;\"  >\
			<div style=\"text-align:center;\" >\
				<h1 class=\"ink\">Looking for Bestiaries</h1>\
				<loading-spinner></loading-spinner>\
				<img style=\"\height:600px;opacity:0.4\" :src=\"static + 'images/hand-out-of-book.svg'\" />\
			</div>\
		</overlay>\
    </transition>",
    props: {
        player: Object
    },
    data: function() {
        return {
			show: false,
			showInfo: 1,
			cardActive: false,
            user: {}
        }
    },
    mounted: function(){

		this.getUserPointOfViews(this.userId);

    },
    methods: {
        getUserPointOfViews: function (id){
			var self = this; 
			axios.get('/api/new/user/' + id + '/?d=universe_memberships__\
												bus1um_universe__\
												bus1um_ums1u_name__\
												player_characters__\
												bus1pc_present_name__\
												bus1pc_campaigns__\
												bus1pc_es1c_campaign__\
												bus1pc_es1c_ces1c_present_name__\
												gamemaster_for__\
												bus1gmf_present_name__\
												bus1gmf_campaigns__\
												bus1gmf_ecs1c_present_name__\
												')
            .then(function (response) {
				self.user = response.data;
                self.show = true;
            })
            .catch(function (error) {
              console.log(error);
            });

        },   		
		pc_campaign: function (role) {
			if(role.campaigns.length > 0) {
				return role.campaigns[0].campaign.present_name;
			}
			else { return ""; }
		},
		pcsInUniverse: function (universe_id) {
			return this.user.playercharacters.filter(function(e) { return e.universe == universe_id });
		},
		gamemasteredInUniverse: function () {
			return this.user.gamemaster_for.filter(function(e) { return e.universe == id && e.type_spec == "kampagne" });
		},
		rolesInUniverse: function (id){
			var pcs = [];
			var gms = [];

			if(this.user.player_characters) {
				pcs = this.user.player_characters.filter(function(e) { return e.universe == id });
			}
			if(this.user.gamemaster_for) {
				gms = this.user.gamemaster_for.filter(function(e) { return e.universe == id && e.type_spec == "kampagne" });
			}

			return pcs.concat(gms);

		},
		pcCampaignName: function(pc) {

			if(pc.campaigns_full.length > 0) {
				return pc.campaigns_full[0].campaign_full.present_name.name;
			}
			else { return "no campaign" }

		},
        goIngame: function(id_universe, id_campaign, id_role) {

          axios.post('/api/go_ingame/', { universe: id_universe, campaign: id_campaign, role: id_role } )

            .then(function (response) {
              console.log(response.data)
              if(response.data = "is_ingame") {

                window.location.reload();
              }

            })
            .catch(function (error) {
              console.log(error);
            });


        },

    },
    computed: {
		showGoIngamePrompt: function (){
			return this.$store.state.generic.userBestiariesLoaded;
		},
		static: function(){
				return this.$store.state.generic.	staticUrl;
		},
		userId: function (){
			return this.$store.state.generic.session.base.userId;
		}




    }
});
