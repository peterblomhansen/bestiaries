from django.shortcuts import render
from django.views.generic import TemplateView, DetailView, View
from almanak.models import Individual, PlayerCharacter
from rolemaster.models import RolemasterCharacter, Skill, SkillArea, DevelopedSkill
from rolemaster.logic import charactersheet
from rolemaster.logic.charactersheet import SkillRows
from django.db.models import Q
from almanak.api.permissions import CanEditElement

from io import BytesIO
from reportlab.pdfgen import canvas
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4, inch
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Flowable, Frame
from django.http import HttpResponse
from django.utils.text import slugify

from reportlab.platypus import BaseDocTemplate, SimpleDocTemplate, Paragraph, Spacer, PageBreak, PageTemplate
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.rl_config import defaultPageSize
from reportlab.lib.units import inch
import random, math
cm = inch / 2.54
# Create your views here.

# class CharacterSheetView(DetailView):
# 	template_name = "rolemaster/charactersheet.html"
# 	#queryset = Individual.objects.all()
#
# 	def get_queryset(self,)


class CharacterSheetView(View):

	#form_class = MyForm
	permissions = (CanEditElement,)
	template_name = 'rolemaster/charactersheet.html'

	def get(self, request, pk, *args, **kwargs):
		#form = self.form_class(initial=self.initial)

		individual = Individual.objects.get(id=pk)
		#level = lvl
		pc = PlayerCharacter.objects.filter(id=pk)
		if pc:
			individual = PlayerCharacter.objects.get(id=pk)
		rm_character = RolemasterCharacter.objects.filter(id=pk)
		if rm_character:
			rm_character = RolemasterCharacter.objects.get(id=pk)
		return render(request, self.template_name, {"individual": individual, "rm_character": rm_character })




# from reportlab.platypus import BaseDocTemplate, Frame, Paragraph, PageBreak, PageTemplate
# from reportlab.lib.styles import getSampleStyleSheet

class FloatColumn(View):




	def get(self, request, pk, lvl, *args, **kwargs):
		response = HttpResponse(content_type='application/pdf')
		response['Content-Disposition'] = 'filename="somefilename.pdf"'

		words = "lorem ipsum dolor sit amet consetetur sadipscing elitr sed diam nonumy eirmod tempor invidunt ut labore et".split()

		styles=getSampleStyleSheet()
		Elements=[]

		doc = BaseDocTemplate(response,showBoundary=1)
		buffer = BytesIO()
		#Two Columns
		frame1 = Frame(doc.leftMargin, doc.bottomMargin, doc.width/2-6, doc.height, id='col1')
		frame2 = Frame(doc.leftMargin+doc.width/2+6, doc.bottomMargin, doc.width/2-6, doc.height, id='col2')

		Elements.append(Paragraph(" ".join([random.choice(words) for i in range(1000)]),styles['Normal']))
		doc.addPageTemplates([PageTemplate(id='TwoCol',frames=[frame1,frame2]), ])


		#start the construction of the pdf
		doc.build(Elements)



		pdf = buffer.getvalue()
		buffer.close()
		response.write(pdf)
		return response





class PdfTestView(View):
	selected_skills = []
	rm_character = None
	skillrows  = []
	data = {}
	character = {}
	level = 0
	



	def get(self, request, pk, lvl, *args, **kwargs):
		exists =  RolemasterCharacter.objects.filter(pk=pk)
		if not exists:
			return render(None,"rolemaster/no_character_found.html")
		data = self.get_data(request,pk)
		self.level = lvl

		#self.rm_character = RolemasterCharacter.objects.select_related("profession").prefetch_related("profession__skill_costs","profession__skill_costs__skill","weapon_category_priority").get(id=pk)
		sr = SkillRows(self.data,self.character)
		self.skillrows = sr.generate_skillrows()
		filename = slugify(self.character.names.get_queryset()[0].name + "-level-" + lvl + "-rm2-charactersheet")

		response = HttpResponse(content_type='application/pdf')
		response['Content-Disposition'] = 'filename="' + filename + '.pdf"'

		buffer = BytesIO()

		Title = "Hello world"
		pageinfo = "platypus example"
		#doc_empty = SimpleDocTemplate(response)
		doc_empty = SimpleDocTemplate(response,
		                        rightMargin=1,
		                        leftMargin=1,
		                        topMargin=1,
		                        bottomMargin=1,
		                        pagesize=A4)
		doc = self.go(doc_empty)


		pdf = buffer.getvalue()
		buffer.close()
		response.write(pdf)
		return response


	def get_data(self,request, pk):
		u = request.session.get("universe_current")

		self.character = RolemasterCharacter.objects.select_related("profession").prefetch_related("profession__skill_costs","profession__skill_costs__skill","weapon_category_priority","selected_skills","selected_skills__skill","skill_misc_bonuses","skill_comments","character_experience").get(id=pk)

		#print(self.character.profession)
		#data = {}
		#data.skills =  []
		self.data['skills'] = Skill.objects.prefetch_related("supported_area_types","skill_origin_for","skill_target_for").all()
		self.data['areas'] = SkillArea.objects.select_related("universe","skill").filter(Q(universe__id=u)|Q(universe=None),deleted=False)
		self.data['ranks'] = DevelopedSkill.objects.select_related("character","skill","area").prefetch_related("character","skill","area").filter(character__id=pk)
		self.data['costs'] = []
		if self.character.profession:
			self.data['costs'] = self.character.profession.skill_costs


		return self.data

	def myFirstPage(self,canvas, doc):

		canvas.setTitle(self.character.names.get_queryset()[0].name + " Character Sheet")

		styles = getSampleStyleSheet()
		style = styles["Normal"]
		PAGE_HEIGHT=defaultPageSize[1]
		PAGE_WIDTH=defaultPageSize[0]

		stat_data = self.character.stats.get_queryset()
		stat_special_bonuses = self.character.stat_special_bonuses.get_queryset()
		lvl = self.level
		race = self.character.race
		selected_skillrows = self.write_selected_skills()
		selected_skillrows.sort(key=lambda x: x.name, reverse=False)

		stats = charactersheet.StatsTable(race,stat_data,stat_special_bonuses)
		print("LEVEL FOR STATS",lvl)
		data = stats.data_table(lvl)


		Title = [Paragraph("Character Sheet for " + str(self.character) + " - Level " + self.level,style)]
		stats_table = [
			Table(data,
				  style=[
				  	('GRID',(0,0),(-1,-1),1,colors.gray),
					('SPAN',(0,0),(6,0)),
					('SPAN',(0,1),(2,1)),
					('SPAN',(3,1),(6,1)),
				  ]
		  	)
		]
		pageinfo = "pageinfo"
		canvas.saveState()
		header_frame = Frame(0.25*cm, 28.5*cm, 20.5*cm, 1*cm, showBoundary=1)
		stats_frame = Frame(0.25*cm, 18.5*cm, 12*cm, 10*cm, showBoundary=1)
		hits_frame = Frame(12.25*cm, 18.5*cm, 8.5*cm, 10*cm, showBoundary=1)
		selected_skills_frame_1 = Frame(0.25*cm, 0.5*cm, 10.25*cm, 18*cm, showBoundary=1)
		selected_skills_frame_2 = Frame(10.5*cm, 0.5*cm, 10.25*cm, 18*cm, showBoundary=1)


		ss_style = [
			('GRID',(0,0),(-1,-1),1,colors.gray),
		]
		ss_col_widths = [7.25*cm,1.5*cm,1.5*cm]
		ss_col_1 = [Table([["Important Skill","Ranks","Bonus"]],colWidths=ss_col_widths,style=ss_style),]
		ss_col_2 = [Table([["Important Skill","Ranks","Bonus"]],colWidths=ss_col_widths,style=ss_style),]
#			["Skill","Ranks","Bonus"]
#		]

		stat_data = self.character.stats.get_queryset()
		stat_special_bonuses = self.character.stat_special_bonuses.get_queryset()
		lvl = self.level
		race = self.character.race
		level_bonuses = self.character.profession.level_bonuses_of_profession
		misc_bonuses = self.character.skill_misc_bonuses #print(stat_data)
		skill_comments = self.character.skill_comments

		stats = charactersheet.StatsTable(race,stat_data,stat_special_bonuses)
		statstable = stats.data_table(lvl)

		count = 0
		for row in selected_skillrows:
			sb = self.write_skillrow(row,doc,statstable,level_bonuses,misc_bonuses,skill_comments)
			area = ""
			if sb.skillrow.area != None:
				area = sb.skillrow.area_name

			ss = [[str(sb.skillrow.name) + " " + str(area),sb.calculate_total_ranks(),sb.total_bonus()]]
			#ss_content.append(s)
			if count < 27:
				ss_col_1.append(Table(ss,colWidths=ss_col_widths,style=ss_style))
			else:
				ss_col_2.append(Table(ss,colWidths=ss_col_widths,style=ss_style))

			print(count)
			count += 1


		hits_rolled = self.character.body_development_of_character
		rr = charactersheet.Resistance(stats,lvl)
		san = charactersheet.Sanity(stats,lvl)
		#print(hits_obj)
		level_bonus = self.character.profession.level_bonuses_of_profession.get_queryset().filter(category="Body_development")
		#print(level_bonus)
		hits = charactersheet.ConcussionHits(hits_rolled,stats,lvl,level_bonus)
		ini = statstable[6][6] + statstable[10][6]
		print(statstable)

		race_title = "None"
		if race != None:
			race_title = race.names.get_queryset()[0].name

		hits_content = [
						Paragraph("Name: " + self.character.names.get_queryset()[0].name,style),
						Paragraph("Race: " + race_title,style),
						Paragraph("Profession: " + self.character.profession.name ,style),
						Paragraph(" --- ",style),
						Paragraph("HITS",style),
						Paragraph("Base Hits: " + str(hits.calculate_base_hits()) ,style),
						Paragraph("Total Hits: " + str(hits.calculate_total_hits()) ,style),
						Paragraph(" --- ",style),
						Paragraph("INITIATIVE",style),
						Paragraph(str(ini),style),
						Paragraph(" --- ",style),
						Paragraph("RESISTANCE BASE: ",style),
						Paragraph("Body Base: " + str(rr.body_base()) ,style),
						Paragraph("Will Base: " + str(rr.will_base()) ,style),
						Paragraph("Spirit Base: " + str(rr.spirit_base()), style),
						Paragraph(" --- ",style),
						Paragraph("SANITY ",style),
						Paragraph("Temp: " + str(san.get_temp_sanity_base()) ,style),
						Paragraph("Pot: " + str(san.get_pot_sanity_base()) ,style),
						Paragraph("Pot: " + str(san.get_sanity_total_loss()) ,style),




						]
		hits_frame.addFromList(hits_content,canvas)
		header_frame.addFromList(Title,canvas)
		stats_frame.addFromList(stats_table,canvas)
		selected_skills_frame_1.addFromList(ss_col_1,canvas)
		selected_skills_frame_2.addFromList(ss_col_2,canvas)



		canvas.restoreState()


	def myLaterPages(self,canvas, doc):
		styles = getSampleStyleSheet()
		style = styles["Normal"]

		canvas.saveState()

		p = ParagraphStyle('test')
		p.textColor = 'gray'

		P = Paragraph("Page " + str(doc.page) + " - Charactersheet for " + str(self.rm_character) + " Level " + str(self.level),
		          p)

		w, h = P.wrap(doc.width, doc.bottomMargin)
		P.drawOn(canvas, 0.5*cm, 0.2*cm)

		canvas.restoreState()


	def write_selected_skills(self):
		selected_skills = []


		for sel in self.character.selected_skills.get_queryset().filter(selected=True):
			#print(sel)
			s_id = sel.skill.id
			area = None
			if sel.area != None:
				area = sel.area.id

			for rs in self.skillrows:
				rs_id = rs.id
				rs_area = None
				if rs.area != None:
					rs_area = rs.area
				if s_id == rs_id and rs_area == area:
					selected_skills.append(rs)

		return selected_skills


	def write_skillrow(self,skillrow,doc,stats,level_bonuses,misc_bonuses,skill_comments):
		lvl = self.level
		styles = getSampleStyleSheet()
		style = styles["Normal"]
		return charactersheet.SkillBonus(skillrow,lvl,self.skillrows,stats,level_bonuses,misc_bonuses,skill_comments)



	def write_skill_bonus_table(self,skillrow,skill_bonus):

		total_ranks = skill_bonus.calculate_total_ranks()
		rank_bonus = skill_bonus.calculate_rank_bonus()
		stat_bonus = skill_bonus.calculate_stat_bonus()
		level_bonus = skill_bonus.calculate_level_bonus()
		misc_bonuses = skill_bonus.calculate_misc_bonuses()
		total_bonus = rank_bonus + stat_bonus + level_bonus + misc_bonuses
		stats_string = skill_bonus.write_stats()
		data = [[skillrow.name + skill_bonus.source_name,skillrow.area_name,stats_string,total_ranks,rank_bonus,stat_bonus,level_bonus,misc_bonuses,total_bonus]]
		num_cols = len(data[0])

		return Table(data,colWidths=[6*cm,6.5*cm,2.5*cm,1*cm,1*cm,1*cm,1*cm,1*cm,1*cm],style=[
				  	('GRID',(0,0),(-1,-1),1,colors.gray),
			])


	def write_selected_skill(self,skillrow,skill_bonus):

		total_ranks = skill_bonus.calculate_total_ranks()
		rank_bonus = skill_bonus.calculate_rank_bonus()
		stat_bonus = skill_bonus.calculate_stat_bonus()
		level_bonus = skill_bonus.calculate_level_bonus()
		misc_bonuses = skill_bonus.calculate_misc_bonuses()
		total_bonus = rank_bonus + stat_bonus + level_bonus + misc_bonuses
		data = [[skillrow.name + skill_bonus.source_name,total_ranks,total_bonus]]

		return Table(data,colWidths=[6*cm,6.5*cm,2.5*cm,1*cm,1*cm,1*cm,1*cm,1*cm,1*cm],style=[
				  	('GRID',(0,0),(-1,-1),1,colors.gray),
			])


	def write_cat_row(self,category):


		return Table([[category]],colWidths=[21*cm,],style=[
					('GRID',(0,0),(-1,-1),1,colors.gray),
			])

	def write_skills_in_cat(self,cat,doc):

		data =  [[cat.upper(),"","","","",""],["Ranks","Bonus","Skill","Ranks","Bonus","Skill"],]
		cat_table_col_widths = [1.5*cm,1.5*cm,7*cm,1.5*cm,1.5*cm,7*cm]
		cat_table_col_style = [
				#('GRID',(0,0),(-1,-1),1,colors.gray),
				('SPAN',(0,0),(5,0)),
                ('ALIGN',(0,0),(5,0),'CENTER'),
                ('ALIGN',(1,0),(1,-1),'RIGHT'),
                ('ALIGN',(4,0),(4,-1),'RIGHT'),
                ('BACKGROUND',(0,0),(5,0),colors.black),
                ('TEXTCOLOR',(0,0),(5,0),colors.white),
                ('LINEABOVE',(0,0),(-1,2),1,colors.gray),
                ('LINEBEFORE',(3,0),(3,-1),1,colors.gray),
 				('BOX',(0,0),(-1,-1),2,colors.black),
			]

		def find_skill_of_cat(item):
			return item.category == cat

		skills = list(filter(find_skill_of_cat,self.skillrows))

		stat_data = self.character.stats.get_queryset()
		stat_special_bonuses = self.character.stat_special_bonuses.get_queryset()
		lvl = self.level
		race = self.character.race
		level_bonuses = self.character.profession.level_bonuses_of_profession
		misc_bonuses = self.character.skill_misc_bonuses #print(stat_data)
		skill_comments = self.character.skill_comments

		stats = charactersheet.StatsTable(race,stat_data,stat_special_bonuses)
		statstable = stats.data_table(lvl)



		print(len(skills))


		skills_show = []
		no_rank_skills = []
		for i,s in enumerate(skills):


			sb = self.write_skillrow(s,doc,statstable,level_bonuses,misc_bonuses,skill_comments)
			#if s.area_support == False or (s.area_support == True and sb.calculate_total_ranks() > 0) or (s.area_support == True and s.area == None):
			if s.area_support == False or (s.area_support == True and s.area == None) or (s.area_support == True and sb.calculate_total_ranks() > 0):
				if s.area_support == True and s.area == None:
					sb.skillrow.name = sb.skillrow.name + " - Other"
				elif s.area_support == True and s.area != None:
					sb.skillrow.name = sb.skillrow.name + " - " + s.area_name
				skills_show.append(sb)

			# elif s.area_support == True and s.area == None:
			# 	sb.skillrow.name = sb.skillrow.name + " - Other"
			# 	skills_show.append(sb)
			# elif s.area_support == True and sb.calculate_total_ranks() == 0 and s.id not in no_rank_skills:
			# 	# sb.skillrow.name = sb.skillrow.name + " - Other"
			# 	# skills_show.append(sb)
			# 	no_rank_skills.append(s.id)
		for s in skills_show:
			print(s.get_skill_comments())

		num_skills_show = len(skills_show)
		half = math.ceil(num_skills_show / 2)

		dr = []

		for i in range(half):


			print(i)
			area = " "
			if skills_show[i].skillrow.area != None:
				area += skills_show[i].skillrow.area_name

			dr.append(skills_show[i].calculate_total_ranks())
			dr.append(skills_show[i].total_bonus())
			dr.append(str(skills_show[i].skillrow.name) + str(area))

			if half + i < num_skills_show:


				area = " "
				if skills_show[half + i].skillrow.area != None:
					area += skills_show[half + i].skillrow.area_name

				dr.append(skills_show[half + i].calculate_total_ranks())
				dr.append(skills_show[half + i].total_bonus())
				dr.append(str(skills_show[half + i].skillrow.name) + str(area))



			data.append(dr)
			dr = []

		cat_table = Table(data,colWidths=cat_table_col_widths,style=cat_table_col_style)

		return cat_table

	def go(self,doc):
		styles = getSampleStyleSheet()
		Story = [Spacer(1,2*inch)]
		style = styles["Normal"]

		Story.append(PageBreak())
		#print(self.skillrows)

		stat_data = self.character.stats.get_queryset()
		stat_special_bonuses = self.character.stat_special_bonuses.get_queryset()
		lvl = self.level
		race = self.character.race
		level_bonuses = self.character.profession.level_bonuses_of_profession
		misc_bonuses = self.character.skill_misc_bonuses #print(stat_data)

		stats = charactersheet.StatsTable(race,stat_data,stat_special_bonuses)
		statstable = stats.data_table(lvl)

		cats = []
		for sr in self.skillrows:
			if sr.category not in cats:
				cats.append(sr.category)

		for c in cats:
			if c == "Combat" or c == "General" or c == "Medical":

				Story.append(PageBreak())
				Story.append(self.write_skills_in_cat(c,doc))
			else:
				Story.append(self.write_skills_in_cat(c,doc))

		return doc.build(Story, onFirstPage=self.myFirstPage,  onLaterPages=self.myLaterPages)



class CharacterSheetPdfView(View):



	def get(self, request, pk, lvl, *args, **kwargs):
		#form = self.form_class(initial=self.initial)
		individual = Individual.objects.get(id=pk)
		rm_character = RolemasterCharacter.objects.get(id=pk)

		self.character = RolemasterCharacter.objects.select_related("profession").prefetch_related("profession__skill_costs","profession__skill_costs__skill","weapon_category_priority").get(id=pk)

		response = HttpResponse(content_type='application/pdf')
		response['Content-Disposition'] = 'filename="somefilename.pdf"'

		buffer = BytesIO()

		# Create the PDF object, using the BytesIO object as its "file."
		p = canvas.Canvas(buffer)
		doc = SimpleDocTemplate(response, rightMargin=0, leftMargin=0.5 * cm, topMargin=1.0 * cm, bottomMargin=0, )
		# Draw things on the PDF. Here's where the PDF generation happens.
		# See the ReportLab documentation for the full list of functionality.
		#p.drawString(20, 10, str(rm_character))
		elements = []

		data1 = [
			["foo","bar"]
		]
		t1=Table(data1)
		data = [['00', '01', '02', '03', '04'],
		       ['10', '11', '12', '13', '14'],
		       ['20', '21', '22', '23', '24'],
		       ['30', '31', '32', '33', '34']]
		t=Table(data,colWidths=270, rowHeights=79)
		t.setStyle(TableStyle([('ALIGN',(1,1),(-2,-2),'RIGHT'),
		                       ('TEXTCOLOR',(1,1),(-2,-2),colors.red),
		                       ('VALIGN',(0,0),(0,-1),'TOP'),
		                       ('TEXTCOLOR',(0,0),(0,-1),colors.blue),
		                       ('ALIGN',(0,-1),(-1,-1),'CENTER'),
		                       ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
		                       ('TEXTCOLOR',(0,-1),(-1,-1),colors.green),
		                       ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
		                       ('BOX', (0,0), (-1,-1), 0.25, colors.black),
		                       ]))

		elements.append(t)
		elements.append(t1)
		doc.build(elements)

		# Close the PDF object cleanly.
		p.showPage()
		p.save()

		# Get the value of the BytesIO buffer and write it to the response.
		pdf = buffer.getvalue()
		buffer.close()
		response.write(pdf)
		return response
		#return render(request, self.template_name, {"individual": individual, "rm_character": rm_character })







class StatsFlowable(Flowable):
	pass
