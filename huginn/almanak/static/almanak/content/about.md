<h1 class="ink" >What is Bestiaries.org</h1>

Bestiaries.org is a way for gamemasters and roleplayers of building up the world of their roleplaying game piece by piece.

## Relational
Using bestiaries.org you can make pages in what is going to be a great Tome og Bestiary, that holds the knowledge you have of your world. 

Every page represents a person/individual, a group of inviduals, a place/location or an event. These pages are related to each other both within their own type (fx. groups inside groups) or between type (individual inside groups). This makes it easy to keep track the different elements of the world you are.	

[back to Login](/login)