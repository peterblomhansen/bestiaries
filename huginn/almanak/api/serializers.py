from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model
import time
import django.db.models as models
from django.db.models import Q
#from .permissions import PermissionCheck, NameChangePermission, AlmanakPermission
from almanak.core.namehelper import DescriptionContent
from django.utils.html import strip_tags
from almanak.core.time import DateHelper, TimePoint, Interval
from .params import ParamsManager
from . import permissions

#from almanak.core.objectifier import SessionHelper




from rest_framework.serializers import (
	BooleanField,
	CharField,
	DateTimeField,
	HyperlinkedRelatedField,
	ImageField,
	IntegerField,
	ModelSerializer,
	PrimaryKeyRelatedField,
	RelatedField,
	SerializerMethodField,
	StringRelatedField,
	ValidationError,
)

import almanak.models as almanak


class ParamsModelSerializer(ModelSerializer):
    def __init__(self, *args, **kwargs):
        params = kwargs.pop('params',None)
        if not params:
            params = ()

        self.params = params
        super(ParamsModelSerializer, self).__init__(*args, **kwargs)


class NameSerializer(ParamsModelSerializer):	# ns1
	date_string = SerializerMethodField()
	ajax_status = SerializerMethodField()
	show_options = SerializerMethodField()
	is_primary = SerializerMethodField()
	delete_question = SerializerMethodField()
	element_type = SerializerMethodField()
	type_grund = SerializerMethodField()
	type_spec = SerializerMethodField()
	edit_permissions = SerializerMethodField()
	shared_with_full = SerializerMethodField()
	element_id = SerializerMethodField()

	class Meta:
		model = almanak.Name
		exclude = ["element"]

	def get_element_id(self,obj):
		return obj.element.id

	def get_element_type(self,obj):
		return obj.element.element_type

	def get_type_grund(self,obj):
		return obj.element.type_grund

	def get_type_spec(self,obj):
		return obj.element.get_type_spec_obj()

	def get_date_string(self, obj):
		return ""

	def get_ajax_status(self, obj):
		return "not changed"

	def get_show_options(self, obj):
		return False

	def get_is_primary(self, obj):
		if obj.name_type == "prim":
			return True
		else:
			return False

	def get_delete_question(self, obj):
		return False

	def get_shared_with_full(self,obj):
		if "ns1_shared_with" in self.params:
			serializer = ElementSerializer(obj.shared_with, context={'request': self.context['request']},many=True,read_only=True,params=self.params)
			return serializer.data

	def get_edit_permissions(self,obj):
		if "ns1_user_permissions" in self.params:
			user = self.context['request'].user
			session = self.context['request'].session
			return permissions.AlmanakPermission.user_can_edit_name(self.context['request'],obj)





class AgeSerializer(ParamsModelSerializer):

    class Meta:
        model = almanak.Age
        fields = "__all__"



class UniverseSerializer(ParamsModelSerializer):
	name = SerializerMethodField()
	ages = SerializerMethodField() #AgeSerializer(many=True)
	campaigns = SerializerMethodField()
	users = SerializerMethodField()
	kins = SerializerMethodField()
	group_types = SerializerMethodField()
	owners = SerializerMethodField()


	class Meta:
		model = almanak.Universe
		fields = "__all__"

	def get_kins(self,obj):
		if "us1_kins" in self.params:
			uni_id = self.context['request'].session.get("universe_current")
			kins = Kin.objects.filter(universe=uni_id)
			#kins = uni_cur.elements_in_universe.filter(type_spec="race").filter(deleted=False)
			serializer = ElementSerializer(kins, many=True, context={'request': self.context['request']})
			return serializer.data
		else:
			return False

	def get_name(self,obj):
		if "name" in self.params:
			return obj.name

	def get_users(self,obj):
		if "users" in self.params:
			serializer = BestiariesUserSerializer(obj.users, context={'request': self.context['request']},many=True,read_only=True,params=self.params)
			return serializer.data
		else:
			return None

	def get_owners(self,obj):
		if "owners" in self.params:
			uni_id = self.context['request'].session.get("universe_current")
			query = obj.users.filter(universes__universe_owner=True)
			serializer = BestiariesUserSerializer(query, context={'request': self.context['request']},many=True,read_only=True,params=self.params)
			return serializer.data

	def get_ages(self,obj):
		if "ages" in self.params:
			serializer = AgeSerializer(obj.ages, context={'request': self.context['request']},many=True,read_only=True,params=self.params)
			return serializer.data
		else:
			return None

	def get_campaigns(self,obj):
		if "us1_campaigns" in self.params:
			cmpgns = obj.elements_in_universe.filter(type_spec="kampagne").filter(deleted=False)
			serializer = ElementSerializer(cmpgns, many=True, context={'request': self.context['request']},params=self.params)
			return serializer.data
		else:
			return None

	def get_group_types(self,obj):
	    if "us1_group_types" in self.params:
	        #cmpgns = obj.elements_in_universe.filter(type_spec="kampagne").filter(deleted=False)
	        serializer = GroupTypeSerializer(obj.group_types, many=True, context={'request': self.context['request']},params=self.params)
	        return serializer.data
	    else:
	        return None


class UniverseMembershipSerializer(ParamsModelSerializer):
	universe = SerializerMethodField()

	class Meta:
		model = almanak.UniverseMembership
		fields = "__all__"

	def get_universe(self,obj):
		if "universe" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"ums1u")
			serializer = UniverseSerializer(obj.universe, context={'request': self.context['request']},params=subparams)
			return serializer.data


class UserLoginSerializer(ModelSerializer):
    authenticated = BooleanField(read_only=True)
    username = CharField(required=True,allow_blank=True)

    class Meta:
        model = almanak.BestiariesUser
        fields = [
            'username',
            'password',
            'authenticated',

        ]
        extra_kwargs = { "password":
                            { "write_only": True }
                            }
    def validate(self,data):
        user_obj = None
        username = data.get("username", None)
        password = data.get("password")
        if not username:
            raise ValidationError("A username is required to login");

        user = BestiariesUser.objects.filter(
                Q(username=username)
            ).distinct()

        if user.exists() and user.count() == 1:
            user_obj = user.first()
        else:
            raise ValidationError("The username was not found")

        if user_obj:
            if not user_obj.check_password(password):
                raise ValidationError("Incorrect password")
        data["jens"] = password
        data["authenticated"] = True

        return data



class BestiariesUserSerializer(ParamsModelSerializer):                            # id: BUS1
	gamemaster_for      	= SerializerMethodField()
	player_characters   	= SerializerMethodField()
	universe_memberships 	= SerializerMethodField()
	unshare_question    	= SerializerMethodField()
	delete_question	    	= SerializerMethodField()
	links 					= SerializerMethodField()
	authored_elements		= SerializerMethodField()
	authored_descriptions 	= SerializerMethodField()
	authored_images 		= SerializerMethodField()

	class Meta:
		model = almanak.BestiariesUser
		exclude = ("password",)

	def get_unshare_question(self,obj):
		return False

	def get_delete_question(self,obj):
		return False

	def get_links(self,obj):
		if "bus1_links" in self.params:
			return obj.get_links(self.context['request'])

	def get_gamemaster_for(self,obj):
		if "gamemaster_for" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"bus1gmf")
			serializer = ElementSerializer(obj.gamemaster_for, context={'request': self.context['request']}, many=True, read_only=True, params=subparams)
			return serializer.data
		else:
			return None

	def get_player_characters(self,obj):
		if "player_characters" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"bus1pc")
			serializer = ElementSerializer(obj.player_characters, context={'request': self.context['request']}, read_only=True, many=True, params=subparams)
			return serializer.data
		else:
			return None

	def get_universe_memberships(self,obj):
		if "universe_memberships" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"bus1um")
			serializer = UniverseMembershipSerializer(obj.universes, context={'request': self.context['request']},many=True, params=subparams)
			return serializer.data
		else:
			return None


	def get_authored_elements(self,obj):
		if "authored_elements" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"bus1ae")
			serializer = ElementSerializer(obj.authored_elements, context={'request': self.context['request']},many=True, params=self.params)
			return serializer.data
		else:
			return None


	def get_authored_descriptions(self,obj):
		if "authored_descriptions" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"bus1ad")
			serializer = DescriptionSerializer(obj.authored_descriptions, context={'request': self.context['request']},many=True, params=subparams)
			return serializer.data
		else:
			return None

	def get_authored_images(self,obj):
		if "bus1_authored_images" in self.params:
			serializer = ImageSerializer(obj.authored_images, context={'request': self.context['request']},many=True, params=self.params)
			return serializer.data
		else:
			return None



class ElementsInDescriptionSerializer(ModelSerializer):

    class Meta:
        model = almanak.DescriptionElementRelation
        fields = "__all__"


class ImageSerializer(ParamsModelSerializer):
	permissions = SerializerMethodField()
	elements_in_image = SerializerMethodField()
	ingame_author = SerializerMethodField()
	shared_with = SerializerMethodField()
	author = SerializerMethodField()
	campaigns =  SerializerMethodField()

	class Meta:
		model = almanak.Image
		fields = "__all__"

	def get_author(self,obj):
		if "is1_author" in self.params:
			serializer = BestiariesUserSerializer(obj.author, context={'request': self.context['request']},params=self.params)
			return serializer.data
		else:
			return False

	def get_ingame_author(self,obj):
		if "is1_ingame_author" in self.params:
			serializer = ElementSerializer(obj.ingame_author, context={'request': self.context['request']},params=self.params)
			return serializer.data
		else:
			return False

	def get_shared_with(self,obj):
		if "is1_shared_with" in self.params:
			serializer = ElementSerializer(obj.shared_with, context={'request': self.context['request']},params=self.params,many=True)
			return serializer.data
		else:
			return False


	def get_elements_in_image(self,obj):
		if "is1_elements" in self.params:
			serializer = ImageElementRelationSerializer(obj.elements_in_image, context={'request': self.context['request']},params=self.params,many=True)
			return serializer.data
		else:
			return False


	def get_campaigns(self,obj):
		if "is1_campaigns" in self.params:
			serializer = CampaignSerializer(obj.campaigns, context={'request': self.context['request']},params=self.params,many=True)
			return serializer.data
		else:
			return False


	def get_permissions(self,obj):
		user = self.context['request'].user
		session = self.context['request'].session

		return PermissionCheck.description_permissions(user,session,obj)


class ImageElementRelationSerializer(ParamsModelSerializer):
	image = SerializerMethodField()
	element = SerializerMethodField()

	class Meta:
	    model = almanak.ImageElementRelation
	    fields = "__all__"

	def get_image(self,obj):
		if "iers1_image" in self.params:
			serializer = ImageSerializer(obj.image, context={'request': self.context['request']},params=self.params)
			return serializer.data
		else:
			return False

	def get_element(self,obj):
		if "iers1_element" in self.params:
			serializer = ElementSerializer(obj.element, context={'request': self.context['request']},params=self.params)
			return serializer.data
		else:
			return False


class CampaignDescriptionSerializer(ParamsModelSerializer):

    class Meta:
        model = almanak.CampaignDescription
        fields = "__all__"


class CampaignElementSerializer(ParamsModelSerializer):                  # ID: CES1

	campaign_full = SerializerMethodField()

	class Meta:
		model = almanak.CampaignElement
		fields = "__all__"


	def get_campaign_full(self,obj):
		if "campaign" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"ces1c")
			serializer = CampaignSerializer(obj.campaign, context={'request': self.context['request']},params=subparams)
			return serializer.data
		else:
			return obj.campaign.id


class RelationTitleSerializer(ParamsModelSerializer):

	ajax_status = SerializerMethodField()
	delete_question = SerializerMethodField()

	class Meta:
		model = almanak.RelationTitle
		fields = "__all__"

	def get_ajax_status(self,obj):
		return { "message": "Not Changed", "class": "alert alert--success", "show_edit": False }

	def get_delete_question(self,obj):
		return False


class ElementSerializer(ParamsModelSerializer):                 # id: ES1
	author_full			= SerializerMethodField()
	birth_interval 		= SerializerMethodField()
	birth_name 			= SerializerMethodField()
	birth_number 		= SerializerMethodField()
	birth_string 		= SerializerMethodField()
	campaigns_full		= SerializerMethodField()
	created_obj			= SerializerMethodField()
	death_interval 		= SerializerMethodField()
	death_name 			= SerializerMethodField()
	death_string 		= SerializerMethodField()
	delete_question 	= SerializerMethodField()
	editors_full 		= SerializerMethodField()
	links 				= SerializerMethodField()
	names 				= SerializerMethodField()
	names_edit 			= SerializerMethodField()
	present_name 		= SerializerMethodField()
	secret_obj			= SerializerMethodField()
	shared_with_full	= SerializerMethodField()
	universe_full 		= SerializerMethodField()
	updated_obj			= SerializerMethodField()
	user_permissions 	= SerializerMethodField()
	user_can_edit		= SerializerMethodField()
	type_spec_obj		= SerializerMethodField()
	created_unix		= SerializerMethodField()
	names 				= SerializerMethodField() #NamePostSerializer(many=True)

	class Meta:
		model = almanak.Element
		fields =   "__all__"


	# Param Fields
	def get_type_spec_obj(self,obj):
		return obj.get_type_spec_obj()

	def get_created_obj(self,obj):
		if "es1_created_obj" in self.params:
			created_obj = {}
			split1 = str(obj.created).split()
			created_obj["dato"] = split1[0]
			split2 = split1[1].split(".")
			created_obj["tid"] = split2[0]
			created_obj["dato_tid"] = split1[0] + " " + split2[0]
			date_time = DateTimeField(obj.created)
			return created_obj

	def get_links(self,obj):
		if "es1_links" in self.params:
			return obj.get_links(self.context['request'])

	def get_author_full(self,obj):
		if "es1_author" in self.params:
			serializer = BestiariesUserSerializer(obj.author, context={'request': self.context['request']}, params = self.params)
			return serializer.data
		else:
			return obj.author.id

	def get_birth_interval(self,obj):
		if "es1_birthdeath" in self.params:
			session = self.context['request'].session #campaign_current_id = self.context['request'].session.get("campaign_current")
			sh = SessionHelper
			now = sh.ingame_now_timepoint(session)
			birth_timepoint = TimePoint(obj.birth_age,obj.birth_year,obj.birth_month,obj.birth_date,obj.birth_hours,obj.birth_minutes,obj.birth_seconds)
			return str(Interval(birth_timepoint,now))
		else:
			return None

	def get_birth_name(self,obj):
		return "(Birth)"

	def get_birth_number(self,obj):
		return obj.birth_number

	def get_birth_string(self,obj):
		d = DateHelper()
		return d.date_string(obj.birth_age,obj.birth_year,obj.birth_month,obj.birth_date)

	def get_campaigns_full(self,obj): # es1c
		if "campaigns" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"es1c")
			serializer = CampaignElementSerializer(obj.campaigns_containing, context={'request': self.context['request']}, many=True, read_only=True,params=subparams)
			return serializer.data
		else:
			return None

	def get_death_interval(self,obj):
		if "es1_birthdeath" in self.params:
			session = self.context['request'].session #campaign_current_id = self.context['request'].session.get("campaign_current")
			sh = SessionHelper
			now = sh.ingame_now_timepoint(session)
			birth_timepoint = TimePoint(obj.death_age,obj.death_year,obj.death_month,obj.death_date,obj.death_hours,obj.death_minutes,obj.death_seconds)
			return str(Interval(birth_timepoint,now))
		else:
			return None

	def get_death_name(self,obj):
		return "(Death)"

	def get_death_string(self,obj):
		d = DateHelper()
		return d.date_string(obj.death_age,obj.death_year,obj.death_month,obj.death_date)

	def get_delete_question(self,obj):
		return False

	def get_editors_full(self,obj):
		if "es1_editors" in self.params:
			serializer = BestiariesUserSerializer(obj.editors, context={'request': self.context['request']}, many=True, read_only=True)
			return serializer.data
		else:
			return None

	def get_shared_with_full(self,obj):
		if "shared_with" in self.params:
			serializer = BestiariesUserSerializer(obj.shared_with, context={'request': self.context['request']}, many=True, read_only=True)
			return serializer.data
		else:
			return None

	def get_name_chosen(self,obj):
		campaign_current_id = self.context['request'].session.get("campaign_current")
		names = ChooseName(obj,campaign_current_id)
		return names.get_present_name()

	def get_names(self,obj):
		if "es1_names" in self.params:
			serializer = NameSerializer(obj.names, context={'request': self.context['request']}, many=True, read_only=True)
			return serializer.data
		else:
			return None

	def get_names_edit(self,obj):
		return False


	def get_present_name(self,obj):
		if "present_name" in self.params:
			return obj.get_present_name(self.context['request'])
		else:
			return None

	def get_secret_obj(self,obj):
		if "es1_secret_obj" in self.params:
			text = "public"
			if obj.secret:
				text = "secret"
			return {"text": text}

	def get_universe_full(self,obj):
		if "universe_name" in self.params:
			serializer = UniverseSerializer(obj.universe, context={'request': self.context['request']}, params = ("us1_name") )
			return serializer.data
		elif "universe_full" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"es1uf")
			serializer = UniverseSerializer(obj.universe, context={'request': self.context['request']}, params = subparams)
			return serializer.data
		else:
			return obj.universe.id

	def get_updated_obj(self,obj):
		if "es1_updated_obj" in self.params:
			updated_obj = {}
			split1 = str(obj.updated).split()
			updated_obj["dato"] = split1[0]
			split2 = split1[1].split(".")
			updated_obj["tid"] = split2[0]
			updated_obj["dato_tid"] = split1[0] + " " + split2[0]
			date_time = DateTimeField(obj.updated)
			return updated_obj

	def get_user_permissions(self,obj):
		if "es1_user_permissions" in self.params:
			user = self.context['request'].user
			session = self.context['request'].session
			return PermissionCheck.element_permissions(user,session,obj)
		else:
			return None

	def get_user_can_edit(self,obj):
		if "es1_can_user_edit" in self.params:
			return permissions.AlmanakPermission.user_can_edit_element(self.context['request'],obj)
		else:
			return None

	def get_created_unix(self,obj):
		return str(obj.created)





class ElementByNameSerializer(ModelSerializer):
    element = ElementSerializer(params=("es1_names"))
    class Meta:
        model = almanak.Name
        fields = "__all__"


class EventPlaceRelationSerializer(ParamsModelSerializer):
	event_full = SerializerMethodField()
	place_full = SerializerMethodField()

	class Meta:
		model = almanak.EventPlaceRelation
		fields = "__all__"

	def get_event_full(self,obj):
		if "event" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"eprs1e")			
			serializer = EventSerializer(obj.event, context={'request': self.context['request']}, params = subparams)
			return serializer.data
		else:
			return obj.event.id

	def get_place_full(self,obj):
		if "place" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"eprs1p")			
			serializer = PlaceSerializer(obj.place, context={'request': self.context['request']}, params = subparams)
			return serializer.data
		else:
			return obj.place.id



class EntityEventRelationSerializer(ParamsModelSerializer):
	entity_full = SerializerMethodField()
	event_full = SerializerMethodField()
	delete_question = SerializerMethodField()

	class Meta:
		model = almanak.EntityEventRelation
		fields = "__all__"


	def get_event_full(self,obj):
		if "event" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"eers1ev")			
			serializer = EventSerializer(obj.event, context={'request': self.context['request']}, params = subparams)
			return serializer.data
		else:
			return obj.event.id

	def get_entity_full(self,obj):
		if "entity" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"eers1en")
			if obj.entity.type_spec == "player_character":
				pc = almanak.PlayerCharacter.objects.get(pk=obj.entity.id)
				serializer = PlayerCharacterSerializer(pc, context={'request': self.context['request']}, params = subparams)
			else:
				serializer = ElementSerializer(obj.entity, context={'request': self.context['request']}, params = subparams)
			return serializer.data
		else:
			return obj.entity.id

	def get_delete_question(self,obj):
		return False


class ElementItemRelationSerializer(ParamsModelSerializer):
	owner_full			= SerializerMethodField()
	owned_full			= SerializerMethodField()
	delete_question 	= SerializerMethodField()
	update_question 	= SerializerMethodField()

	class Meta:
		model = almanak.ElementItemRelation
		fields = "__all__"


	def get_owner_full(self,obj):
		if "owner" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"eirs1or")			
			serializer = ElementSerializer(obj.owner, context={'request': self.context['request']}, params = subparams)
			return serializer.data
		else:
			return obj.owner.id

	def get_owned_full(self,obj):
		if "owned" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"eirs1od")			
			serializer = ItemSerializer(obj.owned, context={'request': self.context['request']}, params = subparams)
			return serializer.data
		else:
			return obj.owned.id

	def get_delete_question(self,obj):
		return False

	def get_update_question(self,obj):
		return False


class ElementUniqueItemRelationSerializer(ParamsModelSerializer):
	owner_full = SerializerMethodField()
	owned_full = SerializerMethodField()
	delete_question = SerializerMethodField()

	class Meta:
		model = almanak.ElementUniqueItemRelation
		fields = "__all__"

	def get_owner_full(self,obj):
		if "owner" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"euirs1or")			
			serializer = ElementSerializer(obj.owner, context={'request': self.context['request']}, params = subparams)
			return serializer.data
		else:
			return obj.owner.id


	def get_owned_full(self,obj):
		if "owned" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"euirs1od")			
			serializer = ElementSerializer(obj.owned, context={'request': self.context['request']}, params = subparams)
			return serializer.data
		else:
			return obj.owned.id


	def get_delete_question(self,obj):
		return False



# class ElementAboveRelationSerializer(ParamsModelSerializer):

#     above = SerializerMethodField()
#     delete_question = SerializerMethodField()
#     edit_question = SerializerMethodField()
#     above_title =  SerializerMethodField()
#     below_title =  SerializerMethodField()

#     group_type_titles =  SerializerMethodField()


#     class Meta:
#         model = almanak.ElementRelation
#         fields = "__all__"

#     def get_above(self,obj):
#         serializer = ElementSerializer(obj.above, context={'request': self.context['request']},params=self.params)
#         return serializer.data

#     def get_group_type_titles(self,obj):
#         if obj.above.type_grund == "gruppe" and obj.above.type_spec != "gruppe":
#             gr = Group.objects.get(pk=obj.above.id)
#             titles = gr.group_type.titles
#             serializer = RelationTitleSerializer(titles, context={'request': self.context['request']},params=self.params,many=True )
#             return serializer.data
#         else:
#             return 0


#     def get_edit_question(self,obj):
#         return False

#     def get_delete_question(self,obj):
#         return False

#     def get_above_title(self,obj):
#         params = self.params
#         if obj.above_title:
#             serializer = RelationTitleSerializer(obj.above_title, context={'request': self.context['request']},params=self.params )
#             return serializer.data
#         else:
#             return { "id": 0 }

#     def get_below_title(self,obj):
#         if obj.below_title:
#             serializer = RelationTitleSerializer(obj.below_title, context={'request': self.context['request']})
#             return serializer.data

#     # def get_queryset(self,obj):
#     #     return self.objects.filter(deleted=False)


# class ElementBelowRelationSerializer(ParamsModelSerializer):
#     below = SerializerMethodField()
#     delete_question = SerializerMethodField()
#     above_title =  SerializerMethodField()
#     below_title =  SerializerMethodField()


#     class Meta:
#         model = ElementRelation
#         fields = "__all__"

#     def get_below(self,obj):
#         serializer = ElementSerializer(obj.below, context={'request': self.context['request']}, params=self.params )
#         return serializer.data

#     def get_delete_question(self,obj):
#         return False

#     def get_above_title(self,obj):
#         if obj.above_title:
#             serializer = RelationTitleSerializer(obj.above_title, context={'request': self.context['request']})
#             return serializer.data

#     def get_below_title(self,obj):
#         if obj.below_title:
#             serializer = RelationTitleSerializer(obj.below_title, context={'request': self.context['request']})
#             return serializer.data


class DescriptionElementRelationSerializer(ParamsModelSerializer):
	element = SerializerMethodField()
	description = SerializerMethodField()
	class Meta:
		model = almanak.DescriptionElementRelation
		fields = "__all__"

	def get_element(self,obj): # ders1e
		if "element" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"ders1e")
			serializer = ElementSerializer(obj.element, context={'request': self.context['request']},params=subparams)
			return serializer.data
		else:
			return False


	def get_description(self,obj):  # ders1d
		if "description" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"ders1d")
			serializer = DescriptionSerializer(obj.description, context={'request': self.context['request']},params=subparams)
			return serializer.data
		else:
			return False

class DescriptionElementRelationDetailSerializer(ParamsModelSerializer):
    description = SerializerMethodField()

    class Meta:
        model = almanak.DescriptionElementRelation
        fields = "__all__"

    def get_description(self,obj):
            serializer = DescriptionSerializer(obj.description, context={'request': self.context['request']},params="ds1_author")
            return serializer.data



class DescriptionSerializer(ParamsModelSerializer):                           # ID: DS1
	author_full						= SerializerMethodField()
	campaigns_full					= SerializerMethodField()
	campaigns_containing_session 	= SerializerMethodField()
	created_obj 					= SerializerMethodField()
	element_relations		 		= SerializerMethodField() # DescriptionElementRelationSerializer(many=True)
	content_stripped 				= SerializerMethodField()
	content_full 					= SerializerMethodField()
	#permissions 					= SerializerMethodField()
	shared_with_full				= SerializerMethodField()
	ingame_author_full				= SerializerMethodField()
	user_permissions 				= SerializerMethodField()
	updated_obj 					= SerializerMethodField()
	ingame_time_string				= SerializerMethodField()
	user_can_edit					= SerializerMethodField()	
	url								= SerializerMethodField()

	class Meta:
		model = almanak.Description
		fields = "__all__"

	def get_shared_with_full(self,obj):
		if "shared_with" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"ds1sw")			
			serializer = ElementSerializer(obj.shared_with.get_queryset(), context={'request': self.context['request']}, many=True, params = subparams)
			return serializer.data
		else:
			return None

	def get_author_full(self,obj):
		if "author" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"ds1a")
			serializer = BestiariesUserSerializer(obj.author, context={'request': self.context['request']}, params = subparams)
			return serializer.data
		else:
			return obj.author.id

	def get_ingame_author_full(self,obj):
		if "ingame_author" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"ds1ia")
			if obj.ingame_author:
				serializer = IndividualSerializer(obj.ingame_author, context={'request': self.context['request']}, params = subparams)
				return serializer.data
			return None
		else:
			return obj.author.id

	def get_campaigns_full(self,obj):
		if "campaigns" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"ds1c")
			serializer = CampaignSerializer(obj.campaigns, context={'request': self.context['request']}, many=True, read_only=True, params = subparams)
			return serializer.data
		else:
			return None

	def get_campaigns_containing_session(self,obj):
		if "ds1_campaigns_containing_session" in self.params:
			for f in obj.campaigns_containing.get_queryset():
				if (f.campaign_id == self.context['request'].session.get("campaign_current")):
					serializer = CampaignDescriptionSerializer(f, context={'request': self.context['request']}, read_only=True)
					return serializer.data

			return None
		else:
			return None

	def get_content_full(self,obj):
		dc = DescriptionContent(obj)
		return dc.description_rest_complex()

	def get_content_stripped(self,obj):
		if "content_stripped" in self.params:
			content_br_to_space = None
			if obj.content:
				content_br_to_space = obj.content.replace("<br>"," ").replace("<br/>"," ").replace("<br />"," ")
			plain_text = strip_tags(content_br_to_space)
			u = plain_text.strip()[:1]
			r = plain_text.strip()[1:]
			short = plain_text[ 0 : 40]
			link = ""
			if obj.description_type == "in_game":
				link = "<a class='ink' href='/almanak/description/show/" + str(obj.id) + "'>" + str(short) + "</a>"
			elif obj.description_type == "off_game":
				link = "<a class='rubrik' href='/almanak/description/show/" + str(obj.id) + "'>" + str(short) + "</a>"
			return {'uncial': u, 'rest': r, 'plain_text': plain_text, 'short': short, 'link': link}
		else:
			return False


	def get_created_obj(self,obj):
		if "ds1_created_obj" in self.params:
			created_obj = {}
			split1 = str(obj.created).split()
			created_obj["dato"] = split1[0]
			split2 = split1[1].split("+")
			created_obj["tid"] = split2[0]
			created_obj["dato_tid"] = split1[0] + " " + split2[0]
			date_time = DateTimeField(obj.created)
			return created_obj
		else:
			return False

	def get_element_relations(self,obj):
		if "element_relations" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"ds1er")
			serializer = DescriptionElementRelationSerializer(obj.element_relations, context={'request': self.context['request']},params=subparams,many=True)
			return serializer.data
		else:
			return False

	def get_links(self,obj):			# ikke lavet endnu - strukturen gjort klar
		if "ds1_links" in self.params:
			return obj.get_links(self.context['request'])

	def get_permissions(self,obj):
		user = self.context['request'].user
		session = self.context['request'].session
		return True  #PermissionCheck.description_permissions(user,session,obj)

	def get_updated_obj(self,obj):
		if "ds1_updated_obj" in self.params:
			updated_obj = {}
			split1 = str(obj.updated).split()
			updated_obj["dato"] = split1[0]
			split2 = split1[1].split("+")
			updated_obj["tid"] = split2[0]
			updated_obj["dato_tid"] = split1[0] + " " + split2[0]
			date_time = DateTimeField(obj.updated)
			return updated_obj
		else:
			return False

	def get_user_permissions(self,obj):
		if "ds1_user_permissions" in self.params:
			user = self.context['request'].user
			session = self.context['request'].session
			return PermissionCheck.description_permissions(user,session,obj)
		else:
			return None


	def get_ingame_time_string(self,obj):
		d = DateHelper()
		return d.date_string(obj.ingame_age,obj.ingame_year,obj.ingame_month,obj.ingame_date)			


	def get_user_can_edit(self,obj):
		if "can_user_edit" in self.params:
			return permissions.AlmanakPermission.user_can_edit_description(self.context['request'],obj)
		else:
			return None
	
	def get_url(self,obj):
		if obj.url != None and len(obj.url.name) > 0:
			return "/media/" + obj.url.name
		else:
			return None

    # class DescriptionElementRelationSerializer(ParamsModelSerializer):
    #     #description = DescriptionSerializer()
    #     element = ElementSerializer(
    #     class Meta:
    #         model = DescriptionElementRelation
    #         fields = "__all__"




class ElementDescriptionImagesSerializer(ModelSerializer):
    descriptions_of_element = DescriptionElementRelationSerializer(many=True)
    #descriptions_of_element = SerializerMethodField()
    images_of_element = ImageElementRelationSerializer(many=True)

    class Meta:
        model = almanak.Element
        fields = ("descriptions_of_element","images_of_element")


class EventSerializer(ElementSerializer):
    #names = NameSerializer(read_only=True, many=True)
    #descriptions_of_element = DescriptionElementRelationSerializer(many=True)
#    parents = ElementRelationSerializer(many=True)

    class Meta:
        model = almanak.Event
        fields = "__all__"

    def get_birth_name(self,obj):
        return "Beginning"

    def get_death_name(self,obj):
        return "End"


class RpgElementSerializer(EventSerializer):
	#offgame_start = DateTimeField() # SerializerMethodField()
	#offgame_start_time = DateTimeField() # SerializerMethodField()

	#offgame_start = DateTimeField(format='%Y-%m-%d %H:%M:%S')
	#campaigns = SerializerMethodField()

	class Meta:
		model = almanak.RpgElement
		fields = "__all__"


	# def get_offgame_start_date(self,obj):
	# 	serializer = DateTimeField(obj.offgame_start,"%Y-%m-%d")
	# 	print(serializer.__dict__)
	# 	return serializer
	#
	# def get_offgame_start_time(self,obj):
	# 	serializer = DateTimeField(obj.offgame_start,"%H:%M:%S")
	# 	return serializer
	# def get_campaigns(self,obj):
	# 	if "es1_campaigns" in self.params:
	# 		print(obj.campaigns_containing.get_queryset())
	# 		for f in obj.campaigns_containing.get_queryset():
	# 			print(f)
			# for o in obj.campaigns_containing.get_queryset():
			# 	print(o)



class RpgSessionSerializer(RpgElementSerializer):

	in_campaign = SerializerMethodField()
	gamemasters = SerializerMethodField()
	participants = SerializerMethodField()

	class Meta:
		model = almanak.RpgSession
		fields = "__all__"       #fields = ["names","offgame_start",]
		#exclude = ("session_number","in_campaign","gamemasters",)

	def get_gamemasters(self,obj):
		if "rs1_gamemasters" in self.params:
			serializer = BestiariesUserSerializer(obj.gamemasters, context={'request': self.context['request']}, many=True, read_only=True)
			return serializer.data
		else:
			return None

	def get_in_campaign(self,obj):
		if "rs1_in_campaign" in self.params:
			serializer = CampaignSerializer(obj.in_campaign,context={'request': self.context['request']},params=self.params)
			return serializer.data
		else:
			return None

	def get_participants(self,obj):
		if "participants" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"rss1pa")			
			serializer = EntityEventRelationSerializer(obj.participants,context={'request': self.context['request']},params=subparams, many=True, read_only=True)
			return serializer.data
		else:
			return None

class GroupTypeSerializer(ParamsModelSerializer):

	titles = SerializerMethodField()
	groups = SerializerMethodField()
	ajax_status = SerializerMethodField()

	class Meta:
		model = almanak.GroupType
		fields = "__all__"

	def get_titles(self,obj):
		if "gts1_titles" in self.params:
			serializer = RelationTitleSerializer(obj.titles.filter(deleted=False),context={'request': self.context['request']},params=self.params, many=True)
			return serializer.data
		else:
			return None

	def get_groups(self,obj):
		if "gts1_groups_count" in self.params:
			return len(obj.groups)
		elif "gts1_groups" in self.params:
			serializer = GroupSerializer(obj.groups,context={'request': self.context['request']},params=self.params, many=True)
			return serializer.data
		else:
			return None

	def get_ajax_status(self,obj):
		return { "message": "Not Changed" }



class GroupSerializer(ElementSerializer):                           # id: gs1
	#names = NameSerializer(read_only=True, many=True)
	#    descriptions_of_element = DescriptionElementRelationSerializer(many=True)
	#    parents = ElementRelationSerializer(many=True)
	group_type = SerializerMethodField()


	class Meta:
		model = almanak.Group
		fields = "__all__"

	def get_birth_name(self,obj):
		return "Formed"

	def get_death_name(self,obj):
		return "Dissolved"

	def get_group_type(self,obj):
		if "gs1_group_type" in self.params:
			serializer = GroupTypeSerializer(obj.group_type,context={'request': self.context['request']},params=self.params)
			return serializer.data
		else:
			return None





class KinSerializer(GroupSerializer):                                   # ID ks1
    individuals_of_kin = SerializerMethodField() #ElementMinimalSerializer(read_only=True, many=True)

    class Meta:
        model = almanak.Kin
        fields = "__all__"

    def get_birth_name(self,obj):
        return "Formed"

    def get_death_name(self,obj):
        return "Dissolved"

    def get_individuals_of_kin(self,obj):
        if "ks1_indivuals" in self.params:
            serializer = ElementSerializer(obj.individuals_of_kin,context={'request': self.context['request']},params=self.params)
            return serializer.data
        else:
            return None


class IndividualSerializer(ElementSerializer):	 # ID: IS1
	#    names = NameSerializer(read_only=True, many=True)
	child_relations		= SerializerMethodField()
	kin					= SerializerMethodField() #ElementMinimalSerializer()
	parent_relations	= SerializerMethodField()
	#    descriptions_of_element = DescriptionElementRelationSerializer(many=True

	class Meta:
		model = almanak.Individual
		fields = "__all__"


	def create(self, validated_data):
		names_data = validated_data.pop('names')
		# above_data = validated_data.pop('element_relations_above')
		# below_data = validated_data.pop('element_relations_below')		
		element = almanak.Individual.objects.create(**validated_data)

		for name_data in names_data:
			almanak.Name.objects.create(element=element, **name_data)
		return element



	def get_birth_name(self,obj):
		return "Birth"

	def get_child_relations(self,obj):	# is1cr
		if "child_relations" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"is1cr")
			subparams.append("below")
			subparams.append("irs1b_present_name")
			serializer = IndividualRelationSerializer(obj.child_relations, many=True, context={'request': self.context['request']}, params=subparams)
			return serializer.data
		else:
			return None

	def get_death_name(self,obj):
		return "Death"

	def get_kin(self,obj):
		if obj.kin:
			if "is1_kin" in self.params:
				serializer = KinSerializer(obj.kin,context={'request': self.context['request']},params=self.params)
				return serializer.data
			else:
				return {"id":obj.kin.id}
		else:
			return None
			
	def get_parent_relations(self,obj):	# is1pr
		if "parent_relations" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"is1pr")
			subparams.append("above")
			subparams.append("irs1a_present_name")
			serializer = IndividualRelationSerializer(obj.parent_relations, many=True, context={'request': self.context['request']}, params=subparams)
			return serializer.data
		else:
			return None


class PlayerCharacterSerializer(IndividualSerializer):  # id: PCS1
    roleplayer_full		= SerializerMethodField()

    class Meta:
        model = almanak.PlayerCharacter
        fields = "__all__"

    def get_roleplayer_full(self,obj):
        if "roleplayer" in self.params:
            serializer = BestiariesUserSerializer(obj.roleplayer,context={'request': self.context['request']},params=self.params)
            return serializer.data
        else:
            return {"id":obj.roleplayer.id}


class PlaceSerializer(ElementSerializer):

    class Meta:
        model = almanak.Place
        fields = "__all__"

    def get_birth_name(self,obj):
        return "Emergence"

    def get_death_name(self,obj):
        return "Downfall"


class ItemSerializer(ElementSerializer):

    class Meta:
        model = almanak.Item
        fields = "__all__"

    def get_birth_name(self,obj):
        return "Created"

    def get_death_name(self,obj):
        return "Destroyed"




class CampaignSerializer(EventSerializer):                  # ID: CS1
	ingame_now_age = AgeSerializer(many=False)
	ingame_now_string = SerializerMethodField()
	gamemasters_full = SerializerMethodField() #BestiariesUserSerializer(many=True)
	#universe = SerializerMethodField() #UniverseSerializer()
	#campaigns = SerializerMethodField()
	sessions = SerializerMethodField()

	class Meta:
		model = almanak.Campaign
		fields = "__all__"


	def get_gamemasters_full(self,obj):
		if "gamemasters" in self.params:
			serializer = BestiariesUserSerializer(obj.gamemasters, context={'request': self.context['request']}, many=True, read_only=True)
			return serializer.data
		else:
			return None

	def get_sessions(self,obj):
		if "sessions" in self.params:
			serializer = RpgSessionSerializer(obj.sessions.filter(deleted=False), context={'request': self.context['request']}, many=True, read_only=True)
			return serializer.data
		else:
			return None

	def get_ingame_now_string(self,obj):
		d = DateHelper()
		return d.date_string(obj.ingame_now_age,obj.ingame_now_year,obj.ingame_now_month,obj.ingame_now_date)


class PlayerCharacterParticipants(ModelSerializer):
    entity = PlayerCharacterSerializer()

    class Meta:
        model = almanak.EntityEventRelation
        fields = "__all__"




class SessionUniverseSerializer(ModelSerializer):
    ages = AgeSerializer(many=True)
    users = BestiariesUserSerializer(many=True)
    kins = SerializerMethodField()
    player_characters_in_universe = SerializerMethodField()


    class Meta:
        model = almanak.Universe
        fields = "__all__"

    def get_campaigns(self,obj):
        uni_id = self.context['request'].session.get("universe_current")
        uni_cur = Universe.objects.get(pk=uni_id)
        cmpgns = uni_cur.elements_in_universe.filter(type_spec="kampagne").filter(deleted=False)
        serializer = ElementSerializer(cmpgns, many=True, context={'request': self.context['request']})
        return serializer.data

    def get_kins(self,obj):
        uni_id = self.context['request'].session.get("universe_current")
        kins = Kin.objects.filter(universe=uni_id)
        #kins = uni_cur.elements_in_universe.filter(type_spec="race").filter(deleted=False)
        serializer = ElementSerializer(kins, many=True, context={'request': self.context['request']})
        return serializer.data


    def get_player_characters_in_universe(self,obj):
        uni_id = self.context['request'].session.get("universe_current")
        pcs = PlayerCharacter.objects.filter(universe=uni_id)
        serializer = ElementSerializer(pcs, many=True, context={'request': self.context['request']})
        return serializer.data

class SessionSerializer(ElementSerializer):
    ingame_now_age = AgeSerializer(many=False)
    ingame_now_string = SerializerMethodField()
    universe = UniverseSerializer(params=("us1_campaigns"))
    #names = NameSerializer(many=True)
    class Meta:
        model = almanak.Campaign
        fields = ("ingame_now_age","ingame_now_string","universe")
#        exclude =


    def get_ingame_now_string(self,obj):
        d = DateHelper()
        return d.date_string(obj.ingame_now_age,obj.ingame_now_year,obj.ingame_now_month,obj.ingame_now_date)


## UPDATE SERIALIZERS


class NameUpdateSerializer(ModelSerializer):

    class Meta:
        model = almanak.Name
        fields = "__all__"

# class DescriptionUpdateSerializer(ModelSerializer):
#     #elements_in_description = PrimaryKeyRelatedField(many=True, read_only=True)
#
#     class Meta:
#         model = almanak.Description
#         fields =   "__all__"


class ElementDescriptionSerializer(ModelSerializer):

    class Meta:
        model = almanak.DescriptionElementRelation
        fields =   "__all__"


class EntityEventRelationUpdateSerializer(ModelSerializer):

    class Meta:
        model = almanak.EntityEventRelation
        fields = "__all__"


class EventPlaceRelationUpdateSerializer(ModelSerializer):

    class Meta:
        model = almanak.EventPlaceRelation
        fields = "__all__"



# class NameCreateSerializer(ModelSerializer):

#     class Meta:
#         model = Name
#         fields = ('name','name_type','deleted','secret','element')


# class GroupTypeCreateSerializer(ModelSerializer):

#     class Meta:
#         model = GroupType
#         fields = ("id","name","universe","description")




# class RelationTitleCreateSerializer(ModelSerializer):

#     class Meta:
#         model = RelationTitle
#         fields = ('name','name_plural','group_type','preposition','description')




class ElementRelationSerializer(ParamsModelSerializer):			#ers1
	above_full		= SerializerMethodField()
	below_full		= SerializerMethodField()
	delete_question = SerializerMethodField()
	above_title		= SerializerMethodField()

	class Meta:
		model = almanak.ElementRelation
		fields = "__all__"

	def get_above_full(self,obj):	# ers1a
		if "above" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"ers1a")
			serializer = ElementSerializer(obj.above, context={'request': self.context['request']}, params = subparams )
			return serializer.data																		 
		else:
			return obj.above.id

	def get_below_full(self,obj):	# ers1b
		if "below" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"ers1b")			
			serializer = ElementSerializer(obj.below, context={'request': self.context['request']}, params = subparams )
			return serializer.data
		else:
			return obj.below.id

	def get_above_title(self,obj):
		if "above_titles" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"ers1at")
			serializer = RelationTitleSerializer(obj.above_title, context={'request': self.context['request']},params=subparams )
			return serializer.data
		else:
			return { "id": 0 }

	def get_delete_question(self,obj):
		return False


class IndividualRelationSerializer(ElementRelationSerializer):	# id: IRS1
	above_full		= SerializerMethodField()
	below_full		= SerializerMethodField()
	
	def get_above_full(self,obj):	# irs1a
		if "above" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"irs1a")
			individual = almanak.Individual
			serializer = IndividualSerializer(individual.objects.get(pk = obj.above.id), context={'request': self.context['request']}, params = subparams)
			return serializer.data
		else:
			return obj.below.id
			
	def get_below_full(self,obj):	# irs1b
		if "below" in self.params:
			subparams = ParamsManager.find_sub_params(self.params,"irs1b")
			individual = almanak.Individual
			serializer = IndividualSerializer(individual.objects.get(pk = obj.below.id), context={'request': self.context['request']}, params = subparams)
			return serializer.data
		else:
			return obj.below.id
	

# class ElementRelationCreateSerializer(ModelSerializer):
#     # below = PrimaryKeyRelatedField(required=False,read_only=True)
#     # above = PrimaryKeyRelatedField(required=False,read_only=True)

#     class Meta:
#         model = almanak.ElementRelation
#         fields = ['below','above','relation_type']


# class CampaignElementCreateSerializer(ParamsModelSerializer):

# 	class Meta:
# 		model = almanak.CampaignElement
# 		fields = "__all__"


# class EntityEventRelationCreateSerializer(ModelSerializer):
#     class Meta:
#         model = EntityEventRelation
#         fields = ['event','entity']

# class EventPlaceRelationCreateSerializer(ModelSerializer):
#     class Meta:
#         model = EventPlaceRelation
#         fields = ['event','place']



# class ElementCreateSerializer(ModelSerializer):
#     names = NameCreateSerializer(many=True, read_only=True)
#     #elements_above = ElementRelationCreateSerializer(many=True)

#     class Meta:
#         model = Element
#         fields = ["elements_above","element_type","type_grund","type_spec","rpg_type","session_number","universe","author","in_campaign","campaigns","secret"]


# class RpgSessionCreateSerializer(ElementCreateSerializer):
#     # elements_above = ElementRelationCreateSerializer(source='element-relation',many=True)

#     class Meta:
#         model = RpgSession
#         fields = ["id","element_type","type_grund","type_spec","rpg_type","session_number","universe","author","in_campaign","campaigns","secret"]



# class EventCreateSerializer(ModelSerializer):

#     class Meta:
#         model = Event
#         fields = ["id","element_type","type_grund","type_spec","universe","author","campaigns","secret"]


# class PlaceCreateSerializer(ModelSerializer):

#     class Meta:
#         model = Place
#         fields = ["id","element_type","type_grund","type_spec","universe","author","campaigns","secret"]


# class GroupUpdateSerializer(ModelSerializer):

# 	class Meta:
# 		model = Group
# 		fields = ["id","element_type","type_grund","type_spec","universe","author","campaigns","secret","group_type"]

# class IndividualCreateSerializer(ModelSerializer):

# 	class Meta:
# 		model = Individual
# 		fields = ["id","element_type","type_grund","type_spec","universe","author","campaigns","secret"]


# class PlayerCharacterCreateSerializer(ModelSerializer):

# 	class Meta:
# 		model = PlayerCharacter
# 		fields = ["id","element_type","type_grund","type_spec","universe","author","campaigns","secret","roleplayer"]


# class GroupCreateSerializer(ModelSerializer):

#     class Meta:
#         model = Group
#         fields = ["id","element_type","type_grund","type_spec","universe","author","campaigns","secret","group_type"]


# class GroupTypeUpdateSerializer(ModelSerializer):

#     class Meta:
#         model = GroupType
#         fields = ["id","name","description"]


# class CampaignImageUpdateSerializer(ModelSerializer):

# 	class Meta:
# 		model = CampaignImage
# 		fields = ["id","image","campaign","approved"]

# class RelationTitleUpdateSerializer(ModelSerializer):

#     class Meta:
#         model = RelationTitle
#         fields = ["id","name","name_plural","preposition","description","deleted"]


# class ElementUniqueItemRelationUpdateSerializer(ModelSerializer):

#     class Meta:
#         model = ElementUniqueItemRelation
#         fields = ["owner","owned","deleted"]


# class ElementItemRelationUpdateSerializer(ModelSerializer):

#     class Meta:
#         model = ElementItemRelation
#         fields = ["owner","owned","deleted"]


# class ElementUniqueItemRelationCreateSerializer(ModelSerializer):

#     class Meta:
#         model = ElementUniqueItemRelation
#         fields = ["owner","owned"]


# class ElementItemRelationCreateSerializer(ModelSerializer):
#     class Meta:
#         model = ElementItemRelation
#         fields = ["owner","owned","number_of_items","ingame_age","ingame_year","ingame_month","ingame_date","ingame_hours","ingame_minutes","ingame_seconds"]

# class ItemCreateSerializer(ModelSerializer):

#     class Meta:
#         model = Item
#         fields = ["id","element_type","type_grund","type_spec","universe","author","campaigns","secret","unique_item"]


# class ImageCreateSerializer(ModelSerializer):

# 	class Meta:
# 		model = Image
# 		fields = ["id","url","title","author","universe","campaigns","image_type","secret"]


# class DescriptionCreateSerializer(ModelSerializer):

#     class Meta:
#         model = Description
#         fields = ["id","content","description_type","secret","deleted","campaigns","elements_in_description","author","universe"]


# class ImageUpdateSerializer(ModelSerializer):

# 	class Meta:
# 		model = Image
# 		fields = ["id","url","title","author","image_type","ingame_author","shared_with","deleted"]


# class ElementImageRelationCreateSerializer(ModelSerializer):

# 	class Meta:
# 		model = ImageElementRelation
# 		fields = ["id","element","image","relevance"]





# class ElementImageRelationDeleteView(ModelSerializer):

# 	class Meta:
# 		model = ImageElementRelation
# 		fields = ["id"]
