var welcome = Vue.component('welcome', {
    template: "\
	<div v-if=\"Object.keys($store.state.generic.session.full).length > 0\">\
        <h3 class=\"rubrik\" >This is the</h3>\
        <h1 class=\"ink\"  >{{ universe.name }}</h1>\
        <h3 class=\"rubrik\" style=\"margin-top:0\" >Bestiary</h3>\
        <h3>You are logged in as {{ user.first_name }} {{ user.last_name }}</h3>\
        <h3>{{ campaign.present_name.name }}</h3>\
	</div>",
	mounted: function (){
	
	},
	computed: {
		user: function () {
			return this.$store.state.generic.session.full.user_details;
        },
        universe: function (){
            return this.$store.state.generic.session.full.universe;
        },
        campaign: function (){
            return this.$store.state.generic.session.full.campaign_full;
        },	
		role: function (){
			var role = "";
			if (this.$store.state.generic.session.base.roleId > 0) {
				role = "<a class=\"ink\" href=\"/almanak/side/" + this.$store.state.generic.session.full.player_character.id + "\">" + this.$store.state.generic.session.full.player_character.present_name.name + "</a>";
			}
			else {
				role = "a gamemaster";
			}
			return role;
		},        	
	},
	
});
