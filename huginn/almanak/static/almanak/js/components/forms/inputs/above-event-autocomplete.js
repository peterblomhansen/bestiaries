var aboveEventAutocomplete = Vue.component('above-event-autocomplete', {
	template: "\
	<div class='grid grid--create-input'  >\
		<label for='new-name'>Within</label>\
		<div>\
			<p v-if=\"relationsAbove && relationsAbove.length > 0\" v-for=\"(relationAbove,i) in relationsAbove\" ><a class=\"ink\" :href=\"'/almanak/element/' + relationAbove.element_id\">{{ relationAbove.name }}</a> <span @click=\"removeAbove(i)\" style=\"cursor:pointer\" >&times;</span></p>\
			<span style=\"opacity: 0.7\" v-if=\"relationsAbove.length === 0\" >Not within another Event</span>\
		</div>\
		<div></div>\
		<autocomplete \
			:on-button-click='setAbove' \
			filter='element__type_grund=begivenhed' \
			buttonText='Add Event'  \
			placeholder-text='Add Event' \
		></autocomplete>\
		</div>",
	methods: {
		setAbove: function (event){
			var aboveId = event.element_id;
			this.$store.commit(
				"createDataPushToArray",
				{	
					location: this.$store.state.create,
					array: "element_relations_above",
					entry: {
						relation_type: "normal",
						above: aboveId,
						name: event.name,
					}
				});
		},
		removeAbove: function (index){
			this.$store.commit(
				"createDataPopFromArray",
				{
					location: this.$store.state.create,
					array: "element_relations_above",
					index: index
				});
		},
	},
	computed: {
		relationsAbove: function (){
			return this.$store.state.create.element_relations_above;
		},
	}
});
 
