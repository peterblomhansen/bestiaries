var groupMemberships = Vue.component('group-memberships', {
	template: "\
	<transition name=\"fade-slide\">\
		<div v-if=\"$store.state.element.relations.memberships\">\
			<p class=\"rubrik\"><strong>Memberships and titles ({{ numMemberships }})</strong></p>\
			<ul>\
				<li v-for=\"t in titles\" class=\"rubrik\">\
				<span v-if=\"t.above_title.name && t.above_title.name.length > 0\">{{ t.above_title.name }} in </span>\
					<a class=\"ink\" :href=\"'/almanak/side/' +  t.above_full.id\">\
						{{ t.above_full.present_name.name }}\
					</a>\
					<span @click=\"t.delete_question = !t.delete_question\" class=\"rubrik cursor--pointer\" v-if=\"user_can_edit == true\">&times;</span>\
					<p v-if=\"t.delete_question == true\" >\
						Do you want to remove <span class=\"ink\">{{ $store.state.element.masterdata.present_name.name }}</span> as a member of <span class=\"ink\">{{ t.above_full.present_name.name }}</span>?<br/>\
						<button @click=\"deleteRelation(t.id)\" >Yes</button>\
						<button @click=\"t.delete_question = !t.delete_question\">No</button >\
					</p>\
				</li>\
				<li>\
					<button v-if=\"showGetMore\" @click=\"getMoreTitles()\" class=\"button--small\" >More</button>\
					<autocomplete\
						v-if=\"$store.state.element.permissions.write == true\"\
						filter=\"&type=gruppe\"\
						placeholder-text=\"Add a membership\"\
						:button-text=\"'Add ' + $store.state.element.masterdata.present_name.name + ' as member'\"\
						:button-action=\"{ type: 'membershipAdd', data: $store.state.element.masterdata, updateAction: 'getMemberships' }\" >\
					</autocomplete>\
				</li>\
			</ul>\
		</div>\
	</transition>",
	mounted: function (){

	},
	data: function (){
		return {
			page: 1,
		}
	},
	methods: {
		getMoreTitles: function (){
			this.page++;
			this.$store.dispatch("getMemberships", { page: this.page });
		},
		deleteRelation: function(id) {
			this.$store.dispatch("update",{ url: "/api/new/element-relation/" + id + "/", object: { deleted: true }, updateAction: "getMemberships" });
		}		
	},	
	computed: {
		user_can_edit: function (){
			return this.$store.state.element.masterdata.user_can_edit;
		},
		numMemberships: function (){
			if (this.$store.state.element.relations.memberships) {
				return this.$store.state.element.relations.memberships.count;
			}
			else { return 0; }
		},
		titles: function (){
			if (this.$store.state.element.relations.memberships) {
				return this.$store.state.element.relations.memberships.results;
				// return this.$store.state.element.relations.memberships.results.filter(function(d){
				// 	return d.above_title.name.length > 0; 
				// });
			}
			else {
				return [];
			}
		},
		numTitles: function (){
			if(this.$store.state.element.relations.memberships) {
				return this.$store.state.element.relations.memberships.count;
			}else {
				return 0;
			}
		},		
		showGetMore: function (){
			if(this.page * 10 >= this.numTitles) 
			{
				return false;
			}else {
				return true;
			}

		}		
	},


});
