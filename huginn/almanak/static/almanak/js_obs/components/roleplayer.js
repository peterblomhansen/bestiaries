var roleplayer = Vue.component('roleplayer', {
	template: "\
	<div>\
		  <p class=\"rubrik\">Roleplayer: <span class=\"ink\">{{ element.roleplayer.first_name }} {{ element.roleplayer.last_name }}</span></p>\
	</div>\
	",
    data: function() {
        return {

        }
    },
    methods: {

	},
	computed: {
		element: function (){
			return this.$store.state.element.masterdata;
		}
	},
});
