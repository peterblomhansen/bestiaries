var toggleSearchScope = Vue.component('toggle-search-scope', {
	template: "\
			<select v-model=\"scope\" >\
				<option v-for=\"o in scopeOptions\" :value=\"o.value\" >{{ o.name }}</option>\
			</select>"
	,
	data: function (){
		return { 
		}
	},
	methods: {

	},
	computed: {
		scope: {
			get: function (){
				return this.$store.state.generic.session.full.element_search_scope;
			},
			set: function (val) {
				this.$store.state.generic.session.full.element_search_scope = val;
				axios.post("/api/session/",{ element_search_scope: val }).then(function(response){

				});				
			}
		},
		campaignName: function (){
			
			if(this.$store.state.generic.session.full.campaign_full) {
				return this.$store.state.generic.session.full.campaign_full.present_name.name;
			}
			else {
				return "";
			}
		},
		universeName: function (){
			return this.$store.state.generic.session.full.universe_name;
		},
		scopeOptions: function () {
			return [
				{ name: this.campaignName ,value: "campaign"},
				{ name: this.universeName ,value: "universe"}				
			];
		},
	},
	methods: {
		toggleScope: function (scope) {
			this.$store.state.generic.session.full.scope = scope;
		},
	}


});
