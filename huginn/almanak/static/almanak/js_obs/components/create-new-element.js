	var createNewElement = Vue.component('create-new-element', {
    template: "\
        <div class=\"shader--full-screen\">\
		<span class=\"rubrik cursor--pointer\" @click=\"$store.state.element.editors.createNewElement.open = false\" style=\"position:absolute;top:0;left:0.2em;font-size:3em;\" >&times;</span>\
            <div style=\"height:100%;justify-content:center:display:flex;\">\
                <br/><br/>\
                <p style=\"cursor:pointer;font-weight:bold;text-align:right;\" @click=\"$store.state.element.editors.createNewElement.open = false\">&times;</p>\
                <h2 class=\"rubrik\">Create New Page in the Bestiary</h2>\
				<button style=\"width:100%;\" :disabled=\"!valid\" @click=\"createNewElement(createData)\">Create Ńew Page</button>\
				<br/><br/>\
				<p><input style=\"width:100%\" v-model=\"createData.name\" placeholder=\"Name the New Page\" /></p>\
				<p>Secret <input type=\"checkbox\" v-model=\"createData.secret\"></p>\
				<br/><br/>\
                <p class=\"rubrik\" ><strong>Choose kind of page to add to the {{ $store.state.session.universe.name }} Bestiary</strong></p>\
                <div style=\"display:flex;cursor:pointer;justify-content:space-between;\" >\
                    <p>\
						<span @click=\"setElementType('begivenhed','begivenhed','begivenhed')\" :class=\"isTypeCss('begivenhed')\" ><img style=\"height:20px\" :src=\"st + 'images/icons/type/rubrik/begivenhed.svg'\"> Event</span><br/>\
						<span v-if=\"userRoleCurrent == 0\" @click=\"setElementType('begivenhed','rpg_session','rpg_session');loadSessionOfCampaign()\" :class=\"isTypeCss('rpg_session')\" ><img style=\"height:20px;\" :src=\"st + 'images/icons/type/rubrik/rpg_session.svg'\" > Session</span>\
					</p>\
                    <p @click=\"setElementType('sted','sted','sted')\" :class=\"isTypeCss('sted')\" ><img style=\"height:20px\" :src=\"st + 'images/icons/type/rubrik/sted.svg'\"> Place</p>\
                    <p>\
						<span @click=\"setElementType('entitet','individ','individ')\" :class=\"isTypeCss('individ')\" ><img style=\"height:20px\" :src=\"st + 'images/icons/type/rubrik/individ.svg'\"> Individual</span><br/>\
						<span v-if=\"userRoleCurrent == 0\" @click=\"setElementType('entitet','player_character','player_character');loadUsersInUniverse()\" :class=\"isTypeCss('player_character')\" ><img style=\"height:20px\" :src=\"st + 'images/icons/type/rubrik/player_character.svg'\"> Player Character</span>\
					</p>\
                    <p @click=\"setElementType('entitet','gruppe','gruppe');loadGroupTypesOfUniverse()\" :class=\"isTypeCss('gruppe')\" ><img style=\"height:20px\" :src=\"st + 'images/icons/type/rubrik/gruppe.svg'\"> Group</p>\
                    <p @click=\"setElementType('artefakt','artefakt','artefakt')\" :class=\"isTypeCss('artefakt')\" ><img style=\"height:20px\" :src=\"st + 'images/icons/type/rubrik/artefakt.svg'\"> Item</p>\
                </div>\
                <div v-if=\"createData.type_grund.length > 0\" >\
                    <p v-if=\"createData.above != null\" class=\"rubrik\">Related Page: \
						<strong class=\"ink\"> \
							{{ createData.above.name }}\
						</strong>\
						<span @click=\"createData.above = null\" class=\"rubrik cursor--pointer\" >&times;</span>\
					</p>\
					<div v-if=\"createData.type_grund == 'gruppe'\">\
							<p class=\"rubrik\">Is the group of at specific type? \
							<select v-model=\"createData.typeData.group_type\" >\
								<option v-for=\"t in groupTypesOfUniverse\" :value=\"t.id\" >{{ t.name }}</option>\
							</select></p>\
					</div>\
					<div v-if=\"createData.type_grund == 'player_character'\">\
							<p>\
								Choose a Roleplayer: \
								<select v-model=\"createData.typeData.roleplayer\" >\
									<option v-for=\"u in users\" :value=\"u.id\" >{{ u.first_name }} {{ u.last_name }}</option>\
								</select>\
							</p>\
						</div>\
					</div>\
					<div v-if=\"createData.type_grund == 'artefakt'\">\
						{{ newNameDisplay }} is a unique item <input type=\"checkbox\" v-model=\"createData.typeData.unique\" />\
					</div>\
					<div v-if=\"createData.type_grund == 'begivenhed'\">\
						\
					</div>\
					<div v-if=\"createData.type_grund == 'rpg_session'\" >\
						# <input type=\"number\" v-model=\"createData.typeData.rpg_session_number\" /> in {{ $store.state.session.present_name }}\
					</div>\
					<div style=\"display:flex;flex-direction:column\" v-if=\"createData.type_grund != 'rpg_session'\">\
						<p v-if=\"createData.above == null\" class=\"rubrik\">Where does <strong class=\"ink\">{{ newNameDisplay }}</strong> belong?</p>\
						<autocomplete :placeholder-text=\"parentPageName\" v-if=\"createData.above == null\" link-target=\"_blank\" :filter=\"typeAbove\" :button-method=\"'setAbove'\" :button-text=\"'Set related page'\" >\
                    	</autocomplete>\
					</div>\
                </div>\
            </div>\
        </div>",
    props: {
        changeType: Boolean,
        typeSpec: String,
        show: Boolean,
        ingame: Object,
        element: Object,
    },
    mounted: function() {

    },
    data: function() {
        return {
            rpgSession: {},
            newName: "",
        }
    },
    methods: {
        setElementType: function(t,g,s) {

            this.createData.element_type = t;
            this.createData.type_grund = g;
            this.createData.type_spec = s;
            this.createData.above = null;
			//this.createData.typeData.player_character = false;
        },
        isTypeCss:function (type) {
            if (type == this.createData.type_grund) {
                return "";
            }else {
                return "faded";
            }
        },
        changeValue: function() {
            this.show = !this.show;
        },
        closeDialog: function() {
            eventHub.$emit('hideCreateElementDialog');
        },
        createName: function(el) {
            var self = this;
            return axios.post('/api/name/create/', {
                name: self.createData.name,
                name_type: "prim",
                deleted: false,
                secret: false,
                element: el.id,
                author: self.userId,
            });
        },
        createRelationAbove: function(el) {
            var self = this;
            return axios.post("/api/elementrelation/create/", {
                above: self.createData.above.element,
                below: el.id,
                relation_type: "normal"
            });
        },
		createRelationCampaign: function(el) {
            var self = this;
            return axios.post("/api/campaignelement/create/", {
                element: el.id,
                campaign: self.$store.state.initial.campaignId,
                approved: 0,
				canon: 0,
            });
        },
        createNested: function(el,createData) {

			var self = this;

			axios.post("/api/change_current_element_id/",{ new_element_id: el.id}).then(function(response) {

				var nested = [self.createName(el),self.createRelationCampaign(el)];
				if(createData.above != null) {
					nested.push(self.createRelationAbove(el));
				}
//				var nested = [self.createName(el), self.createRelationAbove(el),self.createRelationCampaign(el)];

	            axios.all(nested).then(axios.spread(function() {
	                //eventHub.$emit('reloadData');
	                window.location.href = "/almanak/side/" + el.id;
	                //eventHub.$emit('hideCreateElementDialog');
	            })).catch(function(error) {});
			});

        },
        createNewElement: function(createData) {
            var self = this;
            var postData = {};

			if(createData.type_grund == "player_character") {
				createData.type_grund = "player_character";
				createData.type_spec = "player_character";
				createData.roleplayer = createData.typeData.roleplayer;
			}
			if(createData.type_grund == "rpg_session") {
				createData.in_campaign = campaignId;
				createData.session_number = self.createData.typeData.rpg_session_number;

			}

			if(createData.typeData.group_type != null) {

				createData.group_type = createData.typeData.group_type;

			}
			if(createData.typeData.unique == true) {

				createData.unique_item = true;
			}

            axios.post("/api/" + createData.type_grund + "/create/", createData).then(function(response) {
                console.log(response.status)
                if (response.status == 201) {
                    console.log(response.data);
                    self.createNested(response.data, createData);
                }
            }).catch(function(error) {
                console.log(error);
            });
        },
		loadGroupTypesOfUniverse: function() {
			var self = this;
			axios.get("/api/session/" + campaignId + "?d=us1_ages__cs1_universe__us1_campaigns__us1_name__us1_users__us1_kins__us1_group_types__gts1_titles&format=json").then(function(response) {
				self.$store.state.session = response.data;

			}).catch(function(error) {
				console.log(error);
			});

		},
		loadUsersInUniverse: function() {
			var self = this;
			if(campaignId){
				axios.get("/api/session/" + campaignId + "/?d=us1_ages__cs1_universe__us1_name__us1_users&format=json").then(function(response) {
					self.$store.state.session = response.data;
					self.$store.state.element.loaded.sessiondata = true;
					self.$store.state.initial.sessionLoaded = true
				}).catch(function(error) {
					console.log(error);
				});
			}
		},
		loadSessionOfCampaign: function(){
			var self = this;
			if(campaignId){
				axios.get("/api/session/" + campaignId + "/?d=us1_ages__cs1_universe__us1_name__cs1_sessions&format=json").then(function(response) {
					self.$store.state.session = response.data;
					self.$store.state.element.loaded.sessiondata = true;
					self.$store.state.initial.sessionLoaded = true
					self.suggestNextSessionNumber();
				}).catch(function(error) {
					console.log(error);
				});
			}
		},
		suggestNextSessionNumber: function() {
			var self = this;
            var next = 1;
            if (self.$store.state.session.sessions.length > 0) {
				var lastestSession = _.sortBy(self.$store.state.session.sessions,[function(o) { return o.session_number; }]).reverse()[0];
				next = lastestSession.session_number + 1;
            }

			self.createData.typeData.rpg_session_number = next;
        }
    },
    computed: {
		parentPageName: function (){



			return "Find Page";
		},
		userRoleCurrent: function () {
			return this.$store.state.initial.roleId;
		},
		users: function (){
			return this.$store.state.session.universe.users;
		},
		newNameDisplay: function(){
			if(this.createData.name.length > 0) {
				return this.createData.name
			}
			else {
				return "The New " + this.$store.getters.elementTypeTranslated(this.createData.type_spec);
			}
		},
        valid: function(){
            if(this.createData.name.length > 0 && this.createData.type_grund.length > 0 && this.createData.type_spec.length > 0 && this.createData.element_type.length > 0){
				if(this.createData.type_grund == "player_character") {
					if(this.createData.typeData.roleplayer != null) {
						return true;
					}else {
						return false;
					}
				}else {
                	return true;
				}
            }
            else {
                return false;
            }
        },
        typeAbove: function(){
            if(this.createData.type_grund != "individ" && this.createData.type_grund != "player_character"){
                return this.createData.type_grund;
            }
            else {
                return "gruppe";
            }
        },
        st: function(){
            return this.$store.state.initial.staticUrl;
        },
        createData: function() {
            return this.$store.state.element.editors.createNewElement.data;
        },
		groupTypesOfUniverse: function(){
			return this.$store.state.session.universe.group_types;
		},
        elementTypes: function() {
            var types = {
                element_type: "s",
                type_grund: "t",
                type_spec: "sp"
            }
            if (this.typeSpec == "rpg_session") {
                types.element_type = "begivenhed";
                types.type_grund = "rpg_session";
                types.type_spec = "rpg_session";
            }
            return types;
        },

    }
});
