from django.conf.urls import url
from django.contrib import admin

from .views import(
	CharacterSheetView, CharacterSheetPdfView, PdfTestView
)

urlpatterns = [
    #url(r'^$', TemplateTest.as_view(),name="base" ),
	url(r'^charactersheet/(?P<pk>[0-9]+)/$', CharacterSheetView.as_view(),name="charactersheet" ),
	url(r'^charactersheet/(?P<pk>[0-9]+)/(?P<lvl>[0-9]+)/pdf/$', PdfTestView.as_view(),name="charactersheetPDF" ),
	url(r'^pdftest/$', PdfTestView.as_view(),name="test" ),
]
