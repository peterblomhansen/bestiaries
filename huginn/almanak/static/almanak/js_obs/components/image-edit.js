//
//
// var now = Math.round((new Date()).getTime() / 1000);
//
//
var imageEdit = Vue.component('image-edit', {
   template: "\
   		<div class=\"beskrivelse__editor\" style=\"overflow:auto\">\
			<div :class=\"updatingDataClass\" >\
				<div style=\"padding:1em;\">\
					<div class=\"row\">\
						<div class=\"column\">\
					  		<div v-if=\"canCloseEditor == true\" class=\"cursor--pointer\" @click=\"closeEditor()\"><strong>Close Editor</strong></div>\
					  		<div v-if=\"canCloseEditor == false\" class=\"faded\" >Close</div>\
							{{ updatingData }}\
						</div>\
						<div class=\"column\" style=\"text-align:right;\">\
							<p v-if=\"dataEditor.id\" class=\"cursor--pointer\" @click=\"delete_question = !delete_question;updatingDataClass = 'alert alert--danger'\" ><strong>Delete</strong></a>\
							<div v-if=\"delete_question == true\" >Do you want to delete this image? <button @click=\"deleteImage()\">Yes</button> <button @click=\"delete_question = !delete_question;updatingDataClass = 'alert alert--success'\">No</button> </div>\
							<p >Uploaded by {{ dataEditor.author.first_name }} {{ dataEditor.author.last_name }}</p>" +
				    		"<p v-if=\"dataEditor.campaigns.length != 0\"  >for <a class=\"alert--link\" target=\"_blank\" :href=\"'/almanak/side/' + k.id\" v-for=\"k in dataEditor.campaigns\">{{ k.present_name }} </a></p>" +
				    		"<p v-if=\"dataEditor.created\">Created: {{ createdSplit.date }} at {{ createdSplit.time.substring(0,5) }}</p>" +
				    		"<p v-if=\"dataEditor.updated\">Last updated: {{ updatedSplit.date }} at {{ updatedSplit.time.substring(0,5) }}</p>" +
				 		"</div>" +
					"</div>\
		 		</div>" +
		 	"</div>\
			<p class=\"alert alert--danger\" style=\"padding:1em\" v-show=\"hasNoLinks && dataEditor.id != null\">This image has no links, and therefore will not be shown anywhere.</p>\
			<div style=\"padding: 0 1em 0 1em;\">\
				<div v-if=\"dataEditor.id != null && dataEditor.url.length > 0\">\
					<div style=\"display:flex;padding:1em;\">" +
				     	"<div style=\"flex-grow:1;flex:1;\">" +
				       		"<button :class=\"classButtonOffgame\" @click=\"setTypeGame('off_game')\" >Offgame Note</button> \
				       		<button :class=\"classButtonIngame\" @click=\"setTypeGame('in_game')\" >Ingame Description</button>\
						</div>\
						<div v-if=\"dataEditor.image_type == 'in_game'\">\
							<p class=\"rubrik\" style=\"text-align:right;\">Ingame Author: \
								<span v-if=\"dataEditor.ingame_author\">\
									<a class=\"ink\" target=\"_blank\" href=\"'/almanak/side/' + dataEditor.ingame_author.id\">\
										{{ dataEditor.ingame_author.present_name }}\
									</a>\
								</span>\
								<span v-else >Not set</span>\
								<img class=\"cursor--pointer\" @click=\"showChangeIngameAuthor = !showChangeIngameAuthor\" :src=\"st + '/images/icons/pencil.svg'\" style=\"cursor: pointer; height: 1em;\">\
							</p>\
							<p v-if=\"showChangeIngameAuthor == true\" ><strong class=\"rubrik\">Change ingame author: </strong>\
								<autocomplete link-target=\"_blank\" admin=\"true\" :filter=\"'individ'\" :buttonMethod=\"'imageChangeIngameAuthor'\" :buttonText=\"'Change Ingame Author'\" ></autocomplete>\
							</p>\
						</div>\
						<div v-if=\"dataEditor.description_type == 'in_game'\">\
							<p class=\"rubrik\" style=\"text-align:right;\">Ingame Author: \
								<span v-if=\"dataEditor.ingame_author\">\
									<a class=\"ink\" target=\"_blank\" href=\"'/almanak/side/' + dataEditor.ingame_author.id\">\
										{{ dataEditor.ingame_author.present_name }}\
									</a>\
								</span>\
								<span v-else >Not set</span>\
								<img class=\"cursor--pointer\" @click=\"showChangeIngameAuthor = !showChangeIngameAuthor\" :src=\"st + '/images/icons/pencil.svg'\" style=\"cursor: pointer; height: 1em;\">\
							</p>\
							<p v-if=\"showChangeIngameAuthor == true\" ><strong class=\"rubrik\">Change ingame author: </strong>\
								<autocomplete link-target=\"_blank\" admin=\"true\" :filter=\"'individ'\" :buttonMethod=\"'changeIngameAuthor'\" :buttonText=\"'Change Ingame Author'\" >\
								</autocomplete>\
							</p>\
						</div>\
				  	</div>\
					<div style=\"display:flex;padding:0 1em 0 1em;\">\
						<div style=\"flex-grow:1;flex:1;\" >\
				       		<button :class=\"classButtonHemmelig\" @click=\"setSecrecy(true)\" >Secret</button>\
				       		<button :class=\"classButtonOffentlig\" @click=\"setSecrecy(false)\" >Public</button>\
				   		</div>\
				       <div style=\"flex-grow:1;flex:1;text-align:right;\" v-if=\"dataEditor.secret == true\">\
					   	<p class=\"rubrik\">Shared with: <strong class=\"cursor--pointer\" @click=\"showAddSharedWith = !showAddSharedWith\" >+</strong></p>\
						<p v-if=\"showAddSharedWith == true\">\
							<strong class=\"rubrik\">Share with:</strong>\
				     		<autocomplete link-target=\"_blank\"  :filter=\"'individ'\" :buttonMethod=\"'shareImageWith'\" :buttonText=\"'Add'\" >\
				        	</autocomplete>\
						</p>\
					   </div>\
					</div>\
					<div>\
				         <ul v-if=\"dataEditor.secret == true\" class=\"beskrivelse__link-tags\" style=\"display:flex;align-items: flex-end;flex-direction:row-reverse;margin:0 1em 0 1em;\"  >" +
				           "<li class=\"ink--background\" v-for=\"d in dataEditor.shared_with\"><a class=\"ink--background\" :href=\"'/almanak/side/' + d.id\"  >{{ d.present_name }} </a><span style=\"cursor:pointer;color:#fff;\" @click=\"unshare(d.id)\">&times;</span>\
						   </li>\
					   	</ul>\
					</div>\
					<img  style=\"width:100%;margin-top:1em;\" :src=\"dataEditor.url\">\
					\
					<div style=\"margin-top:1em;\" >\
						<autocomplete link-target=\"_blank\" :buttonMethod=\"'imageAddLink'\" :buttonText=\"'Add Relation'\" ></autocomplete>\
					</div>\
					<ul class=\"beskrivelse__link-tags\">\
				      <li v-for=\"lnk in dataEditor.elements_in_image\" :class=\"inkBackgroundType\">\
				        <img :src=\"elementTypeIcon(lnk.element.element_type,lnk.element.type_grund,lnk.element.type_spec)\" style=\"height:20px;\" />\
				        <a :href=\"'/almanak/side/' + lnk.element.id\">{{ lnk.element.present_name }}</a>\
				        <input type=\"text\" v-model=\"lnk.relevance\" @input=\"updateRelevance(lnk.id,lnk.relevance)\" />\
						<span style=\"color:#fff;padding-left:0.5em;cursor:pointer;\" @click=\"deleteElementRelation(lnk)\" v-if=\"dataEditor.elements_in_image.length > 1\" >&times;</span>\
				      </li>\
				    </ul>\
					\
					\
				</div>\
				<div v-else  >\
					<div v-if=\"uploadingInProgress == true\" class=\"shader--full-screen\">\
					<h3>OPLOADING IMAGE - PlEASE WAIT</h3>\
					</div>\
					<div style=\"display:flex;\">\
						<div style=\"flex:1;\" >\
							Title of the new Image: <input type=\"text\" v-model=\"newImage.title\" placeholder=\"Title\" /><br/>\
							Image to be upladed: <input type=\"file\" @change=\"addImageFile\" id=\"myForm\" ><br/>\
							Add at least one link: <autocomplete link-target=\"_blank\" :buttonMethod=\"'imageNewAddLink'\" :buttonText=\"'Add link to image'\"></autocomplete>\
						</div>\
						<div v-if=\"newImage.preview != null\" style=\"position:relative;flex:1;\">\
							<div style=\"position:absolute;z-index;1000;top:0;bottom:0;left:0;right:0;background:rgba(0,0,0,0.4);justify-content:center;display:flex\">\
								<p style=\"color:#fff;font-weight:bold;\">PREVIEW</p>\
							</div>\
							<img style=\"width:50%\" :src=\"newImage.preview\" />\
						</div>\
						</div>\
						<div v-if=\"newImage.links.length > 0\">\
							<ul class=\"beskrivelse__link-tags\">\
						      <li v-for=\"lnk in newImage.links\" :class=\"inkBackgroundType\">\
						        <img :src=\"elementTypeIcon(lnk.element_type,lnk.type_grund,lnk.type_spec)\" style=\"height:20px;\" />\
						        <a :href=\"'/almanak/side/' + lnk.id\">{{ lnk.name }}</a>\
						      </li>\
						    </ul>\
						</div>\
						<button @click=\"newImageCreate\" :disabled=\"!newImageIsValid\" >Add New Image</button>\
						\
					</div>\
				</div>\
			</div>\
		</div>",

    data: function () {
      return {
		  showAddSharedWith: false,
		  showChangeIngameAuthor: false,
		  delete_question: false,
		  uploadingInProgress: false,


	  };
  //
     },
    computed: {
	st: function(){
        return this.$store.state.initial.staticUrl;
    },
	 newImage: function (){
		 return this.$store.state.element.editors.image.newImage;
	 },
  	 canCloseEditor: function(){
		 return true;
	 },
	 dataEditor: function(){
		 return this.$store.state.element.editors.image.data;
	 },
	 createdSplit: function (){
		 return this.dateTimeSplit(this.dataEditor.created)
	 },
	 updatedSplit: function (){
		 return this.dateTimeSplit(this.dataEditor.updated);
	 },
	 user: function(){
		 return this.$store.state.user;
	 },
	 hasNoLinks: function(){
		 if(this.dataEditor.elements_in_image.length > 0) {
			 return false;
		 }
		 else {
			 return true;
		 }
	 },
	 newImageIsValid: function(){
		 var isValid = false;

		 if(this.newImage.file != null && this.newImage.title.length > 0 && this.newImage.links.length > 0) {
			 isValid = true;
		 }

		 return isValid;

	 },
	 classButtonIngame: function () {

        if(this.dataEditor.image_type == "in_game") {

          return "btn-ingame btn-ingame--active";

	  }else if(this.dataEditor.image_type == "off_game") {

          return "btn-ingame btn-ingame";

        }


     },
     classButtonOffgame: function () {

       if(this.dataEditor.image_type == "in_game") {

         return "btn-offgame";

	 }else if(this.dataEditor.image_type == "off_game") {

         return "btn-offgame btn-offgame--active";

       }

     },
	 classButtonHemmelig: function() {

 		var type = "";
 		var active = "";

 		if(this.dataEditor.image_type == "in_game") {

 			type = "btn-ingame";
 			if(this.dataEditor.secret == true) {
 				active = "btn-ingame--active";
 			}

		}else if(this.dataEditor.image_type == "off_game") {

 			type = "btn-offgame";
 			if(this.dataEditor.secret == true) {
 				active = "btn-offgame--active";
 			}

         }

 		return type + " " + active;

     },
     classButtonOffentlig: function () {

 		var type = "";
 		var active = "";

 		if(this.dataEditor.image_type == "in_game") {

 			type = "btn-ingame";
 			if(this.dataEditor.secret == false) {
 				active = "btn-ingame--active";
 			}
 //          return "btn-ingame";

		}else if(this.dataEditor.image_type == "off_game") {

 			type = "btn-offgame";
 			if(this.dataEditor.secret == false) {
 				active = "btn-offgame--active";
 			}

         }

 		return type + " " + active;

     },
	 updatingData: {
		   get() {
			   return this.$store.state.element.editors.image.dataStatus;
		   },
		   set(value) {
			   this.$store.state.element.editors.image.dataStatus = value;
		   }
	 },
	 updatingDataClass: {
		 get() {
			 return this.$store.state.element.editors.image.dataStatusClass;
		 },
		 set(value) {
			 this.$store.state.element.editors.image.dataStatusClass = value;
		 }
	 },
	 inkBackgroundType: function() {

       var cssClass = "";

       if(this.dataEditor.image_type == "in_game") {
         cssClass =  "ink--background";
       }
       else if(this.dataEditor.image_type == "off_game") {
           cssClass = "rubrik--background";
       }

       return cssClass;

     },

 },
 methods: {
	 deleteImage: function (){
		 axios.patch("/api/image/" + this.dataEditor.id + "/update/", { deleted: true  })
 		  .then(function(response) {

 			  window.location.reload();
 		  }).catch(function(error) {
 			  console.log(error);
 	  	});
	 },
	 updateRelevance: function(id,rel) {

		var self = this;

		var imageId = this.$store.state.element.editors.image.data.id;

		window.clearTimeout(this.timer);

		this.timer = window.setTimeout(function(){

			 self.$store.dispatch(
			   "consumerRequest",
			   {
				 method:"patch",
				 url :"/api/elementimagerelation/" + id + "/update/",
				 data: { "relevance": rel  },
				 mutation: { name: "openImageEditor", payload: imageId },
			 });

  		},500);



	},

	 deleteElementRelation: function (lnk) {

		 var imageId = this.$store.state.element.editors.image.data.id;

		 this.$store.dispatch(
		   "consumerRequest",
		   {
			 method:"delete",
			 url :"/api/elementimagerelation/" + lnk.id + "/delete/",
			 data: {  },
			 mutation: { name: "openImageEditor", payload: imageId },
		 });

	 },
	 elementTypeIcon: function (type,type_grund,type_spec) {
	   //console.log(type + " - " + type_grund + " - " + type_spec);

	   var filename = type;
	 //
	   if(type == "begivenhed") {
		 if(type_spec == "kampagne" || type_spec == "rpg_session"){
		   filename = type_spec;
		 }
	   }
	//    }else if(type == "sted") {
	 //
	  if (type == "entitet") {
		filename = type_grund;
		if(type_grund == "player_character") {
		  filename = "individ"
		}
	  }
	 //
   //
	  return this.st + "images/icons/type/white/" + filename + ".svg";

   },
	 setTypeGame: function (type_game) {

       this.dataEditor.description_type = type_game;
       this.updatingData = "Updating";
       this.updatingDataClass = "alert alert--warning";


       this.updateToDB([this.toDbTypeGame(type_game)]);

     },
     setAuthorIngame: function(idIndivid) {

       console.log("setAuthorIngame");

       this.updatingData = "Updating";
       this.updatingDataClass = "alert alert--warning";

       this.updateToDB([this.toDbAuthorIngame(idIndivid)]);

     },
	 setHemmelig: function(val) {

       this.dataEditor.hemmelig = val;
       this.updatingData = "Updating";
       this.updatingDataClass = "alert alert--warning";
       this.updateToDB([this.toDbHemmelig()]);

     },
	 closeEditor: function(){
		 this.$store.commit("closeImageEditor");
	 },
	 newImageCreate: function (){
		
		 var myForm = document.getElementById('myForm');
		 var formData = new FormData();
		 formData.append("url",this.newImage.file,this.newImage.file.name);
		 formData.append("author",this.user.id);
		 formData.append("title",this.newImage.title);
		 formData.append("image_type",this.dataEditor.image_type);
		 formData.append("secret",this.dataEditor.secret);
		 formData.append("universe",universeId);
		 //formData.append("campaigns",[campaignId]);
//		    			  campaigns: [editor.dataEditor.campaigns[0].id],

		 var self = this;
		 self.uploadingInProgress = true;
		 var config = { headers: { 'Content-Type': undefined } };
		 	axios({
			  method: 'post',
			  url: '/api/image/create/',
			  data: formData,
			 headers: { 'Content-Type': undefined }
		 }).then(function(response){
			 self.$store.state.element.editors.image.dataStatus = "Uploading New Image";
			 self.$store.state.element.editors.image.dataStatusClass = "alert alert--warning";

			 axios.post("/api/campaignimage/create/", { image: response.data.id, campaign: campaignId }).then(function(campaignsResponse){

			 })

			 var numLinks = self.newImage.links.length;
			 _.each(self.newImage.links,function(l){
				 obj = {
					 element: l.element,
					 image: response.data.id,
					 relevance: 0,
					 coordinates: ""
				 };

				 axios.post("/api/elementimagerelation/create/", obj).then(function(response2){


					numLinks--
					if(numLinks == 0) {
				 		self.$store.dispatch("openImageEditor",response.data.id);
						self.$store.state.element.editors.image.dataStatus = "New Image Created";
		   			 	self.$store.state.element.editors.image.dataStatusClass = "alert alert--success";
					}
					 self.uploadingInProgress = true;
				 });
			 });
		 });


	 },
	 dateTimeSplit: function(dt){
		   if(dt.indexOf("T") != -1) {
			   var split = dt.split("T");
			   var date = split[0];
			   var time = split[1];
		   }else {
			   date = "Not set";
			   time = "";
		   }
		   return {
			   "date": date,
			   "time": time
		   }
	 },
	 addImageFile: function(e){
		 var self = this;
		 this.newImage.file = e.target.files[0];
		 console.log(e.target.files[0]);
        var reader = new FileReader();

		reader.readAsDataURL(this.newImage.file);

		reader.onload = function (e) {
			console.log("sfsd");
            self.newImage.preview =  e.target.result;

        };



	},
	toDbTypeGame: function(type_game) {

      //return axios.post("/api/beskrivelse_data/change_type_game", { id: this.dataEditor.id, type_game: type_game });
      return axios.patch("/api/image/" + this.dataEditor.id + "/update/", { image_type: type_game  });

    },
	setSecrecy:function(isSecret) {

      this.updateDescriptionAjax({ secret: isSecret })
      this.dataEditor.secret = isSecret;

    },
    updateDescriptionAjax: function(changeData) {
      var self = this;
      axios.patch("/api/image/" + this.dataEditor.id + "/update/", changeData ).then(function(response) {
        self.$store.state.element.editors.dataStatus = "Updated";
      })

    },
	unshare: function(id){
      var self = this;

	  var sharedWithIds = []
	  _.each(this.dataEditor.shared_with,function(d,i){
		  sharedWithIds.push(d.id);
	  });
	  var i = sharedWithIds.indexOf(id);
	  sharedWithIds.splice(i,1);

      // _.remove(this.dataEditor.shared_with, {
      //     id
      // });

      this.updatingData = "Updating";
      this.updatingDataClass = "alert alert--warning";

	  this.$store.dispatch(
		  "consumerRequest",
		  {
			method:"patch",
			url :"/api/image/" + self.dataEditor.id + "/update/",
			data: { shared_with: sharedWithIds },
			mutation: { name: "openImageEditor", payload: self.dataEditor.id },
		  }
	  );



    },
	updateToDB: _.debounce(function(calls){

		var editor = this;
        var imageId = editor.dataEditor.id


   	      editor.updatingData = "Updating";
   	      editor.updatingDataClass = "alert alert--warning"

   	      axios.all(calls)
   	        .then(axios.spread(function (call1,call2,call3,call4) {
   			  editor.$store.state.element.editors.dataStatus = "Updated";
   	          editor.updatingData = "Updated";
   	          editor.updatingDataClass = "alert alert--success"

   	          console.log(imageId);
   	          editor.$store.commit("openImageEditor",imageId);





   	        })

   	      ).catch(function (error) {
   	            editor.updatingData = "Error";
   	            editor.updatingDataClass = "alert alert--danger"
   	            console.log(error);
             });


	},300),

 }

});
