var newsList = Vue.component('news-list', {
    template: "\
    <transition name=\"fade\" v-if=\"!_.isEmpty(news)\" >\
        <div>\
            <h3 class=\"rubrik\" >Latest Pages</h3>\
            <div v-for=\"el in news.results\" :key=\"el.id\" >\
                <p class='toc__row'>\
                    <img :title='getElementTypeSecrecy(el)' :class='isSecret(el.secret)' style=\"height:14px\" :src=\"staticUrl + 'images/icons/type/rubrik/' + el.type_grund + '.svg' \" />&nbsp;\
                    <span class='ink'>\
                        <a class='ink' :href=\"'/almanak/element/' + el.id + '/'\" >{{ el.present_name.name }}</a>\
                    </span>\
                    <span class='toc__fill faded'></span>\
                    <span class='rubrik' style='text-align:right' >{{ dur(el.created) }}</span>\
                </p>\
            </div>\
        </div>\
    </transition>\
    ",
    data: function() {
        return { }
    },
    mounted: function (){

        this.$store.dispatch("getLatestElementsOfCampaign");
    },
    methods: {
        dur: function(yt) {
            var x = new moment();
            var y = new moment(yt);
            var duration = moment.duration(y.diff(x));
            return duration.humanize(true);
        },
        isSecret: function (elementIsSecret){
            return elementIsSecret ? 'faded' : '';
        },
        getElementTypeSecrecy: function (element){
            var publicSecret = element.secret === true ? 'Secret' : 'Public';
            return publicSecret + " " + element.type_spec_obj.text;
        }
    },
    computed: {
        news: function (){
            return this.$store.state.news.elementNewestCreated;
        },
        campaign: function (){
            return this.$store.state.generic.session.full.campaign_full;
        },
        staticUrl: function (){
            return this.$store.state.generic.staticUrl;
        },

    }


});
