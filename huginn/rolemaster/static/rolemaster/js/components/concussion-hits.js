var concussionHits = Vue.component('concussion-hits', {
	template:"<div style=\"border: 1px gray solid; padding:0.2em; margin:0.2em;\">\
		<h3>HITS</h3>\
		<table>\
			<tr><th>Type</th><th># Ranks</th><th>Hits</th></tr>\
			<tr><td>Level 0:</td> <td>{{ bodyDevGmHobby }}</td><td><input type=\"text\" v-model=\"hits[0]\" ><button @click=\"updateHits(0)\">Update</button></td></tr>\
			<tr v-for=\"bdev in bodyDev.dev_ranks\" v-if=\"bdev.num_ranks > 0\" ><td>Level {{ bdev.level }}: </td> <td> {{ bdev.num_ranks }}</td><td><input type=\"text\" v-model=\"hits[bdev.level]\" ><button @click=\"updateHits(bdev.level)\">Update</button></td></td></tr>\
		</table>\
		<p>Base hits: {{ baseHits }}</p>\
		<p>Total hits: {{ totalHits }}</p>\
	</div>",
    data: function() {
        return {
        }
    },
    methods: {
		updateHits: function(level) {
			var self = this;
			var rolled = this.$store.state.character.body_development_of_character.find(function(d){
				return d.level == level;
			});
			var i = this.$store.state.character.body_development_of_character.findIndex(function(d){
				return d.level == level;
			});

			if(rolled) {
				console.log(rolled.id);
				axios.patch("/rm/api/bodydevelopment/" + rolled.id + "/update/",{ hits: this.hits[level] }).then(function(response){
					console.log(response.data);
					self.$store.state.character.body_development_of_character[i] = response.data.hits;

				})
			}
			else {
				newBodyDev = {
					level: level,
					hits: this.hits[level],
					character: this.$store.state.character.id,

				}

				axios.post("/rm/api/bodydevelopment/create/",newBodyDev).then(function(response){
					self.$store.state.character.body_development_of_character.push(response.data);
				})
			}


		},
	},
	computed: {
		level: function (){
			return this.$store.state.initial.level.viewed;
		},
		bodyDev: function(){

			return this.$store.state.skillrows.find(function(s){
				//console.log(s.name)
				return s.name == "Body Development";
			});

		},
		bodyDevGmHobby: function(){
			return this.bodyDev.gm_ranks.num_ranks + this.bodyDev.hobby_ranks.num_ranks
		},
		hits: function (){
			//var data = this.$store.state.character.body_development_of_character;
			var hitsArray = []
			// .each(data,function(i,d){
			// 	var hits = 0
            //
			// })
			for (i = 0; i <= this.level; i++) {
			    var hits = 0;
				var rolled = this.$store.state.character.body_development_of_character.find(function(d) {
					return d.level == i
				});
				if(rolled) { hits = rolled.hits }
				hitsArray.push(hits);
			}

			return hitsArray;

		},
		baseHits: function(){
			function add(a, b) {
			    return a + b;
			}

			return this.hits.reduce(add, 0);

		},
		totalHits: function () {
			return this.$store.state.stats;
		},

	}
});
