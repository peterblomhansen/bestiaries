var userSelect = Vue.component('user-select', {
    template: "\
		<select @change=\"test\" v-model=\"selected\">\
			<option v-for=\"user in users\" :value=\"user.id\"  >\
				{{ user.first_name }} {{ user.last_name }}\
			</option>\
		</select>\
	",
	props: {
		selectedId: Number,
		onSelection: Function,
	},
	mounted: function (){
		this.selected = this.selectedId;
	},
	data: function (){
		return {
			selected: 0,
		}
	},
	computed: {
		users: function () {
			return this.$store.state.generic.session.full.universe.users;
		},
		
	},
	methods: {
		test: function ($event){
			var id = $event.target.selectedOptions[0].value;
			this.onSelection(id);
		},
		isSelected: function (userId){
			return userId === this.selectedId;
		}		
	}
	
	
});
