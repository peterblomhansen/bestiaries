from django.core.management.base import BaseCommand, CommandError
from django.db import connections
from collections import namedtuple
from django.contrib.auth.models import AbstractUser
from almanak.models import BestiariesUser


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'




    def handle(self, *args, **options):

        def namedtuplefetchall(cursor):
            #"Return all rows from a cursor as a namedtuple"
            desc = cursor.description
            nt_result = namedtuple('Result', [col[0] for col in desc])
            return [nt_result(*row) for row in cursor.fetchall()]

        def importUsers():
            st = ""
            crsr = connections['legacy'].cursor()

            crsr.execute("SELECT * FROM deltager")
            user = namedtuplefetchall(crsr)

            for u in user:
            #    if u.id != 1:
                email = ""
                if u.email_adress is not None:
                    email = u.email_adress

                e = BestiariesUser(pk=u.id, username=u.username, first_name=u.rollespiller_navn, last_name=u.rollespiller_efternavn,email=email,password="1234")
                e.save()


        importUsers()
    #    importUniverseRegistration()



        self.stdout.write(self.style.SUCCESS("users imported"));
