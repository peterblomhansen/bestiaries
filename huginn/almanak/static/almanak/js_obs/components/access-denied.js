var accessDenied = Vue.component('access-denied', {
    template: "\
	<!-- If the user does not have the necessary permissions to view the Page -->\
	<div class=\"shader--full-screen\">\
	  <div class=\"alert--center\">\
		<h3>Access Denied</h3>\
		<h2>{{ accessDeniedMessage }}</h2>\
		<a class=\"rubrik\" href=\"/accounts/profile\">Go to Table of Contents</a>\
	  </div>\
	</div>\
    ",
    data: function() {
        return {
        }
    },
    methods: {},
	computer: {

	}
});
