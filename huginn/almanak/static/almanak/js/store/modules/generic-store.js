var genericStore = {
    state: {
        ui: {
            popupGlobal: false,
        },
        pageTitle: "",
        staticUrl: document.getElementById("static-url").value,
        userBestiariesLoaded: false, 
        session: {
            base: {
                userId: parseInt(document.getElementById("user-id").value),
                universeId: parseInt(document.getElementById("universe-id").value),
                campaignId: parseInt(document.getElementById("campaign-id").value),
                roleId: parseInt(document.getElementById("role-id").value),
            },
            // Populated by actions.getSession()
            full: { },
        },
        actionMapper: {
            element: {
                individ: [
                    "elementGetNames",
                    "getFamilyRelations",
                    "getDescriptionsOfElement",
                    "getMemberships",
                    "getExperiences",
                    "elementGetOwned",
                    "elementGetUniqueOwned",
                    "elementGetOwners",                    
                ],
                player_character: [
                    "elementGetNames",
                    "getFamilyRelations",
                    "getDescriptionsOfElement",
                    "getMemberships",
                    "getExperiences",
                    "elementGetOwned",
                    "elementGetUniqueOwned",
                    "elementGetOwners",

                ],
                gruppe: ["elementGetNames","getDescriptionsOfElement","elementGetOwners"],
                sted: ["elementGetNames","getDescriptionsOfElement","elementGetOwners"],
                begivenhed:["elementGetNames","getDescriptionsOfElement","elementGetOwners"],
                rpg_session: ["elementGetNames","getDescriptionsOfElement","elementGetOwners"],
                kampagne: ["elementGetNames","getDescriptionsOfElement","elementGetOwners"],
                artefakt: ["elementGetNames","getDescriptionsOfElement","elementGetOwners"],

            },
            description: [],
            profile: [],
        }

    },
    getters: {
        getUser: function (state){
            return function (id) {
                return _.find(state.session.full.universe.users, {id:id});
            }
        },
        ageInUniverse: function (state) {
            //console.log("ageInUniverse",state);
            return state.session.full && state.session.full.universe ? state.session.full.universe.ages: {};
        },
        isUserGamemasterOfCampaign: function (state){
            return function (campaignId){

                return campaignId ? state.session.base.roleId === 0 && state.session.base.campaignId == campaignId: false;
            }
        },           
    },
    mutations: {
        setSession: function (state,data){
            Vue.set(state.session,"full",data);
        }
    },
    actions: {
        // Responsible for running async api calls to populate relevant page
        // based on the prop.section property that defines the page type.
        // Looks to context.state.actionMapper for an array of action names defined by props.section
        loader: function (context,props){
            var maps = context.state.actionMapper;
            if(props.section == "element") {
                _.each(maps.element[props.map], function(i){
                    context.dispatch(i);
                });              
            }
            else {
                _.each(maps[props.map], function(i){
                    context.dispatch(i);
                });
            }

        },
        getSession: function(context) {
            axios.get('/api/session/?d=es1_present_name')	
                .then(function (response) {
                    context.commit("setSession",response.data);
                })
                .catch(function (error) {
                    console.log(error);
                });	            
        },
        create: function (context,data) {
            axios.post(data.url,data.object).then(function(response){
                context.dispatch(data.updateAction);

            })
            .catch(function (error) {
              console.log(error);
              if(error.response.status == 403) {
               // context.state.permissions.read = false;
            }
            });
        },
        update: function (context,data) {
            axios.patch(data.url,data.object).then(function(response){
                context.dispatch(data.updateAction);

            })
            .catch(function (error) {
              console.log(error);
            });
        },
        delete: function (context,data) {
            axios.delete(data.url,data.object).then(function(response){
                context.dispatch(data.updateAction);

            })
            .catch(function (error) {
              console.log(error);
            });
        },        
        updateAfter: function (context,data) {
            console.log("updateAfterCreate",data)
            context.commit(data.updateMutation);
        }        
     
    }
}