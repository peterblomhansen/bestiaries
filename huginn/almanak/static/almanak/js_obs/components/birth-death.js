var birthDeath = Vue.component('birth-death', {
	template: "<div>\
		<div v-if=\"element.type_spec == 'rpg_session'\">\
			<offgame-time></offgame-time>\
		</div>\
		<p class=\"rubrik\">\
			<strong class=\"rubrik\">{{ element.birth_name }}:</strong>\
			<span class=\"ink\" v-if=\"element.birth_string != false\" >\
				{{ element.birth_string }} ({{ element.birth_interval }})\
			</span>\
			<span class=\"ink\" v-else>Unknown</span>\
			<img v-if=\"element.user_permissions.write == true\" @click=\"editBirthDeath = !editBirthDeath\" :src=\"st + 'images/icons/pencil.svg'\" style=\"cursor:pointer;height:1em;\" />\
		</p>\
		<p class=\"rubrik\">\
			<strong class=\"rubrik\">{{ element.death_name }}:</strong>\
			 <span class=\"ink\" v-if=\"element.death_string != false\" >\
			 	{{ element.death_string }}\
			</span>\
			<span class=\"ink\" v-else>Unknown</span>\
		</p>\
		<div v-if=\"editBirthDeath == true\">\
			<p class=\"faded\" >{{ ajaxStatus }}</p>\
				<div @change=\"updateBirthDeath()\" >\
					<strong class=\"rubrik\">Edit {{ element.birth_name }}:</strong><br/>\
					<input class=\"input--narrow\" v-model.number=\"element.birth_date\" type=\"number\" :disabled=\"isBirthNotDefined == true ? true : false\" />\
		            month:\
		            <select v-model=\"element.birth_month\" :disabled=\"isBirthNotDefined == true ? true : false\" >\
		              <option value=\"0\">-</option>\
		              <option v-for=\"m in months.options\" :value=\"m.value\">{{ m.text }}</option>\
		            </select>\
		            year: <input class=\"input--narrow\" v-model.number=\"element.birth_year\" type=\"number\" :disabled=\"isBirthNotDefined == true ? true : false\" />\
		            age:\
					<select v-model=\"element.birth_age\">\
						<option :value=\"null\">-</option>\
						<option v-for=\"a in ages\" :value=\"a.id\" >{{ a.initials }}</option>\
					</select>\
					hour: <input class=\"input--narrow\" type=\"number\" v-model=\"element.birth_hours\" :disabled=\"isBirthNotDefined == true ? true : false\">\
					minute: <input class=\"input--narrow\" type=\"number\" v-model=\"element.birth_minutes\" :disabled=\"isBirthNotDefined == true ? true : false\">\
					second: <input class=\"input--narrow\" type=\"number\" v-model=\"element.birth_seconds\" :disabled=\"isBirthNotDefined == true ? true : false\">\
				</div>\
				<div @change=\"updateBirthDeath()\" >\
					<strong class=\"rubrik\">Edit {{ element.death_name }}:</strong><br/>\
					<input class=\"input--narrow\" v-model.number=\"element.death_date\" type=\"number\" :disabled=\"isDeathNotDefined == true ? true : false\" />\
		            month:\
		            <select v-model=\"element.death_month\" :disabled=\"isDeathNotDefined == true ? true : false\" >\
		              <option value=\"0\">-</option>\
		              <option v-for=\"m in months.options\" :value=\"m.value\">{{ m.text }}</option>\
		            </select>\
		            year: <input class=\"input--narrow\" v-model.number=\"element.death_year\" type=\"number\" :disabled=\"isDeathNotDefined == true ? true : false\" />\
		            age:\
					<select v-model=\"element.death_age\">\
						<option :value=\"null\">-</option>\
						<option v-for=\"a in ages\" :value=\"a.id\" >{{ a.initials }}</option>\
					</select>\
					hour: <input class=\"input--narrow\" type=\"number\" v-model=\"element.death_hours\" :disabled=\"isDeathNotDefined == true ? true : false\" >\
					minute: <input class=\"input--narrow\" type=\"number\" v-model=\"element.death_minutes\" :disabled=\"isDeathNotDefined == true ? true : false\">\
					second: <input class=\"input--narrow\" type=\"number\" v-model=\"element.death_seconds\" :disabled=\"isDeathNotDefined == true ? true : false\">\
				</div>\
			</div>\
		</div>",
    data: function() {
        return {
			editBirthDeath: false,
			ajaxStatus: "No Changes",
			months: {
                select: "",
                options: [{
                    text: "jan",
                    value: 1
                }, {
                    text: "feb",
                    value: 2
                }, {
                    text: "mar",
                    value: 3
                }, {
                    text: "apr",
                    value: 4
                }, {
                    text: "may",
                    value: 5
                }, {
                    text: "jun",
                    value: 6
                }, {
                    text: "jul",
                    value: 7
                }, {
                    text: "aug",
                    value: 8
                }, {
                    text: "sep",
                    value: 9
                }, {
                    text: "oct",
                    value: 10
                }, {
                    text: "nov",
                    value: 11
                }, {
                    text: "dec",
                    value: 12
                }, ]
            },

        }
    },
	computed: {
		offgameTimeInterval: function(){
			var start = this.offgameStartArray;
			var end = this.offgameEndArray;
			var final = "";
			var withinDay = false;
			if(start.date == end.date && start.month == end.month && start.year == end.year) {
				return start.date + "-" + end.date + "-" + start.month + " from " + start.hour + ":" + start.minute + "to " + end.hour + ":" + end.minute;
			}else {
				return start.date + "-" + end.date + "-" + start.month;
			}


			return final;
		},
		offgameStartArray: function(){
			return this.dateArray(this.element.offgame_start);
		},
		offgameEndArray: function(){
			return this.dateArray(this.element.offgame_end);
		},
		element: function(){
			return this.$store.state.element.masterdata;
		},
		ages: function (){
			return this.$store.state.session.universe.ages;
		},
		st: function (){
			return this.$store.state.initial.staticUrl;
		},
		isBirthNotDefined: function() {
            def = false;
            if (this.element.birth_age == null) {
                def = true;
            }
            return def;
        },
        isDeathNotDefined: function() {
            def = false;
            if (this.element.death_age == null) {
                def = true;
            }
            return def;
        },
	},
    methods: {
		monthName: function(monthNum){
			console.log(monthNum);
			monthNumInt = parseInt(monthNum);
			var months = [
				"None",
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December",
			];

			return months[monthNumInt];

		},
		dateArray: function(dt) {
			if (dt != null) {
				var date = dt.split("T")[0];
				var time = dt.split("T")[1];

				return {
					date: date.split("-")[2],
					month: date.split("-")[1],
					year: date.split("-")[0],
					hour: time.split(":")[0],
					minute: time.split(":")[1],
					second: time.split(":")[2],
				};

			}
			else {
				return {
					date: null,
					month: null,
					year: null,
					hour: null,
					minute: null,
					second: null,
				};
			}

		},
		updateBirthDeath: function (){
			var self = this;

			var newBirthDeath = {
				"birth_age": this.element.birth_age,

				"birth_year": this.element.birth_year,
			    "birth_month": this.element.birth_month,
			    "birth_date": this.element.birth_date,
			    "birth_hours": this.element.birth_hours,
			    "birth_minutes": this.element.birth_minutes,
			    "birth_seconds": this.element.birth_seconds,

			    "death_age": this.element.death_age,
			    "death_year": this.element.death_year,
			    "death_month": this.element.death_month,
			    "death_date": this.element.death_date,
			    "death_hours": this.element.death_hours,
			    "death_minutes": this.element.death_minutes,
			    "death_seconds": this.element.death_seconds,

			}

			this.ajaxStatus = "Updating";

			axios.patch("/api/element/" + self.element.id + "/update/",newBirthDeath).then(function(response){
				self.ajaxStatus = "Updated";
				//self.store.dispatch("getElement");
			});

			console.log(newBirthDeath);


		}


	}
});
