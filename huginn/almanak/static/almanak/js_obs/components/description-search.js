var descriptionSearch = Vue.component('description-search', {
	template: "<p>\
		<input placeholder=\"search in descriptions\" type=\"text\" v-model=\"query\" @keyup=\"searchInDescriptions()\">\
	</p>\
	",

    data: function() {
        return {
            query: "",
			hitsCount: 0,
    //        please: "sfdsgdsgsgsd"
        }
    },
    methods: {
		searchInDescriptions: function() {
			var self = this;
			console.log("search")

			window.clearTimeout(this.timer);
			//var millisecBeforeRedirect = 10000;
			this.timer = window.setTimeout(function(){

				if(self.query.length > 2) {
					self.$store.state.element.loaded.descriptionsImages = false;
					self.$store.state.element.descriptions = [];

					console.log(self.regexQuery)

					axios.get("/api/description_by_content/?format=json&search=" + self.regexQuery).then(function(response){
						self.$store.state.element.descriptions = response.data;
						self.$store.state.element.loaded.descriptionsImages = true;
						self.hitsCount = response.data.length;

					}).catch(function(error) {
			            console.log(error);
			        });

				}
				else if(self.query.length == 0) {
					self.$store.state.element.loaded.descriptionsImages = false;
					self.$store.state.element.descriptions = [];
					self.hitsCount = 0;
					self.$store.commit("ajaxDescriptionsImages");
				}
			},500);

		},
		timerInactivity: function(){
	          var app = this;
	          console.log("Updating in 3 seconds");

	          window.clearTimeout(this.timer);
	          //var millisecBeforeRedirect = 10000;
	          this.timer = window.setTimeout(function(){

	          app.updateToDB([app.toDbContent()]);

	            _.each(app.linksToDeletion,function(d,i){
	                app.updateToDB([app.toDbRemoveRelation(d.id)]);
	            });

	            _.each(app.linksToAdd,function(d,i){
	                app.updateToDB([app.toDBCreateElementRelation(d.element.id)]);
	            });


			},2000);


	    },

	},
	computed: {
		descriptions: function(){
			return this.$store.state.element;
		},
		regexQuery: function() {
			return this.query.replace(" ","\\s")

		},
		descriptionsLoaded: {
			get: function(){
				return this.$store.state.element.descriptionsImages;
			},
			set: function(){
				return false;

			}
		}
		// query: function() {
		// 	this.$store.state.
		// }
	}
});
