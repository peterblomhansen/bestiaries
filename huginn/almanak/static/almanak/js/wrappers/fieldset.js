var fieldsetWrapper = Vue.component('fieldset-wrapper', {
    template: "\
    <fieldset>\
        <legend v-if=\"legend\" >{{ legend }}</legend>\
        <slot></slot>\
    </fieldset>",
    props: {
        legend: String,
    },

});
