function ElementUniqueItemRelationModel(
	id=null,
	owner=null,
	owned = null,
	start_age = null,
	start_year = 0,
	start_month = 0,
	start_date = 0,
	start_hour = 0,
	start_minute = 0,
	start_second = 0,
	stop_age = null,
	stop_year = 0,
	stop_month = 0,
	stop_date = 0,
	stop_hour = 0,
	stop_minute = 0,
	stop_second = 0	

){
	this.id = id;
	this.owner = owner;
	this.owned = owned;
	this.start_age = start_age;
	this.start_year = start_year;
	this.start_month = start_month;
	this.start_date = start_date;
	this.start_hour = start_hour;
	this.start_minute = start_minute;
	this.start_second = start_second;
	this.stop_age = stop_age;
	this.stop_year = stop_year;
	this.stop_month = stop_month;
	this.stop_date = stop_date;
	this.stop_hour = stop_hour;
	this.stop_minute = stop_minute;
	this.stop_second = stop_second;	

}