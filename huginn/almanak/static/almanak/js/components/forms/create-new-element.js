var createNewElement = Vue.component('create-new-element', {
	template: "\
		<overlay grid-class=\"tome-grid\">\
			<div style='grid-column: 1 / 4;'>\
				<h2 class='ink'>Create new {{ chosenType !== null ? chosenType : 'page' }}</h2>\
				<h3 class=\"rubrik\">for <a :href=\"'/almanak/element/' + getCampaignId + '/'\" class=\"ink\" target=\"_blank\">{{ getCampaignName }}</a> \
				in <a href=\"/almanak/table-of-content/\" class=\"ink\" target=\"_blank\" >{{ getBestiaryName }}</a></h3>\
			</div>\
			<div style=\"grid-column:4 / 5;text-align:right\">\
				<button @click=\"closeMenu()\" class=\"button--close-modal\" >&times;</button>\
		  	</div>\
			<div>\
				<div :class='hasActiveClass(\"individual\")' class='create-menu__point' @click='chooseType(\"individual\",\"entitet\",\"individ\",\"individ\")'><img class='icon--bookmark' :src=\"staticUrl + 'images/icons/type/rubrik/individ.svg'\" /><h2 class='rubrik' >Individual</h2></div>\
				<div :class='hasActiveClass(\"group\")' class='create-menu__point' @click='chooseType(\"group\",\"entitet\",\"gruppe\",\"gruppe\")'><img class='icon--bookmark' :src=\"staticUrl + 'images/icons/type/rubrik/gruppe.svg'\" /><h2  class='rubrik' >Group</h2></div>\
				<div :class='hasActiveClass(\"place\")' class='create-menu__point' @click='chooseType(\"place\",\"sted\",\"sted\",\"sted\",)'><img class='icon--bookmark' :src=\"staticUrl + 'images/icons/type/rubrik/sted.svg'\" /><h2 class='rubrik' >Place</h2></div>\
				<div :class='hasActiveClass(\"event\")' class='create-menu__point' @click='chooseType(\"event\",\"begivenhed\",\"begivenhed\",\"begivenhed\")'><img class='icon--bookmark' :src=\"staticUrl + 'images/icons/type/rubrik/begivenhed.svg'\" /><h2 class='rubrik' >Event</h2></div>\
				<div :class='hasActiveClass(\"rpg_session\")' v-if='isGm' class='create-menu__point' @click='chooseType(\"rpg_session\",\"begivenhed\",\"rpg_session\",\"rpg_session\")'><img class='icon--bookmark' :src=\"staticUrl + 'images/icons/type/rubrik/rpg_session.svg'\" /><h2  class='rubrik' >Session</h2></div>\
				<div :class='hasActiveClass(\"item\")' class='create-menu__point' @click='chooseType(\"item\",\"artefakt\",\"artefakt\",\"artefakt\")'><img class='icon--bookmark' :src=\"staticUrl + 'images/icons/type/rubrik/artefakt.svg'\" /><h2 class='rubrik'>Item</h2></div>\
			</div>\
			<div style='grid-column: 2/4;'>\
				<h2 class='rubrik' v-if='chosenType === null'>Choose type</h2>\
				<create-new-form :type='chosenType' v-if='chosenType !== null' ></create-new-form>\
			</div>\
			<div style='text-align:right;grid-column:4 / 5'>\
				<p v-if='createData.isCreating === true' >Creating Page</p>\
				<button @click='createNewElement()' :disabled='!createDataValidated'>Create New </button>\
				<a v-if='createData.hasCreated === true' :href=\"'/almanak/element/' + createData.id\" >Go to new page</a>\
			</div>\
		</overlay>\
	",
	data: function () {
		return  {
			chosenType: null,
		}
	},
	methods: {
		createNewElement: function () {
			this.$store.dispatch("createNewElement",this.chosenType);
		},
		closeMenu: function () {
			this.$store.commit("activateEditor",null);			
		},
		chooseType: function (type,element_type,type_grund,type_spec) {
			Vue.set(this,"chosenType",type);
			this.$store.commit("setElementTypes",{
				element_type: element_type,
				type_grund: type_grund,
				type_spec: type_spec,
			});
		},
		hasActiveClass(type) {
			return type === this.chosenType ? "create-menu__point--active" : "";
		}
	},
	computed: {
		staticUrl: function (){
			return this.$store.state.generic.staticUrl;
		},
		isGm: function (){
			return this.$store.getters.isUserGamemasterOfCampaign(this.$store.state.generic.session.base.campaignId);
		},
		createDataValidated: function (){
			return this.$store.getters['validateCreateNewElementData'];
		},
		publicSecret: function (){
			return this.createData.secret === true ? 'secret' : 'public';
		}, 
		createData: function (){
			return this.$store.state.create;
		},
		getBestiaryName: function () {
			return this.$store.state.generic.session.full.universe_name
		},
		getCampaignName: function () {
			return this.$store.state.generic.session.full.campaign_full.present_name.name;
		},	
		getCampaignId: function () {
			return this.$store.state.generic.session.full.campaign_full.id;
		},					
	}
});