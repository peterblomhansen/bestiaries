# from .core.ingametime import
#from almanak.models import Campaign, Age
from almanak.core.time import TimePoint, Interval
from almanak.core.objectifier import SessionHelper
import random
from django.utils.html import strip_tags
import re
from bs4 import BeautifulSoup, Tag, NavigableString
from django.template import RequestContext
from django.http import HttpRequest
from django.apps import apps
from django.db.models import Q
#from almanak.core.ingame_helper import SessionManager
#from almanak.api.serializers import CampaignSerializer

#req = HttpRequest()

class PresentName:

	def __init__(self,obj,session = None):

		self.names = obj.names.filter(deleted=False)
		self.session = session

		#sm = SessionManager(session)
		sh = SessionHelper
		#print(sh.ingame_now_timepoint(session))
		# print(session.get("campaign_full").items())
		#serializer = CampaignSerializer(session.get("campaign_full"))
		#print(serializer.data)


class ChooseName:
	def __init__(self,obj,request = None):

		self.session = request.session

		Age = apps.get_model(app_label='almanak', model_name='Age')
		Campaign = apps.get_model(app_label='almanak', model_name='Campaign')
		#print(session)
		#    campaign_current_id = self['request'].session.get("campaign_current")
		if self.session != None and self.session != 0:
			sh = SessionHelper#cc = Campaign.objects.get(pk=campaign_current)
			self.now = sh.ingame_now_timepoint(self.session) #TimePoint(cc.ingame_now_age,cc.ingame_now_year,cc.ingame_now_month,cc.ingame_now_date,cc.ingame_now_hours,cc.ingame_now_minutes,cc.ingame_now_seconds)
		else:
			self.now = TimePoint(None,0,0,0,0,0,0)

#		if obj.birth_age and obj.birth_year and obj.birth_month and obj.birth_date and obj.birth_hours and obj.birth_minutes and obj.birth_seconds:
		self.ele_birth = TimePoint(obj.birth_age,obj.birth_year,obj.birth_month,obj.birth_date,obj.birth_hour,obj.birth_minute,obj.birth_second)
#		else:
#			self.ele_birth = TimePoint(None,0,0,0,0,0,0)

		if obj.death_age and obj.death_year and obj.death_month and obj.death_date and obj.death_hour and obj.death_minute and obj.death_second:
			self.ele_death = TimePoint(obj.death_age,obj.death_year,obj.death_month,obj.death_date,obj.death_hour,obj.death_minute,obj.death_second)
		else:
			self.ele_death = TimePoint(None,0,0,0,0,0,0)

		self.names = obj.names.filter(Q(deleted=False),
								Q(secret=False)|
 								Q(secret=True,author=request.user)|
								Q(secret=True,shared_with__in=[request.session.get("role_current")])|
								Q(secret=True,element__campaigns__gamemasters__in=[request.user.id])|
								Q(secret=True,element__id__in=request.user.player_characters.get_queryset())
								).distinct()


	def get_present_name_str(self):

		n_id = self.get_present_name()
		n = self.names.filter(pk=n_id)
		if n and n[0].name:
			return n[0].name
		return "Title not public or allowed"

	def get_present_name(self):

		birth_to_now = Interval(self.ele_birth,self.now)
		death_to_now = Interval(self.ele_death,self.now)

		if self.now.age == None and len(self.names.filter(name_type='prim')) > 0:
			return self.choose_name_simple()

		elif self.now.age != None and len(self.names.filter(name_type='prim')) > 0:
			if len(self.names.filter(name_type="prim")) > 0:
				if self.ele_birth.age == None and self.ele_death.age == None:
					return self.choose_name_simple()
				elif self.ele_birth.age != None and birth_to_now.latest() == 1:
					return self.choose_name_simple()
				elif self.ele_death.age != None and death_to_now.latest() == 2:
					return self.choose_name_simple()
				else:
					return self.choose_name_complex()
			second_choice = self.names.all()[0]
			#second_choice = random.choice(self.names.all())
			return second_choice.id

		return 0



	def choose_name_simple(self):

		#print(self.names.__dict__)

		prim_names = self.names.filter(name_type='prim')

		chosen_name = prim_names[0]
		#chosen_name = random.choice(prim_names)
		return chosen_name.id



	def choose_name_complex(self):

		chosen_names = []
		pot_names = self.names.filter(name_type="prim")

		if len(pot_names) > 0:
			for n in self.names.filter(name_type="prim"):

				n_start = TimePoint(n.start_age,n.start_year,n.start_month,n.start_date,n.start_hour,n.start_minute,n.start_second)
				n_end = TimePoint(n.end_age,n.end_year,n.end_month,n.end_date,n.end_hour,n.end_minute,n.end_second)

				start_to_now = Interval(n_start,self.now)
				end_to_now = Interval(n_end,self.now)

				if n_start.age == None and n_end.age == None:
					chosen_names.append(n)
				elif n_start.age == None and end_to_now.latest() == 1:
					chosen_names.append(n)
				elif n_end.age == None and start_to_now.latest() == 2:
					chosen_names.append(n)
				elif n_start.age != None and start_to_now.latest() == 2 and n_end.age != None and end_to_now.latest() == 1:
					chosen_names.append(n)

			if len(chosen_names) > 0:
				chosen_name = chosen_names[0] #random.choice(chosen_names)
				return chosen_name.id
			else:
				return 0

		else:
			return 0




class DescriptionContent:

	def __init__(self,obj):

	    self.content = obj.content


	def description_rest_complex(self):

		rest_complex = self.content

		if self.validate_content() == True:
			soup = BeautifulSoup(rest_complex, 'html.parser')

			return soup.prettify()
		
		return ""


	def strip_tags(self,html,invalid_tags):
		soup = BeautifulSoup(html, 'html.parser')

		for tag in soup.findAll(True):
			if tag.name in invalid_tags:
				s = ""

				for c in tag.contents:
					if not isinstance(c, NavigableString):
						c = self.strip_tags(str(c), invalid_tags)
					s += str(c)

				tag.replaceWith(s)

		return soup



	def validate_content(self):

		if self.content:
			stripped = strip_tags(self.content)

			if len(stripped) > 0:
			     return True
			else:
			     return False
		return False

	def tag_start(self,tag):
		check_len = len(tag) + 1
		only_links = self.cleanhtml_keep_tag(self.content, tag)
		if only_links[:check_len] == "<" + tag:
			return True
		else:
			return False



	def cleanhtml_keep_tag(self,raw_html, tagname):
	    cleanr = re.compile('<(?!\/?' + tagname + '(?=>|\s.*>))\/?.*?>')
	    cleantext = re.sub(cleanr, '', raw_html)
	    return cleantext

	def clean_tags(html):
		REMOVE_ATTRIBUTES = [
		'lang','language','onmouseover','onmouseout','script','style','font',
		'dir','face','size','color','style','class','width','height','hspace',
		'border','valign','align','background','bgcolor','text','link','vlink',
		'alink','cellpadding','cellspacing']

		#doc = '''<html><head><title>Page title</title></head><body><p id="firstpara" align="center">This is <i>paragraph</i> <a onmouseout="">one</a>.<p id="secondpara" align="blah">This is <i>paragraph</i> <b>two</b>.</html>'''
		soup = BeautifulSoup.BeautifulSoup(html)
		for tag in soup.recursiveChildGenerator():
			try:
				tag.attrs = [(key,value) for key,value in tag.attrs
							if key not in REMOVE_ATTRIBUTES]
			except AttributeError: 
				# 'NavigableString' object has no attribute 'attrs'
				pass
		return soup.prettify() 