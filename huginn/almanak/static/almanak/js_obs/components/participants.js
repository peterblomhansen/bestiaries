"use strict";
var participants = Vue.component('participants', {
    template: "\
        <div>\
            <h3 class=\"rubrik\" >Participants <strong class=\"cursor--pointer\" v-if=\"masterdata.user_permissions.write == true\" @click=\"addParticipants = !addParticipants\">+</strong></h3>\
            <div v-if=\"addParticipants == true\">\
                <p class=\"rubrik\">Add groups and individuals to <strong class=\"ink\">{{ masterdata.present_name }}</strong></p>\
                <autocomplete parent-component=\"participants\" :action-url=\"'entityeventrelation/create/'\" :filter=\"'entitet'\" :button-method=\"'createRelation'\" :button-text=\"'Add as participant'\" >\
                </autocomplete>\
            </div>\
            <div v-for=\"p in participantsSpec\">\
                <p><a class=\"ink\" :href=\"'/almanak/side/' + p.entity.id\" >{{ p.entity.present_name }}</a>\
                    <strong @click=\"p.entity.delete_question = !p.entity.delete_question\" class=\"rubrik cursor--pointer\">&times;</strong>\
                </p>\
                <div v-if=\"p.entity.delete_question == true\">\
                    <p>Do you want to remove {{ p.entity.present_name }} as a participant in {{ masterdata.present_name }}? <br/>\
                        <button @click=\"removeParticipant(p.id)\" >Yes</button> <button @click=\"p.entity.delete_question = !p.entity.delete_question\" >No</button>\
                    </p>\
                </div>\
            </div>\
        </div>\
    ",
    data: function() {
        return {
            addParticipants: false,
        }
    },
    methods: {
        removeParticipant: function(id) {
                var data = {
                    "id": id,
                    "view": "entityeventrelation",
                    "updateObject": {
                        "deleted": true,
                    }

                };
                console.log(data);
                this.$store.dispatch("updateRelation",data);
        },

    },
    computed: {
        participants: function(){
            return this.$store.state.element.relations.participants;
        },
		participantsSpec: function (){
			if(this.$store.state.element.relations.participants) {
				return this.$store.state.element.relations.participants.filter(function(p){
						return p.entity.type_grund != "player_character";
				});
			}else {
				return this.participants;
			}
		},
        masterdata: function(){
            return this.$store.state.element.masterdata;
        },
    }
});
