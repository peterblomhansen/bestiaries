var gender = Vue.component('gender', {
    template: "\
    <div>\
      <p class=\"rubrik\">\
        Gender:\
        <span class=\"ink\" v-if=\"element.gender == null\">Unknown</span>\
        <span class=\"ink\" v-else >{{ element.gender }}</span>\
        <img v-if=\"element.user_permissions.write == true\" style=\"height:1em;cursor:pointer;\" :src=\"st + 'images/icons/pencil.svg'\" @click=\"editGender = !editGender\" />\
      </p>\
      <div v-if=\"editGender == true\">\
        <p ><strong class=\"rubrik\">Edit Gender </strong> <span class=\"ajax__update-text\">{{ editGenderAjaxStatus }}</span> {{ element.gender }}</p>\
        <p class=\"ink\" >\
          <input type=\"radio\" id=\"one\" value=\"Female\" v-model=\"element.gender\" @click=\"updateGender('Female')\">\
          Female\
        </p>\
        <p class=\"ink\">\
          <input type=\"radio\" id=\"two\" value=\"Male\" v-model=\"element.gender\" @click=\"updateGender('Male')\">\
          Male\
        </p>\
        <p class=\"ink\">\
          <input type=\"radio\" id=\"three\" :value=\"null\" v-model=\"element.gender\" @click=\"updateGender(null)\">\
          Unknown\
\
        </p>\
      </div>\
    </div>\
    ",
    props: {
        //gender: String,
        //dataLoaded: Boolean,
        //element: Object,
        //st: String,
        //isEditor: Boolean,
    },
    data: function() {
        return {
            editGender: false,
            editGenderAjaxStatus: "not changed"
        }
    },
    computed: {
        element: function(){
            return this.$store.state.element.masterdata;
        },
        st: function(){
            return this.$store.state.initial.staticUrl;
        },
		characterSheetLink: function (){
			var uni = this.$store.state.session.universe.slug;
			var elId = this.$store.state.element.id;

			return "https://" + uni + ".bestiaries.org/deltager/charactersheet/" + elId + "/100/";
		}
    },
    methods: {
        updateGender: function(gender) {
            this.editGenderAjaxStatus = "updating";
            this.updateGenderAjax(this.element,gender);
        },
        updateGenderAjax: function(el,gender) {
            var self = this;
			console.log(el.gender);
            axios.patch('/api/individual/' + el.id + '/update/', {
                gender: gender
            }).then(function(response) {
                console.log(response.data)
                self.editGenderAjaxStatus = "updated";
                //self.$store.commit("GetElement");
                //eventHub.$emit('reloadData');
            }).catch(function(error) {
                console.log(error);
                if (error.response.status == 403) {
                    //self.names[i].ajax_status = "permission denied: no changes was written to the Bestiary";
                }
            });
        },
    }
});
