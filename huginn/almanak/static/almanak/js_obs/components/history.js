var history = Vue.component('history', {
    template:"\
        <div>\
            <h3 class=\"rubrik\">History <span class=\"cursor--pointer\" v-if=\"masterdata.user_permissions.write == true\" @click=\"addHistory = !addHistory\">+</span></h3>\
                <div v-if=\"addHistory == true\">\
                    <autocomplete parent-component=\"history\" :action-url=\"'eventplacerelation/create/'\" :filter=\"'begivenhed'\" :button-method=\"'createRelation'\" :button-text=\"'Add to History'\" >\
                    </autocomplete>\
                </div>\
                <div v-for=\"h in history\">\
                    <p ><a :href=\"'/almanak/side/' + h.event.id\" class=\"ink\">{{ h.event.present_name }}</a>\
                        <span v-if=\"masterdata.user_permissions.write == true\" @click=\"h.event.delete_question = !h.event.delete_question\" >&times;</span>\
                    </p>\
                    <div>\
                        <p v-if=\"h.event.delete_question == true\" >Do you want to remove {{h.event.present_name}} as a part of the history of {{masterdata.present_name}}?\
                        <button @click=\"deleteRelation(h.id)\" >Yes</button> <button @click=\"h.delete_question = false\" >No</button>\
                        </p>\
                    </div>\
                </div>\
        </div>\
    ",
    data: function() {
        return {
            hello: "Hello World",
            addHistory: false,
        }
    },
    methods: {
        deleteRelation: function(id){
            var data = {
                "id": id,
                "view": "eventplacerelation",
                "updateObject": {
                    "deleted": true,
                }

            };
            this.$store.dispatch("updateRelation",data);

        },
    },
    computed: {
        history: function(){
            return this.$store.state.element.relations.history;
        },
        masterdata: function (){
            return this.$store.state.element.masterdata;
        },
    },
});
