from django.apps import apps
from almanak.core.time import TimePoint
#import almanak.models as almanak_models
#from almanak.models import Universe, Age



class SessionHelper:

	def ingame_now_timepoint(session):

		if session == 0:
			return TimePoint(None,0,0,0,0,0,0)
		else:
			Age = apps.get_model(app_label='almanak', model_name='Age')
			Campaign = apps.get_model(app_label='almanak', model_name='Campaign')
			Universe = apps.get_model(app_label='almanak', model_name='Universe')

			campaign = session.get("campaign_full")

			if campaign:
				u = campaign['universe_full']
				a = campaign['ingame_now_age']
				if a:
					u_obj = Universe(id = u['id'], name = u['name'], slug = u['slug'])
					ingame_now_age = Age(id = a['id'], num_years = a['num_years'], initials =  a['initials'], name =  a['name'], num_months = a['num_months'], age_number = a['age_number'], universe = u_obj)
					ig_tp = TimePoint(ingame_now_age,campaign['ingame_now_year'],campaign['ingame_now_month'],campaign['ingame_now_date'],0,0,0)

					return ig_tp

				return TimePoint(None,0,0,0,0,0,0)
					

			else:
				return TimePoint(None,0,0,0,0,0,0)
