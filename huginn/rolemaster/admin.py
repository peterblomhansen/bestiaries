from django.contrib import admin
from rolemaster.models import (
		Stats,
		StatBonusDistribution,
		StatSpecialBonus,
		RolemasterCharacter,
		Skill,
		SkillArea,
		SkillAreaType,
		SkillAreaSimilarity,
		SkillSkillSimilarity,
		CharacterWeaponCategoryPriority,
		SkillMiscBonus,
		Profession,
		ProfessionLevelBonus,
		SkillComment
)

class RolemasterCharacterAdmin(admin.ModelAdmin):
	pass

class StatsAdmin(admin.ModelAdmin):
	pass


class StatSpecialBonusAdmin(admin.ModelAdmin):
	pass


class StatBonusDistributionAdmin(admin.ModelAdmin):
	pass

class SkillAreaAdmin(admin.ModelAdmin):
	pass

class SkillAreaSimilarityAdmin(admin.ModelAdmin):
	pass


class SkillSkillSimilarityAdmin(admin.ModelAdmin):
	pass

class CharacterWeaponCategoryPriorityAdmin(admin.ModelAdmin):
	pass

class SkillMiscBonusAdmin(admin.ModelAdmin):
	pass


class SkillAreaTypeInline(admin.StackedInline):
    model = SkillAreaType
    fields = ["skill","area_type"]
    extra = 0

class ProfessionLevelBonusInline(admin.StackedInline):
    model = ProfessionLevelBonus
    fields = ["category","bonus"]
    extra = 0


class ProfessionAdmin(admin.ModelAdmin):
	inlines = [ProfessionLevelBonusInline,]


class SkillAdmin(admin.ModelAdmin):
	inlines = [SkillAreaTypeInline,]

class ProfessionLevelBonusAdmin(admin.ModelAdmin):
	pass


class SkillCommentAdmin(admin.ModelAdmin):
	pass


admin.site.register(RolemasterCharacter, RolemasterCharacterAdmin)
admin.site.register(SkillArea, SkillAreaAdmin)
admin.site.register(Skill, SkillAdmin)
admin.site.register(SkillComment, SkillCommentAdmin)
admin.site.register(SkillAreaSimilarity, SkillAreaSimilarityAdmin)
admin.site.register(SkillSkillSimilarity, SkillSkillSimilarityAdmin)
admin.site.register(CharacterWeaponCategoryPriority, CharacterWeaponCategoryPriorityAdmin)
admin.site.register(SkillMiscBonus, SkillMiscBonusAdmin)
admin.site.register(Profession, ProfessionAdmin)
admin.site.register(ProfessionLevelBonus, ProfessionLevelBonusAdmin)
admin.site.register(StatBonusDistribution, StatBonusDistributionAdmin)
admin.site.register(Stats, StatsAdmin)
admin.site.register(StatSpecialBonus, StatSpecialBonusAdmin)
