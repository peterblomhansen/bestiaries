var ownedBy = Vue.component('owned-by', {
	template: "\
	<div>\
		<h3 class=\"rubrik\">Owned by ({{ownedBy.count}})</h3>\
		<p class=\"rubrik\" v-for=\"u in ownedBy.results\"><a class=\"ink\" :href=\"'/almanak/side/' + u.owner_full.id\" >{{ u.owner_full.present_name.name }}</a> {{ u.owner_full.type_spec }}</p>\
	</div>\
	",
	data: function() {
		return {

		}
	},
	methods: {

	},
	computed: {
		ownedBy: function(){
			if(this.$store.state.element.relations.ownedBy) {
				return this.$store.state.element.relations.ownedBy;
			}else {
				return {}
			}            
		}

	}
});
