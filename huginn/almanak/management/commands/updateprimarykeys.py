from django.core.management.base import BaseCommand, CommandError
from django.db import connections
from collections import namedtuple
from django.contrib.auth.models import AbstractUser
from almanak.models import Description, Element


class Command(BaseCommand):
	help = 'Updates the Primary keys after Data Migration'

	def handle(self, *args, **options):



		dbnew = connections['default'].cursor()


		def update_next_key_of_table(table):

			#q = dbnew.execute("""SELECT id FROM %s ORDER BY id DESC LIMIT 1""",(str(table)))
			dbnew.execute("SELECT id FROM " + table + " ORDER BY id DESC LIMIT 1;")

			if dbnew.rowcount == 1:
				pk = dbnew.fetchall()[0][0] + 1

				dbnew.execute("ALTER SEQUENCE " + table + "_id_seq RESTART WITH " + str(pk) + ";")

				self.stdout.write(self.style.WARNING("primary keys for " + table + " updated to " + str(pk)))


		self.stdout.write(self.style.WARNING("Updating primary keys"))

		update_next_key_of_table("almanak_age")
		update_next_key_of_table("almanak_bestiariesuser")
		update_next_key_of_table("almanak_campaigndescription")
		update_next_key_of_table("almanak_campaignelement")
		update_next_key_of_table("almanak_description")
		update_next_key_of_table("almanak_description_shared_with")
		update_next_key_of_table("almanak_descriptionelementrelation")
		update_next_key_of_table("almanak_element_editors")
		update_next_key_of_table("almanak_element")
		update_next_key_of_table("almanak_element_shared_with")
		update_next_key_of_table("almanak_elementitemrelation")
		update_next_key_of_table("almanak_elementrelation")
		update_next_key_of_table("almanak_elementuniqueitemrelation")
		update_next_key_of_table("almanak_entityeventrelation")
		update_next_key_of_table("almanak_eventplacerelation")
		update_next_key_of_table("almanak_grouptype")
		update_next_key_of_table("almanak_image_campaigns")
		update_next_key_of_table("almanak_image")
		update_next_key_of_table("almanak_image_shared_with")
		update_next_key_of_table("almanak_imageelementrelation")
		update_next_key_of_table("almanak_name")
		update_next_key_of_table("almanak_relationtitle")
		update_next_key_of_table("almanak_rpgelement_gamemasters")
		update_next_key_of_table("almanak_universe")
		update_next_key_of_table("almanak_universemembership")


		update_next_key_of_table("rolemaster_armortype")
		update_next_key_of_table("rolemaster_bodydevelopment")
		update_next_key_of_table("rolemaster_characterweaponcategorypriority")
		update_next_key_of_table("rolemaster_developedskill")
		update_next_key_of_table("rolemaster_profession")
		update_next_key_of_table("rolemaster_professionlevelbonus")		
		update_next_key_of_table("rolemaster_selectedskill")
		update_next_key_of_table("rolemaster_skill")
		update_next_key_of_table("rolemaster_skillarea")
		update_next_key_of_table("rolemaster_skillareasimilarity")
		update_next_key_of_table("rolemaster_skillareatype")
		update_next_key_of_table("rolemaster_skillmiscbonus")
		update_next_key_of_table("rolemaster_skillskillsimilarity")
		update_next_key_of_table("rolemaster_statbonusdistribution")
		update_next_key_of_table("rolemaster_stats")

		self.stdout.write(self.style.WARNING("Primary keys Updated"))
