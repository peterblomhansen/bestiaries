var generatePdf = Vue.component('generate-pdf', {
	template: "\
	<div  >\
		<button v-if=\"levelReady > -1\" @click=\"generatePdf()\" >Generate PDF for level {{ levelReady }}</button>\
		<page-loading v-if=\"PDFstarted == true\" text=\"GENERATING PDF\" >\
		</page-loading>\
	</div>\
		",
    data: function() {
		return {
			PDFstarted: false,
		}
    },
	mounted: function (){
	},
    methods: {
		generatePdf: function (level) {
			this.PDFstarted = true;
			location.href = "/rm/charactersheet/" + individualId + "/" + this.levelReady + "/pdf/";

		},
		// MOVED TO $store.getters
		// getNearestValidatedLevel: function (level) {
		// 	var self = this;
		// 	var lvlInt = parseInt(level);
		//
		// 	function valid(lvlInt)  {
		// 		var vl = false;
		// 		function statsPresent(l,type){
		// 			var found = self.statsAll.filter(function(s) {
		// 				return s.level == l && s.stat_type == type;
		// 			})
		// 			return found.length;
		//
		// 		}
		// 		var temp = statsPresent(lvlInt,"temp");
		// 		var pot = statsPresent(lvlInt,"pot");
		// 		if(temp > 0 && pot > 0) {
		// 			vl = lvlInt
		// 		}
		//
		// 		return vl;
		// 	}
		//
		// 	if(valid(lvlInt)) {
		// 		return valid(lvlInt);
		// 	}else {
		// 		return this.getNearestValidatedLevel(lvlInt - 1);
		// 	}
		//
		//
		// },
	},
	computed: {
		statsAll: function(){
				return this.$store.state.character.stats;
		},
		level: function(){
			return this.$store.state.initial.level;
		},
		levelReady: function (){

			return this.$store.getters.getNearestValidatedLevel(this.level.viewed);

		},


	}
});
