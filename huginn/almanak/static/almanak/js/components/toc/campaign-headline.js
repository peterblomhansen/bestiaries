var campaignHeadline = Vue.component('campaign-headline', {
	template: "\
		<div style='grid-column: 1 / 3;text-align:center' v-if='campaign !== undefined'>\
			<h3 class='rubrik' >Campaign</h3>\
			<h1 class='ink' style='font-size:3rem;margin-bottom:1rem;' >{{ campaign.present_name.name }}</h1>\
		</div>\
	",
	computed: {
        campaign: function (){
            return this.$store.state.generic.session.full.campaign_full;
		},
	}
});