# Bestiaries - Almanak

This is the core module of the Bestiaries Web Application.

## Bestiaries

Bestiaries is a web application to manage a fictional world.

It is designed towards gamemasters and players of roleplaying games, that wants to have one place where the world around their game can develop over time.

Bestiaries makes this possible through the ability to create places, events, groups and indviduals and furthermore describe them and most important relate everything to eachother.

Bestiaries is a Django Project that makes heavy use of The Django Rest Framework to power the Vue.js frontend.

## The Almanak   

The Bestiaries Almanak this the Django App, that hold the core functionality of the whole Bestiaries Project. Its is RPG system agnostic so that it is possible to integrate any RPG system if the given module is compatible with the Almanak.

### The Almanak solves two central task in building fiction:

1. Describing characters, places and events
2. Relating characters to eachother as well as relating them to places and events.

### The Almanak Data Model:

Technically The Almanak solves the above by a simple but dynamic data model heavily reliant on inheritance. There is two types of content

1. Element (Page)
2. Description

#### Element

The Element represents a page in a given bestiary (world/universe). The element has basic information such as names and at what times the element exists. Also it has information on which other elements, it is related to and how. The Almanak application utilizes many ways to relate elements to eachother, both elements of the same type and elements of different types. This makes it a very flexible tool as the user can themselves decide on how they want to structure their universe including expanding adn changing this structure. The types of elements currently supported by the Almanak is listed below:

* Element
  * Entity
    * Group
	  * Kin
    * Individual
      * Player Character
  * Event
    * Campaign
    * Roleplaying Session
  * Place

#### Description   

Descriptions are texts based or images based prose that describes on or more elements from a certain angel or perspective, that can be either an person outside the fiction (user, player, gamemaster) or a someone inside the fiction (a group or individual).

* Description
* Image



## Documentation



### Backend and API

The backend logic of the Almanak API is built upon standards of Django. Futhermore it makes heavy use of The Django Rest Framework Library for converting Python Objects to JSON Object and the Django Filter Library Rest Framework extension to enable dynamic filtering through url parameters.

Here is some other important core guidelines
* All list are paginated browsable


### Frontend

The frontend and user interface of the Almanak Application is served by javascript built upon the Vue.js framework.

It take advantages of the vuex library manages state of the user interface. With a central $store every type of page works as a "one page application" built from components which data all refer to the vuex $store.



