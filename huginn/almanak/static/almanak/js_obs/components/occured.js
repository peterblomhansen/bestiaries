var occured = Vue.component('occured', {
    template:"\
    <div>\
        <p class=\"rubrik\"><strong>Occured:</strong> <span class=\"cursor--pointer\" v-if=\"masterdata.user_permissions.write == true\" @click=\"addOccured = !addOccured\" >+</span></p>\
        <div v-if=\"addOccured == true\">\
            <p class=\"rubrik\">Add a place where <strong class=\"ink\">{{ masterdata.present_name}}</strong> occured</p>\
            <autocomplete parent-component=\"occured\" :action-url=\"'eventplacerelation/create/'\" :filter=\"'sted'\" :button-method=\"'createRelation'\" :button-text=\"'Add to Occured'\" >\
            </autocomplete>\
        </div>\
        <div v-for=\"o in occured\">\
            <p>\
                <a class=\"ink\" :href=\"'/almanak/side/' + o.place.id\">- {{ o.place.present_name }}</a>\
                <span v-if=\"masterdata.user_permissions.write == true\" class=\"cursor--pointer\" @click=\"o.place.delete_question = !o.place.delete_question\" >&times;</span>\
            </p>\
            <div v-if=\"o.place.delete_question == true\">\
                <p>Do you want to remove {{ o.place.present_name }} as a place where {{ masterdata.present_name }} occured? <br/><button @click=\"removeOccured(o.id)\" >Yes</button> <button @click=\"o.place.delete_question = !o.place.delete_question\"  >No</button></p>\
            </div>\
        </div>\
    </div>\
    ",
    data: function() {
        return {
            addOccured: false,
        }
    },
    methods: {
        removeOccured: function(id){
            var data = {
                "id": id,
                "view": "eventplacerelation",
                "updateObject": {
                    "deleted": true,
                }

            };
            this.$store.dispatch("updateRelation",data);

        },

    },
    computed: {
        occured: function(){
            return this.$store.state.element.relations.occured;
        },
        masterdata: function(){
            return this.$store.state.element.masterdata;
        },
    }
});
