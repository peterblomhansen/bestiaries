from django.core.management.base import BaseCommand, CommandError
from django.db import connections
from collections import namedtuple
from almanak.models import BestiariesUser, Element, Universe, Age, Event, Description, Image
import datetime

class Command(BaseCommand):
	help = 'Closes the specified poll for voting'
	def handle(self, *args, **options):
		def namedtuplefetchall(cursor):
			#"Return all rows from a cursor as a namedtuple"
			desc = cursor.description
			nt_result = namedtuple('Result', [col[0] for col in desc])
			return [nt_result(*row) for row in cursor.fetchall()]


		def importElements():

			crsr = connections['legacy'].cursor()

			crsr.execute("SELECT * FROM element")

			elements = namedtuplefetchall(crsr)
			# e = el[0]
			for e in elements:

				u = Universe.objects.get(pk=e.id_univers)
				b = BestiariesUser.objects.get(pk=1)
				if e.forfatter:
					b = BestiariesUser.objects.get(pk=e.forfatter)

				crtd = datetime.datetime.fromtimestamp(float(e.created))
				pdtd = datetime.datetime.fromtimestamp(float(e.updated))
				h = False
				d = False

				if e.id_birth_alder == 0 or not Age.objects.filter(pk=e.id_birth_alder).exists():
					age_birth = None
				else:
					age_birth = Age.objects.get(pk=e.id_birth_alder)

				if e.id_death_alder == 0 or not Age.objects.filter(pk=e.id_death_alder).exists():
					age_death = None
				else:
					age_death = Age.objects.get(pk=e.id_death_alder)


				b_kl = e.birth_klokken
				d_kl = e.death_klokken
				if b_kl is None:
					b_kl = "00:00:00"
				if d_kl is None:
					d_kl = "00:00:00"

				if e.hemmelig == 3:
					h = True
				else:
					h = False
				if e.slettet == 1:
					d = True

				new = Element(
					pk = e.id,
					universe = u,
					created = crtd,
					updated = pdtd,
					author = b,
					secret = h,
					deleted = d,
					element_type = e.type,
					type_grund = e.type_grund,
					type_spec = e.type_spec,
					birth_age = age_birth,
					birth_year = e.birth_aar,
					birth_month = e.birth_maaned,
					birth_date = e.birth_dag,
					birth_hours = str(b_kl).split(":")[0],
					birth_minutes = str(b_kl).split(":")[1],
					birth_seconds = str(b_kl).split(":")[2],
					death_age = age_death,
					death_year = e.death_aar,
					death_month = e.death_maaned,
					death_date = e.death_dag,
					death_hours = str(d_kl).split(":")[0],
					death_minutes = str(d_kl).split(":")[1],
					death_seconds = str(d_kl).split(":")[2],
				)
				new.save()

			print(str(len(elements)) + " Elements Imported")



		def importEvents():
			dbleg = connections['legacy'].cursor()
			dbnew = connections['default'].cursor()
			dbleg.execute("SELECT * FROM begivenhed")

			events = namedtuplefetchall(dbleg)
			for b in events:

				dbnew.execute("""INSERT INTO almanak_event (element_ptr_id,event_type) VALUES (%s,%s)""",(b.id,b.type_rpg))

			print(str(len(events)) + " Events Imported")


		def importRpgElements():
			dbleg = connections['legacy'].cursor()
			dbnew = connections['default'].cursor()
			dbleg.execute("SELECT * FROM rpg_element")

			rpg_elements = namedtuplefetchall(dbleg)
			for r in rpg_elements:
				start = datetime.datetime.fromtimestamp(float(r.offgame_start))
				slut = datetime.datetime.fromtimestamp(float(r.offgame_slut))
				dbnew.execute("""INSERT INTO almanak_rpgelement (event_ptr_id,offgame_start,offgame_end,rpg_type) VALUES (%s,%s,%s,%s)""",(r.id,start,slut,r.type_form))

			print(str(len(rpg_elements)) + " RPG Elements Imported")


		def importRpgCampaigns():
			dbleg = connections['legacy'].cursor()
			dbnew = connections['default'].cursor()
			dbleg.execute("SELECT * FROM kampagne")

			campaigns = namedtuplefetchall(dbleg)
			for c in campaigns:
				age = c.id_ingame_nu_alder
				if c.id_ingame_nu_alder == 0:
					age = None
				dbnew.execute("""INSERT INTO almanak_campaign (rpgelement_ptr_id,ingame_now_year,ingame_now_month,ingame_now_date,ingame_now_hours,ingame_now_minutes,ingame_now_seconds,ingame_now_age_id) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)""",(c.id,c.ingame_nu_aar, c.ingame_nu_maaned,c.ingame_nu_dag,0,0,0,age))

			print(str(len(campaigns)) + " Campaigns Imported")


		def importRpgSessions():
			dbleg = connections['legacy'].cursor()
			dbnew = connections['default'].cursor()
			dbleg.execute("SELECT * FROM rpg_session")

			sessions = namedtuplefetchall(dbleg)
			for s in sessions:
				cmp = s.kampagne_id
				if s.kampagne_id != 0:
					dbnew.execute("""INSERT INTO almanak_rpgsession (rpgelement_ptr_id,session_number,in_campaign_id) VALUES (%s,%s,%s)""",(s.id,s.nummer,cmp))

			print(str(len(sessions)) + " Roleplaying Sessions Imported")


		def importPlaces():
			dbleg = connections['legacy'].cursor()
			dbnew = connections['default'].cursor()
			dbleg.execute("SELECT * FROM sted")

			places = namedtuplefetchall(dbleg)
			for p in places:
			#    cmp = s.kampagne_id
			#    if s.kampagne_id != 0:
					dbnew.execute("""INSERT INTO almanak_place (element_ptr_id) VALUES (%s)""",[p.id])

			print(str(len(places)) + " Places Imported")

		def importEntities():
			dbleg = connections['legacy'].cursor()
			dbnew = connections['default'].cursor()
			dbleg.execute("SELECT * FROM entitet")

			entities = namedtuplefetchall(dbleg)
			for e in entities:
			#    cmp = s.kampagne_id
			#    if s.kampagne_id != 0:
					dbnew.execute("""INSERT INTO almanak_entity (element_ptr_id,entity_type) VALUES (%s,%s)""",(e.id, e.type_kvant))

			print(str(len(entities)) + " Entities Imported")


		def importGroupTypes():
			dbleg = connections['legacy'].cursor()
			dbnew = connections['default'].cursor()
			dbleg.execute("SELECT * FROM gruppe_type")

			grouptypes = namedtuplefetchall(dbleg)
			for g in grouptypes:
			#    cmp = s.kampagne_id
			#    if s.kampagne_id != 0:
				if g.id_univers != 0:
					dbnew.execute("""INSERT INTO almanak_grouptype (id,name,name_plural,description,universe_id,deleted) VALUES (%s,%s,%s,%s,%s,%s)""",(g.id, g.navn, g.navn_pl, g.beskrivelse, g.id_univers,False))

			print(str(len(grouptypes)) + " GroupTypes Imported")


		def importGroups():
			dbleg = connections['legacy'].cursor()
			dbnew = connections['default'].cursor()
			dbleg.execute("SELECT * FROM gruppe")

			groups = namedtuplefetchall(dbleg)
			for g in groups:
					if g.type_aet == "race":
						is_k = True
					else:
						is_k = False
					gt = g.gruppe_type_id
					if g.gruppe_type_id == 0:
						gt = None

					dbnew.execute("""INSERT INTO almanak_group (entity_ptr_id,is_kin,group_type_id) VALUES (%s,%s,%s)""",(g.id, is_k, gt))

			print(str(len(groups)) + " Groups Imported")


		def importKins():
			dbleg = connections['legacy'].cursor()
			dbnew = connections['default'].cursor()
			dbleg.execute("SELECT id FROM race")

			kins = namedtuplefetchall(dbleg)
			for ki in kins:
			#    cmp = s.kampagne_id
			#    if s.kampagne_id != 0:
					dbnew.execute("""INSERT INTO almanak_kin (group_ptr_id) VALUES (%s)""",[ki.id])

			print(str(len(kins)) + " Kins Imported")


		def importIndividuals():
			dbleg = connections['legacy'].cursor()
			dbnew = connections['default'].cursor()
			dbleg.execute("SELECT * FROM individ")

			individuals = namedtuplefetchall(dbleg)
			for i in individuals:
						r = i.race
						if i.race == 0:
							r = None
						if i.koen == "Kvinde":
							gender = "Female"
						elif i.koen == "Mand":
							gender = "Male"
			#    cmp = s.kampagne_id
			#    if s.kampagne_id != 0:
						dbnew.execute("""INSERT INTO almanak_individual (entity_ptr_id,individual_type,gender,kin_id) VALUES (%s,%s,%s,%s)""",(i.id, i.type_char,gender,r))

			print(str(len(individuals)) + " Individuals Imported")



		def importPlayerCharacters():
			dbleg = connections['legacy'].cursor()
			dbnew = connections['default'].cursor()
			dbleg.execute("SELECT * FROM player_character")

			player_characters = namedtuplefetchall(dbleg)
			for p in player_characters:
			#    cmp = s.kampagne_id
			#    if s.kampagne_id != 0:
						dbnew.execute("""INSERT INTO almanak_playercharacter (individual_ptr_id,roleplayer_id) VALUES (%s,%s)""",(p.id, p.id_deltager))

			print(str(len(player_characters)) + " PlayerCharacters Imported")


		def importItems():
			dbleg = connections['legacy'].cursor()
			dbnew = connections['default'].cursor()
			dbleg.execute("SELECT * FROM artefakt")

			items = namedtuplefetchall(dbleg)
			for a in items:
						if a.type_unika == "unika":
							un = True
						else:
							un = False
			#    cmp = s.kampagne_id
			#    if s.kampagne_id != 0:
						dbnew.execute("""INSERT INTO almanak_item (element_ptr_id,unique_item) VALUES (%s,%s)""",(a.id, un))

			print(str(len(items)) + " Items Imported")


		def importNames():
			dbleg = connections['legacy'].cursor()
			dbnew = connections['default'].cursor()
			dbleg.execute("SELECT * FROM navn")

			names = namedtuplefetchall(dbleg)
			for n in names:
						row = False
						sa = n.id_start_alder
						if n.id_start_alder == 0:
							sa = None
						ea = n.id_slut_alder
						if n.id_slut_alder == 0:
							ea = None
						dbleg2 = connections['legacy'].cursor()
						dbleg2.execute("SELECT id FROM element")
						els = namedtuplefetchall(dbleg2)
						for e in els:
							if e.id == n.id_element:
								row = True

			#    cmp = s.kampagne_id
			#    if s.kampagne_id != 0:
						if row == True:
							dbnew.execute("""INSERT INTO almanak_name (id, name, element_id, secret, name_type, start_age_id, start_year, start_month, start_date,start_hours,start_minutes,start_seconds, end_age_id, end_year, end_month, end_date,end_hours,end_minutes,end_seconds,deleted) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",(n.id, n.navn, n.id_element,False, n.type, sa, n.start_aar, n.start_maaned, n.start_dag,0,0,0,ea , n.slut_aar, n.slut_maaned, n.slut_dag,0,0,0,False))

			print(str(len(names)) + " Names Imported")


		def importDescriptions():
			dbleg = connections['legacy'].cursor()
			dbnew = connections['default'].cursor()
			dbleg.execute("SELECT * FROM beskrivelse")

			descriptions = namedtuplefetchall(dbleg)
			for d in descriptions:
				if d.overskrift != "0" and len(d.overskrift) > 0:
					txt = d.overskrift + "<br/>" + d.tekst
				else:
					txt = d.tekst
				crtd = datetime.datetime.fromtimestamp(float(d.created))
				pdtd = datetime.datetime.fromtimestamp(float(d.updated))
				if d.hemmelig == 3:
					h = True
				else:
					h = False
				if d.slettet == 1:
					rem = True
				else:
					rem = False
				kl = d.ingame_klokken
				if kl is None:
					kl = "00:00:00"
				hours = str(kl).split(":")[0],
				minutes = str(kl).split(":")[1],
				seconds = str(kl).split(":")[2],

				if d.id_ingame_tid_alder == 0:
					age = None
				else:
					age = d.id_ingame_tid_alder

				author = d.id_deltager
				if not d.id_deltager:
					author = 1

				dbnew.execute("""INSERT INTO almanak_description (id,content,description_type,author_id,created,updated,secret,deleted,ingame_age_id, ingame_year,ingame_month, ingame_date,ingame_hours,ingame_minutes,ingame_seconds) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",
					(d.id,txt,d.type_game,author,crtd,pdtd,h,rem,age,d.ingame_tid_aar,d.ingame_tid_maaned,d.ingame_tid_dag,hours,minutes,seconds))


			print(str(len(descriptions)) + " Descriptions Imported")

		def importImages():
			dbleg = connections['legacy'].cursor()
			dbnew = connections['default'].cursor()
			dbleg.execute("SELECT * FROM billede")

			descriptions = namedtuplefetchall(dbleg)
			for d in descriptions:

				crtd = datetime.datetime.fromtimestamp(float(d.created))
				pdtd = datetime.datetime.fromtimestamp(float(d.updated))

				url_str = "gallery/" + d.url

				if d.hemmelig == 3:
					h = True
				else:
					h = False
				if d.slettet == 1:
					rem = True
				else:
					rem = False
				kl = d.ingame_klokken
				if kl is None:
					kl = "00:00:00"
				hours = str(kl).split(":")[0],
				minutes = str(kl).split(":")[1],
				seconds = str(kl).split(":")[2],

				if d.id_ingame_tid_alder == 0:
					age = None
				else:
					age = d.id_ingame_tid_alder


				dbnew.execute("""INSERT INTO almanak_image (id,url,image_type,title,height,width,author_id,created,updated,secret,deleted,ingame_age_id, ingame_year,ingame_month, ingame_date,ingame_hours,ingame_minutes,ingame_seconds) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",
				(d.id,url_str,"off_game",d.titel,d.height,d.width,d.id_deltager,crtd,pdtd,h,rem,age,d.ingame_tid_aar,d.ingame_tid_maaned,d.ingame_tid_dag,hours,minutes,seconds))


			print(str(len(descriptions)) + " Images Imported")


		def importIngameDescriptions():
			dbleg = connections['legacy'].cursor()
			dbnew = connections['default'].cursor()
			dbleg.execute("SELECT * FROM ingame_beskrivelse")

			ig_des = namedtuplefetchall(dbleg)
			for d in ig_des:
						row = False
						dbleg2 = connections['default'].cursor()
						dbleg2.execute("SELECT id FROM almanak_description")
						des = namedtuplefetchall(dbleg2)
						for e in des:
							if e.id == d.id:
								row = True
						forfatter = False
						if d.forfatter != None and d.forfatter != 0:
							forfatter = True

						if row == True and forfatter == True:

							des = Description.objects.get(pk=d.id)
							des.ingame_author_id = d.forfatter
							des.save()
							#dbnew.execute("""UPDATE almanak_description SET ingame_author_id = """ + d.forfatter + """ WHERE id = """ )
							#dbnew.execute("""INSERT INTO almanak_ingamedescription (description_ptr_id,ingame_author_id) VALUES (%s,%s)""",(d.id, d.forfatter))

			print(str(len(ig_des)) + " Ingame Images Imported")

		def importIngameImages():
			dbleg = connections['legacy'].cursor()
			dbnew = connections['default'].cursor()
			dbleg.execute("SELECT * FROM ingame_billede")

			ig_img = namedtuplefetchall(dbleg)
			for d in ig_img:

				forfatter = False
				if d.forfatter != None or d.forfatter != 0:
					forfatter = True
				if forfatter == True:

					img = Image.objects.get(pk=d.id)
					img.ingame_author_id = d.forfatter
					img.save()
					#dbnew.execute("""INSERT INTO almanak_ingameimage (image_ptr_id,ingame_author_id) VALUES (%s,%s)""",(d.id, d.forfatter))

			print(str(len(ig_img)) + " Ingame Images Imported")


		def importRelationTitles():
			dbleg = connections['legacy'].cursor()
			dbnew = connections['default'].cursor()
			dbleg.execute("SELECT * FROM gruppe_type_titel")

			el_tit = namedtuplefetchall(dbleg)
			for t in el_tit:
				# uni_owner = False
				# if u.universejer == 1:
				#     uni_owner = True
				# view = u.type_aktuel
				# if u.type_aktuel == 0:
				#     view = None

				dbnew.execute("""INSERT INTO almanak_relationtitle (id,group_type_id,name,description,name_plural,preposition,deleted) VALUES (%s,%s,%s,%s,%s,%s,%s)""",(t.id,t.id_type,t.navn,t.beskrivelse,t.navn_pl,t.prep,False))

			print(str(len(el_tit)) + " Group Type Title Imported")



		importElements()
		importEvents()
		importRpgElements()
		importRpgCampaigns()
		importRpgSessions()
		importPlaces()
		importEntities()
		importGroupTypes()
		importGroups()
		importKins()
		importIndividuals()
		importPlayerCharacters()
		importItems()
		importNames()
		importDescriptions()
		importImages()
		importIngameDescriptions()
		importIngameImages()
		importRelationTitles()

		self.stdout.write(self.style.SUCCESS("Elements imported"))
