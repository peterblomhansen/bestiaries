// <article class="beskrivelse" >
//
//   <aside class="beskrivelse__options rubrik" style="text-align:center;z-index:5;position:absolute;right:15px;background-color:white;">
// 	<img @click="loadedData.relevans++" alt="Pil op" data-toggle="tooltip" data-placement="left" title="" src="<? echo base_url(); ?>img/ikoner/relevans_pilop.svg" data-relevans="200" data-element_id="1763" data-beskrivelse_id="2110" class="pilop relevans-pil" data-original-title="Sæt relevansen én op" width="25">
// 	<p>{{ loadedData.relevans }}</p>
// 	<img @click="loadedData.relevans--" alt="Pil ned" data-toggle="tooltip" data-placement="left" title="" src="<? echo base_url(); ?>img/ikoner/relevans_pilned.svg" data-relevans="200" data-element_id="1763" data-beskrivelse_id="2110" class="pilned relevans-pil" data-original-title="Sæt relevansen én ned" width="25">
//
//
//   </aside>
//
//   <img style="max-width:100%;min-width:100%;margin-bottom:0.4em;" :src="'/galleri/' + loadedData.url" />
// <!-- {{ loadedData.url }} -->
//
//   <aside class="beskrivelse__footer" >
// 	<div>
// 	  <p @click="showDeltmed = !showDeltmed" style="cursor:pointer">{{ hemmeligOffentlig }} <span v-if="loadedData.hemmelig == '3'">({{ loadedData.delt_med.length }})</span></p>
// 	  <ul v-if="loadedData.hemmelig == '3' && showDeltmed == true">
// 		<li v-for="delt in loadedData.delt_med">
// 		  <a class="ink" :href="delt.id_entitet">{{ delt.navn_entitet }}</a>
// 		</li>
// 	  </ul>
// 	</div>
// 	<div style="text-align:right;">
// 	  <p>
// 		af {{ CompAuthor }} - <span v-if="loadedData.type_game == 'off_game'">{{ updatedOffgame }}</span>
// 					  <span v-else>{{ loadedData.ingame_tid_navn }}</span>
// 		<a v-if="permissionEdit" href="#" class="rediger" @click="edit = !edit"> </a>
// 	  </p>
// 	</div>
//   </aside>
//
// </article>

var imageShow = Vue.component('image-show', {

	template: "\
 	<article class=\"beskrivelse\" style=\"position:relative\" >\
		<aside class=\"beskrivelse__options rubrik\" style=\"position:absolute;top:0;right:0;padding:10px;background-color:#fff;\">\
			<img @click=\"loadedData.relevans++\" alt=\"Pil op\" :src=\"st + 'images/icons/relevans_pilop.svg'\" class=\"pilop relevans-pil\" >\
			<p>{{ loadedData.relevance }}</p>\
			<img @click=\"loadedData.relevans--\" alt=\"Pil ned\"  :src=\"st + 'images/icons/relevans_pilned.svg'\" class=\"pilned relevans-pil\" >\
		</aside>\
		\
		<img style=\"max-width:100%;min-width:100%;margin-bottom:0.4em;\" :src=\"loadedData.image.url\" />\
		<p>{{ loadedData.image.title }}</p>\
	<aside class=\"beskrivelse__footer\" >" +
	  "<div>" +
		"<p @click=\"showSharedWith = !showSharedWith\" style=\"cursor:pointer\">{{ hemmeligOffentlig }}<span v-if=\"loadedData.image.secret == true\">({{ loadedData.image.shared_with.length }})</span></p>" +
		"<ul v-if=\"loadedData.image.secret == true && showSharedWith == true\">" +
		  "<li v-for=\"delt in loadedData.image.shared_with\">" +
			"<a class=\"ink\" :href=\"delt.id_entitet\">{{ delt.present_name }}</a>" +
		  "</li>" +
		"</ul>" +
		"<a class=\"rubrik\" :href=\"'/almanak/image/show/' + loadedData.image.id\">Full screen</a>" +
	  "</div>" +
	  "<div style=\"text-align:right;\">" +
		"<p>" +
		  "by  <span v-if=\"loadedData.image.description_type == 'in_game' && loadedData.image.ingame_author\"  >{{ loadedData.image.ingame_author.present_name }}</span>" +
		  "<span v-else >{{ loadedData.image.author.first_name }} {{ loadedData.image.author.last_name }}</span>" +
		"</p>" +
		"<p>" +
		  "<span v-if=\"loadedData.image.ingame_age == null\">{{ updatedOffgame }}</span>" +
		  "<span v-else>{{ ingameTimeComp }}</span>" +
		  "<img v-if=\"loadedData.image.permissions.write == true\" style=\"height:1em;cursor:pointer;\" :src=\"st + 'images/icons/pencil.svg'\" @click=\"openEditor(loadedData.image.id)\" />" +
		"</p>" +
	  "</div>" +
	"</aside>\
	</article>\
	",
	props: {
		loadedData: Object,
	},
    data: function() {
        return {
		  showSharedWith: false,
        }
    },
    methods: {
		ingame_date_str: function(day, month, year, age = 0, min = 0, sec = 0) {

            var self = this;

            //console.log(self.$store.state.session);
            var ageIndex = self.$store.state.session.universe.ages.indexOf(age);

            var ageComp = "Not defined";
            if (ageIndex > -1) {
                ageComp = self.$store.state.session.universe.ages[ageIndex].name
            }
            dateString = "Not set"
            if (day != 0 && month != 0 && year != 0 && age != null) {
                var monthNames = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
                var mIndex = parseInt(month) - 1;
                dateString = day + " " + monthNames[mIndex] + " " + year + " " + ageComp;
            }
            return dateString;
        },
		openEditor: function(id) {
			this.$store.dispatch("openImageEditor",id);
		},

	},
	computed: {
		st: function (){
          return this.$store.state.initial.staticUrl;
        },
		hemmeligOffentlig: function() {
            if (this.loadedData.image.secret == false) {
                return "Public";
            } else if (this.loadedData.image.secret == true) {
                return "Secret";
            }
        },
		updatedOffgame: function() {
            return this.loadedData.image.updated
        },
		ingameTimeComp: function() {
            return this.ingame_date_str(this.loadedData.image.ingame_date, this.loadedData.image.ingame_month, this.loadedData.image.ingame_year, this.loadedData.image.ingame_age)
        },

	}
});
