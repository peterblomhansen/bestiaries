from .models import BestiariesUser, Campaign, PlayerCharacter, Universe, UniverseMembership
from django.contrib.auth.models import User
from django.db.models import Q


class BestiariesAuthentication(object):
    def authenticate(self, request, username=None, password=None):

        if not username:
            return None #raise ValidationError("A username is required to login");

        user = BestiariesUser.objects.filter(
                Q(username=username)
            ).distinct()
        #print(user)
        if user.exists() and user.count() == 1:
            user_obj = user.first()
        else:
            return None #raise ValidationError("The username was not found")

        if user_obj:
            if not user_obj.check_password(password):
                return None #raise ValidationError("Incorrect password")


            if not user_obj.universes:
                return None
            else:
                self.go_ingame(request,user_obj.default_universe,user_obj.id)

                session = request.session
#                session[''] =

            return user_obj

        return None


    def get_user(self, user_id):
        try:
            return BestiariesUser.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None



    def go_ingame(self, request, user_id, universe_id, campaign_id, role_id):

        session = request.session


        validated_user = False
        validated_universe = False
        validated_campaign = False
        validated_role = False

        if user_id == request.user.id:
            user_obj = BestiariesUser.objects.get(pk=user_id)
            if user_obj:
                validated_user =  user_id
                print("user validated")

                universe_mem_query = UniverseMembership.objects.filter(universe__pk=universe_id,participant_id=user_id)
                universe_mem = universe_mem_query.first()
                universe = Universe.objects.get(pk=universe_mem.universe.id)
                if universe:
                    validated_universe = universe.id
                    print("universe validated " + str(universe.id))

                    campaign = Campaign.objects.get(pk=campaign_id)
                    print(campaign.universe.id)
                    if campaign and campaign.universe.id == universe.id:
                        validated_campaign = campaign.id
                        print("campaign validated")
                        print(role_id)
                        if role_id == 0: # User trying to authenticate as gamemaster

                            for cm in user_obj.gamemaster_for.all():
                                #print(str(cm.id) + " - " + str(campaign.id))
                                if cm.id == campaign.id:
                                    validated_role = 0
                                    print("gm role validated ")


                        elif role_id != 0:
                            validated_user_role = False
                            for pc in user_obj.player_characters.all():
                                if pc.id == role_id:
                                    validated_user_role = True

                            if validated_user_role == True:
                                pc = PlayerCharacter.objects.get(pk=role_id)
                                for cm in pc.campaigns.all():
                                    if cm.id == campaign.id:
                                        validated_role = role_id


        if validated_user > 0 and validated_universe > 0 and validated_campaign > 0 and validated_role >= 0:
            print("Validated Credentials")
            print(validated_user)
            print(validated_universe)
            print(validated_campaign)
            print(validated_role)
            return True



        return False
