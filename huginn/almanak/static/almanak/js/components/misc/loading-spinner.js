var loadingSpinner = Vue.component('loading-spinner', {
    template: "\
    <div style=\"margin-top:2em;display:flex;flex-direction:column;align-items:center;height:100%;width:100%;\">\
        <div class=\"loading\">\
            <div class=\"spinner\">\
                    <div class=\"mask\">\
                    <div class=\"maskedCircle\"></div>\
                    </div>\
                </div>\
            </div>\
        </div>\
    </div>\
    ",
	props: {
		text: String
	},
    data: function() {
        return { }
    },
});
