var experiences = Vue.component('experiences', {
    template: "\
    <div>\
        <transition name=\"fade\">\
            <div class=\"placeholder\" v-if=\"!experiences.results\" >\
                <h3 class=\"placeholder--h3\" ></h3>\
                <ul class=\"placeholder--ul\">\
                    <li class=\"placeholder--li\" v-for=\"i in 10\">\
                    </li>\
                </ul>\
            </div>\
        </transition>\
        <transition name=\"fade\">\
            <div v-if=\"experiences && experiences.results\">\
                <h3 class=\"ink\">Experience ({{ experiences.count }})</h3>\
                <ul class=\"tome__list\">\
                    <li v-for=\"e in experiences.results\" >\
                        \
                            <span v-if=\"e.event_full.birth_string != false\" class=\"rubrik\">{{ e.event_full.birth_string }}</span>\
                            <span v-else class=\"rubrik\">Unknown</span>\
                            - <a class=\"ink\" :href=\"'/almanak/side/' + e.event_full.id\">{{ e.event_full.present_name.name }}</a>\
                            <span v-if=\"$store.state.element.permissions.write\" class=\"rubrik cursor--pointer\" @click=\"e.delete_question = !e.delete_question\">&times;</span>\
                        \
                        <div v-if=\"e.delete_question === true\" >\
                            <p class=\"rubrik\">\
                                Do you want to remove\
                                <a class=\"ink\" :href=\"'/almanak/side/' + e.event_full.id\">\
                                    {{ e.event_full.present_name.name }}\
                                </a>\
                                as an experience of \
                                <a class=\"ink\" :href=\"'/almanak/side/' + $store.state.element.id\">\
                                    {{ $store.state.element.masterdata.present_name.name }}\
                                </a><br/>\
                                <button @click=\"removeExperience(e.id)\">Yes</button>\
                                <button @click=\"e.event_full.delete_question = !e.event_full.delete_question\" >No</button>\
                            </p>\
                        </div>\
                    </li>\
                    <li v-if=\"user_can_edit == true\" >\
                    <button v-if=\"showGetMore\" @click=\"getMoreExperiences()\" class=\"button--small\" >More</button>\
                    <autocomplete filter=\"&element__element_type=begivenhed\" placeholder-text=\"Add experience\" button-text=\"Add as experience\" :button-action=\"{ type: 'ExperienceAdd', data: $store.state.element.masterdata, updateAction: 'getExperiences' }\" ></autocomplete></li>\
                </ul>\
            </div>\
        </transition >\
    </div>\
    ",
	props: {
		text: String
	},
    data: function() {
        return {
            page: 1,
         }
    },
    methods: {
		removeExperience: function(id) {
			this.$store.dispatch("update",{ url: "/api/new/entity-event-relation/" + id + "/", object: { deleted: true }, updateAction: "getExperiences" });
		},        
		getMoreExperiences: function() {
			this.page++;
			this.$store.dispatch("getExperiences", { page: this.page });

		},        
    },
    computed: {
		user_can_edit: function (){
			return this.$store.state.element.masterdata.user_can_edit;
		},        
        experiences: function(){
            if (this.$store.state.element.relations.experiences) {
                return this.$store.state.element.relations.experiences;
            }else {
                return [];
            }
        },
		numExperiences: function (){
            if(this.$store.state.element.relations.experiences) {
                return this.$store.state.element.relations.experiences.count;
            }else {
                return 0;
            }

		},        
		showGetMore: function (){
			if(this.page * 10 >= this.numExperiences) 
			{
				return false;
			}else {
				return true;
			}

		}	         
    }
});