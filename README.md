# Bestiaries Dockerized

* Vue.js frontend 
* Python Django backend 
* Gunicorn CGI server 
* Nginx webserver 
* Postgresql database 

docker-compose exec web python manage.py runserver 0.0.0.0:8080

