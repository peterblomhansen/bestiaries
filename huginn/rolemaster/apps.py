from django.apps import AppConfig


class RolemasterConfig(AppConfig):
    name = 'rolemaster'
