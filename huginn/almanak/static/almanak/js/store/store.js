var store = new Vuex.Store({
    modules: {
        description: descriptionStore,
        generic: genericStore,
        element: elementStore,
        news: newsStore,
        editors: editorsStore,
        toc: tocStore,
        create: createStore,
    }

});