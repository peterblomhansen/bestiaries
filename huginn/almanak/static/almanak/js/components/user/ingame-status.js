        //~ <p class=\"rubrik\" ><span v-html=\"role\" ></span> in <a class=\"ink\" :href=\"'/almanak/side/' + campaign.id \">{{ campaign.present_name}}</a> </p>\
        //~ <p class=\"rubrik\" >{{ campaign.ingame_now_string }}</p>\

var ingameStatus = Vue.component('ingame-status', {
    template: "\
        <div v-if=\"!_.isEmpty($store.state.generic.session.full)\" >\
			<p class=\"rubrik\" ><span v-html=\"role\" ></span> in <a class=\"ink\" :href=\"'/almanak/side/' + campaign.id \">{{ campaign.present_name.name	}}</a> </p>\
			<p class=\"rubrik\" >{{ campaign.ingame_now_string }}</p>\
        </div>",
    mounted: function() {
	
	},
    data: function() {
        return {

        }
    },
    methods: {},
    computed: {
		session: function () {
			return this.$store.generic.session.full;
		},
		campaign: function () {
			return this.$store.state.generic.session.full.campaign_full;			
		},
		roleId: function (){
			return this.$store.state.generic.session.base.roleId;
		},		
		role: function (){
			var role = "";
			if (this.roleId > 0) {
				role = "<a class=\"ink\" href=\"/almanak/side/" + this.$store.state.generic.session.full.player_character.id + "\">" + this.$store.state.generic.session.full.player_character.present_name.name + "</a>";
			}
			else {
				role = "Gamemaster";
			}
			return role;
		},

    }
});
