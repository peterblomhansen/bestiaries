var skill = Vue.component('skill', {
	template: "\
		<tr >\
			<td class=\"skill_name\">{{skill.name}}</td>\
			<td>{{ stats }}</td>\
			<td>{{ skillCost }} - {{ allowed }}</td>\
			<td> #ranks </td>\
			<td> gm </td>\
			<td> hobby </td>\
			<td class=\"skill__num-rank\" v-for=\"(r,i) in ranksArray\">\
				<input type=\"text\" v-model=\"r.num_ranks\" @keyup=\"r.changed = true\" @blur=\"changeNumRanks(r,i)\" />\
			</td>\
		</tr>\
	",
	props: {
		skill: Object,
		index: String,
	},
	mounted: function(){
		this.skill.ranksArray = this.developedRanks();
	},
	data: function (){
		return {
			ranksArray: [],
		}
	},
	methods: {
		// numRanks: function(id = null,area = null, level = null) {
        //
		// 	var numRanks = this.ranksAll.find(function(r){
		// 		return r.skill == id && r.area == area && r.level == level;
		// 	});
        //
		// 	if(numRanks) {
		// 		numRanks.ajax_lock = false;
		// 		numRanks.css_valid = "";
		// 		numRanks.changed = false;
		// 		return numRanks;
		// 	} else {
		// 		return {
		// 			id:	null,
		// 			level: level,
		// 			num_ranks: 0,
		// 			development_type: "dev",
		// 			skill: id,
		// 			area: area,
		// 			character: this.characterId,
		// 			ajax_lock: false,
		// 			css_valid: "",
		// 			changed: false,
		// 		};
        //
		// 	}
        //
		// //	return 0;
		// },
		findRanks: function (lvl,type = "dev") {
			var numRanks = this.ranks.find(function(r){
				return r.level == lvl && r.development_type == type;
			});

			if(numRanks) {
				return numRanks;
			} else {
				return {
					id:	null,
					level: lvl,
					num_ranks: 0,
					development_type: type,
					skill: this.skill.id,
					area: this.skill.area,
					character: this.characterId,
					ajax_lock: false,
					css_valid: "",
					changed: false,
				};

			}
		},
		changeNumRanks: function (rnks,index){
			var self = this;
			console.log(rnks.num_ranks);
			if(rnks.changed == true) {
				console.log("Changed");
				if (parseInt(rnks.num_ranks) <= parseInt(this.allowed)) {
					console.log("Valid");
					if(rnks.id == null) {
						console.log("Create");
						axios.post("/rm/api/developedskill/create/",rnks).then(function(response){
							self.ranksArray[index] = response.data;
						})

					}
					else if (rnks.id > 0) {
						console.log("Update");
						axios.patch("/rm/api/developedskill/" + rnks.id + "/update/",rnks).then(function(response){
							self.ranksArray[index] = response.data;
						});
					}


				}
				else {
					console.log("InValid");
					self.ranksArray[index].num_ranks = 0;
					//self.ranksArray[index].
					//var i = this.developedRanks.findIndex(function (r) {return rnks.level == r.level  });
					//this.developedRanks[i].num_ranks = 0;

				}
			}
		},
		updateNumRanks: function(ranksObject) {
			var self = this;

			var validated = this.validateDevelopment(ranksObject.num_ranks);

			if(validated == false) {
				ranksObject.changed = false;
				ranksObject.num_ranks = 0;
				self.ranksSkill[ranksObject.level].num_ranks = 0;
			}
			if(ranksObject.changed == true) {
				ranksObject.ajax_lock = true;
	 			if(ranksObject.id == null) {
					console.log("No entry yet - create " + ranksObject.id);
					//if(ranksObject.num_ranks > 0) {
						axios.post("/rm/api/developedskill/create/",ranksObject).then(function(response){
							self.ranksAll.push(response.data);
							self.ranksSkill[ranksObject.level] = response.data;
							ranksObject.ajax_lock = false;
							ranksObject.changed = false;
							// console.log("Created",ranksObject);
						});
					//}
				}
				else if (ranksObject.id > 0) {
					var self = this;
					console.log("Update entry " + ranksObject.id + ": num: " + ranksObject.num_ranks);

					axios.patch("/rm/api/developedskill/" + ranksObject.id + "/update/",ranksObject).then(function(response){
						// var i = self.ranksAll.findIndex(function(r){ return r.id == response.data.id })
						// self.ranksAll[i] = response.data;

						self.ranksSkill[ranksObject.level] = response.data;
						ranksObject.ajax_lock = false;
						ranksObject.changed = false;
					});
				}

			}

		},
		developedRanks: function (){
			var self= this;
			var dev = [];

			_.each(this.levels,function(l,i){
				dev.push(self.findRanks(l));
			});

			//self.
			return dev;

		},
		gmRanks: function (){

			return {};
		},
		hobbyRanks: function (){
			//dev = [];
			return {};
		},

	},
	computed: {
		allowed: function (){
			var self = this;
			var allowed = 0;
			var skillCostObj = this.professionSkillCosts.find(function(c){
				return c.skill == self.skill.id;
			});
			if(!skillCostObj) {
				return false;
			}
			var c1 = parseInt(skillCostObj.cost_1);
			var c2 = parseInt(skillCostObj.cost_2);
			var c3 = parseInt(skillCostObj.cost_3);

			if(c1 > 0 && c2 == 0 && c3 == 0) {
				//console.log("Allowed 1");
				allowed = 1;
			}
			else if(c1 > 0 && c2 > 0 && c3 == 0) {
				//console.log("Allowed 2");
				allowed = 2;
			}
			else if(c1 > 0 && c2 > 0 && c3 > 0) {
				//console.log("Allowed 3");
				allowed = 3;
			}
			else if(c1 > 0 && c2 > 0 && c3 == -1) {
				//console.log("Allowed infinit");
				allowed = 100;
			}
			else if(c1 > 0 && c2 == -1 ) {
				//console.log("Allowed infinit");
				allowed = 100;
			}

			return allowed;
		},
		characterId: function (){
			return this.$store.state.character.id;
		},
		levels: function (){
			var levels = [];
			n = 1;
			while (n <= this.$store.state.initial.level.viewed) {
				levels.push(n);
				n++;
			}
			return levels;
		},
		ranksAll: function (){
			return this.$store.state.character.dev_ranks;
		},
		ranks: function (){
			var self = this;
			var matchSkillArea = function(s){
				return s.skill == self.skill.id && s.area == self.skill.area;
			}

			return this.$store.state.character.dev_ranks.filter(matchSkillArea);
		},
		allowedRanks: function (){
			return
		},
		stats: function(){
			var statsString = "";
			statsString = this.skill.stat_1 + "/" + this.skill.stat_2 + "/" +this.skill.stat_3;
			if(this.skill.stat_1 == "0" || this.skill.stat_1 == null) {
				statsString = "None";
			}
			else if (this.skill.stat_2 == null){
				statsString = this.skill.stat_1;
			}
			else if (this.skill.stat_3 == null) {
				statsString = this.skill.stat_1 + "/" + this.skill.stat_2;
			}
			else {
				statsString = this.skill.stat_1 + "/" + this.skill.stat_2 + "/" + this.skill.stat_3;

			}

			return statsString;

		},
		skillCost: function (){
			var self = this;
			var skillCost = "-"
			var skillCostObj = this.professionSkillCosts.find(function(c){

				return c.skill == self.skill.id;
			})

			if(skillCostObj) {
				skillCost = skillCostObj;

				//var statsString = "";
				//statsString = this.skill.stat_1 + "/" + this.skill.stat_2 + "/" +this.skill.stat_3;
				if(skillCostObj.cost_1 == 0) {
					skillCost = "None";
				}
				else if (skillCostObj.cost_2 == 0){
					skillCost = skillCostObj.cost_1;
				}
				else if (skillCostObj.cost_2 == -1) {
					skillCost = skillCostObj.cost_1 + "/*";
				}
				else if (skillCostObj.cost_3 == 0) {
					skillCost = skillCostObj.cost_1 + "/" + skillCostObj.cost_2;
				}
				else {
					skillCost = skillCostObj.cost_1 + "/" + skillCostObj.cost_2 + "/" + skillCostObj.cost_3;

				}



			}




			return skillCost;

		},
		professionSkillCosts: function (){
			if(this.$store.state.character.profession != null) {
				return this.$store.state.character.profession.skill_costs;
			}
			else {
				return [];
			}
		},


	},
});
