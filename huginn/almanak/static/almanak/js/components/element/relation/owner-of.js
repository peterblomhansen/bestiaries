var ownerOf = Vue.component('owner-of', {
	template: "\
	<div >\
		<h3 class=\"rubrik\">Property</h3>\
		<transition name=\"fade\">\
			<div v-if=\"ownerOfUniques && ownerOfUniques.results\" >\
				<h4  class=\"rubrik\">Unique Items ({{ownerOfUniques.count}})</h4>\
				<ul >\
					<li v-for=\"u in ownerOfUniques.results\" class=\"rubrik\">\
						<a class=\"ink\" :href=\"'/almanak/side/' + u.owned_full.id\" >\
							{{ u.owned_full.present_name.name }}\
						</a>\
						{{ property_type(u.owned_full) }}\
						&nbsp;<strong class=\"cursor--pointer\" @click=\"u.delete_question = !u.delete_question\" >&times;</strong>\
						<div v-if=\"u.delete_question == true\" >Do you want to remove <span class=\"ink\">{{ u.owned_full.present_name.name }}</span> as property of <span class=\"ink\">{{ $store.state.element.masterdata.present_name.name }}</span>?<br/><button @click=\"deleteElemenItemRelation(u)\" >Yes</button> <button @click=\"u.delete_question = !u.delete_question\"  >No</button></div>\
						\
					</li>\
					<li>\
						<button v-if=\"showGetMoreUnique\" @click=\"getMoreUniqueItems()\" class=\"button--small\">more</button>\
						<autocomplete\
							v-if=\"$store.state.element.permissions.write == true\"\
							placeholder-text=\"Add unique property\"\
							button-text=\"Add as property\"\
							:button-action=\"{ type: 'propertyUniqueAdd', data: $store.state.element.masterdata, updateAction: 'elementGetUniqueOwned' }\"\
						>\
						</autocomplete>\
					</li>\
				</ul>\
			</div>\
		</transition>\
		<transition name=\"fade\">\
			<div class=\"placeholder\" v-if=\"!ownerOfUniques.results\" >\
				<h3 class=\"placeholder--h3\" ></h3>\
				<ul class=\"placeholder--ul\">\
					<li class=\"placeholder--li\" v-for=\"i in 10\">\
					</li>\
				</ul>\
			</div>\
		</transition>\
		<h4 class=\"rubrik\">Other Items ({{ownerOfItems.count}})</h4>\
		<ul>\
			<li v-for=\"u in ownerOfItemsFiltedResults\" class=\"rubrik\">\
				<span v-if=\"u.update_question == false\" class=\"rubrik\">{{ u.number_of_items }} pcs. </span> \
				<input min=\"1\" v-if=\"u.update_question == true\" type=\"number\" style=\"width:50px;margin-right:0.5em\" v-model=\"u.number_of_items\">\
				<a class=\"ink\" :href=\"'/almanak/side/' + u.owned_full.id\" >\
					{{ u.owned_full.present_name.name }}\
				</a>\
				<button v-if=\"u.update_question == false\" @click=\"u.update_question = !u.update_question\" class=\"button--small\" >edit</button>\
				<span v-if=\"u.update_question == true\">\
					<button @click=\"updateElemenItemRelation(u)\" class=\"button--small\" >update</button> <button @click=\"u.update_question = !u.update_question\" class=\"button--small\" >cancel</button>\
				</span>\
				&nbsp;<strong class=\"cursor--pointer\" @click=\"u.delete_question = !u.delete_question\" >&times;</strong>\
				<div v-if=\"u.delete_question == true\" >Do you want to remove these {{ u.number_of_items }} <span class=\"ink\">{{ u.owned_full.present_name.name }}</span> as property of <span class=\"ink\">{{ $store.state.element.masterdata.present_name.name }}</span>?<br/><button @click=\"u.number_of_items = 0;updateElemenItemRelation(u)\" >Yes</button> <button @click=\"u.delete_question = !u.delete_question\"  >No</button></div>\
			</li>\
			<li>\
				<button v-if=\"showGetMore\" @click=\"getMoreItems()\" class=\"button--small\">more</button>\
				<input type=\"number\" style=\"width:50px;margin-right:0.5em\" v-model=\"newRelationNumberOfItems\">\
				<autocomplete\
					:disabled=\"newRelationNumberOfItems < 1\"\
					filter=\"&element__type_spec=artefakt\"\
					v-if=\"$store.state.element.permissions.write == true\"\
					:placeholder-text=\"addPropertyPlacerholder\"\
					button-text=\"Add as property\"\
					:button-action=\"{ type: 'propertyAdd', data: { element:$store.state.element.masterdata, RelationNumberOfItems: newRelationNumberOfItems }, updateAction: 'elementGetOwned' }\"\
				></autocomplete>\
			</li>\
		</ul>\
	</div>\
	",
	data: function() {
		return {
			showAddNewProperty: false,
			newRelationNumberOfItems: 0,
			itemsPage: 1,
			itemsUniquePage: 1,
		}
	},
	methods: {
		deleteElemenItemRelation: function (relation) {
			this.$store.dispatch("update",{ object: { deleted: true }, url: "/api/new/element-unique-item-relation/" + relation.id + "/", updateAction: "elementGetUniqueOwned" });
		},
		updateElemenItemRelation: function (relation) {
			var updatedRelation = new ElementItemRelationModel(relation);
			updatedRelation.ingame_age		= this.ingameNow.age;
			updatedRelation.ingame_year 	= this.ingameNow.year;
			updatedRelation.ingame_month 	= this.ingameNow.month;
			updatedRelation.ingame_date 	= this.ingameNow.date;
			updatedRelation.ingame_hour 	= this.ingameNow.hour;
			updatedRelation.ingame_minute 	= this.ingameNow.minute;
			updatedRelation.ingame_second 	= this.ingameNow.second;


			this.$store.dispatch("create",{ object: updatedRelation, url: "/api/new/element-item-relation/", updateAction: "elementGetOwned" });

		},
		createItemElementRelations: function(rel) {
			var self = this;



			// var updatedRelation = {
			// 	owner: this.element.id,
			// 	owned: rel.owned.id,
			// 	number_of_items:rel.number_of_items,
			// 	ingame_age: this.ingameNow.age,
			// 	ingame_year:this.ingameNow.year,
			// 	ingame_month:this.ingameNow.month,
			// 	ingame_date:this.ingameNow.date,
			// 	ingame_hours:this.ingameNow.hours,
			// 	ingame_minutes:this.ingameNow.minutes,
			// 	ingame_seconds: this.ingameNow.seconds,

			// };

			// axios.post("/api/elementitemrelation/create/",updatedRelation).then(function(){
			// 	self.$store.dispatch("getElementRelations");
			// }).catch(function(error) {
	        //     console.log(error);
	        // });


		},
		property_type: function(el) {
			return el.type_spec.replace("_"," ");
		},
		removeUniqueProperty: function(u) {
			var self = this;
			axios.patch("/api/elementuniqueitemrelation/" + u.id + "/update/", {deleted: true}).then(function(response){
				self.$store.dispatch("getElementRelations");
			}).catch(function(error) {
	            console.log(error);
	        });
		},
		removeProperty: function(u) {
			var self = this;
			axios.patch("/api/elementitemrelation/" + u.id + "/update/", {deleted: true}).then(function(response){
				self.$store.dispatch("getElementRelations");
			}).catch(function(error) {
	            console.log(error);
	        });
		},
		getMoreItems: function () {
			this.itemsPage++;
			this.$store.dispatch("elementGetOwned", { page: this.itemsPage });
		}, 
		getMoreUniqueItems: function () {
			this.itemsUniquePage++;
			this.$store.dispatch("elementGetUniqueOwned", { page: this.itemsUniquePage });			
		}, 		
	},
	computed: {
		ingameNow: function(){
			var cmpgn = this.$store.state.generic.session.full.campaign_full;

			var ingameNow = {
				age: cmpgn.ingame_now_age.id,
				year: cmpgn.ingame_now_year,
				month: cmpgn.ingame_now_month,
				date: cmpgn.ingame_now_date,
				hour: cmpgn.ingame_now_hour,
				minute: cmpgn.ingame_now_minute,
				second: cmpgn.ingame_now_second
			};

			return ingameNow;

		},
		static: function (){
            return this.$store.state.initial.staticUrl;
        },
		ownerOfUniques: function(){
			if(this.$store.state.element.relations.ownedUnique) {
				return this.$store.state.element.relations.ownedUnique
			}else {
				return {}
			}
		},
		ownerOfItems: function(){
			if(this.$store.state.element.relations.owned) {
				return this.$store.state.element.relations.owned
			} else {
				return {}
			}
		},
		ownerOfItemsFiltedResults: function (){
			if (this.ownerOfItems.results) {
				return  _.filter(this.ownerOfItems.results,function(i){ return i.number_of_items > 0 }); //this.ownerOfItems.results.filter(function(i){ return i.number_of_items > 0 && i.update_question == false })	
			} else {
				return [];
			}
		},
		element: function(){
			return this.$store.state.element;
		},
		numItems: function (){
            if(this.$store.state.element.relations.owned) {
                return this.$store.state.element.relations.owned.count;
            }else {
                return 0;
            }
		}, 
		numUniqueItems: function (){
            if(this.$store.state.element.relations.ownedUnique) {
                return this.$store.state.element.relations.ownedUnique.count;
            }else {
                return 0;
            }
		},		       
		showGetMore: function (){
			if(this.itemsPage * 10 >= this.numItems) 
			{
				return false;
			}else {
				return true;
			}
		},	
		showGetMoreUnique: function (){
			if(this.itemsUniquePage * 10 >= this.numUniqueItems) 
			{
				return false;
			}else {
				return true;
			}
		},
		addPropertyPlacerholder: function (){
			return this.newRelationNumberOfItems < 1 ? "Choose amount": "Add Property";

		},	

	}
});
