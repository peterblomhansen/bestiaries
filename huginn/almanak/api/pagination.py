from rest_framework.pagination import PageNumberPagination, CursorPagination

class TenPerPagePagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
#    page_size_query_param = 'page_size'
#    max_page_size = 1000

class TwentyPerPagePagination(PageNumberPagination):
    page_size = 20
    page_size_query_param = 'page_size'

class AlmanakCursorPagination(CursorPagination):
	page_size = 10
