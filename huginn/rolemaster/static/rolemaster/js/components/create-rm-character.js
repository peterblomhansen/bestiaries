var createRmCharacter = Vue.component('create-rm-character', {
	template: "<div>\
		<button @click=\"initiateRmCharacter()\">Create Rolemaster Character</button>\
	</div>",
	data: function (){
		return {
			professionChosen: 0,
			newProfessionName: "",
			professionNameValidateMessage: "",
		}
	},
	methods: {
		initiateRmCharacter: function(){

			axios.post("/rm/api/rm_character/create/", { id: this.individual.id, race: this.individual.kin, profession: this.professionChosen }).then(function(){
				 window.location.reload();
			 }).catch(function(error) {
                 console.log(error);
             });

		},
		createProfession: function(){
			var self = this;
			axios.post("/rm/api/profession/create/", { name: this.newProfessionName }).then(function(response){
				self.$store.state.professions.push(response.data);
				self.professionChosen = response.data.id;
				self.newProfessionName = "";
			 }).catch(function(error) {
                 console.log(error);
             });
		},

	},
	computed: {
		individual: function (){
			return this.$store.state.individual;
		},
		professions: function(){
			return this.$store.state.professions;
		},
		professionNames: function () {
			return this.professions.map(function(p){
				return p.name;
			});
		},
		// validateProfessionName: function() {
		// 	var valid = false
        //
		// 	if(this.newProfessionName.length > 0 && this.professionNames.indexOf(this.newProfessionName) == -1) {
		// 		valid = true;
		// 		this.professionNameValidateMessage = "";
		// 	}
		// 	if(this.newProfessionName.length == 0) {
		// 		this.professionNameValidateMessage = "The Profession must have a name";
		// 	}
		// 	if(this.professionNames.indexOf(this.newProfessionName) > -1) {
		// 		this.professionNameValidateMessage = "There is already a Profession with this name";
		// 	}
        //
        //
		// 	return valid;
		// },

	}
});
