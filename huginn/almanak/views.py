from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import TemplateView, DetailView, View
from .models import Element
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from almanak.models import Universe, BestiariesUser, Description, Image
from django.core import serializers
from django.http import HttpRequest
from django.conf import settings
from django.views.generic.base import RedirectView
from django.conf.urls.static import static
import os
import markdown


class DescriptionDetailView(LoginRequiredMixin, DetailView):
	queryset = Description.objects.filter(deleted=False)
	#template_name = "almanak/description_show.html"

	def get_template_names(self, **kwargs):

		d = self.get_object()
		universe_current = self.request.session.get("universe_current")
		if universe_current == d.universe.id:
			return "almanak/description/description_full.html"
		else:
		    return  "error/wrong_universe.html"


class ImageDetailView(LoginRequiredMixin, DetailView):
	queryset = Description.objects.filter(deleted=False)
	#template_name = "almanak/description_show.html"

	def get_template_names(self, **kwargs):

		d = self.get_object()
		universe_current = self.request.session.get("universe_current")
		if universe_current == d.universe.id:
			return "almanak/description/image_full.html"
		else:
		    return  "error/wrong_universe.html"


class SideRedirectView(RedirectView):

	  url = 'https://google.com/?q=%(term)s'


class ElementDetailView(LoginRequiredMixin, DetailView):
	context_object_name = 'element'
	queryset = Element.objects.all()
	login_url = '/login/'


	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context["debug"] = settings.DEBUG
		universe_current = self.request.session.get("universe_current")
		if universe_current != context['element'].universe.id:
			context["user"] = self.request.user
		return context


	def get_template_names(self, **kwargs):
		e = self.get_object()
		universe_current = self.request.session.get("universe_current")

		if not universe_current:
			return  "user/go_ingame.html"

		elif universe_current == e.universe.id:
			if e:
				tmpl = e.type_grund
				if e.deleted == False:
					return "almanak/element/" + tmpl + ".html"
				else:
					return "error/element_deleted.html"
			else:
				return "almanak/element/element.html"
		else:
			return  "error/wrong_universe.html"


class OtherUserDetailView(LoginRequiredMixin, DetailView):
    context_object_name = 'otheruser'
    queryset = BestiariesUser.objects.all()
    login_url = '/login/'
    template_name = "user/otheruser.html"




class TableOfContentView(LoginRequiredMixin, View):
	login_url = '/login/'
	redirect_field_name = '/accounts/profile/'
	context_object_name = 'user'
	template_name = "user/table-of-content.html"

	def get(self, *args, **kwargs):
		user = self.request.user
		session = self.request.session

		universe_current = session.get("universe_current")

		if not universe_current:
			return render(self.request, 'user/go_ingame.html', {'user': self.request.user, 'session': self.request.session })

		return render(self.request, 'user/table-of-content.html', {'user': self.request.user, 'session': self.request.session })



class StyleGuideView(TemplateView):

	template_name = "guide/styleguide.html"




class TemplateTest(TemplateView):

    template_name = "almanak/base.html"


class MarkdownView(TemplateView):

	template_name = "test/markdown.html"

	def serve_markdown_content(self, filename):
		settings_dir = os.path.dirname(__file__)
		PROJECT_ROOT = os.path.abspath(os.path.dirname(settings_dir))
		CONTENT_FOLDER = os.path.join(PROJECT_ROOT, 'almanak/static/almanak/content/')
		file = CONTENT_FOLDER + filename + '.md'
		f = open(file)
		cnt = f.read()
		html = markdown.markdown(cnt, extensions=['md_in_html'])
		return html

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		file = kwargs['file']
		context['md'] = self.serve_markdown_content(file)
		return context	


class FrontPage(MarkdownView):


	def dispatch(self, request, *args, **kwargs):

		user_current = request.session.get('user_current')
		universe_current = request.session.get('universe_current')
		campaign_current = request.session.get('campaign_current')
		role_current = request.session.get('role_current')

		if user_current != None and universe_current != None and campaign_current != None and role_current != None:
			return redirect('/almanak/table-of-content/')

		return super().dispatch(request, *args, **kwargs)
