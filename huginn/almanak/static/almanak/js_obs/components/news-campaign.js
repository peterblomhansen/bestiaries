var newsCampaign = Vue.component('news-campaign', {
    template: '\
    <div>\
		<h1 class="rubrik">{{session.present_name}} news</h1>\
		<br>\
		<news-block :news-object="news.campaign.newElements" heading="New pages"></news-block>\
		<news-block :news-object="news.campaign.newDescriptions" heading="New descriptions"></news-block>\
		<news-block :news-object="news.campaign.updatedElements" heading="Updated pages"></news-block>\
		<news-block :news-object="news.campaign.updatedDescriptions" heading="Updated descriptions"></news-block>\
	</div>\
    ',
	computed: {
		news: function(){
			return this.$store.state.profile.news;
		},
		session: function(){
			return this.$store.state.session;
		},
	}
});
