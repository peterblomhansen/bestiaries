var ownedBy = Vue.component('owned-by', {
	template: "\
	<div v-if=\"ownedBy.length > 0\">\
		<h3 class=\"rubrik\">Owned by</h3>\
		<p class=\"rubrik\" v-for=\"u in ownedBy\"><a class=\"ink\" :href=\"'/almanak/side/' + u.owner.id\" >{{ u.owner.present_name }}</a> {{ owner_type(u.owner) }}</p>\
	</div>\
	",
	props: {
		player: Object
	},
	data: function() {
		return {

		}
	},
	methods: {
		owner_type: function(el) {
			return el.type_spec.replace("_"," ");
		}

	},
	computed: {
		ownedBy: function(){
			var id = this.$store.state.element.masterdata.id;
			return this.$store.state.element.relations.uniques.filter(function(e){ return e.owned == id });
		}

	}
});
